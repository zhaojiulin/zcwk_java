package com.zcwk.integralmall.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zcwk.core.util.Enums;
import com.zcwk.core.util.StringUtil;
import com.zcwk.core.util.UtilException;
import com.zcwk.integralmall.dao.GoodsDao;
import com.zcwk.integralmall.model.Goods;
import com.zcwk.integralmall.model.GoodsType;
import com.zcwk.integralmall.view.GoodsView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Administrator on 2017/5/6.
 */
@Service
@Repository("GoodsService")
public class GoodsService {
    @Autowired
    GoodsDao goodsDao;
    @Autowired
    GoodsTypeService goodsTypeService;

    /**
     * 新增或修改
     *
     * @param
     * @throws UtilException
     */
    @Transactional(rollbackFor = {Exception.class})
    public void saveOrUpdate(Goods goods) throws UtilException {
        if (StringUtil.isBlank(goods.getName())) {
            throw new UtilException("名称填写有误");
        }
        if (StringUtil.isBlank(goods.getIntegral()) || goods.getIntegral() <= 0) {
            throw new UtilException("兑换积分填写有误");
        }
        if (StringUtil.isBlank(goods.getStock()) || goods.getStock() <= 0) {
            throw new UtilException("库存填写有误");
        }
        if (StringUtil.isBlank(goods.getDesc()) ) {
            throw new UtilException("基本信息填写有误！");
        }
        if (StringUtil.isBlank(goods.getPrice()) || goods.getPrice().compareTo(BigDecimal.ZERO)< 0) {
            throw new UtilException("市场价格填写有误");
        }
        GoodsType goodsType=goodsTypeService.getGoodsType(goods.getType());
        if(goodsType==null){
            throw new UtilException("商品类型错误.");
        }
        Goods db = this.goodsDao.selectByPrimaryKey(goods.getId());
        goods.setUpdateTime(new Date());
        if (db != null) {
            //修改
            int update = this.goodsDao.updateByPrimaryKeySelective(goods);
            if (update < 1) {
                throw new UtilException("更新失败！");
            }
        }else{
            //新增
            goods.setCreateTime(new Date());
            goods.setState(goods.getState());
            int insert = this.goodsDao.insertSelective(goods);
            if (insert < 1) {
                throw new UtilException("新增失败！");
            }
        }
    }

    /**
     * 根据id获取商品信息
     * @param Id
     * @return
     */
    public Goods getGooodsById(long Id){
        return goodsDao.selectByPrimaryKey(Id);
    }
/**
     * 根据id删除
     * @param Id
     * @return
     */
    public int deleteById(long Id){
        return goodsDao.deleteByPrimaryKey(Id);
    }

    /**
     * 根据条件获取标列表
     *
     * @return
     * @throws Exception
     */
    public PageInfo getListByParam(GoodsView view) {
        PageHelper.startPage(view.getPageNo(), view.getPageSize());
        List list = this.goodsDao.selectByParam(view);
        PageInfo page = new PageInfo(list);
        return page;
    }

    /**
     * 根据Id上下架
     *
     * @param Id
     * @param state
     * @throws UtilException
     */
    public void lanchOrBan(long Id, int state) throws UtilException {
        Goods goods = new Goods();
        goods.setId(Id);
        goods.setState(state);
        goods.setUpdateTime(new Date());
        if (state == Enums.State.Cancel.getIndex()) {
            goods.setOffTime(new Date());
        }
        int result = this.goodsDao.updateByPrimaryKeySelective(goods);
        if (result < 1) {
            throw new UtilException("操作失败，请重试！");
        }
    }

    /**
     * 改变库存
     * @param Id
     * @param num
     * @return
     * @throws UtilException
     */
    public Goods deductStock(long Id,int num) throws UtilException{
        Goods goods=goodsDao.selectByPrimaryKey(Id);
        if(goods==null){
            throw new UtilException("商品信息错误.");
        }
        if(goods.getState()==Enums.GoodsState.OffShelves.getIndex()){
            throw new UtilException("该商品已下架.");
        }
        if(goods.getOffTime().compareTo(new Date())<= 0){
            throw new UtilException("该商品已下架.");
        }
        if(goods.getStock()<num){
            throw new UtilException("该商品库存不足.");
        }
        Goods _goods=new Goods();
        _goods.setId(goods.getId());
        _goods.setStock(-num);
        _goods.setHadStock(num);
        int count= goodsDao.updateStock(_goods);
        if(count>0){
            return goods;
        }else{
            throw new UtilException("该商品库存不足.");
        }
    }


    /**
     * 推荐
     *
     * @return
     * @throws Exception
     */
    public void recommend(Goods goods) {
        this.goodsDao.updateByPrimaryKeySelective(goods);
    }

    /**
     * 自动下架
     * @throws UtilException
     */

    public void checkOff()throws UtilException{
     List<Goods> list=this.goodsDao.check();
        for (Goods goods:list){
            Goods dbgoods=new Goods();
            if(goods.getOffTime()!=null&&goods.getOffTime().compareTo(new Date())<= 0||goods.getStock() <= 0){
                dbgoods.setId(goods.getId());
                dbgoods.setState(Enums.GoodsState.OffShelves.getIndex());
                dbgoods.setUpdateTime(new Date());
                int result=this.goodsDao.updateByPrimaryKeySelective(dbgoods);
                if (result < 1)
                    throw new UtilException("商品自动下架失败！");
            }
        }
    }

}
