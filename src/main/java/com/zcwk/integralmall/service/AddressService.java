package com.zcwk.integralmall.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zcwk.core.service.UserService;
import com.zcwk.core.util.StringUtil;
import com.zcwk.core.util.UtilException;
import com.zcwk.integralmall.model.Address;
import com.zcwk.core.model.User;
import com.zcwk.integralmall.dao.AddressDao;
import com.zcwk.integralmall.view.AddressView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by Administrator on 2017/5/6.
 */
@Service
@Repository("AddressService")
public class AddressService {
    @Autowired
    AddressDao addressDao;
    @Autowired
    UserService userService;

    /**
     * 获取用户地址信息
     * @param Id
     * @param userId
     * @return
     */
    public Address getAddress(long Id,long userId){
        return addressDao.selectByUserId(Id,userId);
    }


    /**
     * 获取用户地址信息
     * @param Id
     * @param
     * @return
     */
    public void deleteById(long Id){
        this.addressDao.deleteByPrimaryKey(Id);
    }


    /**
     * 保存地址信息
     * @param address
     * @throws UtilException
     */
    public void save(Address address) throws UtilException {
        User user=userService.getUserById(address.getUserId());
        if(user==null){
            throw new UtilException("该用户不存.");
        }
        if (StringUtil.isBlank(address.getAddressee())) {
            throw new UtilException("收件人填写有误");
        }
        if (address.getTelphone() == null || !(Pattern.compile("^1[34578]{1}[0-9]{9}").matcher(address.getTelphone()).matches()))
            throw new UtilException("手机号码格式不正确");

        if (StringUtil.isBlank(address.getProv()) || StringUtil.isBlank(address.getCity()) ||StringUtil.isBlank(address.getArea())|| StringUtil.isBlank(address.getAddress())) {
            throw new UtilException("地址信息填写有误");
        }
        address.setCreateTime(new Date());
        Address model=addressDao.selectByPrimaryKey(address.getId());
        if(model==null){
            addressDao.insertSelective(address);
        }else{
            addressDao.updateByPrimaryKeySelective(address);
        }
    }


    /**
     * 根据条件获取列表
     *
     * @return
     * @throws Exception
     */
    public PageInfo getListByParam(AddressView view) {
        PageHelper.startPage(view.getPageNo(), view.getPageSize());
        List list = this.addressDao.selectByParam(view);
        PageInfo page = new PageInfo(list);
        return page;
    }

}
