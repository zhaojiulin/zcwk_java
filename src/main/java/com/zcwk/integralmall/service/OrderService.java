package com.zcwk.integralmall.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.parser.impl.H2Parser;
import com.zcwk.core.model.User;
import com.zcwk.core.service.BonusService;
import com.zcwk.core.service.CouponService;
import com.zcwk.core.service.IntegralService;
import com.zcwk.core.service.UserService;
import com.zcwk.core.util.Enums;
import com.zcwk.core.util.StringUtil;
import com.zcwk.core.util.UtilException;
import com.zcwk.integralmall.dao.OrderDao;
import com.zcwk.integralmall.model.Address;
import com.zcwk.integralmall.model.Order;
import com.zcwk.integralmall.model.Goods;
import com.zcwk.integralmall.view.OrderView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Administrator on 2017/5/6.
 */
@Service
@Repository("OrderService")
public class OrderService {
    @Autowired
    GoodsService goodsService;
    @Autowired
    IntegralService integralService;
    @Autowired
    OrderDao orderDao;
    @Autowired
    AddressService addressService;
    @Autowired
    UserService userService;
    @Autowired
    BonusService bonusService;
    @Autowired
    CouponService couponService;

    /**
     * 下单
     *
     * @param
     * @throws UtilException
     */
    @Transactional(rollbackFor = {Exception.class})
    public void createOrder(long userId, int num, long goodsId, int addressId, String mobile) throws Exception {
        Address address = null;
        User user = userService.getUserById(userId);
        if (user == null) {
            throw new UtilException("该用户不存.");
        }
        if (num <= 0) {
            throw new UtilException("兑换数量有误.");
        }
        //订单编号
        String orderNum = (new SimpleDateFormat("yyyyMMddHHmmss").format(new Date())).substring(6, 14) + ((user.getId()).toString()).substring(3,7) + (int) (Math.random() * (1000 - 100) + 100)+(int) (Math.random() * (10 - 1) + 1);
        //更改库存
        Goods goods = goodsService.deductStock(goodsId, num);
        Goods dbgoods = goodsService.getGooodsById(goodsId);
        int integral = 0;
        integral = num * goods.getIntegral();
        //限兑
        if (goods.getLimitType() != Enums.LimitType.None.getIndex()) {
            exchangeLimit(userId, goodsId, num);
        }
        if (integral > user.getScore()) {
            throw new UtilException("积分不足.");
        }
        if (goods.getVirtualType() == Enums.GoodsType.TelephoneFare.getIndex()) {
            if (StringUtil.isBlank(mobile)) {
                throw new UtilException("请填写要充值的手机号..");
            }
        }
        //扣除积分
        integralService.sendIntegral(Enums.Actions.Exchange.getIndex(), userId, -integral, "商品(" + goods.getName() + ")兑换");
        //实物商品验证地址信息
        if (goods.getVirtualType() == Enums.GoodsType.Entity.getIndex()) {
            address = addressService.getAddress(addressId, userId);
            if (address == null) {
                throw new UtilException("地址信息有误.");
            }
        }
        //创建订单
        Order order = new Order();
        if (goods.getVirtualType() == Enums.GoodsType.Entity.getIndex() || goods.getVirtualType() == Enums.GoodsType.TelephoneFare.getIndex()) {
            order.setState(Enums.OrderState.NewOrder.getIndex());
        } else {
            order.setState(Enums.OrderState.Complete.getIndex());
        }
        order.setCreateTime(new Date());
        order.setUserId(userId);
        order.setGoodsId(goodsId);
        order.setGoodsType(dbgoods.getType());
        order.setNum(num);
        order.setIntegral(goods.getIntegral());
        order.setTotalIntegral(integral);
        if (goods.getVirtualType() == Enums.GoodsType.Entity.getIndex()) {
            order.setAddress(address.getAddress());
            order.setCity(address.getCity());
            order.setProv(address.getProv());
            order.setArea(address.getArea());
            order.setAddressee(address.getAddressee());
        }

        if (goods.getVirtualType() != Enums.GoodsType.Entity.getIndex()) {
            order.setTelphone(mobile);
        } else {
            order.setTelphone(address.getTelphone());
        }
        order.setOrderNum(orderNum);
        orderDao.insertSelective(order);
        if (goods.getVirtualType() != Enums.GoodsType.Entity.getIndex() || goods.getVirtualType() != Enums.GoodsType.TelephoneFare.getIndex()) {
            sendVirtual(userId, goodsId, num);
        }

    }

    /**
     * 取消订单
     *
     * @param orderId
     */
    public void cancelOrder(long orderId) throws UtilException {
        int count = orderDao.updateState(orderId, Enums.OrderState.NewOrder.getIndex(), Enums.OrderState.Cancel.getIndex());
        if (count == 0) {
            throw new UtilException("订单取消失败.");
        }
    }

    /**
     * @param order
     */
    public void updateOrder(Order order) throws UtilException {
        order.setUpdateTime(new Date());
        int count = orderDao.updateByPrimaryKeySelective(order);
        if (count == 0) {
            throw new UtilException("订单取消失败.");
        }
    }

    /**
     * 订单完成
     *
     * @param orderId
     */
    public void completeOrder(long orderId) throws UtilException {
        int count = orderDao.updateState(orderId, Enums.OrderState.NewOrder.getIndex(), Enums.OrderState.Complete.getIndex());
        if (count == 0) {
            throw new UtilException("订单更新失败.");
        }
    }

    /**
     * 根据id获取
     *
     * @param Id
     * @return
     */
    public Order getOrderById(long Id) {
        return orderDao.selectByPrimaryKey(Id);
    }


    /**
     * 根据条件获取订单
     *
     * @return
     * @throws Exception
     */
    public PageInfo getListByParam(OrderView view) {
        PageHelper.startPage(view.getPageNo(), view.getPageSize());
        List list = this.orderDao.selectByParam(view);
        PageInfo page = new PageInfo(list);
        return page;
    }

    /**
     * 发放虚拟商品
     */
    public void sendVirtual(long userId, long goodsId, int num) throws UtilException {
        Goods goods = this.goodsService.getGooodsById(goodsId);
        User user = this.userService.getUserById(userId);
        if (goods != null && user != null) {
            for (int i = 0; i < num; i++) {
                if (goods.getVirtualType() == Enums.GoodsType.Coupon.getIndex()) {
                    this.couponService.sendCoupon(goods.getVirtualId(), user.getMobile());
                } else if (goods.getVirtualType() == Enums.GoodsType.Deductible.getIndex()) {
                    this.bonusService.sendBonus(goods.getVirtualId(), user.getMobile());
                }
            }

        }
    }


    /**
     * 限兑处理
     */

    public void exchangeLimit(long userId, long goodsId, int num) throws UtilException {


        Goods goods = this.goodsService.getGooodsById(goodsId);
        if (goods.getLimitType() == Enums.LimitType.Day.getIndex()) {
           int exchanged = this.orderDao.dayCheck(userId, goodsId);
            if (exchanged + num > goods.getLimitNum() || num > goods.getLimitNum()) {
                throw new UtilException("每人限兑" + goods.getLimitNum() + "份/天.");
            }
        } else if (goods.getLimitType() == Enums.LimitType.Forever.getIndex()) {
            int exchanged = this.orderDao.foreverCheck(userId, goodsId);
            if (exchanged + num > goods.getLimitNum() || num > goods.getLimitNum()) {
                throw new UtilException("每人仅限兑换" + goods.getLimitNum() + "份.");
            }
        }


    }


}


