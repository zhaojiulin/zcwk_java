package com.zcwk.integralmall.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.parser.impl.H2Parser;
import com.zcwk.core.util.UtilException;
import com.zcwk.integralmall.dao.GoodsDao;
import com.zcwk.integralmall.dao.GoodsTypeDao;
import com.zcwk.integralmall.model.GoodsType;
import com.zcwk.integralmall.view.GoodsTypeView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Administrator on 2017/5/6.
 */
@Service
@Repository("GoodsTypeService")
public class GoodsTypeService {
    @Autowired
    GoodsTypeDao goodsTypeDao;

    @Autowired
    GoodsDao goodsDao;

    /**
     * 新增或修改
     *
     * @param goodsType
     * @throws UtilException
     */
    @Transactional(rollbackFor = {Exception.class})
    public void saveOrUpdate(GoodsType goodsType) throws UtilException {
        if (goodsType.getName() == null) {
            throw new UtilException("请输入商品类型名称！");
        }
        if (goodsType.getImg() == null) {
            throw new UtilException("请添加类型图标！");
        }
        goodsType.setUpdateTime(new Date());
        GoodsType db = this.goodsTypeDao.selectByPrimaryKey(goodsType.getId());

        if (db != null) {
            //更新
            int update = this.goodsTypeDao.updateByPrimaryKeySelective(goodsType);
            if (update < 1) {
                throw new UtilException("更新失败！");
            }
        } else {
            //新增
            goodsType.setCreateTime(new Date());
            int insert = this.goodsTypeDao.insertSelective(goodsType);
            if (insert < 1) {
                throw new UtilException("新增失败！");
            }
        }
    }


    public GoodsType getGoodsType(long id) {
        return goodsTypeDao.selectByPrimaryKey(id);
    }

    /* * 根据id删除
     * @param Id
     * @return
     */
    public int deleteById(long Id) throws UtilException {
        long type = Id;
        List<HashMap> list = this.goodsDao.Using(type);
        if (list.size() > 0) {
            throw new UtilException("请先下架所有此类型商品.");
        } else {
            return goodsTypeDao.deleteByPrimaryKey(Id);
        }
    }

    /**
     * 根据条件获取标列表
     *
     * @return
     * @throws Exception
     */
    public PageInfo getListByParam(GoodsTypeView view) {
        PageHelper.startPage(view.getPageNo(), view.getPageSize());
        List list = this.goodsTypeDao.selectByParam(view);
        PageInfo page = new PageInfo(list);
        return page;
    }

    /**
     * 根据Id是否启用
     *
     * @param Id
     * @param state
     * @throws UtilException
     */
    public void onOrOff(long Id, int state) throws UtilException {
        GoodsType goodsType = null;
        goodsType.setId(Id);
        goodsType.setState(state);
        goodsType.setUpdateTime(new Date());
        int result = this.goodsTypeDao.updateByPrimaryKeySelective(goodsType);
        if (result < 1) {
            throw new UtilException("操作失败，请重试！");
        }
    }
}
