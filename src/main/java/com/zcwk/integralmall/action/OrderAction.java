package com.zcwk.integralmall.action;

import com.github.pagehelper.PageInfo;
import com.zcwk.core.util.Result;
import com.zcwk.core.util.UtilException;
import com.zcwk.integralmall.model.Order;
import com.zcwk.integralmall.service.OrderService;
import com.zcwk.integralmall.view.OrderView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Administrator on 2017/5/6.
 */
@RestController
@RequestMapping({"/order"})
public class OrderAction {
    @Autowired
    OrderService orderService;

    /**
     * 创建订单
     * @param
     * @return
     * @throws UtilException
     */
    @RequestMapping(value = "/createOrder")
    public Object createOrder(long userId,int num,long goodsId,int addressId,String mobile) throws Exception {
        this.orderService.createOrder(userId,num,goodsId,addressId,mobile);
        return new Result().Success();
    }


    /**
     * 根据id查询
     * @param id
     * @return
     */
    @RequestMapping("/getOrder/{id}")
    public Object getOrder(@PathVariable Long id) {
        return orderService.getOrderById(id);
    }





    @RequestMapping(value = "/getList")
    @ResponseBody
    public Object getList(OrderView view) {
        PageInfo page = this.orderService.getListByParam(view);
        return new Result().Page(page).Success();
    }

    /**
     * 更新
     * @param order
     * @return
     * @throws UtilException
     */
    @RequestMapping(value = "/updateOrder")
    public Object updateOrder(Order order) throws UtilException {
        this.orderService.updateOrder(order);
        return new Result().Success();
    }

    /**
     * 限制
     * @param
     * @return
     * @throws UtilException
     */
    @RequestMapping(value = "/checkExchange")
    public Object checkExchange(long userId,long goodsId, int num) throws UtilException {
        this.orderService.exchangeLimit(userId,goodsId,num);
        return new Result().Success();
    }

}
