package com.zcwk.integralmall.action;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageInfo;
import com.zcwk.core.util.Enums;
import com.zcwk.core.util.Result;
import com.zcwk.core.util.UtilException;
import com.zcwk.integralmall.dao.GoodsTypeDao;
import com.zcwk.integralmall.model.GoodsType;
import com.zcwk.integralmall.service.GoodsTypeService;
import com.zcwk.integralmall.view.GoodsTypeView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Administrator on 2017/5/6.
 */
@RestController
@RequestMapping({"/goodsType"})
public class GoodsTypeAction {

    @Autowired
    GoodsTypeService goodsTypeService;

    /**
     * 新增或修改
     *
     * @param goodsType
     * @return
     * @throws UtilException
     */
    @RequestMapping(value = "/saveOrUpdate")
    public Object saveOrUpdate(@RequestParam("goodsType") String goodsTypestr) throws UtilException {
        GoodsType goodsType= JSON.parseObject(goodsTypestr,GoodsType.class);
        this.goodsTypeService.saveOrUpdate(goodsType);
        return new Result().Success();

    }

    /**
     * 根据Id删除商品类型
     *
     * @param Id
     * @return
     * @throws UtilException
     */
    @RequestMapping(value = "/delete")
    public Object delete(long Id) throws UtilException {
        this.goodsTypeService.deleteById(Id);
        return new Result().Success();

    }

    /**
     * 根据id查询
     * @param id
     * @return
     */
    @RequestMapping("/getGoodsType/{id}")
    public Object getgetGoodstype(@PathVariable Long id) {
        return goodsTypeService.getGoodsType(id);
    }



    @RequestMapping(value = "/getList")
    @ResponseBody
    public Object getList(GoodsTypeView goodsTypeView) {
        PageInfo page = this.goodsTypeService.getListByParam(goodsTypeView);
        return new Result().Page(page).Success();
    }

    /**
     * 根据Id是否启用
     * @param Id
     * @param state
     * @return
     * @throws UtilException
     */
    @RequestMapping(value = "/onOrOff")
    public Object onOrOff(Long Id,int state)throws UtilException{
        this.goodsTypeService.onOrOff(Id,state);
        return new Result().Success();
    }


}
