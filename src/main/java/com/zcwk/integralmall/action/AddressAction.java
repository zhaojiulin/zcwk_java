package com.zcwk.integralmall.action;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageInfo;
import com.zcwk.core.util.Result;
import com.zcwk.core.util.UtilException;
import com.zcwk.integralmall.model.Address;
import com.zcwk.integralmall.service.AddressService;
import com.zcwk.integralmall.view.AddressView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Administrator on 2017/5/6.
 */
@RestController
@RequestMapping({"/address"})
public class AddressAction {
         @Autowired
    AddressService addressService;

    /**
     * 新增或修改
     *
     * @param address
     * @return
     * @throws UtilException
     */
    @RequestMapping(value = "/saveOrUpdate")
    public Object saveOrUpdate( String address) throws UtilException {
        Address adds= JSON.parseObject(address,Address.class);
        this.addressService.save(adds);
        return new Result().Success();

    }

    /**
     * 根据Id删除商品类型
     *
     * @param Id
     * @return
     * @throws UtilException
     */
    @RequestMapping(value = "/delete")
    public Object delete(long Id) throws UtilException {
        this.addressService.deleteById(Id);
        return new Result().Success();

    }
        /**
         * 根据id查询
         * @param Id
         * @param userId
         * @return
         */
    @RequestMapping("/getAddress")
    public Object getAddress(Long Id,long userId) {
        return addressService.getAddress(Id,userId);
    }


    /**
     * 根据条件查询
     * @param view
     * @return
     */

    @RequestMapping(value = "/getList")
    @ResponseBody
    public Object getList(AddressView view) {
        PageInfo page = this.addressService.getListByParam(view);
        return new Result().Page(page).Success();
    }

}
