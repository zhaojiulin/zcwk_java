package com.zcwk.integralmall.action;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageInfo;
import com.zcwk.core.util.Enums;
import com.zcwk.core.util.Result;
import com.zcwk.core.util.UtilException;
import com.zcwk.integralmall.model.Goods;
import com.zcwk.integralmall.model.GoodsType;
import com.zcwk.integralmall.service.GoodsService;
import com.zcwk.integralmall.view.GoodsView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

/**
 * Created by Administrator on 2017/5/6.
 */
@RestController
@RequestMapping({"/goods"})
public class GoodsAction {
    @Autowired
    GoodsService goodsService;
    /**
     * 新增或修改
     *
     * @param goodsstr
     * @return
     * @throws UtilException
     */
    @RequestMapping(value = "/saveOrUpdate")
    public Object saveOrUpdate(@RequestParam("goods") String goodsstr) throws UtilException {
        Goods goods= JSON.parseObject(goodsstr,Goods.class);
        this.goodsService.saveOrUpdate(goods);
        return new Result().Success();

    }

    /**
     * 根据Id删除商品类型
     *
     * @param Id
     * @return
     * @throws UtilException
     */
    @RequestMapping(value = "/delete")
    public Object delete(long Id) throws UtilException {
        this.goodsService.deleteById(Id);
        return new Result().Success();

    }

    /**
     * 根据id查询
     * @param id
     * @return
     */
    @RequestMapping("/getGoods/{id}")
    public Object getCrowdFunding(@PathVariable Long id) {
        return goodsService.getGooodsById(id);
    }




    /**
     * 根据条件查询
     * @param view
     * @return
     */

    @RequestMapping(value = "/getList")
    @ResponseBody
    public Object getList(GoodsView view) {
        PageInfo page = this.goodsService.getListByParam(view);
        return new Result().Page(page).Success();
    }

    /**
     * 根据Id上下架
     * @param goodsId
     * @param state
     * @return
     * @throws UtilException
     */
    @RequestMapping(value = "/lanchOrBan")
    public Object lanchOrBan(Long goodsId,int state)throws UtilException{
        this.goodsService.lanchOrBan(goodsId,state);
        return new Result().Success();
    }

    /**
     * 推荐
     * @param
     * @return
     * @throws UtilException
     */
    @RequestMapping(value = "/recommend")
    public Object recommend(@RequestParam("goods") String goodsStr) throws UtilException {
        Goods goods = JSON.parseObject(goodsStr, Goods.class);
        this.goodsService.recommend(goods);
        return new Result().Success();
    }


}
