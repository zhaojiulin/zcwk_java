package com.zcwk.integralmall.dao;

import com.zcwk.core.core.model.QueryParam;
import com.zcwk.integralmall.model.Address;
import com.zcwk.integralmall.view.AddressView;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * 项目dao
 * 
 * @author zp
 * @since 2017年3月16日
 *
 */
public interface AddressDao {

    int deleteByPrimaryKey(Long id);

    int insert(Address record);

    int insertSelective(Address record);

    Address selectByPrimaryKey(Long id);

    Address selectByUserId(@Param("id") Long id,@Param("userId") Long userId);

    int updateByPrimaryKeySelective(Address record);

    int updateByPrimaryKeyWithBLOBs(Address record);

    int updateByPrimaryKey(Address record);
    public List<HashMap> selectByParam(@Param("view") AddressView orderView);

}