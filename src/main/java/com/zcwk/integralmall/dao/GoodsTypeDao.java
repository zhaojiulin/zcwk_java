package com.zcwk.integralmall.dao;

import com.zcwk.integralmall.model.GoodsType;
import com.zcwk.integralmall.view.GoodsTypeView;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;

/**
 * 项目dao
 * 
 * @author zp
 * @since 2017年3月16日
 *
 */
public interface GoodsTypeDao {
    int deleteByPrimaryKey(Long id);

    int insert(GoodsType record);

    int insertSelective(GoodsType record);

    GoodsType selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(GoodsType record);

    int updateByPrimaryKeyWithBLOBs(GoodsType record);

    int updateByPrimaryKey(GoodsType record);
    public List<HashMap> selectByParam(@Param("view") GoodsTypeView goodsTypeView);

}