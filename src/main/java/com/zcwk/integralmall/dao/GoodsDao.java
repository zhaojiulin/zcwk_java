package com.zcwk.integralmall.dao;

import com.zcwk.integralmall.model.Goods;
import com.zcwk.integralmall.model.GoodsType;
import com.zcwk.integralmall.view.GoodsView;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

/**
 * 项目dao
 * 
 * @author zp
 * @since 2017年3月16日
 *
 */
public interface GoodsDao {
    int deleteByPrimaryKey(Long id);

    int insert(Goods record);

    int insertSelective(Goods record);

    Goods selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Goods record);

    int updateByPrimaryKey(Goods record);

    int updateStock(Goods record);

    public List<HashMap> selectByParam(@Param("view") GoodsView goodsView);

    @Select({"select * from c_goods where state=1"})
    @ResultMap({"BaseResultMap"})
    public List<Goods> check();

    @Select({"select * from c_goods where state=1 and type=#{type,jdbcType=BIGINT}"})
    @ResultMap({"BaseResultMap"})
    public List<HashMap> Using(@Param("type") Long type );

}