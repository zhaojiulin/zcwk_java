package com.zcwk.integralmall.dao;

import com.zcwk.integralmall.model.Order;
import com.zcwk.integralmall.view.OrderView;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import sun.util.calendar.LocalGregorianCalendar;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * 项目dao
 *
 * @author zp
 * @since 2017年3月16日
 */
public interface OrderDao {

    int deleteByPrimaryKey(Long id);

    int insert(Order record);

    int insertSelective(Order record);

    Order selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Order record);

    int updateByPrimaryKey(Order record);

    public List<HashMap> selectByParam(@Param("view") OrderView orderView);

    @Update({"update c_order set state=#{newState,jdbcType=INTEGER} and update_time=now() where id = #{id,jdbcType=BIGINT} and state=#{state,jdbcType=INTEGER}"})
    public int updateState(@Param("id") Long id, @Param("state") int state, @Param("newState") int newState);

    @Select({"select IFNULL(sum(num),0) from c_order where user_id=#{userId,jdbcType=BIGINT} and goods_id=#{goodsId,jdbcType=BIGINT} and DATE_FORMAT(create_time,'%Y%m%d')=DATE_FORMAT(NOW(),'%Y%m%d')"})
     int  dayCheck(@Param("userId") Long userId, @Param("goodsId") Long goodsId);


      @Select({"select IFNULL(sum(num),0) from c_order where user_id=#{userId,jdbcType=BIGINT} and goods_id=#{goodsId,jdbcType=BIGINT}"})
     int foreverCheck(@Param("userId") Long userId, @Param("goodsId") Long goodsId);
}