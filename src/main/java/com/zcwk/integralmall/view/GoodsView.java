package com.zcwk.integralmall.view;

import com.zcwk.core.util.StringUtil;
import com.zcwk.core.util.UtilException;
import com.zcwk.integralmall.model.Goods;
import com.zcwk.integralmall.model.GoodsType;
import org.springframework.beans.BeanUtils;

import java.math.BigDecimal;

/**
 * Created by Administrator on 2017/3/23.
 */
public class GoodsView extends Goods{
    /**
     * 项目view
     *
     * @author zp
     * @since 2017年3月17日
     */


    private static final long serialVersionUID = 1L;

    /**
     * 名称
     */
    private String name;
    /**
     * 开始时间
     */
    private String bTime;
    /**
     * 结束时间
     */
    private String eTime;
    /**
     * 页码
     **/
    private Integer pageNo = 1;
    /**
     * 每页条数
     **/
    private Integer pageSize = 20;

    /**
     * 开始积分
     **/
    private Integer bIntegral;
    /**
     * 结束积分
     **/
    private Integer eIntegral;
    /**
     * 手机号
     **/
    private Integer typeState;
    /**
     * 积分排序
     */
    private  String integralSort;
    /**
     * 兑换量排序
     */
    private String hadStockSort;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getbTime() {
        return bTime;
    }

    public void setbTime(String bTime) {
        this.bTime = bTime;
    }

    public String geteTime() {
        return eTime;
    }

    public void seteTime(String eTime) {
        this.eTime = eTime;
    }

    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }


    public static GoodsView instance(Goods Goods) {
        GoodsView view = new GoodsView();
        BeanUtils.copyProperties(Goods, view);
        return view;
    }

    public Goods prototype() {
        Goods goods = new GoodsView();
        BeanUtils.copyProperties(this, goods);
        return goods;
    }

    /**
     * 校验内容
     * @throws UtilException
     */
    public void checkContent() throws UtilException{
        
    }

    public Integer getbIntegral() {
        return bIntegral;
    }

    public void setbIntegral(Integer bIntegral) {
        this.bIntegral = bIntegral;
    }

    public Integer geteIntegral() {
        return eIntegral;
    }

    public void seteIntegral(Integer eIntegral) {
        this.eIntegral = eIntegral;
    }

    public Integer getTypeState() {
        return typeState;
    }

    public void setTypeState(Integer typeState) {
        this.typeState = typeState;
    }

    public String getIntegralSort() {
        return integralSort;
    }

    public void setIntegralSort(String integralSort) {
        this.integralSort = integralSort;
    }

    public String getHadStockSort() {
        return hadStockSort;
    }

    public void setHadStockSort(String hadStockSort) {
        this.hadStockSort = hadStockSort;
    }
}

