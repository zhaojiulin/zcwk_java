package com.zcwk.integralmall.view;

import com.zcwk.integralmall.model.GoodsType;
import org.springframework.beans.BeanUtils;

/**
 * Created by Administrator on 2017/3/23.
 */
public class GoodsTypeView extends GoodsType {
    /**
     * 项目view
     *
     * @author zp
     * @since 2017年3月17日
     */


    private static final long serialVersionUID = 1L;

    /**
     * 名称
     */
    private String name;
    /**
     * 开始时间
     */
    private String bTime;
    /**
     * 结束时间
     */
    private String eTime;
    /**
     * 页码
     **/
    private Integer pageNo = 1;
    /**
     * 每页条数
     **/
    private Integer pageSize = 20;

    /**
     * 手机号
     **/

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getbTime() {
        return bTime;
    }

    public void setbTime(String bTime) {
        this.bTime = bTime;
    }

    public String geteTime() {
        return eTime;
    }

    public void seteTime(String eTime) {
        this.eTime = eTime;
    }

    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }


    public static GoodsTypeView instance(GoodsType GoodsType) {
        GoodsTypeView view = new GoodsTypeView();
        BeanUtils.copyProperties(GoodsType, view);
        return view;
    }

    public GoodsType prototype() {
        GoodsType goodsType = new GoodsTypeView();
        BeanUtils.copyProperties(this, goodsType);
        return goodsType;
    }

}

