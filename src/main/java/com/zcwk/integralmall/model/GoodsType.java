package com.zcwk.integralmall.model;

import java.util.Date;

/**
 * Created by Administrator on 2017/5/6.
 */
public class GoodsType {
    private Long Id;

    private String name;//类型名称

    private String img;//图片

    private Date createTime;//创建时间

    private Date updateTime;//修改时间

    private Integer state;//是否启用

    private Integer isDelete;//是否删除  0 否   1  是

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }
}
