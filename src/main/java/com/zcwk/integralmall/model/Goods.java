package com.zcwk.integralmall.model;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by Administrator on 2017/5/6.
 */
public class Goods {
    private long Id;

    private String name;//名称

    private Integer type;//类型

    private Integer integral;//积分

    private Integer stock;//库存

    private Integer hadStock;//已兑换数量

    private BigDecimal price;//市场价格

    private Integer state;//状态

    private Date createTime;//创建时间

    private Date beginTime;//开始时间

    private Date updateTime;//修改时间

    private Date offTime;//下架时间

    private Date recommendTime;//推荐时间

    private Integer isRecommend;// 推荐  0 否   1  是

    private String image;//图片

    private String desc;//描述

    private String memo;//备注

    private Integer virtualId;// 虚拟产品Id

    private Integer virtualType;//虚拟产品类型

    private Integer limitNum;

    private Integer limitType;

    private Integer isDelete;//是否已删除  0 否   1  是

    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getIntegral() {
        return integral;
    }

    public void setIntegral(Integer integral) {
        this.integral = integral;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public Integer getHadStock() {
        return hadStock;
    }

    public void setHadStock(Integer hadStock) {
        this.hadStock = hadStock;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getOffTime() {
        return offTime;
    }

    public void setOffTime(Date offTime) {
        this.offTime = offTime;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public Integer getVirtualType() {
        return virtualType;
    }

    public void setVirtualType(Integer virtualType) {
        this.virtualType = virtualType;
    }

    public Integer getVirtualId() {
        return virtualId;
    }

    public void setVirtualId(Integer virtualId) {
        this.virtualId = virtualId;
    }

    public Date getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(Date beginTime) {
        this.beginTime = beginTime;
    }

    public Date getRecommendTime() {
        return recommendTime;
    }

    public void setRecommendTime(Date recommendTime) {
        this.recommendTime = recommendTime;
    }


    public Integer getIsRecommend() {
        return isRecommend;
    }

    public void setIsRecommend(Integer isRecommend) {
        this.isRecommend = isRecommend;
    }

    public Integer getLimitNum() {
        return limitNum;
    }

    public void setLimitNum(Integer limitNum) {
        this.limitNum = limitNum;
    }

    public Integer getLimitType() {
        return limitType;
    }

    public void setLimitType(Integer limitType) {
        this.limitType = limitType;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }
}
