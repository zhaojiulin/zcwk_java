package com.zcwk.core.web;

import com.zcwk.core.model.SystemSet;
import com.zcwk.core.service.SystemSetService;
import com.zcwk.core.util.CommonUtil;
import com.zcwk.core.util.Enums;
import com.zcwk.core.util.Result;
import com.zcwk.core.util.UtilException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by pwx on 2016/11/17.
 */
@RestController
@RequestMapping({"/systemSet"})
public class SystemSetController {
    @Autowired
    SystemSetService systemSetService;

    /**
     * 获取所有参数
     *
     * @return
     * @throws Exception
     */
    @RequestMapping("/getAll")
    @ResponseBody
    public Object getAll() {
        return this.systemSetService.getAll();
    }
    
    /**
     * 获取单个参数
     * @return
     * @throws UtilException 
     * @throws Exception
     */
    @RequestMapping("/getParamVal")
    @ResponseBody
    public Object getParamVal(String key) throws UtilException {
    	return systemSetService.getParamVal(key);
    }

    @RequestMapping("/getSystemSet")
    @ResponseBody
    public Object getByKey() throws UtilException {
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("key", this.systemSetService.getParamVal(Enums.Params.ApiKey.getKey()));
        map.put("static_url", this.systemSetService.getParamVal(Enums.Params.StaticPath.getKey()));
        map.put("base", this.systemSetService.getParamVal(Enums.Params.Base.getKey()));
        map.put("apiBase", this.systemSetService.getParamVal(Enums.Params.ApiBaseUrl.getKey()));
        map.put("authGateway", this.systemSetService.getParamVal(Enums.Params.AuthGateway.getKey()));
        //个推透传类容配置
        Object o = systemSetService.getParamVal(Enums.Params.GeTui.getKey());
    	if(o != null){
    		Map<String, String> config = CommonUtil.parseMap(o.toString());
    		String transmissionType = config.get("transmissionType");//接收应用
    		map.put("transmissionType", transmissionType);
    	}
        return map;
    }

    @RequestMapping("/saveSet")
    @ResponseBody
    public Object saveSet(SystemSet set) {
        this.systemSetService.save(set);
        return new Result().Success();
    }
}
