package com.zcwk.core.web;

import com.zcwk.core.model.Youmi;
import com.zcwk.core.model.User;
import com.zcwk.core.service.UserService;
import com.zcwk.core.service.YoumiService;
import com.zcwk.core.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 有米
 * Created by pwx on 2017/3/14.
 */
@RestController
@RequestMapping({"/youmi"})
public class YoumiController {

    @Autowired
    YoumiService youmiService;

    @Autowired
    UserService userService;

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ResponseBody
    public Object save(Youmi youmi) throws Exception {
        this.youmiService.save(youmi);
        return new Result().Success();
    }


}
