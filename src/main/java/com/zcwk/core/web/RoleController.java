/**
 *
 */
package com.zcwk.core.web;

import java.util.ArrayList;
import java.util.List;

import com.zcwk.core.util.UtilException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.github.pagehelper.PageInfo;
import com.zcwk.core.model.Role;
import com.zcwk.core.service.RoleService;
import com.zcwk.core.util.Result;

/**
 * @author dengqun
 *         <p/>
 *         下午6:35:59
 */
@RestController
@RequestMapping({"/role"})
public class RoleController {

    @Autowired
    RoleService roleService;

    /**
     * @param id
     * @return
     * @throws Exception
     */
    @RequestMapping("/get/{id}")
    @ResponseBody
    public Object get(@PathVariable long id) throws Exception {
        Role role = this.roleService.get(id);
        return role;
    }

    /**
     * 查询角色带分页
     *
     * @param pageNo
     * @param pageSize
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getList")
    @ResponseBody
    public Object getList(int pageNo, int pageSize, String state)
            throws Exception {
        PageInfo page = this.roleService.selectRolePage(pageNo, pageSize, state);
        return new Result().Page(page).Success();
    }

    @RequestMapping(value = "/save")
    @ResponseBody
    public Object save(Role role) throws UtilException {
        this.roleService.save(role);
        return new Result().Success();
    }

    @RequestMapping(value = "/saveRights")
    @ResponseBody
    public Object saveRights(long roleId, String rightsIds)  throws UtilException{
        this.roleService.saveRights(roleId, rightsIds);
        return new Result().Success();
    }
}
