/**
 *
 */
package com.zcwk.core.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.zcwk.core.model.Seo;
import com.zcwk.core.service.SeoService;
import com.zcwk.core.util.Result;

/**
 * @author dengqun
 *         <p/>
 *         上午9:22:34
 */
@RestController
@RequestMapping({"/seo"})
public class SeoController {

    @Autowired
    SeoService seoService;

    /**
     * @param seo
     * @return
     * @throws Exception
     */
    @RequestMapping("/save")
    @ResponseBody
    public Object save(Seo seo) throws Exception {
        this.seoService.save(seo);
        return new Result().Success();
    }

    /**
     * @return
     * @throws Exception
     */
    @RequestMapping("/get")
    @ResponseBody
    public Object get() {
        return this.seoService.get();
    }
}
