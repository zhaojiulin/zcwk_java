package com.zcwk.core.web;

import com.zcwk.core.service.CommonService;
import com.zcwk.core.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by pwx on 2016/9/29.
 */
@RestController
@RequestMapping({"/common"})
public class CommonController {
    @Autowired
    UtilCache cache;
    @Autowired
    CommonService commonService;



    @RequestMapping("/getTotal")
    @ResponseBody
    public Object getTotal() {
        return this.cache.getTotal();
    }

    @RequestMapping("/getTotalInvest")
    @ResponseBody
    public Object getTotalInvest() {
        return this.cache.getTotalInvest();
    }

    @RequestMapping("/getSortInvest")
    @ResponseBody
    public Object getSortInvest(int limit) {
        return this.cache.getSortInvest(limit);
    }


    @RequestMapping("/sendCode")
    @ResponseBody
    public Object sendCode(String mobile, String type) throws UtilException {
        String key = mobile + "_" + type;
        String _code = this.cache.getCode(key);
        if (_code != null)
            return new Result().Error("请勿频繁发送");
        String code = CommonUtil.getRandom(6);
        String content ="您正在使用手机验证申请，验证码：" + code + "，有效期2分钟，请勿向任何人透露验证码！";
        System.out.println( "["+mobile+"]"+"您正在使用手机验证申请，验证码：" + code + "，有效期2分钟，请勿向任何人透露验证码！"+CommonUtil.formatDate("yyyy-MM-dd HH:mm:ss", DateUtil.getNow()));
        boolean send = this.commonService.sendSmsMsg(mobile, content);
        if (send) {
            this.cache.setCode(mobile + "_" + type, code);
            return new Result().Success();
        }
        return new Result().Error("短信发送失败");
    }

    @RequestMapping("/checkCode")
    @ResponseBody
    public Object checkCode(String mobile, String type, String code) throws UtilException {
        String key = mobile + "_" + type;
        String _code = this.cache.getCode(key);
        if (_code != null && _code.equals(code)) {
            //this.cache.delCode(key);
            return new Result().Success();
        }
        return new Result().Error("验证码输入有误");
    }


}
