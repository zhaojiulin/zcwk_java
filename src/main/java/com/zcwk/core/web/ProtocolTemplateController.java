package com.zcwk.core.web;

import com.github.pagehelper.PageInfo;
import com.zcwk.core.model.ProtocolTemplate;
import com.zcwk.core.service.ProtocolTemplateService;
import com.zcwk.core.util.Result;
import com.zcwk.core.util.UtilException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by pwx on 2016/10/6.
 */
@RestController
@RequestMapping({"/protocolTemplate"})
public class ProtocolTemplateController {
    @Autowired
    ProtocolTemplateService protocolTemplateService;


    @RequestMapping("/save")
    @ResponseBody
    public Object save(ProtocolTemplate template) throws UtilException {
        this.protocolTemplateService.save(template);
        return new Result().Success();
    }

    @RequestMapping("/getAll")
    @ResponseBody
    public Object getAll(int pageSize, int pageNo, String type) {
        PageInfo page = this.protocolTemplateService.getAll(pageNo, pageSize, type);
        return new Result().Page(page).Success();
    }

    @RequestMapping("/get")
    @ResponseBody
    public Object getProtocol(int id) {
        return this.protocolTemplateService.get(id);
    }
}
