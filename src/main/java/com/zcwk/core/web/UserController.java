package com.zcwk.core.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.zcwk.core.service.AccountService;
import com.zcwk.core.util.UtilCache;
import com.zcwk.core.util.UtilException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.zcwk.core.model.User;
import com.zcwk.core.service.UserService;
import com.zcwk.core.util.Result;

/**
 * Created by pwx on 2016/6/30.
 */
@RestController
@RequestMapping({"/user"})
public class UserController {
    @Autowired
    UserService userService;
    @Autowired
    AccountService accountService;
    @Autowired
    UtilCache cache;

    @RequestMapping(value = "/getUserByMobile")
    @ResponseBody
    public Object getUserByMobile(String mobile) throws Exception {
        User user = this.userService.getUserByMobile(mobile);
        return user;
    }

    @RequestMapping(value = "/get/{id}")
    @ResponseBody
    public Object getUserById(@PathVariable long id) throws Exception {
        Map map = new HashMap<>();
        User user = this.userService.getUserById(id);
        map.put("user", user);
        map.put("account", this.accountService.getAccount(user.getAccountId()));
        return map;
    }

    @RequestMapping(value = "/getUserByAccountId")
    @ResponseBody
    public Object getUserByAccountId(long id) {
        return this.userService.getUserByAccountId(id);
    }

    /**
     * 投资人管理查询
     *
     * @param user
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/queryUsers", method = RequestMethod.POST)
    @ResponseBody
    public Object queryUsers(int pageNo, int pageSize, User user) throws Exception {
        PageInfo page = this.userService.queryUsers(pageNo, pageSize, user);
        return new Result().Page(page).Success();
    }

    /**
     * 查询邀请人
     *
     * @param pageNo
     * @param pageSize
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getInviteUser")
    @ResponseBody
    public Object getInviteUser(int pageNo, int pageSize, String inviteId, String param, String bTime, String eTime) throws Exception {
        PageInfo page = this.userService.getInviteUser(pageNo, pageSize, inviteId, param, bTime, eTime);
        return new Result().Page(page).Success();
    }

    /**
     * 用户注册 必须参数mobile password confirmPassword channel
     *
     * @param user
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ResponseBody
    public Object save(User user) throws Exception {
        this.userService.save(user);
        return new Result().Success();
    }

    /**
     * 用户登录
     *
     * @param user
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ResponseBody
    public Object login(User user) throws Exception {
        User u = this.userService.login(user);
        return new Result().Add("user", u).Success();
    }

    /**
     * 实名
     *
     * @throws Exception
     */
    @RequestMapping(value = "/saveRealName", method = RequestMethod.POST)
    @ResponseBody
    public Object saveRealName(long userId, String realName, String idCard) throws Exception {
        User user = new User();
        user.setId(userId);
        user.setRealName(realName);
        user.setIdCard(idCard);
        boolean flag= cache.checkFrequentAction("realName_"+user.getAccountId(),2);
        if(!flag){
            throw new UtilException("请勿频繁操作");
        }
        boolean result = this.userService.saveRealName(user);
        if (result)
            return new Result().Success();
        else
            return new Result().Error("实名失败");
    }


    /**
     * 邮箱认证
     *
     * @throws Exception
     */
    @RequestMapping(value = "/validEmail")
    @ResponseBody
    public Object validEmail(long userId, String email)
            throws Exception {
        User user = new User();
        user.setId(userId);
        user.setEmail(email);
        this.userService.validEmail(user);
        return new Result().Success();
    }

    /**
     * 设置头像
     *
     * @throws Exception
     */
    @RequestMapping(value = "/saveHeadImg")
    @ResponseBody
    public Object saveHeadImg(long userId, String img)
            throws Exception {
        this.userService.saveHeadImg(userId, img);
        return new Result().Success();
    }

    /**
     * 邮箱认证
     *
     * @throws Exception
     */
    @RequestMapping(value = "/lock")
    @ResponseBody
    public Object lock(long userId, boolean lock)
            throws Exception {
        this.userService.lock(userId, lock);
        return new Result().Success();
    }

    /**
     * 借款帐号锁定
     * @throws Exception
     */
    @RequestMapping(value = "/lockBorrow")
    @ResponseBody
    public Object lockBorrow(long userId) throws Exception {
        this.userService.lockBorrow(userId);
        return new Result().Success();
    }

    /**
     * 借款帐号解锁
     * @throws Exception
     */
    @RequestMapping(value = "/unlockBorrow")
    @ResponseBody
    public Object unlockBorrow(long userId) throws Exception {
        this.userService.unlockBorrow(userId);
        return new Result().Success();
    }

    /**
     * 邮箱认证
     *
     * @throws Exception
     */
    @RequestMapping(value = "/validEmailCall")
    @ResponseBody
    public Object validEmailCall(long userId, long time, String sign)
            throws Exception {
        this.userService.validEmailCall(userId, time, sign);
        return new Result().Success();
    }

    /**
     * 设置交易密码
     *
     * @throws Exception
     */
    @RequestMapping(value = "/savePayPassword")
    @ResponseBody
    public Object savePayPassword(long userId, String password)
            throws Exception {
        User user = new User();
        user.setId(userId);
        user.setPayPassword(password);
        this.userService.savePayPassword(user);
        return new Result().Success();
    }


    /**
     * 设置登录密码
     *
     * @throws Exception
     */
    @RequestMapping(value = "/saveLoginPassword")
    @ResponseBody
    public Object saveLoginPassword(long userId, String password)
            throws Exception {
        this.userService.saveLoginPassword(userId, password);
        return new Result().Success();
    }

    /**
     * 根据条件获取用户列表
     *
     * @throws Exception
     */
    @RequestMapping(value = "/getUserList")
    @ResponseBody
    public Object getUserList(int pageNo, int pageSize, String param, String bTime, String eTime, String type,String channel,String bInvestCount,String eInvestCount,String route,String invite) throws Exception {
        int bInvestCounTemp=0,eInvestCountTemp=0,routeTemp=0,channelTemp=0;
        try {
            bInvestCounTemp = Integer.parseInt(bInvestCount);
        } catch (NumberFormatException e) {
            bInvestCounTemp=-1;
        }
        try {
            eInvestCountTemp = Integer.parseInt(eInvestCount);
        } catch (NumberFormatException e) {
            eInvestCountTemp=-1;
        }
        try {
            routeTemp = Integer.parseInt(route);
        } catch (NumberFormatException e) {
            routeTemp=0;
        }
        try {
            channelTemp = Integer.parseInt(channel);
        } catch (NumberFormatException e) {
            channelTemp=-1;
        }
        PageInfo page = this.userService.getUserByParam(pageNo, pageSize, param, bTime, eTime,routeTemp, type,channelTemp,bInvestCounTemp,eInvestCountTemp,invite);
        return new Result().Page(page).Success();
    }

    /**
     * 设置登录密码
     *
     * @throws Exception
     */
    @RequestMapping(value = "/initLoginPassword")
    @ResponseBody
    public Object initLoginPassword(String mobile, String password) throws Exception {
        this.userService.saveLoginPassword(mobile, password);
        return new Result().Success();
    }

    /**
     * 签到
     *
     * @throws Exception
     */
    @RequestMapping(value = "/signIn")
    @ResponseBody
    public Object signIn(long userId) throws Exception{
        return this.userService.signIn(userId);
    }
}
