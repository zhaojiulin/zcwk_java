package com.zcwk.core.web;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageInfo;
import com.zcwk.core.dao.BankCardMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.zcwk.core.model.*;
import com.zcwk.core.service.*;
import com.zcwk.core.util.*;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping({"/account"})
public class AccountController {
    @Autowired
    AccountService accountService;
    @Autowired
    RechargeService rechargeService;
    @Autowired
    FreezeService freezeService;

    @Autowired
    BankCardMapper bankCardMapper;
    @Autowired
    BankCardService bankCardService;
    @Autowired
    IntegralService integralService;
    @Autowired
    UserService userService;
    @Autowired
    WithdrawService withdrawService;

    @RequestMapping("/get/{id}")
    @ResponseBody
    public Object getAccount(@PathVariable long id) throws Exception {
        Account account = this.accountService.getAccount(id);
        return account;
    }

    @RequestMapping(value = "/saveLineRecharge")
    @ResponseBody
    public Object saveLineRecharge(String mobile, BigDecimal amount, String memo) throws Exception {
        this.accountService.lineRecharge(mobile, amount, memo);
        return new Result().Success();
    }

    @RequestMapping(value = "/bindCard")
    @ResponseBody
    public Object bindCard(long accountId) throws Exception {
        this.accountService.bindCard(accountId);
        return new Result().Success();
    }

    @RequestMapping(value = "/saveLineDebit")
    @ResponseBody
    public Object saveLineDebit(String mobile, BigDecimal amount, String memo) throws Exception {
        this.accountService.lineDebit(mobile, amount, memo);
        return new Result().Success();
    }

    @RequestMapping(value = "/getRechargeList")
    @ResponseBody
    public Object getRechargeList(int pageNo, int pageSize, String param, String bTime, String eTime, String gatWay, String memo, BigDecimal bAmount, BigDecimal eAmount, long accountId,Byte route,String state) throws Exception {
        PageInfo page = this.accountService.getRechargeByParam(pageNo, pageSize, param, bTime, eTime, gatWay, memo, bAmount, eAmount, accountId,route,state);
        return new Result().Page(page).Success();
    }

    @RequestMapping(value = "/getCashFlowList")
    @ResponseBody
    public Object getCashFlowList(int pageNo, int pageSize, String param, String bTime, String eTime, byte type, long accountId) throws Exception {
        PageInfo page = this.accountService.getCashFlowByParam(pageNo, pageSize, param, bTime, eTime, type, accountId);
        return new Result().Page(page).Success();
    }


    @RequestMapping(value = "/getWithdrawList")
    @ResponseBody
    public Object getWithdrawList(int pageNo, int pageSize, String param, String bTime, String eTime, String auditState, String payState, long accountId,Byte route,String auditStartTime,String auditEndTime) throws Exception {
        PageInfo page = this.accountService.getWithdrawByParam(pageNo, pageSize, param, bTime, eTime, auditState, payState, accountId,route,auditStartTime, auditEndTime);
        return new Result().Page(page).Success();
    }

    @RequestMapping(value = "/getPaymentList")
    @ResponseBody
    public Object getPaymentList(int pageNo, int pageSize, String bTime, String eTime, long accountId, String state, String investId) throws Exception {
        PageInfo page = this.accountService.getPaymentByAccount(pageNo, pageSize, bTime, eTime, accountId, state, investId);
        return new Result().Page(page).Success();
    }


    @RequestMapping(value = "/getBankCardList")
    @ResponseBody
    public Object getBankCardList(int pageNo, int pageSize, String param, String bTime, String eTime, String accountId) throws Exception {
        PageInfo page = this.accountService.getBankCardByParam(pageNo, pageSize, param, bTime, eTime, accountId);
        return new Result().Page(page).Success();
    }

    @RequestMapping(value = "/getAccountBankCard")
    @ResponseBody
    public Object getAccountBankCard(long accountId) throws Exception {
        return this.bankCardMapper.selectByAccount(accountId);
    }

    @RequestMapping(value = "/deleteAccountBankCard")
    @ResponseBody
    public Object deleteAccountBankCard(long accountId) throws Exception {
        //this.bankCardService.deleteCard(accountId);
        return new Result().Success();
    }

    @RequestMapping(value = "/getFreezeList")
    @ResponseBody
    public Object getBankCardList(int pageNo, int pageSize, String param, String bTime, String eTime, String accountId, String type) throws Exception {
        PageInfo page = this.freezeService.getFreezeByParam(pageNo, pageSize, param, bTime, eTime, accountId, type);
        return new Result().Page(page).Success();
    }

    @RequestMapping(value = "/saveBankCardById")
    @ResponseBody
    public Object saveBankCardById(@RequestParam("bankCard") String bankCardStr) throws Exception {
        BankCard bankCard = JSON.parseObject(bankCardStr, BankCard.class);
        this.accountService.saveBankCardById(bankCard);
        return new Result().Success();
    }

    @RequestMapping(value = "/saveBankCardByAccount")
    @ResponseBody
    public Object saveBankCardByAccount(@RequestParam("bankCard") String bankCardStr) throws Exception {
        BankCard bankCard = JSON.parseObject(bankCardStr, BankCard.class);
        this.accountService.saveBankCardByAccount(bankCard);
        return new Result().Success();
    }

    @RequestMapping(value = "/saveBankCardByAdmin")
    @ResponseBody
    public Object saveBankCardByAdmin(BankCard bankCard) throws Exception{
        this.accountService.saveBankCardByAdmin(bankCard);
        return new Result().Success();
    }

    @RequestMapping(value = "/getWithdraw")
    @ResponseBody
    public Object getWithdraw(long withdrawId) throws Exception {
        Withdraw w=this.accountService.getWithdraw(withdrawId);
        return new Result().Add("withdraw", w).Success();
    }

    @RequestMapping(value = "/auditWithdraw")
    @ResponseBody
    public Object auditWithdraw(long withdrawId, long auditUserId, byte state, String memo,String fee,String otherFee,String noInvestedFee) throws Exception {
        BigDecimal feeTemp=BigDecimal.ZERO,otherFeeTemp=BigDecimal.ZERO,noInvestedFeeTemp=BigDecimal.ZERO;
        try {
            if(fee!=null && !fee.isEmpty()){
                feeTemp = new BigDecimal(fee);
            }
        } catch (NumberFormatException e) {
            feeTemp=BigDecimal.ZERO;
        }
        try {
            if(otherFee!=null && !otherFee.isEmpty()){
                otherFeeTemp = new BigDecimal(otherFee);
            }
        } catch (NumberFormatException e) {
            otherFeeTemp=BigDecimal.ZERO;
        }
        try {
            if(noInvestedFee!=null && !noInvestedFee.isEmpty()){
                noInvestedFeeTemp = new BigDecimal(noInvestedFee);
            }
        } catch (NumberFormatException e) {
            noInvestedFeeTemp=BigDecimal.ZERO;
        }
        this.accountService.auditWithdraw(withdrawId, auditUserId, state, memo,feeTemp,otherFeeTemp,noInvestedFeeTemp);
        return new Result().Success();
    }

    @RequestMapping(value = "/payWithdraw")
    @ResponseBody
    public Object payWithdraw(long withdrawId, long payUserId, byte state, String memo) throws Exception {
        this.accountService.payWithdraw(withdrawId, payUserId, state,memo);
        return new Result().Success();
    }

    /**
     * 提现
     * @param userId
     * @param accountId
     * @param amount
     * @param payPassword
     * @param route
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/withdraw")
    @ResponseBody
    public Object withdraw(long userId, long accountId, BigDecimal amount, String payPassword,int route) throws Exception {
        this.accountService.withdraw(userId, accountId, amount, payPassword,route);
        return new Result().Success();
    }

    @RequestMapping(value = "/saveAutoInvest")
    @ResponseBody
    public Object saveAutoInvest(@RequestParam("auto") String autoStr) throws Exception {
        AutoInvest auto = JSON.parseObject(autoStr, AutoInvest.class);
        this.accountService.saveAutoInvest(auto);
        return new Result().Success();
    }

    @RequestMapping(value = "/changeAutoInvest")
    @ResponseBody
    public Object changeAutoInvest(long accountId, byte state) throws Exception {
        this.accountService.changeAutoInvest(accountId, state);
        return new Result().Success();
    }


    @RequestMapping(value = "/getAutoInvest")
    @ResponseBody
    public Object getAutoInvest(long accountId) throws Exception {
        AutoInvest auto = this.accountService.getAutoInvest(accountId);
        return auto;
    }

    @RequestMapping(value = "/selectPaymentDays")
    @ResponseBody
    public Object selectPaymentDays(long accountId) {
        return this.accountService.selectPaymentDays(accountId);
    }

    /**
     * 资金平移
     * @param mobile 手机号
     * @param amount 平移金额
     * @param memo 备注
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/capitalTranslation")
    @ResponseBody
    public Object capitalTranslation(String mobile, BigDecimal amount, String memo) throws Exception {
        if(memo==null||memo.isEmpty()){
            memo="资金平移";
        }
        this.accountService.lineRecharge(mobile, amount, memo);
        return new Result().Success();
    }

    /**
     * 积分平移
     * @param mobile 手机号
     * @param integral 平移积分
     * @param memo 备注
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/integralTranslation")
    @ResponseBody
    public Object integralTranslation(String mobile, BigDecimal integral, String memo) throws Exception {
        if(memo==null||memo.isEmpty()){
            memo="积分平移";
        }
        User user=userService.getUserByMobile(mobile);
        if(user!=null){
            this.integralService.sendIntegral(Enums.Actions.IntegralTranslation.getIndex(),user,integral);
            return new Result().Success();
        }else{
            return new Result().Error("该用户不存在.");
        }
    }

    /*提现查询*/
    @RequestMapping(value = "/checkpayQuickOrder")
    @ResponseBody
    public Object checkpayQuickOrder(long withdrawId) throws Exception {
        boolean check=withdrawService.checkpayQuickOrder(withdrawId);
        if (!check) {
            return  new Result().Error("处理失败");
        }else{
            return new Result().Success();
        }
    }
}
