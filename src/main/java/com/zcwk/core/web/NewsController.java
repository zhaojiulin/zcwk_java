package com.zcwk.core.web;

import com.github.pagehelper.PageInfo;
import com.zcwk.core.model.News;
import com.zcwk.core.service.NewsService;
import com.zcwk.core.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by pwx on 2016/7/14.
 */
@RestController
@RequestMapping({"/news"})
public class NewsController {
    @Autowired
    NewsService newsService;

    /**
     * @param news
     * @return
     * @throws Exception
     */
    @RequestMapping("/save")
    @ResponseBody
    public Object save(News news) throws Exception {
        this.newsService.save(news);
        return new Result().Success();
    }


    /**
     * @param id
     * @return
     * @throws Exception
     */
    @RequestMapping("/get/{id}")
    @ResponseBody
    public Object getNews(@PathVariable long id) throws Exception {
        return this.newsService.getNewsById(id);
    }

    /**
     * 查询合作伙伴含分页
     *
     * @param pageNo
     * @param pageSize
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getList")
    @ResponseBody
    public Object getList(int pageNo, int pageSize, String param, String bTime, String eTime, String state, String type)
            throws Exception {
        PageInfo page = this.newsService.getNewsByParam(pageNo, pageSize, param, bTime, eTime, state, type);
        return new Result().Page(page).Success();
    }

    /**
     * 推荐
     * @param id
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/recommend")
    @ResponseBody
    public Object recommend(long id) throws Exception {
        int count= this.newsService.recommend(id);
        System.err.println(count);
        if(count>0)
            return new Result().Success();
        else
            return new Result().Error("推荐失败.");
    }
}
