package com.zcwk.core.web;

import com.github.pagehelper.PageInfo;
import com.zcwk.core.model.BorrowApply;
import com.zcwk.core.service.BorrowApplyService;
import com.zcwk.core.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by pwx on 2016/8/11.
 */
@RestController
@RequestMapping({"/borrowApply"})
public class BorrowApplyController {
    @Autowired
    BorrowApplyService borrowApplyService;

    @RequestMapping("/save")
    @ResponseBody
    public Object save(BorrowApply borrowApply) throws Exception {
        this.borrowApplyService.save(borrowApply);
        return new Result().Success();
    }

    @RequestMapping("/audit")
    @ResponseBody
    public Object save(long id, byte state, String memo) throws Exception {
        this.borrowApplyService.audit(id, state, memo);
        return new Result().Success();
    }

    /**
     * 查询合作伙伴含分页
     *
     * @param pageNo
     * @param pageSize
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getApplyList")
    @ResponseBody
    public Object getList(int pageNo, int pageSize, String param)
            throws Exception {
        PageInfo page = this.borrowApplyService.getApplyByParam(pageNo, pageSize, param);
        return new Result().Page(page).Success();
    }
}
