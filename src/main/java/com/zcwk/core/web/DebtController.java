package com.zcwk.core.web;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageInfo;
import com.zcwk.core.model.Debt;
import com.zcwk.core.service.DebtService;
import com.zcwk.core.util.Enums;
import com.zcwk.core.util.Result;
import com.zcwk.core.util.UtilException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by pwx on 2016/6/6.
 */
@RestController
@RequestMapping({"/debt"})
public class DebtController {
    @Autowired
    DebtService debtService;

    @RequestMapping(value = "/save")
    @ResponseBody
    public Object save(@RequestParam("debt") String debtStr) throws UtilException {
        Debt debt = JSON.parseObject(debtStr, Debt.class);
        this.debtService.save(debt);
        return new Result().Success();
    }

    @RequestMapping(value = "/audit")
    @ResponseBody
    public Object audit(long id, int state, String memo) throws UtilException {
        this.debtService.audit(id, state, memo);
        return new Result().Success();
    }

    @RequestMapping("/get/{id}")
    @ResponseBody
    public Object getDebt(@PathVariable long id) throws UtilException {
        Debt debt = this.debtService.getDebt(id);
        return debt;
    }

    @RequestMapping(value = "/getAuditList")
    @ResponseBody
    public Object getAuditList(int pageNo, int pageSize, String param, String bTime, String eTime) {
        PageInfo page = this.debtService.getDebtsByParam(pageNo, pageSize, (byte) Enums.DebtState.InConfirm.getIndex(), param, bTime, eTime);
        return new Result().Page(page).Success();
    }

    @RequestMapping(value = "/getBidingList")
    @ResponseBody
    public Object getBidingList(int pageNo, int pageSize, String param, String bTime, String eTime) {
        PageInfo page = this.debtService.getDebtsByParam(pageNo, pageSize, (byte) Enums.DebtState.ApprovalPass.getIndex(), param, bTime, eTime);
        return new Result().Page(page).Success();
    }

    @RequestMapping(value = "/cancel")
    @ResponseBody
    public Object cancel(long id, String memo) throws Exception {
        this.debtService.cancelDebt(id, memo);
        return new Result().Success();
    }
    //更新日志搜索编号：1487752910
    @RequestMapping(value = "/cut")
    @ResponseBody
    public Object cut(long id) throws Exception {
        this.debtService.cutDebt(id);
        return new Result().Success();
    }

    @RequestMapping(value = "/getRejectList")
    @ResponseBody
    public Object getRejectList(int pageNo, int pageSize, String param, String bTime, String eTime) {
        PageInfo page = this.debtService.getDebtsByParam(pageNo, pageSize, (byte) Enums.DebtState.ApprovalReject.getIndex(), param, bTime, eTime);
        return new Result().Page(page).Success();
    }

    @RequestMapping(value = "/getCancelList")
    @ResponseBody
    public Object getCancelList(int pageNo, int pageSize, String param, String bTime, String eTime) {
        PageInfo page = this.debtService.getDebtsByParam(pageNo, pageSize, (byte) Enums.DebtState.Cancel.getIndex(), param, bTime, eTime);
        return new Result().Page(page).Success();
    }

    @RequestMapping(value = "/getOverdueList")
    @ResponseBody
    public Object getOverdueList(int pageNo, int pageSize, String param, String bTime, String eTime) {
        PageInfo page = this.debtService.getDebtsByParam(pageNo, pageSize, (byte) Enums.DebtState.Overdue.getIndex(), param, bTime, eTime);
        return new Result().Page(page).Success();
    }

    @RequestMapping(value = "/getAll")
    @ResponseBody
    public Object getAll(int pageNo, int pageSize, String param, String bTime, String eTime) {
        PageInfo page = this.debtService.getDebtsAllByParam(pageNo, pageSize, param, bTime, eTime);
        return new Result().Page(page).Success();
    }

    @RequestMapping(value = "/getPaymentIngList")
    @ResponseBody
    private Object getPaymentIngList(int pageNo, int pageSize, String param, String bTime, String eTime) throws UtilException {
        PageInfo page = this.debtService.getPaymentByParam(pageNo, pageSize, param, bTime, eTime, Enums.State.Wait);
        return new Result().Page(page).Success();
    }

    @RequestMapping(value = "/statPaymentIng")
    @ResponseBody
    private Object statPaymentIng(String param, String bTime, String eTime) throws UtilException {
        PageInfo page = this.debtService.statPaymentIng(param, bTime, eTime, Enums.State.Wait);
        return new Result().Page(page).Success();
    }

    @RequestMapping(value = "/getInList")
    @ResponseBody
    private Object getOngoingList(int pageNo, int pageSize, byte productType, int containNovice) throws UtilException {
        PageInfo page = this.debtService.getInDebts(pageNo, pageSize, productType, containNovice);
        return new Result().Page(page).Success();
    }

    @RequestMapping(value = "/getDebtList")
    @ResponseBody
    private Object getDebtList(int pageNo, int pageSize, byte state,byte isReady, byte productType,byte paymentType,byte containNovice) throws UtilException {
        PageInfo page = this.debtService.getDebtList(pageNo, pageSize, state,isReady, productType, paymentType,containNovice);
        return new Result().Page(page).Success();
    }

    @RequestMapping(value = "/getExpect")
    @ResponseBody
    private Object getExpect(long debtId, BigDecimal amount) throws UtilException {
        return this.debtService.getExpect(debtId, amount);
    }

    @RequestMapping(value = "/getPaymentEndList")
    @ResponseBody
    public Object getPaymentEndList(int pageNo, int pageSize, String param, String bTime, String eTime) throws UtilException {
        PageInfo page = this.debtService.getPaymentByParam(pageNo, pageSize, param, bTime, eTime, Enums.State.Complete);
        return new Result().Page(page).Success();
    }

    @RequestMapping(value = "/payment")
    @ResponseBody
    public Object payment(long debtId, String group, String advance) throws Exception {
        this.debtService.payment(debtId, group, Boolean.valueOf(advance));
        return new Result().Success();
    }

    @RequestMapping(value = "/getPaymentByDebt")
    @ResponseBody
    public Object getPaymentByDebt(String group, String advance) throws Exception {
        HashMap map = this.debtService.getPaymentByDebt(group, Boolean.valueOf(advance));
        return map;
    }

    @RequestMapping(value = "/getPaymentDetailList")
    @ResponseBody
    private Object getPaymentDetailList(int pageNo, int pageSize,String group) throws UtilException {
        PageInfo page = this.debtService.getPaymentDetailList(pageNo, pageSize,group);
        return new Result().Page(page).Success();
    }

    @RequestMapping(value = "/getNoviceDebt")
    @ResponseBody
    public Object getNoviceDebt(int limit) throws Exception {
        List<Debt> list = this.debtService.getNoviceDebt(limit);
        return list;
    }

    @RequestMapping(value = "/buy")
    @ResponseBody
    public Object buy(long debtId, long accountId, BigDecimal amount, String bonusIds, String couponIds,int route) throws UtilException {
        this.debtService.buy(debtId, amount, accountId, false, bonusIds, couponIds,route);
        return new Result().Success();
    }

    @RequestMapping(value = "/getInvests")
    @ResponseBody
    public Object getInvests(int pageNo, int pageSize, String debtId, String accountId, String beginTime, String endTime,Byte route, String param,String channel,String bAmount,String eAmount,String invite) throws UtilException {
        int bAmountTemp=0,eAmountTemp=0,channelTemp=0;
        try {
            bAmountTemp = Integer.parseInt(bAmount);
        } catch (NumberFormatException e) {
            bAmountTemp=-1;
        }
        try {
            eAmountTemp = Integer.parseInt(eAmount);
        } catch (NumberFormatException e) {
            eAmountTemp=-1;
        }
        try {
            channelTemp = Integer.parseInt(channel);
        } catch (NumberFormatException e) {
            channelTemp=-1;
        }
        PageInfo page = this.debtService.getInvests(pageNo, pageSize, debtId, accountId, beginTime, endTime,route, param,channelTemp,bAmountTemp,eAmountTemp,invite);
        return new Result().Page(page).Success();
    }

    /**
     * 推荐
     * @param debtStr
     * @return
     * @throws UtilException
     */
    @RequestMapping(value = "/recommend")
    @ResponseBody
    public Object recommend(@RequestParam("debt") String debtStr) throws UtilException {
        Debt debt = JSON.parseObject(debtStr, Debt.class);
        this.debtService.recommend(debt);
        return new Result().Success();
    }
}
