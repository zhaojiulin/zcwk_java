package com.zcwk.core.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.zcwk.core.model.Complain;
import com.zcwk.core.service.ComplainService;
import com.zcwk.core.util.Result;

/**
 * 意见反馈
 * @author zp
 * @since 2017年2月9日
 * 
 */
@RestController
@RequestMapping({"/complain"})
public class ComplainController {
    @Autowired
    ComplainService complainService;

    /**
     * 保存
     * @param complain
     * @return
     * @throws Exception
     */
    @RequestMapping("/save")
    @ResponseBody
    public Object save(long userId ,String content) throws Exception {
    	complainService.save(userId,content);
        return new Result().Success();
    }


    /**
     * 查询-id
     * @param id
     * @return
     * @throws Exception
     */
    @RequestMapping("/get/{id}")
    @ResponseBody
    public Object findById(@PathVariable long id) throws Exception {
        return complainService.findById(id);
    }
    
    /**
     * 删除-id
     * @param id
     * @return
     * @throws Exception
     */
    @RequestMapping("/del/{id}")
    @ResponseBody
    public Object delById(@PathVariable long id) throws Exception {
        complainService.delById(id);
        return new Result().Success();
    }

    /**
     * 查询-分页
     *
     * @param pageNo
     * @param pageSize
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getList")
    @ResponseBody
    public Object getList(int pageNo, int pageSize, String param, String bTime,String eTime)
            throws Exception {
        PageInfo page = complainService.findByParam(pageNo, pageSize, param, bTime, eTime);
        return new Result().Page(page).Success();
    }
}
