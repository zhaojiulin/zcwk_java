package com.zcwk.core.web;

import com.zcwk.core.dao.RechargeMapper;
import com.zcwk.core.model.BankCard;
import com.zcwk.core.model.Recharge;
import com.zcwk.core.model.User;
import com.zcwk.core.recharge.GatWayFactory;
import com.zcwk.core.recharge.GatWayInterface;
import com.zcwk.core.service.*;
import com.zcwk.core.util.Enums;
import com.zcwk.core.util.Result;
import com.zcwk.core.util.UtilCache;
import com.zcwk.core.util.UtilException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.regex.Pattern;

/**
 * Created by pwx on 2016/9/27.
 */
@RestController
@RequestMapping({"/recharge"})
public class RechargeController {
    @Autowired
    RechargeService rechargeService;
    @Autowired
    RechargeMapper rechargeMapper;
    @Autowired
    SystemSetService systemSetService;
    @Autowired
    UtilCache cache;
    @Autowired
    UserService userService;
    @Autowired
    BankCardService bankCardService;
    @Autowired
    BaoFooService baoFooService;

    /**
     * @return
     * @throws Exception
     */
    @RequestMapping("/createPay")
    @Transactional(rollbackFor = {Exception.class})
    public Object createPay(long accountId, BigDecimal money, String bankId) throws UtilException {
        if (accountId <= 0)
            throw new UtilException("充值用户不存在");
        if (money.compareTo(BigDecimal.valueOf(0)) <= 0)
            throw new UtilException("金额不能为0");
        if (money.compareTo(BigDecimal.valueOf(99999999))> 0)
            throw new UtilException("充值金额不正确");
        try {
            boolean flag= cache.checkFrequentAction("createPay_"+accountId,2);
            if(!flag){
                throw new UtilException("请勿频繁操作");
            }
            Object gateWay = this.systemSetService.getParamVal(Enums.Params.Gateway.getKey());
            if (gateWay == null)
                throw new UtilException("充值异常");
            GatWayInterface _gatWay = GatWayFactory.create(Integer.parseInt(gateWay.toString()));
            Recharge recharge = this.rechargeService.createOrder(accountId, money, Integer.parseInt(gateWay.toString()), Enums.Route.PC.getIndex(), "在线充值");
            HashMap<String, String> map = _gatWay.createGateWay(money, bankId, recharge.getOrderNo());
            return map;
        } catch (Exception ex) {
            return new Result().Error("充值网关异常");
        }
    }

    /**
     * @return
     * @throws Exception
     */
    @RequestMapping("/createAuthPay")
    @Transactional(rollbackFor = {Exception.class})
    public Object createAuthPay(long accountId, BigDecimal money, int route) throws UtilException, UnsupportedEncodingException {
        if (accountId <= 0)
            throw new UtilException("充值用户不存在");
        if (!(Pattern.compile("^\\+?[1-9][0-9]*$").matcher(String.valueOf(money)).matches()) || money.compareTo(BigDecimal.ZERO) <= 0)
            throw new UtilException("请输入正确的金额");
        if (money.compareTo(BigDecimal.valueOf(99999999))> 0)
            throw new UtilException("充值金额不正确");
        try {
            boolean flag= cache.checkFrequentAction("createAuthPay_"+accountId,2);
            if(!flag){
                throw new UtilException("请勿频繁操作");
            }
            Object authGateWay = this.systemSetService.getParamVal(Enums.Params.AuthGateway.getKey());
            if (authGateWay == null)
                throw new UtilException("充值异常");
            GatWayInterface _gatWay = GatWayFactory.create(Integer.parseInt(authGateWay.toString()));
            Recharge recharge = this.rechargeService.createOrder(accountId, money, Integer.parseInt(authGateWay.toString()),route, "快捷支付");
            if (route == Enums.Route.PC.getIndex())
                return _gatWay.createPcAuth(accountId, route, money, recharge.getOrderNo());
            else if (route == Enums.Route.IOS.getIndex() || route == Enums.Route.Android.getIndex())
                return _gatWay.createAppAuth(accountId, route, money, recharge.getOrderNo());
            else if (route == Enums.Route.Wap.getIndex())
                return _gatWay.createWapAuth(accountId, route, money, recharge.getOrderNo());
            return new Result().Error("充值网关异常");
        } catch (Exception ex) {
            return new Result().Error("充值网关异常");
        }
    }

    @RequestMapping("/subPay")
    public Object subPay(String orderId, String code) throws Exception {
        Object authGateWay = this.systemSetService.getParamVal(Enums.Params.AuthGateway.getKey());
        if (authGateWay == null)
            throw new UtilException("充值异常");
        GatWayInterface _gatWay = GatWayFactory.create(Integer.parseInt(authGateWay.toString()));
        if (_gatWay.subPay(orderId, code))
            return new Result().Success();
        return new Result().Error("充值失败");
    }

    @RequestMapping("/getOrder")
    public Object getDebt(String orderNo) {
        return this.rechargeMapper.selectByOrderNo(orderNo);
    }

    @RequestMapping("completeOrder")
    @Transactional(rollbackFor = {Exception.class})
    public Object completeOrder(String orderNo) throws Exception {
        Recharge recharge = this.rechargeMapper.selectByOrderNo(orderNo);
        if(recharge==null){
            throw new UtilException("订单不存在.");
        }
//        Object gateWay = this.systemSetService.getParamVal(Enums.Params.Gateway.getKey());
//        if (gateWay == null)
//            throw new UtilException("网关异常");
        GatWayInterface _gatWay = GatWayFactory.create(recharge.getGatway());
        boolean check = true;
        if(recharge.getRoute()==Enums.Route.PC.getIndex() && recharge.getMemo().equals("在线充值")){
            check = _gatWay.checkGatWayOrder(orderNo);
        } else if (recharge.getRoute() == Enums.Route.PC.getIndex())
            check = _gatWay.checkPcAuthOrder(orderNo);
        else if (recharge.getRoute() == Enums.Route.Wap.getIndex() || recharge.getRoute() == Enums.Route.IOS.getIndex() || recharge.getRoute() == Enums.Route.Android.getIndex())
            check = _gatWay.checkWapAuthOrder(orderNo);
        if (check) {
            boolean result = this.rechargeService.updateOrderSuccess(orderNo, Enums.FundType.OnLineRecharge, "在线支付成功");
            return result ? new Result().Success() : new Result().Error("处理失败");
        }
        return new Result().Error("处理失败,清核查订单状态");
    }

    @RequestMapping("/cardBinQuery")
    public Object cardBinQuery(String orderNo) throws Exception {
//        Object gateWay = this.systemSetService.getParamVal(Enums.Params.AuthGateway.getKey());
//        if (gateWay == null)
//            throw new UtilException("网关异常");
        GatWayInterface _gatWay = GatWayFactory.create(Enums.GatWay.FY.getIndex());
        return new Result().Add("data", _gatWay.cardBinQuery(orderNo)).Success();
    }

    @RequestMapping("/directBindCard")
    public Object directBindCard(long accountId) throws Exception{
        String bindId= baoFooService.directBindCard(accountId);
        return new Result().Add("bindId", bindId).Success();
    }

    //宝付预充值
    @RequestMapping("/preCretifyPay")
    public Object preCretifyPay(long accountId,BigDecimal money,int route) throws Exception{
        HashMap map= baoFooService.preCretifyPay(accountId, money, route);
        return new Result().Add("businessNo", map.get("businessNo")).Add("orderNo", map.get("orderNo")).Add("amount", map.get("amount")).Add("bankCard",map.get("bankCard")).Success();
    }

    //宝付确认充值
    @RequestMapping("/certainCretifyPay")
    public Object certainCretifyPay(long accountId,String businessNo,String orderNo,String smsCode) throws Exception{
        String business= baoFooService.certainCretifyPay(accountId,businessNo,orderNo,smsCode);
        return new Result().Add("businessNo", business).Success();
    }

}
