package com.zcwk.core.web;

import com.github.pagehelper.PageInfo;
import com.zcwk.core.model.Product;
import com.zcwk.core.service.ProductService;
import com.zcwk.core.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

/**
 * Created by pwx on 2016/10/10.
 */
@RestController
@RequestMapping({"/product"})
public class ProductController {

    @Autowired
    ProductService productService;

    /**
     * 查询产品信息
     *
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getAll")
    @ResponseBody
    public Object getAll(String state)
            throws Exception {
        return this.productService.getAll(state);
    }

    /**
     * 保存产品信息
     *
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save")
    @ResponseBody
    public Object save(Product product)
            throws Exception {
        this.productService.save(product);
        return new Result().Success();
    }

    /**
     * 根据ID查询产品信息
     *
     * @return
     * @throws Exception
     */
    @RequestMapping("/get/{id}")
    @ResponseBody
    public Object get(@PathVariable int id)
            throws Exception {
        return this.productService.get(id);
    }

}
