package com.zcwk.core.web;

import com.github.pagehelper.PageInfo;
import com.zcwk.core.model.Ads;
import com.zcwk.core.service.AdsService;
import com.zcwk.core.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by pwx on 2016/7/22.
 */
@RestController
@RequestMapping({"/ads"})
public class AdsController {
    @Autowired
    AdsService adsService;

    /**
     * @param ads
     * @return
     * @throws Exception
     */
    @RequestMapping("/save")
    @ResponseBody
    public Object save(Ads ads) throws Exception {
        this.adsService.save(ads);
        return new Result().Success();
    }


    /**
     * @param id
     * @return
     * @throws Exception
     */
    @RequestMapping("/get/{id}")
    @ResponseBody
    public Object getAds(@PathVariable long id) throws Exception {
        return this.adsService.get(id);
    }

    /**
     * 查询合作伙伴含分页
     *
     * @param pageNo
     * @param pageSize
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getList")
    @ResponseBody
    public Object getPartnerList(int pageNo, int pageSize, String state, String showType,String itemType) throws Exception {
        PageInfo page = this.adsService.selectAll(pageNo, pageSize, state, showType,itemType);
        return new Result().Page(page).Success();
    }
}
