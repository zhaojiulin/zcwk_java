package com.zcwk.core.web;

import com.zcwk.core.recharge.GatWayFactory;
import com.zcwk.core.recharge.GatWayInterface;
import com.zcwk.core.util.CommonUtil;
import com.zcwk.core.util.Enums;
import com.zcwk.core.withdraw.GateWayFactory;
import com.zcwk.core.withdraw.GateWayInterface;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.zcwk.core.withdraw.GateWayFactory;
import com.zcwk.core.withdraw.GateWayInterface;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by pwx on 2016/9/27.
 */
@RestController
@RequestMapping({"/call"})
public class CallController {

    /** 宝付 **/

    /**
     * @return
     * @throws Exception
     */
    @RequestMapping("/bfGateWay")
    public Object bfGateWay(HttpServletRequest request) throws Exception {
        GatWayInterface gatWay = GatWayFactory.create(Enums.GatWay.BF.getIndex());
        if (gatWay.callGateWay(CommonUtil.parseRequestMap(request))) {
            return "OK";
        }
        return "";
    }

    @RequestMapping("/bfPcAuth")
    public Object bfPcAuth(HttpServletRequest request) throws Exception {
        GatWayInterface gatWay = GatWayFactory.create(Enums.GatWay.BF.getIndex());
        if (gatWay.callPcAuth(CommonUtil.parseRequestMap(request))) {
            return "OK";
        }
        return "";
    }

    @RequestMapping("/bfAppAuth")
    public Object bfAppAuth(HttpServletRequest request) throws Exception {
        GatWayInterface gatWay = GatWayFactory.create(Enums.GatWay.BF.getIndex());
        if (gatWay.callAppAuth(CommonUtil.parseRequestMap(request))) {
            return "OK";
        }
        return "";
    }

    @RequestMapping("/bfWapAuth")
    public Object bfWapAuth(HttpServletRequest request) throws Exception {
        GatWayInterface gatWay = GatWayFactory.create(Enums.GatWay.BF.getIndex());
        if (gatWay.callWapAuth(CommonUtil.parseRequestMap(request))) {
            return "OK";
        }
        return "";
    }

    @RequestMapping("/bfTP")
    public Object bfTP(HttpServletRequest request) throws Exception {
        GateWayInterface gateWay = GateWayFactory.create(Enums.GatWay.BF.getIndex());
        if (gateWay.payTP(CommonUtil.parseRequestMap(request))) {
            return "OK";
        }
        return "";
    }

    @RequestMapping("/bfPay")
    public Object bfPay(HttpServletRequest request) throws Exception {
        GateWayInterface gateWay = GateWayFactory.create(Enums.GatWay.BF.getIndex());
        if (gateWay.callPay(CommonUtil.parseRequestMap(request))) {
            return "OK";
        }
        return "";
    }


    /** 汇潮支付 **/

    /**
     * @return
     * @throws Exception
     */
    @RequestMapping("/hcGateWay")
    public Object hcGateWay(HttpServletRequest request) throws Exception {
        GatWayInterface gatWay = GatWayFactory.create(Enums.GatWay.HC.getIndex());
        if (gatWay.callGateWay(CommonUtil.parseRequestMap(request))) {
            return "ok";
        }
        return "";
    }

    @RequestMapping("/hcPcAuth")
    public Object hcPcAuth(HttpServletRequest request) throws Exception {
        GatWayInterface gatWay = GatWayFactory.create(Enums.GatWay.HC.getIndex());
        if (gatWay.callPcAuth(CommonUtil.parseRequestMap(request))) {
            return "ok";
        }
        return "";
    }

    @RequestMapping("/hcAppAuth")
    public Object hcAppAuth(HttpServletRequest request) throws Exception {
        GatWayInterface gatWay = GatWayFactory.create(Enums.GatWay.HC.getIndex());
        if (gatWay.callAppAuth(CommonUtil.parseRequestMap(request))) {
            return "ok";
        }
        return "";
    }

    @RequestMapping("/hcWapAuth")
    public Object hcWapAuth(HttpServletRequest request) throws Exception {
        GatWayInterface gatWay = GatWayFactory.create(Enums.GatWay.HC.getIndex());
        if (gatWay.callWapAuth(CommonUtil.parseRequestMap(request))) {
            return "ok";
        }
        return "";
    }


    /** 富友 **/

    /**
     * @return
     * @throws Exception
     */
    @RequestMapping("/fyGateWay")
    public Object fyGateWay(HttpServletRequest request) throws Exception {
        GatWayInterface gatWay = GatWayFactory.create(Enums.GatWay.FY.getIndex());
        if (gatWay.callGateWay(CommonUtil.parseRequestMap(request))) {
            return "ok";
        }
        return "";
    }

    @RequestMapping("/fyPcAuth")
    public Object fyPcAuth(HttpServletRequest request) throws Exception {
        GatWayInterface gatWay = GatWayFactory.create(Enums.GatWay.FY.getIndex());
        if (gatWay.callPcAuth(CommonUtil.parseRequestMap(request))) {
            return "ok";
        }
        return "";
    }

    @RequestMapping("/fyAppAuth")
    public Object fyAppAuth(HttpServletRequest request) throws Exception {
        GatWayInterface gatWay = GatWayFactory.create(Enums.GatWay.FY.getIndex());
        if (gatWay.callAppAuth(CommonUtil.parseRequestMap(request))) {
            return "ok";
        }
        return "";
    }

    @RequestMapping("/fyWapAuth")
    public Object fyWapAuth(HttpServletRequest request) throws Exception {
        GatWayInterface gatWay = GatWayFactory.create(Enums.GatWay.FY.getIndex());
        if (gatWay.callWapAuth(CommonUtil.parseRequestMap(request))) {
            return "ok";
        }
        return "";
    }

    @RequestMapping("/fyTP")
    public Object fyTP(HttpServletRequest request) throws Exception {
        GateWayInterface gateWay = GateWayFactory.create(Enums.GatWay.FY.getIndex());
        if (gateWay.payTP(CommonUtil.parseRequestMap(request))) {
            return "ok";
        }
        return "";
    }

    @RequestMapping("/fyPay")
    public Object fyPay(HttpServletRequest request) throws Exception {
        GateWayInterface gateWay = GateWayFactory.create(Enums.GatWay.FY.getIndex());
        if (gateWay.callPay(CommonUtil.parseRequestMap(request))) {
            return "ok";
        }
        return "";
    }

}
