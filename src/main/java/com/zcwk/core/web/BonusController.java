package com.zcwk.core.web;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageInfo;
import com.zcwk.core.model.BonusRecord;
import com.zcwk.core.model.BonusSet;
import com.zcwk.core.service.BonusService;
import com.zcwk.core.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

/**
 * Created by pwx on 2016/7/27.
 */
@RestController
@RequestMapping({"/bonus"})
public class BonusController {
    @Autowired
    BonusService bonusService;

    /**
     * @param bonusSet
     * @return
     * @throws Exception
     */
    @RequestMapping("/save")
    @ResponseBody
    public Object save(BonusSet bonusSet) throws Exception {
        this.bonusService.saveSet(bonusSet);
        return new Result().Success();
    }

    /**
     * 查询红包设置列表
     *
     * @param pageNo
     * @param pageSize
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getBonusSetList")
    @ResponseBody
    public Object getBonusSetList(int pageNo, int pageSize, String bTime, String eTime, String state)
            throws Exception {
        PageInfo page = this.bonusService.getBonusSetByParam(pageNo, pageSize, bTime, eTime, state);
        return new Result().Page(page).Success();
    }

    /**
     * 查询红包列表
     *
     * @param pageNo
     * @param pageSize
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getBonusRecordList")
    @ResponseBody
    public Object getBonusRecordList(int pageNo, int pageSize, String accountId, boolean isExpiry, String productType, String periodUnit, String period, String state, String param, String bTime, String eTime, String type,String buyAmount) throws Exception {
        BigDecimal buyAmountTemp= BigDecimal.ZERO;
        try {
            if(buyAmount==null)
                buyAmount="";
            buyAmountTemp =new BigDecimal(buyAmount);
        } catch (NumberFormatException e) {
            buyAmountTemp= BigDecimal.ZERO;
        }
        PageInfo page = this.bonusService.getBonusRecordByParam(pageNo, pageSize, accountId, isExpiry, productType, periodUnit, period, state, param, bTime, eTime, type,buyAmountTemp);
        return new Result().Page(page).Success();
    }

    /**
     * @param id
     * @return
     * @throws Exception
     */
    @RequestMapping("/getBonusSet/{id}")
    @ResponseBody
    public Object getAds(@PathVariable int id) throws Exception {
        return this.bonusService.getBonusSetById(id);
    }


    /**
     * 保存红包设置
     *
     * @return
     * @throws Exception
     */
    @RequestMapping("/saveBonusSet")
    @ResponseBody
    public Object saveBonusSet(@RequestParam("bonusSet") String bonusSetStr) throws Exception {
        BonusSet bonusSet = JSON.parseObject(bonusSetStr, BonusSet.class);
        this.bonusService.saveSet(bonusSet);
        return new Result().Success();
    }


    /**
     * 发放红包
     *
     * @return
     * @throws Exception
     */
    @RequestMapping("/sendBonus")
    @ResponseBody
    public Object sendBonus(int bonusId, String mobile) throws Exception {
        this.bonusService.sendBonus(bonusId, mobile);
        return new Result().Success();
    }
}
