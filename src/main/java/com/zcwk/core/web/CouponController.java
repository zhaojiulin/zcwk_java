package com.zcwk.core.web;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageInfo;
import com.zcwk.core.model.CouponSet;
import com.zcwk.core.service.CouponService;
import com.zcwk.core.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

/**
 * Created by pwx on 2016/12/6.
 */
@RestController
@RequestMapping({"/coupon"})
public class CouponController {
    @Autowired
    CouponService couponService;

    /**
     * @param couponSet
     * @return
     * @throws Exception
     */
    @RequestMapping("/save")
    @ResponseBody
    public Object save(CouponSet couponSet) throws Exception {
        this.couponService.saveSet(couponSet);
        return new Result().Success();
    }

    /**
     * 查询加息券设置列表
     *
     * @param pageNo
     * @param pageSize
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getCouponSetList")
    @ResponseBody
    public Object getCouponSetList(int pageNo, int pageSize, String bTime, String eTime, String state)
            throws Exception {
        PageInfo page = this.couponService.getCouponSetByParam(pageNo, pageSize, bTime, eTime, state);
        return new Result().Page(page).Success();
    }

    /**
     * 查询加息券列表
     *
     * @param pageNo
     * @param pageSize
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getCouponRecordList")
    @ResponseBody
    public Object getCouponRecordList(int pageNo, int pageSize, String accountId, boolean isExpiry, String productType, String periodUnit, String period, String state, String param, String bTime, String eTime,String buyAmount) throws Exception {
        BigDecimal buyAmountTemp= BigDecimal.ZERO;
        try {
            if(buyAmount==null)
                buyAmount="";
            buyAmountTemp =new BigDecimal(buyAmount);
        } catch (NumberFormatException e) {
            buyAmountTemp= BigDecimal.ZERO;
        }
        PageInfo page = this.couponService.getCouponRecordByParam(pageNo, pageSize, accountId, isExpiry, productType, periodUnit, period, state, param, bTime, eTime,buyAmountTemp);
        return new Result().Page(page).Success();
    }

    /**
     * @param id
     * @return
     * @throws Exception
     */
    @RequestMapping("/getCouponSet/{id}")
    @ResponseBody
    public Object getAds(@PathVariable int id) throws Exception {
        return this.couponService.getCouponSetById(id);
    }


    /**
     * 保存加息券设置
     *
     * @return
     * @throws Exception
     */
    @RequestMapping("/saveCouponSet")
    @ResponseBody
    public Object saveCouponSet(@RequestParam("couponSet") String couponSetStr) throws Exception {
        CouponSet couponSet = JSON.parseObject(couponSetStr, CouponSet.class);
        this.couponService.saveSet(couponSet);
        return new Result().Success();
    }


    /**
     * 发放加息券
     *
     * @return
     * @throws Exception
     */
    @RequestMapping("/sendCoupon")
    @ResponseBody
    public Object sendBonus(int couponId, String mobile) throws Exception {
        this.couponService.sendCoupon(couponId, mobile);
        return new Result().Success();
    }
}
