package com.zcwk.core.web;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageInfo;
import com.zcwk.core.model.Activity;
import com.zcwk.core.service.ActivityService;
import com.zcwk.core.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by pwx on 2016/10/28.
 */
@RestController
@RequestMapping({"/activity"})
public class ActivityController {
    @Autowired
    ActivityService activityService;

    /**
     * @param activityStr
     * @return
     * @throws Exception
     */
    @RequestMapping("/save")
    @ResponseBody
    public Object save(@RequestParam("activity") String activityStr) throws Exception {
        Activity activity = JSON.parseObject(activityStr, Activity.class);
        this.activityService.save(activity);
        return new Result().Success();
    }


    /**
     * @param id
     * @return
     * @throws Exception
     */
    @RequestMapping("/get/{id}")
    @ResponseBody
    public Object getActivity(@PathVariable long id) throws Exception {
        return this.activityService.getActivityById(id);
    }

    /**
     * 查询活动列表
     *
     * @param pageNo
     * @param pageSize
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getList")
    @ResponseBody
    public Object getList(int pageNo, int pageSize, String bTime, String eTime, String state, String showType) throws Exception {
        PageInfo page = this.activityService.getActivityByParam(pageNo, pageSize, bTime, eTime, state, showType);
        return new Result().Page(page).Success();
    }
}
