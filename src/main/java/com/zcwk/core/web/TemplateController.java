package com.zcwk.core.web;

import com.github.pagehelper.PageInfo;
import com.zcwk.core.model.Template;
import com.zcwk.core.service.TemplateService;
import com.zcwk.core.util.Result;
import com.zcwk.core.util.UtilException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by pwx on 2016/9/30.
 */
@RestController
@RequestMapping({"/template"})
public class TemplateController {
    @Autowired
    TemplateService templateService;

    @RequestMapping("/save")
    @ResponseBody
    public Object save(Template template) throws UtilException {
        this.templateService.save(template);
        return new Result().Success();
    }

    @RequestMapping("/getAll")
    @ResponseBody
    public Object getAll(int pageSize, int pageNo, String type, String action, String state) {
        PageInfo page = this.templateService.getAll(pageNo, pageSize, type, action, state);
        return new Result().Page(page).Success();
    }

    @RequestMapping("/get")
    @ResponseBody
    public Object get(int id) {
        return this.templateService.get(id);
    }

}
