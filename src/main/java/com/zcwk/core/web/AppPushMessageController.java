package com.zcwk.core.web;

import com.alibaba.fastjson.JSON;
import com.zcwk.core.model.AppUpload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.zcwk.core.model.AppClient;
import com.zcwk.core.service.AppPushMessageService;
import com.zcwk.core.util.Result;
import com.zcwk.core.view.AppPushMessageView;

/**
 * 个推记录
 * @author zp
 * @since 2017年2月9日
 * 
 */
@RestController
@RequestMapping({"/appPushMessage"})
public class AppPushMessageController {
    @Autowired
    AppPushMessageService appPushMessageService;
    

    /**
     * 保存app标识
     * @param
     * @return
     * @throws Exception
     */
    @RequestMapping("/saveAppClient")
    @ResponseBody
    public Object saveAppClient(String clientId,Long userId,int route) throws Exception {
    	appPushMessageService.saveAppClient(clientId,userId,route);
        return new Result().Success();
    }

    /**搜索编号（1488356026）
     * 保存app
     * @param
     * @return
     * @throws Exception
     */
    @RequestMapping("/saveApp")
    @ResponseBody
    public Object saveApp(@RequestParam("appUpload") String uploadStr) throws Exception {
        AppUpload appUpload = JSON.parseObject(uploadStr, AppUpload.class);
        return appPushMessageService.saveApp(appUpload);
    }


    /**
     * app
     * @param
     * @return
     * @throws Exception
     */
    @RequestMapping("/getAppList")
    @ResponseBody
    public Object getAppList(int pageNo, int pageSize) throws Exception {
        PageInfo page=appPushMessageService.AllAppList(pageNo, pageSize);
        return new Result().Page(page).Success();
    }

    /**
     * 个推推送
     * @param entity
     * @return
     * @throws Exception
     */
    @RequestMapping("/appPushMessage")
    @ResponseBody
    public Object appPushMessage(AppPushMessageView entity) throws Exception {
        return appPushMessageService.appPushMessage(entity);
    }

    /**
     * 查询个推记录-分页
     *
     * @param pageNo
     * @param pageSize
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getList")
    @ResponseBody
    public Object getList(int pageNo, int pageSize, int status, String bTime,String eTime)
            throws Exception {
        PageInfo page = appPushMessageService.findByParam(pageNo, pageSize, status, bTime, eTime);
        return new Result().Page(page).Success();
    }


    /**
     * 查询最新app
     *
     * @return
     * @throws Exception
     *
     */
    @RequestMapping(value = "/getNewestApp")
    @ResponseBody
    public Object getNewestApp() throws Exception {
        return this.appPushMessageService.getNewestApp();
    }

}
