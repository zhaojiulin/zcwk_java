package com.zcwk.core.web;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageInfo;
import com.zcwk.core.model.Channel;
import com.zcwk.core.service.ChannelService;
import com.zcwk.core.service.UserService;
import com.zcwk.core.util.Result;
import com.zcwk.core.util.UtilException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2017/3/15.
 */
@RestController
@RequestMapping({"/channel"})
public class ChannelController {
    @Autowired
    UserService userService;
    @Autowired
    ChannelService channelService;

    @RequestMapping(value = "/channelct",method = RequestMethod.POST)
    @ResponseBody
    public Object chanelct(String sTime, String bTime,String eTime, long channelTy) throws Exception {
        Map map = new HashMap<>();
        map.put ("list",this.channelService.selectChannel(sTime, bTime,eTime,channelTy));
        return map;
    }

    //搜索编号：1489626435
    //保存渠道
    @RequestMapping(value = "/saveChannel")
    @ResponseBody
    public Object savaChannel(String channelSp)throws  Exception{
        Channel channelstr = JSON.parseObject(channelSp, Channel.class);
        return channelService.saveChannel(channelstr);
    }

    //根据code查询
    @RequestMapping(value = "/getCode")
    @ResponseBody
    public Object getCode( String code)throws  Exception{
        return channelService.getChannelByCode(code);
    }

    /**
     * 根据id查询渠道信息
     * @param id
     * @return
     * @throws UtilException
     */
    @RequestMapping("/get/{id}")
    @ResponseBody
    public Object get(@PathVariable long id) throws UtilException {
        Channel channel = this.channelService.getChannel(id);
        return channel;
    }

    /**
     * 根据条件查询列表
     * @param
     * @return
     * @throws Exception
     */
    @RequestMapping("/getSpreadList")
    @ResponseBody
    public Object getSpreadList(int pageNo, int pageSize,String bTime,String eTime) throws Exception {
        PageInfo page=channelService.SelectAllchannelList(pageNo, pageSize,bTime,eTime);
        return new Result().Page(page).Success();
    }

}
