package com.zcwk.core.web;

import com.github.pagehelper.PageInfo;
import com.zcwk.core.service.TransferService;
import com.zcwk.core.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

/**
 * Created by pwx on 2016/12/9.
 */
@RestController
@RequestMapping({"/transfer"})
public class TransferController {


    @Autowired
    TransferService transferService;

    @RequestMapping(value = "/getAll")
    @ResponseBody
    public Object getAll(String accountId, int pageNo, int pageSize) throws Exception {
        PageInfo page = this.transferService.getAll(accountId, pageNo, pageSize);
        return new Result().Page(page).Success();
    }

    @RequestMapping(value = "/getSure")
    @ResponseBody
    public Object getSure(long accountId, int pageNo, int pageSize) throws Exception {
        PageInfo page = this.transferService.getSureTransfer(accountId, pageNo, pageSize);
        return new Result().Page(page).Success();
    }

    @RequestMapping(value = "/create")
    @ResponseBody
    public Object create(long investId, BigDecimal award) throws Exception {
        boolean result = this.transferService.save(investId, award);
        if (result)
            return new Result().Success();
        return new Result().Error("转让失败");
    }

    @RequestMapping(value = "/buy")
    @ResponseBody
    public Object buy(long transferId, BigDecimal amount, long accountId, String bonusId) throws Exception {
        this.transferService.buy(transferId, amount, accountId, bonusId);
        return new Result().Success();
    }


}
