package com.zcwk.core.web;

import com.zcwk.core.model.PlatformInfo;
import com.zcwk.core.service.PlatformService;
import com.zcwk.core.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by pwx on 2016/8/2.
 */
@RestController
@RequestMapping({"/platform"})
public class PlatformController {
    @Autowired
    PlatformService platformService;

    /**
     * @param platform
     * @return
     * @throws Exception
     */
    @RequestMapping("/save")
    @ResponseBody
    public Object save(PlatformInfo platform) throws Exception {
        this.platformService.save(platform);
        return new Result().Success();
    }

    /**
     * @return
     * @throws Exception
     */
    @RequestMapping("/get")
    @ResponseBody
    public Object get() {
        return this.platformService.get();
    }
}
