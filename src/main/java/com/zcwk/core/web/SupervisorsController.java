/**
 *
 */
package com.zcwk.core.web;

import java.util.HashMap;
import java.util.List;

import com.zcwk.core.model.Rights;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.github.pagehelper.PageInfo;
import com.zcwk.core.model.Supervisors;
import com.zcwk.core.service.SupervisorsService;
import com.zcwk.core.util.Result;

/**
 * @author dengqun
 *         <p/>
 *         上午11:18:50
 */

@RestController
@RequestMapping({"/supervisors"})
public class SupervisorsController {
    @Autowired
    SupervisorsService supervisorsService;

    /**
     * 后台管理员添加
     *
     * @return
     * @throws Exception
     */
    @RequestMapping("/save")
    @ResponseBody
    public Object save(Supervisors supervisors) throws Exception {
        Result result = new Result();
        this.supervisorsService.save(supervisors);
        return result.Success();
    }

    /**
     * @param id
     * @return
     * @throws Exception
     */
    @RequestMapping("/get/{id}")
    @ResponseBody
    public Object getSupervisors(@PathVariable long id) throws Exception {
        Result result = new Result();
        Supervisors supervisors = supervisorsService.selectSupervisorsById(id);
        return supervisors;
    }

    /**
     * 管理员列表查询含分页
     *
     * @param pageNo
     * @param pageSize
     * @param supervisors
     * @return
     * @throws Exception
     */
    @RequestMapping("/getList")
    @ResponseBody
    public Object getList(int pageNo, int pageSize, Supervisors supervisors) throws Exception {
        PageInfo page = this.supervisorsService.selectSupervisorsPage(pageNo, pageSize, supervisors);
        return new Result().Page(page).Success();
    }

    /**
     * 后台登录
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/login")
    @ResponseBody
    public Object login(String name,String password,String googleCode) throws Exception {
        Result result = new Result();
        Supervisors u = this.supervisorsService.login(name,password,googleCode);
        return u;
    }

    /**
     * 获取树形权限列表
     *
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getRightsList")
    @ResponseBody
    public Object getRightsList(long roleId) throws Exception {
        List<Rights> list = this.supervisorsService.getRightsList(roleId);
        return list;
    }
    
    /**
     * 生成googleKey
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/genSecret")
    @ResponseBody
    public Object genSecret(long id,String name) throws Exception {
        return supervisorsService.genSecret(id,name);
    }
}
