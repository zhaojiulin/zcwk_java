package com.zcwk.core.web;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zcwk.core.model.InviteSet;
import com.zcwk.core.service.InviteService;
import com.zcwk.core.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

/**
 * Created by pwx on 2016/8/24.
 */
@RestController
@RequestMapping({"/invite"})
public class InviteController {
    @Autowired
    InviteService inviteService;

    /**
     * @return
     * @throws Exception
     */
    @RequestMapping("/saveInviteSet")
    @ResponseBody
    public Object saveInviteSet(InviteSet set) throws Exception {
        this.inviteService.saveSet(set);
        return new Result().Success();
    }

    /**
     * @return
     * @throws Exception
     */
    @RequestMapping("/getInviteSet")
    @ResponseBody
    public Object getInviteSet() {
        return this.inviteService.getSet();
    }

    @RequestMapping(value = "/getAllInviteAward")
    @ResponseBody
    public Object getAllInviteAward(int pageNo, int pageSize, String param, String bTime, String eTime, String inviteId) {
        PageInfo page = this.inviteService.getAllInviteAward(pageNo, pageSize, param, bTime, eTime, inviteId);
        return new Result().Page(page).Success();

    }

    @RequestMapping(value = "/getAllInviteAwardSum")
    @ResponseBody
    public Object getAllInviteAward(String bTime, String eTime, String inviteId) {
        BigDecimal totalAmount = this.inviteService.getSum(bTime, eTime, inviteId);
        return totalAmount;
    }
}
