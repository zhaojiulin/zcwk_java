package com.zcwk.core.web;

import com.github.pagehelper.PageInfo;
import com.zcwk.core.model.IntegralRule;
import com.zcwk.core.service.IntegralService;
import com.zcwk.core.util.Result;
import com.zcwk.core.util.UtilException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by pwx on 2016/8/23.
 */
@RestController
@RequestMapping({"/integral"})
public class IntegralController {
    @Autowired
    IntegralService integralService;

    @RequestMapping(value = "/getAllRecord")
    @ResponseBody
    public Object getAuditList(int pageNo, int pageSize, String param, String bTime, String eTime, String userId, String action) {
        PageInfo page = this.integralService.getIntegralRecordByParam(pageNo, pageSize, param, bTime, eTime, userId, action);
        return new Result().Page(page).Success();
    }

    @RequestMapping(value = "/getAllRule")
    @ResponseBody
    public Object getAuditList(int pageNo, int pageSize, String action, String state) {
        PageInfo page = this.integralService.getIntegralRuleByParam(pageNo, pageSize, action, state);
        return new Result().Page(page).Success();
    }

    @RequestMapping("/getRule/{id}")
    @ResponseBody
    public Object getDebt(@PathVariable long id) throws UtilException {
        IntegralRule rule = this.integralService.getRule(id);
        return rule;
    }

    @RequestMapping("/saveRule")
    @ResponseBody
    public Object getDebt(IntegralRule rule) throws UtilException {
        this.integralService.saveRule(rule);
        return new Result().Success();
    }
}
