package com.zcwk.core.web;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Date;

import com.lowagie.text.DocumentException;
import com.zcwk.core.dao.UserMapper;
import com.zcwk.core.model.SystemSet;
import com.zcwk.core.service.CommonService;
import com.zcwk.core.service.SystemSetService;
import com.zcwk.core.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zcwk.core.dao.AccountMapper;
import com.zcwk.core.model.Account;
import com.zcwk.core.model.Debt;
import com.zcwk.core.model.User;
import com.zcwk.core.service.AccountService;
import com.zcwk.core.service.DebtService;

@RestController
@RequestMapping({"/test"})
public class TestController {

    @Autowired
    UtilCache cache;

//    @Autowired
//    AccountService accountService;
//
//    @Autowired
//    AccountMapper accountMapper;
//    @Autowired
//    DebtService debtService;
//
//    @Autowired
//    UserMapper userMapper;
//    @Autowired
//    SystemSetController systemSetService;

//    @RequestMapping("/saveAccount/{id}")
//    public Object saveUser(@PathVariable long id) throws Exception {
//        User u = new User();
//        u.setId(id);
//        Account account = accountService.save(u);
//        if (account != null) {
//            return new Result().Add("data", account).Success();
//        }
//        return new Result().Error("此账户不存在");
//    }
//
//    @RequestMapping("/getaccount/{id}")
//    public Object getAccount(@PathVariable long id) throws Exception {
//        BigDecimal amount = BigDecimal.valueOf(80);
//        BigDecimal number = BigDecimal.valueOf(0.04);
//        amount.multiply(number).setScale(0);
//        Account account = accountService.getAccount(id);
//        return account;
//    }
//
//    @RequestMapping("/getdebt/{id}")
//    public Object getDebt(@PathVariable long id) throws Exception {
//        Debt debt = debtService.getDebt(id);
//        return debt;
//    }
//
//    @RequestMapping("/buy/{id}/{cid}")
//    public Object buy(@PathVariable long id, @PathVariable long cid) throws Exception {
//        // Account account=new Account();
//        // account.setId((long)1000007);
//        // account.setBalance(BigDecimal.valueOf(10000000));
//        // accountService.updateAccount(account);
//
//        // account=accountService.getAccount(account.getId());
//        // account.setSign(DataSafety.encryptAccount(account));
//        // accountMapper.updateByPrimaryKeySelective(account);
//        long investId = debtService.buy(id, BigDecimal.valueOf(1000), cid,
//                false, "", "",0);
//        return investId;
//    }
//
//    @Autowired
//    SystemSetService systemSetService;
//
//    @RequestMapping("/bj")
//    public void bj() throws UtilException {
//        BigDecimal amount = BigDecimal.valueOf(10000);
//        double monthRate1 = 0.128 / 12;
//        int month1 = 12;
//        BigDecimal c = BigDecimal.valueOf(0);
//        BigDecimal k = BigDecimal.ZERO;
//        BigDecimal r = BigDecimal.ZERO;
//        for (int i = 1; i < month1 + 1; i++) {
//            k = ((amount.subtract(c)).multiply(BigDecimal.valueOf(monthRate1))).setScale(2, BigDecimal.ROUND_HALF_UP);
//            r = (i == month1 ? amount.subtract(c) : amount.divide(BigDecimal.valueOf(month1), 2, BigDecimal.ROUND_HALF_UP));
//            c = c.add(amount.divide(BigDecimal.valueOf(month1), 2, BigDecimal.ROUND_HALF_UP));
//
//            System.out.println("本金：" + r + "，利息：" + k);
//        }
//    }
//
//
//    public void lx() {
//        BigDecimal invest = new BigDecimal(23000); // 本金
//        double yearRate = 0.12; // 年利率
//        int year = 1;//期限
//        double monthRate = yearRate / 12;
//        int month = year * 12;
//        System.out.println("---------------------------------------------------");
//        // 每月利息  = 剩余本金 x 贷款月利率
//        BigDecimal monthIncome = invest.multiply(new BigDecimal(monthRate * Math.pow(1 +
//                monthRate, month))).divide(new BigDecimal(Math.pow(1 + monthRate, month) - 1), 2,
//                BigDecimal.ROUND_HALF_UP);
//        BigDecimal monthInterest;
//        BigDecimal capital = invest;
//        BigDecimal tmpCapital = BigDecimal.ZERO;
//        BigDecimal sumInterest = BigDecimal.ZERO;
//        BigDecimal sumCapital = BigDecimal.ZERO;
//        for (int i = 1; i < month + 1; i++) {
//            capital = capital.subtract(tmpCapital);
//            monthInterest = capital.multiply(new BigDecimal(monthRate)).setScale(2, BigDecimal
//                    .ROUND_HALF_UP);
//            tmpCapital = (i == month ? capital : monthIncome.subtract(monthInterest));
//            //if(i==month) monthIncome
//            System.out.println("第" + i + "月利息： " + monthInterest + "|" + tmpCapital + "|" + capital);
//            sumCapital = sumCapital.add(tmpCapital);
//            sumInterest = sumInterest.add(monthInterest);
//        }
//        System.out.println("本金总和：" + sumCapital + " 利息总和：" + sumInterest);
//
//
//        BigDecimal corpus = invest.divide(new BigDecimal(12), 2, BigDecimal.ROUND_HALF_UP);
//        BigDecimal monthRate1 = BigDecimal.valueOf(0.12).divide(BigDecimal.valueOf(12), 2, BigDecimal.ROUND_HALF_UP);
//        BigDecimal interest = BigDecimal.valueOf(0);
//        HashMap<Date, Object[]> map = new HashMap<Date, Object[]>();
//        Date preDate = new Date();
//        Date currDate = null;
//        for (int i = 1; i <= 12; i++) {
//            interest = ((invest.subtract(corpus.multiply(BigDecimal.valueOf(i - 1)))).multiply(monthRate1)).setScale(2, BigDecimal.ROUND_HALF_UP);
//            currDate = CommonUtil.addMonth(preDate, 1);
//            map.put(currDate, new Object[]{corpus, interest, CommonUtil.compareDay(preDate, currDate), i});
//            preDate = currDate;
//            System.out.println("第" + i + "月利息： " + corpus + "|" + interest);
//        }
//    }
//
//    @Autowired
//    CommonService commonService;
//    @Autowired
//    PdfUtil pdfUtil;
//    @Autowired
//    UtilCache cache;
//
//    @RequestMapping("/bf")
//    public void bf() throws UtilException, IOException, DocumentException {
//        Thread current = Thread.currentThread();
//        System.out.println("线程ID:" + current.getId());
//        Date dt = new Date();
//        System.out.println(CommonUtil.getFormatTime(dt, "yyyy-MM-dd hh:mm:ss"));
//        String protocol = this.cache.getProtocol(Enums.ProtocolType.Debt.getIndex());
//        this.pdfUtil.create(String.valueOf(dt.getTime()), protocol);
//        //commonService.realName("王","612323");
//        //debtService.buy(279, BigDecimal.valueOf(100), 1000117, false, "");
//        //accountService.withdraw(1000128, 1000117, java.math.BigDecimal.valueOf(10), 20, "654321");
//        //UtilSms.sendMsg("","");
//    }
//

//    @RequestMapping("/getUserInviteCode")
//    public Object getUserInviteCode(String mobile) throws UtilException {
//        return this.createInviteCode(mobile);
//    }
//
//    private String createInviteCode(String mobile) throws UtilException{
//        Object key=this.systemSetService.getParamVal(Enums.Params.userInviteCode.getKey());
//        Map<String, String> config =null;
//        if(key!=null){
//            config=CommonUtil.parseMap(key.toString());
//        }
//        if(config!=null && config.get("useMobile").equals("1") && mobile!=null && !mobile.isEmpty()){
//            if (this.userMapper.selectUserByInviteCode(mobile) == null)
//                return mobile;
//            return createInviteCode(null);
//        }else{
//            String a = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
//            StringBuffer result = new StringBuffer();
//            for (int i = 0; i < 6; i++) {
//                int rand = (int) (Math.random() * a.length());
//                result = result.append(a.charAt(rand));
//            }
//            if (this.userMapper.selectUserByInviteCode(result.toString()) == null)
//                return result.toString();
//            return createInviteCode(null);
//        }
//    }
//   @RequestMapping("/checkFrequentAction")
//    public Object checkFrequentAction(String mobile) throws UtilException {
//        boolean flag= cache.checkFrequentAction("recharge",2);
//        System.err.println("flag="+flag);
//        return flag;
//    }

}

