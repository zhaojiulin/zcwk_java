/**
 *
 */
package com.zcwk.core.web;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.github.pagehelper.PageInfo;
import com.zcwk.core.model.News;
import com.zcwk.core.model.UserLevel;
import com.zcwk.core.service.UserLevelService;
import com.zcwk.core.util.Result;

/**
 * @author pangwenxuan
 *         <p/>
 *         下午2:51:52
 */
@RestController
@RequestMapping({"/userLevel"})
public class UserLevelController {
    @Autowired
    UserLevelService userLevelService;

    /**
     * 保存用户等级
     *
     * @param level
     * @return
     * @throws Exception
     */
    @RequestMapping("/save")
    @ResponseBody
    public Object save(UserLevel level) throws Exception {
        this.userLevelService.save(level);
        return new Result().Success();
    }


    /**
     * 根据ID获取等级信息
     *
     * @param id
     * @return
     * @throws Exception
     */
    @RequestMapping("/get/{id}")
    @ResponseBody
    public Object getLevel(@PathVariable long id) throws Exception {
        return this.userLevelService.getLevelById(id);
    }

    /**
     * 查询用户等级
     *
     * @param pageNo
     * @param pageSize
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getList")
    @ResponseBody
    public Object getList(int pageNo, int pageSize)
            throws Exception {
        PageInfo page = this.userLevelService.getAll(pageNo, pageSize);
        return new Result().Page(page).Success();
    }

}
