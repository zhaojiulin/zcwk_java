/**
 *
 */
package com.zcwk.core.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.github.pagehelper.PageInfo;
import com.zcwk.core.model.News;
import com.zcwk.core.model.Partner;
import com.zcwk.core.service.PartnerService;
import com.zcwk.core.util.Result;

/**
 * @author dengqun
 *         <p/>
 *         上午9:22:02
 */
@RestController
@RequestMapping({"/partner"})
public class PartnerController {
    @Autowired
    PartnerService partnerService;

    /**
     * @param partner
     * @return
     * @throws Exception
     */
    @RequestMapping("/save")
    @ResponseBody
    public Object save(Partner partner) throws Exception {
        partnerService.save(partner);
        return new Result().Success();
    }


    /**
     * @param id
     * @return
     * @throws Exception
     */
    @RequestMapping("/get/{id}")
    @ResponseBody
    public Object getPartner(@PathVariable long id) throws Exception {
        return this.partnerService.selectPartnerByid(id);
    }

    /**
     * 查询合作伙伴含分页
     *
     * @param pageNo
     * @param pageSize
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getList", method = RequestMethod.POST)
    @ResponseBody
    public Object getList(int pageNo, int pageSize, String state)
            throws Exception {
        PageInfo page = this.partnerService.selectAll(pageNo, pageSize, state);
        return new Result().Page(page).Success();
    }
}
