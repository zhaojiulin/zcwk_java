package com.zcwk.core.gboss;
public interface QueryValidatorServicesService extends javax.xml.rpc.Service {
    String getQueryValidatorServicesAddress();

    com.zcwk.core.gboss.QueryValidatorServices getQueryValidatorServices() throws javax.xml.rpc.ServiceException;

    com.zcwk.core.gboss.QueryValidatorServices getQueryValidatorServices(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
