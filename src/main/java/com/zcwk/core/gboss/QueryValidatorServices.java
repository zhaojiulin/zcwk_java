/**
 * QueryValidatorServices.java
 * <p/>
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.zcwk.core.gboss;

public interface QueryValidatorServices extends java.rmi.Remote {
    String querySingle(String userName_, String password_, String type_, String param_) throws java.rmi.RemoteException;

    String queryBatch(String userName_, String password_, String type_, String param_) throws java.rmi.RemoteException;
}
