package com.zcwk.core.gboss;
public class QueryValidatorServicesProxy implements com.zcwk.core.gboss.QueryValidatorServices {
    private String _endpoint = null;
    private com.zcwk.core.gboss.QueryValidatorServices queryValidatorServices = null;

    public QueryValidatorServicesProxy() {
        _initQueryValidatorServicesProxy();
    }

    public QueryValidatorServicesProxy(String endpoint) {
        _endpoint = endpoint;
        _initQueryValidatorServicesProxy();
    }

    private void _initQueryValidatorServicesProxy() {
        try {
            queryValidatorServices = (new com.zcwk.core.gboss.QueryValidatorServicesServiceLocator()).getQueryValidatorServices();
            if (queryValidatorServices != null) {
                if (_endpoint != null)
                    ((javax.xml.rpc.Stub) queryValidatorServices)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
                else
                    _endpoint = (String) ((javax.xml.rpc.Stub) queryValidatorServices)._getProperty("javax.xml.rpc.service.endpoint.address");
            }

        } catch (javax.xml.rpc.ServiceException serviceException) {
        }
    }

    public String getEndpoint() {
        return _endpoint;
    }

    public void setEndpoint(String endpoint) {
        _endpoint = endpoint;
        if (queryValidatorServices != null)
            ((javax.xml.rpc.Stub) queryValidatorServices)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);

    }

    public com.zcwk.core.gboss.QueryValidatorServices getQueryValidatorServices() {
        if (queryValidatorServices == null)
            _initQueryValidatorServicesProxy();
        return queryValidatorServices;
    }

    public String querySingle(String userName_, String password_, String type_, String param_) throws java.rmi.RemoteException {
        if (queryValidatorServices == null)
            _initQueryValidatorServicesProxy();
        return queryValidatorServices.querySingle(userName_, password_, type_, param_);
    }

    public String queryBatch(String userName_, String password_, String type_, String param_) throws java.rmi.RemoteException {
        if (queryValidatorServices == null)
            _initQueryValidatorServicesProxy();
        return queryValidatorServices.queryBatch(userName_, password_, type_, param_);
    }


}