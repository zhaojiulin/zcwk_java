package com.zcwk.core.dao;

import com.zcwk.core.model.Seo;

public interface SeoMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Seo record);

    int insertSelective(Seo record);

    Seo selectByPrimaryKey(Long id);

    Seo selectOne();

    int updateByPrimaryKeySelective(Seo record);

    int updateByPrimaryKey(Seo record);
}