package com.zcwk.core.dao;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.zcwk.core.model.BillDebt;

public interface BillDebtMapper {
    int deleteByPrimaryKey(Long id);

    int insert(BillDebt record);

    int insertSelective(BillDebt record);

    BillDebt selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(BillDebt record);

    int updateByPrimaryKey(BillDebt record);

    @Select({"select * from c_bill_debt where `group`=#{group,jdbcType=VARCHAR} and state=0"})
    @ResultMap({"BaseResultMap"})
    public List<BillDebt> selectByPayment(@Param("group") String group);

    public List<HashMap> selectPaymentAll(@Param("state") byte state, @Param("bTime") String bTime, @Param("eTime") String eTime, @Param("param") String param);

    /**
     * 根据条件获取回款本金和利息
     * @param state
     * @param bTime
     * @param eTime
     * @param param
     * @return
     */
    public List<HashMap> statPayment(@Param("state") byte state, @Param("bTime") String bTime, @Param("eTime") String eTime, @Param("param") String param);


    @Select({"select * from c_bill_debt where debt_id = #{debtId,jdbcType=BIGINT} and state=#{state,jdbcType=TINYINT}"})
    @ResultMap({"BaseResultMap"})
    public List<BillDebt> selectPaymentByState(@Param("debtId") Long debtId, @Param("state") byte state);

    @Select({"select * from c_bill_debt where transfer_id = #{transferId,jdbcType=BIGINT} and is_transfer=#{isTransfer,jdbcType=BIT}"})
    @ResultMap({"BaseResultMap"})
    public List<BillDebt> selectPaymentByTransfer(@Param("transferId") Long transferId, @Param("isTransfer") boolean isTransfer);

    @Select({"select * from c_bill_debt where state=#{state,jdbcType=TINYINT} and payment_time<now()"})
    @ResultMap({"BaseResultMap"})
    public List<BillDebt> selectOverdueDebt(@Param("state") byte state);

    public List<BillDebt> selectByInvestId(@Param("investId") long investId, @Param("state") String state);


    @Update({"update c_bill_debt set state=#{state,jdbcType=TINYINT} where debt_id = #{debtId,jdbcType=BIGINT} and state=0"})
    public void updateStateByDebtId(@Param("debtId") Long debtId, @Param("state") byte state);

    public List<HashMap> selectPaymentDays(@Param("accountId") long accountId);

    public HashMap selectPaymentByDebt(@Param("state") byte state, @Param("group") String group);

    public List<HashMap> selectPaymentDetailByGroup(@Param("state") byte state, @Param("group") String group);

    List<HashMap> selectPaymentByAccount(@Param("bTime") String bTime, @Param("eTime") String eTime, @Param("accountId") long accountId, @Param("state") String state, @Param("investId") String investId);

    @Select({"select ifnull(sum(real_payment_interest),0) from c_bill_debt where state=#{state,jdbcType=TINYINT}"})
    public BigDecimal selectSumInterest(@Param("state") byte state);

    @Select({"select ifnull(sum(real_payment_corpus+real_payment_interest+overdue_fine),0) from c_bill_debt where state=#{state,jdbcType=TINYINT}"})
    public BigDecimal selectSumpayAmount(@Param("state") byte state);

    @Update({"update c_bill_debt set state=#{state,jdbcType=TINYINT} where transfer_id = #{transferId,jdbcType=BIGINT} and is_transfer=#{isTransfer,jdbcType=BIT}"})
    public void updateStateByTransferId(@Param("transferId") Long transferId, @Param("isTransfer") boolean isTransfer, @Param("state") byte state);

    @Select({"select count(1) from c_bill_debt where borrower_id= #{borrowerId,jdbcType=BIGINT} and state=0"})
    public int selectCountByBorrowerId(@Param("borrowerId") long borrowerId);

}