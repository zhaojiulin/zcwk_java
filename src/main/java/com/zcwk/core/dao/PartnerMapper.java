package com.zcwk.core.dao;

import java.util.List;

import com.zcwk.core.model.Partner;
import org.apache.ibatis.annotations.Param;

public interface PartnerMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Partner record);

    int insertSelective(Partner record);

    Partner selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Partner record);

    int updateByPrimaryKey(Partner record);
    
    public List<Partner> selectAll(@Param("state") String state);
}