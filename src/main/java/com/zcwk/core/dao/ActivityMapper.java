package com.zcwk.core.dao;

import com.zcwk.core.model.Activity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ActivityMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Activity record);

    int insertSelective(Activity record);

    Activity selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Activity record);

    int updateByPrimaryKey(Activity record);

    public List<Activity> selectByParam(@Param("state") String state, @Param("bTime") String bTime, @Param("eTime") String eTime, @Param("showType") String showType);
}