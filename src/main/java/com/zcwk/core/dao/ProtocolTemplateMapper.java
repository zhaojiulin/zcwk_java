package com.zcwk.core.dao;

import com.zcwk.core.model.ProtocolTemplate;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ProtocolTemplateMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ProtocolTemplate record);

    int insertSelective(ProtocolTemplate record);

    ProtocolTemplate selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ProtocolTemplate record);

    int updateByPrimaryKeyWithBLOBs(ProtocolTemplate record);

    int updateByPrimaryKey(ProtocolTemplate record);

    List<ProtocolTemplate> selectByParam(@Param("type") String type);
}