package com.zcwk.core.dao;

import com.zcwk.core.model.Withdraw;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

public interface WithdrawMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Withdraw record);

    int insertSelective(Withdraw record);

    Withdraw selectByPrimaryKey(Long id);

    Withdraw selectByOrderNo(@Param("orderNo") String orderNo,@Param("gateWay") byte gateWay);

    int updateByPrimaryKeySelective(Withdraw record);

    int updateByPrimaryKey(Withdraw record);

    public BigDecimal selectSumAmount(@Param("isToday") int isToday);

    List<HashMap> selectByParam(@Param("bTime") String bTime, @Param("eTime") String eTime, @Param("auditState") String auditState, @Param("payState") String payState, @Param("param") String param, @Param("accountId") long accountId,@Param("route") Byte route, @Param("auditStartTime") String auditStartTime, @Param("auditEndTime") String auditEndTime);

    @Select({"select IFNULL(count(1),0) from c_withdraw where audit_state!=-1 and pay_state!=-1 and account_id=#{accountId,jdbcType=BIGINT} and time>=#{time,jdbcType=VARCHAR}"})
    public int selectWithdrawCountByThisMonth(@Param("accountId") long accountId,@Param("time") String time);
}