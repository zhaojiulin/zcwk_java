package com.zcwk.core.dao;

import com.zcwk.core.model.Stat;

public interface StatMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Stat record);

    int insertSelective(Stat record);

    Stat selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Stat record);

    int updateByPrimaryKey(Stat record);
}