package com.zcwk.core.dao;

import com.zcwk.core.model.User;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

public interface UserMapper {
    int deleteByPrimaryKey(Long id);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Long id);

    User selectByAccountId(Long accountId);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    int updateScore(User record);

    int updateSignInInfor(User record);

    int validEmailSuccess(@Param("userId") long userId);

    @Select({"select * from t_user where id_card=#{idCard,jdbcType=VARCHAR} limit 1"})
    @ResultMap({"BaseResultMap"})
    public User selectUserByIdCard(@Param("idCard") String idCard);

    @Select({"select * from t_user where mobile=#{mobile,jdbcType=VARCHAR}"})
    @ResultMap({"BaseResultMap"})
    public User selectUserByMobile(@Param("mobile") String mobile);

    @Select({"select * from t_user where invite_code=#{inviteCode,jdbcType=VARCHAR}"})
    @ResultMap({"BaseResultMap"})
    public User selectUserByInviteCode(@Param("inviteCode") String inviteCode);

    public List<User> selectUsers(User user);

    public List<HashMap> selectInviteUser(@Param("inviteId") String inviteId, @Param("param") String param, @Param("bTime") String bTime, @Param("eTime") String eTime);

    public List<HashMap> selectByParam(@Param("type") String type, @Param("param") String param, @Param("bTime") String bTime, @Param("eTime") String eTime,@Param("route") int route,@Param("channel") int channel,@Param("bInvestCount") int bInvestCount,@Param("eInvestCount") int eInvestCount ,@Param("invite") String invite);

    @Select({"select count(1) from t_user where type=#{type,jdbcType=TINYINT}"})
    public int selectCount(@Param("type") byte type);
    public BigDecimal selectCountId(@Param("isToday") int isToday);

    public List<HashMap> selectRecharge(@Param("sTime") String sTime, @Param("bTime") String bTime, @Param("eTime") String eTime,@Param("channelType") long channelType);
    public List<HashMap> selectTrade( @Param("sTime") String sTime,@Param("bTime") String bTime, @Param("eTime") String eTime,@Param("channelType") long channelType);
    public List<HashMap> selectWithdraw(@Param("sTime") String sTime, @Param("bTime") String bTime, @Param("eTime") String eTime,@Param("channelType") long channelType);
    public List<HashMap> selectBindCard( @Param("sTime") String sTime,@Param("bTime") String bTime, @Param("eTime") String eTime,@Param("channelType") long channelType);
    public List<HashMap> selectRegister(@Param("sTime") String sTime, @Param("bTime") String bTime, @Param("eTime") String eTime,@Param("channelType") long channelType);
    public List<HashMap> selectRealName( @Param("sTime") String sTime,@Param("bTime") String bTime, @Param("eTime") String eTime,@Param("channelType") long channelType);
    public List<HashMap> selectPayment( @Param("sTime") String sTime,@Param("channelType") long channelType);


}