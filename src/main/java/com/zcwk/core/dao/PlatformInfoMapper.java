package com.zcwk.core.dao;

import com.zcwk.core.model.PlatformInfo;

import java.util.HashMap;

public interface PlatformInfoMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(PlatformInfo record);

    int insertSelective(PlatformInfo record);

    PlatformInfo selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(PlatformInfo record);

    int updateByPrimaryKey(PlatformInfo record);

    PlatformInfo selectOne();
}