package com.zcwk.core.dao;

import com.zcwk.core.model.BankCard;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;

public interface BankCardMapper {
    int deleteByPrimaryKey(Long id);

    int insert(BankCard record);

    int insertSelective(BankCard record);

    BankCard selectByPrimaryKey(Long id);

    BankCard selectByAccount(Long accountId);

    int updateByPrimaryKeySelective(BankCard record);

    int updateByPrimaryKey(BankCard record);

    List<HashMap> selectByParam(@Param("bTime") String bTime, @Param("eTime") String eTime, @Param("param") String param, @Param("accountId") String accountId);

    @Delete({"delete from c_bankcard where account_id = #{accountId,jdbcType=BIGINT} "})
    public int deleteBankCard(@Param("accountId") Long accountId);
}