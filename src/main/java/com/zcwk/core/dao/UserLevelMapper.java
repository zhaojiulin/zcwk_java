package com.zcwk.core.dao;

import java.math.BigDecimal;
import java.util.List;

import com.zcwk.core.model.UserLevel;

/**
 * 
 * @author dengqun
 * 
 *         上午11:30:05
 */
public interface UserLevelMapper {
	int deleteByPrimaryKey(Long id);

	int insert(UserLevel record);

	int insertSelective(UserLevel record);

	UserLevel selectByPrimaryKey(Long id);

	UserLevel selectLevelByAmount(BigDecimal amount);

	int updateByPrimaryKeySelective(UserLevel record);

	int updateByPrimaryKey(UserLevel record);

	List<UserLevel> selectLevelAll();
}