package com.zcwk.core.dao;

import com.zcwk.core.model.BonusSet;
import org.apache.ibatis.annotations.Param;
import java.util.HashMap;
import java.util.List;

public interface BonusSetMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(BonusSet record);

    int insertSelective(BonusSet record);

    BonusSet selectByPrimaryKey(int id);

    int updateByPrimaryKeySelective(BonusSet record);

    int updateByPrimaryKey(BonusSet record);

    List<BonusSet> selectByParam(@Param("sendAction") String sendAction, @Param("state") String state, @Param("isExpiry") boolean isExpiry, @Param("bTime") String bTime, @Param("eTime") String eTime);
}