package com.zcwk.core.dao;

import com.zcwk.core.model.RoleRights;

import java.util.List;

public interface RoleRightsMapper {
    int deleteByPrimaryKey(Long id);

    int insert(RoleRights record);

    int insertSelective(RoleRights record);

    RoleRights selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(RoleRights record);

    int updateByPrimaryKey(RoleRights record);

    List<RoleRights> selectByRole(long roleId);
}