package com.zcwk.core.dao;

import com.zcwk.core.model.Account;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.HashMap;
import java.util.List;

public interface AccountMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Account record);

    int insertSelective(Account record);

    Account selectByPrimaryKey(Long id);

    Account selectByUserId(Long user_id);

    int updateAccount(Account record);

    int updateByPrimaryKeySelective(Account record);

    int updateByPrimaryKey(Account record);

    @Update({"update c_account set sign=#{sign,jdbcType=VARCHAR} where id = #{id,jdbcType=BIGINT}"})
    public int initSignById(@Param("id") long id,@Param("sign") String sign);

    @Select({"select * from c_account where sign is null or sign='' limit 10"})
    @ResultMap({"BaseResultMap"})
    public List<Account> selectBySign();
}