package com.zcwk.core.dao;

import com.zcwk.core.model.Channel;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.HashMap;
import java.util.List;

public interface ChannelMapper {
    int deleteByPrimaryKey(Long id);
    int insert(Channel record);

    int insertSelective(Channel record);

    Channel selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Channel record);

    int updateByPrimaryKey(Channel record);

    public List<HashMap> selectchanels();

    @Select({"select name from t_channel where id = #{channelType,jdbcType=BIGINT}"})
    public String  selectChanenl(@Param("channelType") Long channelType);

    public List<HashMap> selectAll(@Param("bTime") String bTime, @Param("eTime") String eTime);

    @Select({"select * from t_channel where code = #{code,jdbcType=VARCHAR} limit 1"})
    Channel  selectCode(@Param("code") String  code);
}