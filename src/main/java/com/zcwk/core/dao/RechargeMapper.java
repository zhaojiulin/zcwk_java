package com.zcwk.core.dao;

import com.zcwk.core.model.Recharge;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

public interface RechargeMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Recharge record);

    int insertSelective(Recharge record);

    Recharge selectByPrimaryKey(Long id);

    int updateSuccess(@Param("id") Long id, @Param("balance") BigDecimal balance);

    Recharge selectByOrderNo(@Param("orderNo") String orderNo);

    int updateByPrimaryKeySelective(Recharge record);

    int updateByPrimaryKey(Recharge record);

    public BigDecimal selectSumAmount(@Param("isToday") int isToday);

    List<HashMap> selectByParam(@Param("bTime") String bTime, @Param("eTime") String eTime, @Param("param") String param, @Param("gatWay") String gatWay, @Param("memo") String memo, @Param("bAmount") BigDecimal bAmount, @Param("eAmount") BigDecimal eAmount, @Param("accountId") long accountId,@Param("route")  Byte route, @Param("isCompleted") String isCompleted);
}