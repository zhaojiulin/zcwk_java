package com.zcwk.core.dao;

import com.zcwk.core.model.Template;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TemplateMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Template record);

    int insertSelective(Template record);

    Template selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Template record);

    int updateByPrimaryKeyWithBLOBs(Template record);

    int updateByPrimaryKey(Template record);

    public List<Template> selectByParam(@Param("action") String action,@Param("type") String type,@Param("state") String state);
}