package com.zcwk.core.dao;

import com.zcwk.core.model.News;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import java.util.HashMap;
import java.util.List;

public interface NewsMapper {
    int deleteByPrimaryKey(Long id);

    int insert(News record);

    int insertSelective(News record);

    News selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(News record);

    int updateByPrimaryKeyWithBLOBs(News record);

    int updateByPrimaryKey(News record);

    public List<News> selectByParam(@Param("type") String type, @Param("state") String state, @Param("param") String param, @Param("bTime") String bTime, @Param("eTime") String eTime);

    @Update({"update t_news set `order`=0,time=now() where id = #{id,jdbcType=BIGINT}"})
    public int recommend(@Param("id") long id);
}