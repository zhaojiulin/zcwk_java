package com.zcwk.core.dao;

import com.zcwk.core.model.Ads;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by pwx on 2016/7/18.
 */
public interface AdsMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Ads record);

    int insertSelective(Ads record);

    Ads selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Ads record);

    int updateByPrimaryKey(Ads record);

    public List<Ads> selectAll(@Param("state") String state, @Param("showType") String showType,@Param("itemType") String itemType);
}
