package com.zcwk.core.dao;

import com.zcwk.core.model.BonusRecord;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public interface BonusRecordMapper {
    int deleteByPrimaryKey(Long id);

    int insert(BonusRecord record);

    int insertSelective(BonusRecord record);

    BonusRecord selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(BonusRecord record);

    int useRecord(BonusRecord record);

    int updateByPrimaryKey(BonusRecord record);

    List<HashMap> selectByParam(@Param("accountId") String accountId, @Param("isExpiry") boolean isExpiry, @Param("productType") String productType, @Param("periodUnit") String periodUnit, @Param("period") String period, @Param("state") String state, @Param("param") String param, @Param("bTime") String bTime, @Param("eTime") String eTime, @Param("type") String type,@Param("buyAmount") BigDecimal buyAmount);

    BigDecimal selectSumAmount(@Param("ids") String ids);

    int selectCountByAccount(@Param("accountId") long accountId, @Param("bonusId") int bonusId);

}