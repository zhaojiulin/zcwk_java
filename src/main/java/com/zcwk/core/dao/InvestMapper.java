package com.zcwk.core.dao;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

import com.zcwk.core.model.BillDebt;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import com.zcwk.core.model.Invest;

public interface InvestMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Invest record);

    int insertSelective(Invest record);

    Invest selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Invest record);

    int updateByPrimaryKey(Invest record);

    public List<HashMap> selectTransfer(@Param("accountId") long accountId, @Param("transferDay") int transferDay);

    public List<Invest> selectNoProtocol();

    @Select({"select * from c_invests where debt_id = #{debtId,jdbcType=BIGINT}"})
    @ResultMap({"BaseResultMap"})
    public List<Invest> selectByDebtId(@Param("debtId") Long debtId);

    public List<HashMap> selectAll(@Param("debtId") String debtId, @Param("accountId") String accountId, @Param("beginTime") String beginTime, @Param("endTime") String endTime,@Param("route") Byte route, @Param("param") String param,@Param("channel") int channel,@Param("bAmount") int bAmount,@Param("eAmount") int eAmount,@Param("invite") String invite);

    public BigDecimal selectSumAmount(@Param("isToday") int isToday);
    public BigDecimal selectSumPsamount(@Param("isToday") int isToday);

    public List<HashMap> selectTotalByDay();

    public List<HashMap> selectSort(@Param("format") String format, @Param("time") String time, @Param("limit") int limit);

    @Select({"select IFNULL(sum(amount),0) from c_invests where state=#{state,jdbcType=TINYINT} and debt_id=#{debtId,jdbcType=BIGINT} and account_id=#{accountId,jdbcType=BIGINT}"})
    public BigDecimal selectSumByDebt(@Param("debtId") long debtId, @Param("accountId") long accountId, @Param("state") byte state);

    @Select({"select * from c_invest where transfer_id = #{transferId,jdbcType=BIGINT} and is_transfer=#{isTransfer,jdbcType=BIT}"})
    @ResultMap({"BaseResultMap"})
    public List<Invest> selectByTransfer(@Param("transferId") Long transferId,@Param("isTransfer") boolean isTransfer);

   @Select({"select * from c_invests where debt_id=#{debtId,jdbcType=BIGINT} and (state=1 or state=0) order by time desc limit 1"})
   @ResultMap({"BaseResultMap"})
   public Invest selectLatest(@Param("debtId") Long debtId);

}