package com.zcwk.core.dao;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.zcwk.core.model.Complain;

/**
 * 意见反馈
 * @author zp
 * @since 2017年2月9日
 *
 */
public interface ComplainMapper {
	
	/**
	 * 删除
	 * @param id
	 * @return
	 */
    int delById(Long id);

    /**
     * 保存
     * @param complain
     * @return
     */
    int save(Complain complain);

    /**
     * 按照id查询
     * @param id
     * @return
     */
    Complain findById(Long id);
    
    /**
     * 按照id查询
     * @param id
     * @return
     */
    Complain findByUserId(Long userId);
    
    /**
     * 按照条件查询
     * @param state
     * @param param
     * @param bTime
     * @param eTime
     * @return
     */
    public List<HashMap> findByParam(@Param("param") String param, @Param("bTime") String bTime, @Param("eTime") String eTime);
    
    
}