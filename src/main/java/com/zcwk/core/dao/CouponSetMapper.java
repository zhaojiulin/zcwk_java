package com.zcwk.core.dao;

import com.zcwk.core.model.CouponSet;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by pwx on 2016/12/6.
 */
public interface CouponSetMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(CouponSet record);

    int insertSelective(CouponSet record);

    CouponSet selectByPrimaryKey(int id);

    int updateByPrimaryKeySelective(CouponSet record);

    int updateByPrimaryKey(CouponSet record);

    List<CouponSet> selectByParam(@Param("sendAction") String sendAction, @Param("state") String state, @Param("isExpiry") boolean isExpiry, @Param("bTime") String bTime, @Param("eTime") String eTime);
}
