package com.zcwk.core.dao;

import com.zcwk.core.model.Rights;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RightsMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Rights record);

    int insertSelective(Rights record);

    Rights selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Rights record);

    int updateByPrimaryKey(Rights record);

    List<Rights> selectAllRights();
}