package com.zcwk.core.dao;

import com.zcwk.core.model.CouponRecord;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

/**
 * Created by pwx on 2016/12/6.
 */
public interface CouponRecordMapper {
    int deleteByPrimaryKey(Long id);

    int insert(CouponRecord record);

    int insertSelective(CouponRecord record);

    CouponRecord selectByPrimaryKey(Long id);

    CouponRecord selectByInvestId(Long investId);

    int updateByPrimaryKeySelective(CouponRecord record);

    int useRecord(CouponRecord record);

    int updateByPrimaryKey(CouponRecord record);

    List<HashMap> selectByParam(@Param("accountId") String accountId, @Param("isExpiry") boolean isExpiry, @Param("productType") String productType, @Param("periodUnit") String periodUnit, @Param("period") String period, @Param("state") String state, @Param("param") String param, @Param("bTime") String bTime, @Param("eTime") String eTime, @Param("buyAmount") BigDecimal buyAmount);

    BigDecimal selectSumAmount(@Param("ids") String ids);

    int selectCountByAccount(@Param("accountId") long accountId, @Param("couponId") int bonusId);
}
