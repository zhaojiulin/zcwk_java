package com.zcwk.core.dao;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;

import com.zcwk.core.model.Freeze;
import com.zcwk.core.model.Invest;
import org.apache.ibatis.annotations.Update;

public interface FreezeMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Freeze record);

    int insertSelective(Freeze record);

    Freeze selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Freeze record);

    int updateByPrimaryKey(Freeze record);

    @Update({"update c_freeze set state=#{state,jdbcType=TINYINT} where relation_id = #{relationId,jdbcType=BIGINT} and state=0 and type= #{type,jdbcType=TINYINT}"})
    public int updateByRelationId(@Param("type") byte type, @Param("relationId") long relationId, @Param("state") byte state);

    public List<HashMap> selectByParam(@Param("accountId") String accountId, @Param("beginTime") String beginTime, @Param("endTime") String endTime, @Param("param") String param, @Param("type") String type);
}