package com.zcwk.core.dao;

import com.zcwk.core.model.InviteSet;

import java.util.HashMap;

public interface InviteSetMapper {
    int deleteByPrimaryKey(Long id);

    int insert(InviteSet record);

    int insertSelective(InviteSet record);

    InviteSet selectByPrimaryKey(Long id);

    InviteSet selectOne();

    int updateByPrimaryKeySelective(InviteSet record);

    int updateByPrimaryKey(InviteSet record);


}