package com.zcwk.core.dao;

import com.zcwk.core.model.SystemSet;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;

public interface SystemSetMapper {
    int deleteByPrimaryKey(Long id);

    int insert(SystemSet record);

    int insertSelective(SystemSet record);

    SystemSet selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SystemSet record);

    int updateByPrimaryKey(SystemSet record);

    List<HashMap> selectAll();
}