package com.zcwk.core.dao;

import com.zcwk.core.model.AutoInvest;
import com.zcwk.core.model.Debt;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

public interface AutoInvestMapper {
    int deleteByPrimaryKey(Long id);

    int insert(AutoInvest record);

    int insertSelective(AutoInvest record);

    AutoInvest selectByPrimaryKey(Long id);

    AutoInvest selectByAccount(@Param("accountId") long accountId);

    int updateByPrimaryKeySelective(AutoInvest record);

    int updateByPrimaryKey(AutoInvest record);

    int updateByAccount(AutoInvest record);

    AutoInvest selectByLastTime(Debt debt);

    @Update({"update c_auto_invest set state=#{state,jdbcType=TINYINT} where account_id = #{accountId,jdbcType=BIGINT} "})
    public int updateState(@Param("accountId") Long accountId, @Param("state") byte state);

    @Update({"update c_auto_invest set last_auto_time=now() where account_id = #{accountId,jdbcType=BIGINT}"})
    public int updateLastTime(@Param("accountId") Long accountId);
}