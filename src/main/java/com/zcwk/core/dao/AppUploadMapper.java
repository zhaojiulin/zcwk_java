package com.zcwk.core.dao;


import com.zcwk.core.model.AppUpload;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Administrator on 2017/2/28.
 */
public interface AppUploadMapper {
    int insertSelective(AppUpload record);

    public List<HashMap> selectAll();
    @Select({"select IFNULL(Max(version),0) from t_app_upload"})
    public long selecrMaxVid();



    AppUpload selectNewestApp();



}
