package com.zcwk.core.dao;

import com.zcwk.core.model.BaoFoo;

/**
 * Created by pwx on 2017/5/27.
 */
public interface BaoFooMapper {

    int insertSelective(BaoFoo baoFoo);

    BaoFoo selectByPrimaryKey(Long id);

    BaoFoo selectByAccount(Long accountId);

    int updateByPrimaryKeySelective(BaoFoo baoFoo);

}
