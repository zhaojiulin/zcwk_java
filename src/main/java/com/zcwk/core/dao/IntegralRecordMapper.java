package com.zcwk.core.dao;

import com.zcwk.core.model.IntegralRecord;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

public interface IntegralRecordMapper {
    int deleteByPrimaryKey(Long id);

    int insert(IntegralRecord record);

    int insertSelective(IntegralRecord record);

    IntegralRecord selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(IntegralRecord record);

    int updateByPrimaryKey(IntegralRecord record);

    List<HashMap> selectByParam(@Param("bTime") String bTime, @Param("eTime") String eTime, @Param("param") String param, @Param("userId") String userId, @Param("action") String action);

    int selectCountByAccount(@Param("userId") long userId, @Param("action") int action);

    //统计时间内积分记录条数
    @Select({"select count(1) from t_integral_record where user_id= #{userId,jdbcType=BIGINT} and action= #{action,jdbcType=TINYINT} and time>=#{startTime,jdbcType=TIMESTAMP} and time<#{endTime,jdbcType=TIMESTAMP} "})
    public int selectTodayCountByAccount(@Param("userId") long userId, @Param("action") int action,@Param("startTime") Date startTime,@Param("endTime") Date endTime);
}