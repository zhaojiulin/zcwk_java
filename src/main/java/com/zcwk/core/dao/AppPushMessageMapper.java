package com.zcwk.core.dao;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.zcwk.core.model.AppClient;
import com.zcwk.core.model.AppPushMessage;
import com.zcwk.core.util.UtilException;
import com.zcwk.core.view.AppPushMessageView;

public interface AppPushMessageMapper {
	
	/**
	 * 保存app标识
	 * @param appClient
	 * @return
	 */
	int saveAppClient(AppClient appClient);

	/**
     * 按照条件查询个推记录
     * @param pageNo 页码
     * @param pageSize 每页数
     * @param status 状态
     * @param bTime 开始时间
     * @param eTime 结束时间
     * @return
     * @throws UtilException
     */
	List<HashMap> findByParam(@Param("pageNo")int pageNo, @Param("pageSize")int pageSize, @Param("status")int status,
			@Param("bTime")String bTime,@Param("eTime") String eTime);

	/**
	 * 保存个推记录
	 * @param appPushMessage
	 */
	void saveAppPushMessage(AppPushMessage appPushMessage);
	
	/**
	 * 根据条件查询推送app标识
	 * @param view
	 * @return
	 */
	List<HashMap> findAppClientByParam(@Param("view")AppPushMessageView view);
	
	/**
	 * 根据条件查询app列表
	 * @param view
	 * @return
	 */
	List<AppClient> findAppClient(AppClient appClient);
	
	/**
	 * 更新app标识
	 * @param appClient
	 * @return
	 */
	int updateAppClient(AppClient appClient);
	
}