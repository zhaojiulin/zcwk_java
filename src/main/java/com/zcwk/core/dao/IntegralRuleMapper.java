package com.zcwk.core.dao;

import com.zcwk.core.model.IntegralRule;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface IntegralRuleMapper {
    int deleteByPrimaryKey(Long id);

    int insert(IntegralRule record);

    int insertSelective(IntegralRule record);

    IntegralRule selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(IntegralRule record);

    int updateByPrimaryKey(IntegralRule record);

    List<IntegralRule> selectByParam(@Param("action") String action, @Param("state") String state);
}