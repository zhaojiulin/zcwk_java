package com.zcwk.core.dao;

import com.zcwk.core.model.Youmi;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

/**
 * 有米
 * Created by pwx on 2017/3/14.
 */
public interface YoumiMapper {

    int deleteByPrimaryKey(Long id);

    int insert(Youmi record);

    Youmi selectByPrimaryKey(Long id);

    Youmi selectYoumiByKey(Youmi youmi);

    int updateUserIdByPrimaryKey(@Param("id") Long id,@Param("userId") Long userId);

    @Update({"update t_youmi set callback_url=#{callbackUrl,jdbcType=VARCHAR},time=now() where id = #{id,jdbcType=BIGINT}"})
    public int updateCallbackUrl(@Param("id") long id,@Param("callbackUrl") String callbackUrl);
}
