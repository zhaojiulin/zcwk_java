package com.zcwk.core.dao;

import com.zcwk.core.model.Cashflow;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;

public interface CashflowMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Cashflow record);

    int insertSelective(Cashflow record);

    Cashflow selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Cashflow record);

    int updateByPrimaryKey(Cashflow record);

    List<HashMap> selectByParam(@Param("bTime") String bTime, @Param("eTime") String eTime, @Param("type") byte type, @Param("param") String param, @Param("accountId") long accountId);
}