package com.zcwk.core.dao;

import java.util.HashMap;
import java.util.List;

import com.zcwk.core.model.Supervisors;

public interface SupervisorsMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Supervisors record);

    int insertSelective(Supervisors record);

    Supervisors selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Supervisors record);

    int updateByPrimaryKey(Supervisors record);
    
    public List<HashMap> selectSupervisors(Supervisors record);
}