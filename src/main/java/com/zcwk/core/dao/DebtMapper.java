package com.zcwk.core.dao;

import com.zcwk.core.model.Debt;
import org.apache.ibatis.annotations.*;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public interface DebtMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Debt record);

    int insertSelective(Debt record);

    Debt selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Debt record);

    int updateByPrimaryKeyWithBLOBs(Debt record);

    int updateByPrimaryKey(Debt record);

    int buyDebt(@Param("id") long id, @Param("amount") BigDecimal amount);

    public List<HashMap> selectByParam(@Param("state") byte state, @Param("param") String param, @Param("bTime") String bTime, @Param("eTime") String eTime);

    public List<HashMap> selectAllByParam(@Param("param") String param, @Param("bTime") String bTime, @Param("eTime") String eTime);
    public BigDecimal selectCountDebt(@Param("isToday") int isToday);
    public BigDecimal selectSumAmount(@Param("isToday") int isToday);

    @Select({"select * from c_debts where (invested_amount=amount or cut_time<#{time,jdbcType=TIMESTAMP}) and state=#{state,jdbcType=TINYINT}"})
    @ResultMap({"BaseResultMap"})
    public List<Debt> selectFullDebt(@Param("time") Date time,@Param("state") byte state);

    @Select({"select min(rate) startRate,max(rate) endRate from c_debts where state>0"})
    public HashMap selectRate();

    public List<Debt> selectInDebts(@Param("productType") byte productType, @Param("containNovice") int containNovice, @Param("state") byte state);

    public List<Debt> getDebtList(@Param("state") byte state,@Param("isReady") byte isReady,@Param("productType") byte productType,@Param("paymentType") byte paymentType, @Param("containNovice") int containNovice);

    public List<Debt> selectNoviceDebt(@Param("limit") int limit);

    public Debt selectAutoDebt(@Param("state") byte state);

    @Select({"select sum(amount) totalDebtAmount,count(amount) totalDebt from c_debts where state>0"})
    public HashMap selectTotalDebtAmount();
}