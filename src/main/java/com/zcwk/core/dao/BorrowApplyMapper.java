package com.zcwk.core.dao;

import com.zcwk.core.model.BorrowApply;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;

import java.util.List;

public interface BorrowApplyMapper {
    int deleteByPrimaryKey(Long id);

    int insert(BorrowApply record);

    int insertSelective(BorrowApply record);

    BorrowApply selectByPrimaryKey(Long id);

    @ResultMap({"BaseResultMap"})
    List<BorrowApply> selectByParam(@Param("param") String param);

    int updateByPrimaryKeySelective(BorrowApply record);

    int updateByPrimaryKeyWithBLOBs(BorrowApply record);

    int updateByPrimaryKey(BorrowApply record);
}