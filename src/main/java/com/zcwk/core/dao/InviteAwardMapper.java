package com.zcwk.core.dao;

import com.zcwk.core.model.InviteAward;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

public interface InviteAwardMapper {
    int deleteByPrimaryKey(Long id);

    int insert(InviteAward record);

    int insertSelective(InviteAward record);

    InviteAward selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(InviteAward record);

    int updateByPrimaryKey(InviteAward record);

    List<HashMap> selectByParam(@Param("bTime") String bTime, @Param("eTime") String eTime, @Param("param") String param, @Param("inviteId") String inviteId);

    BigDecimal selectSumAmount(@Param("bTime") String bTime, @Param("eTime") String eTime, @Param("inviteId") String inviteId);
}