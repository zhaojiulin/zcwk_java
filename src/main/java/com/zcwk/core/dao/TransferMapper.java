package com.zcwk.core.dao;

import com.zcwk.core.model.Transfer;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import java.util.Date;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

public interface TransferMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Transfer record);

    int insertSelective(Transfer record);

    Transfer selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Transfer record);

    int updateByPrimaryKey(Transfer record);

    int buyTransfer(@Param("id") long id, @Param("amount") BigDecimal amount);

    @Select({"select * from c_transfer where invested_amount=amount and state=#{state,jdbcType=TINYINT}"})
    @ResultMap({"BaseResultMap"})
    public List<Transfer> selectFullTransfer(@Param("state") byte state);


    @Select({"select * from c_transfer where cut_time<#{time,jdbcType=TIMESTAMP} and state=#{state,jdbcType=TINYINT}"})
    @ResultMap({"BaseResultMap"})
    public List<Transfer> selectCutTransfer(@Param("time") Date time, @Param("state") byte state);


    public List<HashMap> selectAll(@Param("accountId") String accountId);
}