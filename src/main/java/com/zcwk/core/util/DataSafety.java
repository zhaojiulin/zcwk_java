package com.zcwk.core.util;

import com.zcwk.core.service.SystemSetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zcwk.core.dao.AccountMapper;
import com.zcwk.core.model.*;
import com.zcwk.core.util.MD5Util;

@Service
public class DataSafety {
    @Autowired
    AccountMapper accountMapper;
    @Autowired
    UtilCache cache;
    @Autowired
    SystemSetService systemSetService;

    public Boolean checkAccount(long id) throws UtilException {
        Object key = this.systemSetService.getParamVal(Enums.Params.Key.getKey());
        if (key == null)
            throw new UtilException("请求异常");
        Account account = this.accountMapper.selectByPrimaryKey(id);
        String sign = MD5Util.MD5(account.getId()
                + account.getBalance().toString() + account.getPieceBalance() + account.getFreeze()
                + account.getExperienceAmount() + key.toString());
        return sign.equals(account.getSign());
    }

    public void encryptAccount(long accountId) throws UtilException {
        Object key = this.systemSetService.getParamVal(Enums.Params.Key.getKey());
        if (key == null)
            throw new UtilException("请求异常");
        Account account = this.accountMapper.selectByPrimaryKey(accountId);
        String sign = MD5Util.MD5(account.getId()
                + account.getBalance().toString() + account.getPieceBalance() + account.getFreeze()
                + account.getExperienceAmount() + key.toString());
        Account acc = new Account();
        acc.setId(accountId);
        acc.setSign(sign);
        this.accountMapper.updateByPrimaryKeySelective(acc);
    }
}
