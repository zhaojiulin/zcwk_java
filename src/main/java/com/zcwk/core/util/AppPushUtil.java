package com.zcwk.core.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.gexin.rp.sdk.base.IPushResult;
import com.gexin.rp.sdk.base.impl.AppMessage;
import com.gexin.rp.sdk.base.impl.ListMessage;
import com.gexin.rp.sdk.base.impl.Target;
import com.gexin.rp.sdk.base.payload.APNPayload;
import com.gexin.rp.sdk.http.IGtPush;
import com.gexin.rp.sdk.template.TransmissionTemplate;

public class AppPushUtil {

	/**
	 * app消息推送
	 * @param appId 接收应用
	 * @param appKey 鉴定身份是否合法
	 * @param masterSecret 第三方客户端个推集成鉴权码，用于验证第三方合法性
	 * @param host 推送os域名, host可选填，如果host不填程序自动检测用户网络，选择最快的域名连接下发
	 * @param transmissionContent 透传类容
     * @param content 消息内容
     * @param title 消息标题
     * @param appClients app列表
	 * @return
	 * @throws UtilException
	 */
	public static HashMap pushMessage(String appId,String appKey,String masterSecret,String host,String transmissionContent,String content,String title,String[] appClients) throws UtilException{
		if(appClients.length >0){
			return pushMessageToList(appId, appKey, masterSecret, host, transmissionContent, content, title, appClients);
		}else {
			return pushMessageToAll(appId, appKey, masterSecret, host, transmissionContent, content, title);
		}
	}
	
	/**
	 * app消息推送--列表
	 * @param appId 接收应用
	 * @param appKey 鉴定身份是否合法
	 * @param masterSecret 第三方客户端个推集成鉴权码，用于验证第三方合法性
	 * @param host 推送os域名, host可选填，如果host不填程序自动检测用户网络，选择最快的域名连接下发
	 * @param transmissionContent 透传类容
     * @param content 消息内容
     * @param title 消息标题
     * @param appClients app列表
	 * @return
	 * @throws UtilException
	 */
    public static HashMap pushMessageToList(String appId,String appKey,String masterSecret,String host,String transmissionContent,String content,String title,String[] appClients) throws UtilException {
    	//透传模版
    	TransmissionTemplate template = new TransmissionTemplate();
    	template.setAppId(appId);
        template.setAppkey(appKey);
        template.setTransmissionContent(transmissionContent);//透传类容
        template.setTransmissionType(2);//收到消息是否立即启动应用，1为立即启动，2则广播等待客户端自启动
        template.setAPNInfo(getAPNPayload(content, title));//iOS推送使用该字段
    	
    	IGtPush push = new IGtPush(host, appKey, masterSecret);
    	ListMessage message = new ListMessage();//列表推送消息的消息体
    	message.setOffline(true);// 设置消息离线，并设置离线时间
    	message.setOfflineExpireTime(24 * 1000 * 3600);//// 离线有效时间，单位为毫秒，可选
    	message.setData(template);
    	
    	//配置推送目标
    	List targets = new ArrayList();
    	for (String appClient : appClients) {
    		Target target = new Target();
    		target.setAppId(appId);
    		target.setClientId(appClient);
    		targets.add(target);
		}
    	//taskId用于在推送时去查找对应的message
    	String taskId = push.getContentId(message);
    	IPushResult ret = push.pushMessageToList(taskId, targets);
    	System.out.println(ret.getResponse().toString());
    	String result = ret.getResponse().get("result").toString();
    	if(result.equals("ok")){
    		return new Result().Success();
    	}else {
    		return new Result().Error("推送失败，错误代码：" + result);
		}
    }
    
    /**
	 * app消息推送--应用
	 * @param appId 接收应用
	 * @param appKey 鉴定身份是否合法
	 * @param masterSecret 第三方客户端个推集成鉴权码，用于验证第三方合法性
	 * @param host 推送os域名, host可选填，如果host不填程序自动检测用户网络，选择最快的域名连接下发
	 * @param transmissionContent 透传类容
     * @param content 消息内容
     * @param title 消息标题
	 * @return
	 * @throws UtilException
	 */
    public static HashMap pushMessageToAll(String appId,String appKey,String masterSecret,String host,String transmissionContent,String content,String title) throws UtilException {
    	//透传模版
    	TransmissionTemplate template = new TransmissionTemplate();
    	template.setAppId(appId);
        template.setAppkey(appKey);
        template.setTransmissionContent(transmissionContent);//透传类容
        template.setTransmissionType(2);//收到消息是否立即启动应用，1为立即启动，2则广播等待客户端自启动
        template.setAPNInfo(getAPNPayload(content, title));//iOS推送使用该字段
    	
    	IGtPush push = new IGtPush(host, appKey, masterSecret);
    	AppMessage message = new AppMessage();
    	message.setOffline(true);// 设置消息离线，并设置离线时间
    	message.setOfflineExpireTime(24 * 1000 * 3600);//// 离线有效时间，单位为毫秒，可选
    	message.setData(template);
    	
    	List<String> appIds = new ArrayList<String>();
        appIds.add(appId);
    	message.setAppIdList(appIds);
    	IPushResult ret = push.pushMessageToApp(message);
    	System.out.println(ret.getResponse().toString());
    	String result = ret.getResponse().get("result").toString();
    	if(result.equals("ok")){
    		return new Result().Success();
    	}else {
    		return new Result().Error("推送失败，错误代码：" + result);
		}
    }
    
    /**
     * 定义iOS平台推送消息
     * @param content
     * @param title
     * @return
     */
    private static APNPayload getAPNPayload(String content,String title){
        APNPayload payload = new APNPayload();
        payload.setAutoBadge("+1");//在已有数字基础上加1显示，设置为-1时，在已有数字上减1显示，设置为数字时，显示指定数字
        payload.setContentAvailable(1);//推送直接带有透传数据
        payload.setSound("default");//通知铃声文件名，无声设置（com.gexin.ios.silence）
        payload.setCategory("");//action定义以后，将action进行分类
        APNPayload.DictionaryAlertMsg alertMsg = getDictionaryAlertMsg(content, title);
        payload.setAlertMsg(alertMsg);//复杂消息体（字典形式）
        return payload;
    }
    
    /**
     * 复杂消息体（字典形式）
     * @param content
     * @param title
     * @return
     */
    private static APNPayload.DictionaryAlertMsg getDictionaryAlertMsg(String content,String title){
    	APNPayload.DictionaryAlertMsg alertMsg = new APNPayload.DictionaryAlertMsg();
        alertMsg.setBody(content);
        // iOS8.2以上版本支持
        alertMsg.setTitle(title);
        return alertMsg;
    }
}
