package com.zcwk.core.util;

public class Enums {
    // 标的状态
    public enum DebtState {
        InConfirm("新发标", 0), ApprovalPass("审核成功，筹款中", 5), Full("满标待审核", 6), ApprovalReject(
                "审核失败", -5), Cancel("撤标", -6), InPayment("还款中", 10), Overdue("逾期", 11), EndPayment(
                "还款完成", 15);
        private String name;
        private int index;

        private DebtState(String name, int index) {
            this.name = name;
            this.index = index;
        }

        public int getIndex() {
            return this.index;
        }

        public String getName() {
            return this.name;
        }
    }

    //来源
    public enum Route {
        RG("RG", 0), PC("PC", 1), IOS("IOS", 2), Android("Android", 3), Wap("Wap", 4), PcAuth("PcAuth", 5);
        private String name;
        private int index;

        private Route(String name, int index) {
            this.name = name;
            this.index = index;
        }

        public int getIndex() {
            return this.index;
        }

        public String getName() {
            return this.name;
        }
    }

    // 投资类型
    public enum InvestType {
        Share("份", 1), Amount("金额", 0);
        private String name;
        private int index;

        private InvestType(String name, int index) {
            this.name = name;
            this.index = index;
        }

        public int getIndex() {
            return this.index;
        }

        public String getName() {
            return this.name;
        }
    }

    // 计息方式
    public enum InterestType {
        Now("即投即计息", 0), Full("满标计息", 1);
        private String name;
        private int index;

        private InterestType(String name, int index) {
            this.name = name;
            this.index = index;
        }

        public int getIndex() {
            return this.index;
        }

        public String getName() {
            return this.name;
        }
    }

    // 奖励类型
    public enum AwardType {
        Scale("比例", 1), Fixed("固定", 2);
        private String name;
        private int index;

        private AwardType(String name, int index) {
            this.name = name;
            this.index = index;
        }

        public int getIndex() {
            return this.index;
        }

        public String getName() {
            return this.name;
        }
    }

    //账户类型
    public enum AccountType {
        Investor("投资人", 0), Borrower("借款人", 1);
        private String name;
        private int index;

        private AccountType(String name, int index) {
            this.name = name;
            this.index = index;
        }

        public int getIndex() {
            return this.index;
        }

        public String getName() {
            return this.name;
        }
    }
    
    //用户类型
    public enum UserNature {
    	Persion("个人用户", 0), Organize("企业用户", 1);
        private String name;
        private int index;

        private UserNature(String name, int index) {
            this.name = name;
            this.index = index;
        }

        public int getIndex() {
            return this.index;
        }

        public String getName() {
            return this.name;
        }
    }

    // 资金类型
    public enum FundType {
        Buy("购买", 1),
        Debt("借款", 2),
        InvestAward("投资奖励", 3),
        PaymentInvest("接收回款", 4),
        PaymentDebt("支付回款", 5),
        ThawBuyFreeze("释放投标冻结金额", 6),
        CancelInvest("撤标返还投资金额", 7),
        CancelDebt("撤标返还已筹金额", 8),
        Withdraw("提现", 9),
        ThawWithdrawFreeze("释放提现冻结金额", 10),
        CancelWithdraw("提现不通过", 11),
        OnLineRecharge("在线充值", 12),
        Debit("扣款", 13),
        Bonus("红包", 14),
        InviteAward("邀请投资奖励", 15),
        LineRecharge("线下充值", 16),
        PieceAward("续投奖励", 17),
        Transfer("债转", 18),
        SendTransferAward("发放债转奖励", 19),
        CouponIterest("加息券收益", 20);
        private String name;
        private int index;

        private FundType(String name, int index) {
            this.name = name;
            this.index = index;
        }

        public int getIndex() {
            return this.index;
        }

        public String getName() {
            return this.name;
        }
    }

    // 周期类型
    public enum PeriodUnitType {
        Month("月", 1), Day("日", 2);
        private String name;
        private final int index;

        private PeriodUnitType(String name, int index) {
            this.name = name;
            this.index = index;
        }

        public int getIndex() {
            return this.index;
        }

        public String getName() {
            return this.name;
        }
    }

    // 还款类型
    public enum PaymentType {
        Once("一次性还本付息", 0), Monthly("按月付息,到期还本", 1), Front("收益前置", 2), Principal("等额本金", 3), Interest("等额本息", 4);
        private String name;
        private int index;

        private PaymentType(String name, int index) {
            this.name = name;
            this.index = index;
        }

        public int getIndex() {
            return this.index;
        }

        public String getName() {
            return this.name;
        }

    }

    public enum State {
        Cancel("处理失败", -1), Wait("等待处理", 0), Complete("已处理", 1);
        private String name;
        private int index;

        private State(String name, int index) {
            this.name = name;
            this.index = index;
        }

        public int getIndex() {
            return this.index;
        }

        public String getName() {
            return this.name;
        }
    }

    public enum GatWay {
        RG("RG", 0), BF("BF", 1), HC("HC", 2), FY("FY", 3);
        private String name;
        private int index;

        private GatWay(String name, int index) {
            this.name = name;
            this.index = index;
        }

        public int getIndex() {
            return this.index;
        }

        public String getName() {
            return this.name;
        }
    }

    public enum ProtocolType {
        Debt("普通借款协议", 1);
        private String name;
        private int index;

        private ProtocolType(String name, int index) {
            this.name = name;
            this.index = index;
        }

        public int getIndex() {
            return this.index;
        }

        public String getName() {
            return this.name;
        }
    }

    public enum Actions {
        Register("注册", 1),
        Buy("购买", 2),
        ValidEmail("邮件认证", 3),
        Login("登录", 4),
        PayPassword("设置交易密码", 5),
        RealName("实名", 6),
        BindCard("绑卡", 7),
        Withdraw("提现", 8),
        Recharge("充值", 9),
        Payment("回款", 10),
        WithdrawFail("提现失败", 11),
        FullDebtAward("满标奖励",12),
        BuyCrowdFunding("购买众筹项目",13),
        IntegralTranslation("积分平移", 14),
        SignIn("签到", 15),
        Exchange("商城兑换", 16);
        private String name;
        private int index;

        private Actions(String name, int index) {
            this.name = name;
            this.index = index;
        }

        public int getIndex() {
            return this.index;
        }

        public String getName() {
            return this.name;
        }
    }

    public static String getActionDesc(int index) {
        for (Actions a : Actions.values()) {
            if (index == a.getIndex())
                return a.getName();
        }
        return "";
    }

    public enum BonusType {
        Deductible("抵扣", 1), Cash("现金", 2);
        private String name;
        private int index;

        private BonusType(String name, int index) {
            this.name = name;
            this.index = index;
        }

        public int getIndex() {
            return this.index;
        }

        public String getName() {
            return this.name;
        }
    }

    public enum SendType {
        Sms("短信", 1), Email("邮件", 2);
        private String name;
        private int index;

        private SendType(String name, int index) {
            this.name = name;
            this.index = index;
        }

        public int getIndex() {
            return this.index;
        }

        public String getName() {
            return this.name;
        }
    }

    public enum Params {
        BF("bf"),
        HC("hc"),
        FY("fy"),
        Base("base_url"),
        Piece("piece"),
        Key("key"),
        ApiKey("api_key"),
        StaticPath("static_url"),
        Sms("sms"),
        ChuanglanSms("chaunglan_sms"),
        JianzhouSms("jianzhou_sms"),
        EmaySms("emay_sms"),
        RealName("real_name"),
        ZhimaRealName("zhima_real_name"),
        GZTRealName("gzt_real_name"),
        RealCount("real_count"),
        AuthGateway("auth_gateway"),
        Email("email"),
        Gateway("gateway"),
        ApiBaseUrl("api_base_url"),
        TransferDay("transfer_day"),
        ESign("e_sign"),
        RealNameWay("real_name_way"),
        SmsWay("sms_way"),
        GeTui("ge_tui"),
        googleAuthenticator("google_authenticator"),
        bindCard("bind_card"),
        withdrawConfig("withdraw_config"),
        userInviteCode("user_invite_code"),
        signInConfig("sign_in_config");
        private String key;

        private Params(String key) {
            this.key = key;
        }

        public String getKey() {
            return this.key;
        }
    }

    public enum TotalItems {
        TotalRegister("累计注册", "totalRegister"),
        TodayRegister("今日注册", "todayRegister"),
        TotalInvest("累计投资", "totalInvest"),
        TodayInvest("今日投资", "todayInvest"),
        TotalInterest("累计收益", "totalInterest"),
        TotalDebt("累计借款笔数", "totalDebt"),
        TodayDebt("今日借款笔数", "todayDebt"),

        TotalDebtAmount("累计借款金额", "totalDebtAmount"),
        TotalPaymentAmount("累计还款金额", "totalPaymentAmount"),
        TodayDebtAmount("今日借款金额", "todayDebtAmount"),
        TotalWaitPaymentCount("累计待还笔数", "totalWaitPaymentCount"),
        TotalWaitPaymentAmount("累计待还金额", "totalWaitPaymentAmount"),

        TotalWithdrawAmount("累计提现金额", "totalWithdrawAmount"),
        TodayWithdrawAmount("今日提现金额", "todayWithdrawAmount"),
        TotalRechargeAmount("累计充值金额", "totalRechargeAmount"),
        TodayRechargeAmount("今日充值金额", "todayRechargeAmount"),
        StartRate("开始利率", "startRate"),
        EndRate("结束利率", "endRate"),
        TodayPieceAmount("今日续投金额", "todayPieceAmount");
        private String key;

        private TotalItems(String name, String key) {
            this.key = key;
        }

        public String getKey() {
            return this.key;
        }
    }

    public enum LimitType {
        None("反对", 0), Day("弃权", 1),Forever("永久",2);
        private String name;
        private int index;

        private LimitType(String name, int index) {
            this.name = name;
            this.index = index;
        }

        public int getIndex() {
            return this.index;
        }

        public String getName() {
            return this.name;
        }
    }

    /**
     * 订单状态
     */
    public enum OrderState {
        NewOrder("新订单", 0),Cancel("已取消", -1),Complete("已完成", 1);
        private String name;
        private int index;

        private OrderState(String name, int index) {
            this.name = name;
            this.index = index;
        }

        public int getIndex() {
            return this.index;
        }

        public String getName() {
            return this.name;
        }
    }

    /**
     * 商品状态
     */
    public enum GoodsState {
        OffShelves("下架", 0),OnShelves("上架", 1);
        private String name;
        private int index;

        private GoodsState(String name, int index) {
            this.name = name;
            this.index = index;
        }

        public int getIndex() {
            return this.index;
        }

        public String getName() {
            return this.name;
        }
    }

    /**
     * 商品类型
     */
    public enum GoodsType {
        Entity("实物", 1),TelephoneFare("话费", 2),Deductible("抵扣", 3),Coupon("加息劵", 4);
        private String name;
        private int index;

        private GoodsType(String name, int index) {
            this.name = name;
            this.index = index;
        }

        public int getIndex() {
            return this.index;
        }

        public String getName() {
            return this.name;
        }
    }

}
