package com.zcwk.core.util;

import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.BaseFont;
import org.xhtmlrenderer.pdf.ITextFontResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

import java.io.*;
import java.net.MalformedURLException;

/**
 * Created by pwx on 2016/10/6.
 */
public class PdfUtil {
    private String outPath;

    public void setOutPath(String outPath) {
        this.outPath = outPath;
    }

    public String getOutPath() {
        return this.outPath;
    }

    private String fontPath;

    public void setFontPath(String fontPath) {
        this.fontPath = fontPath;
    }

    public String getFontPath() {
        return this.fontPath;
    }

    public void create(String name, String content) throws IOException, DocumentException {
        OutputStream os = new FileOutputStream(this.getOutPath() + name + ".pdf");
        ITextRenderer renderer = new ITextRenderer();
        ITextFontResolver fontResolver = renderer.getFontResolver();
        fontResolver.addFont(this.getFontPath() + "simsun.ttc",BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
        renderer.setDocumentFromString(content);
        // 解决图片的相对路径问题
        //renderer.getSharedContext().setBaseURL("file:http://121.196.224.90:7088");
        renderer.layout();
        renderer.createPDF(os);
        os.close();
    }
}
