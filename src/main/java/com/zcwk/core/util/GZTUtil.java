package com.zcwk.core.util;

import com.zcwk.core.gboss.Des2;
import com.zcwk.core.gboss.QueryValidatorServices;
import com.zcwk.core.gboss.QueryValidatorServicesProxy;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

import java.io.StringReader;

public class GZTUtil {

    /**
     * 如验证通过，返回user : realname idcard addr sex(1/0)
     *
     * @param realName
     * @param idCard
     * @return
     * @throws
     * @throws Exception
     */
    public static boolean checkPersonID(String realName, String idCard,String gztUrl,String gztAccount,String gztPwd) throws Exception {
        QueryValidatorServicesProxy proxy = new QueryValidatorServicesProxy();
        proxy.setEndpoint(gztUrl);
        QueryValidatorServices service = proxy.getQueryValidatorServices();
        //对调用的参数进行加密
        String key = "12345678";
        String userName = Des2.encode(key, gztAccount);
        String password = Des2.encode(key,gztPwd);
        //System.setProperty("javax.net.ssl.trustStore", "keystore");
        //查询返回结果
        String resultXML = "";
        String datasource = Des2.encode(key, "1A020201");
        //单条
        String param = realName + "," + idCard;
        resultXML = service.querySingle(userName, password, datasource, Des2.encode(key, param));
        //解密
        resultXML = Des2.decodeValue(key, resultXML);
        System.out.println("国政通实名参数： realName="+realName +"&idCard="+idCard);
        System.out.println("国政通实名结果： "+resultXML);
        SAXBuilder builder = new SAXBuilder();
        Document doc = builder.build(new StringReader(resultXML));
        Element root = doc.getRootElement();
        String status = root.getChild("message").getChild("status").getText();
        if (!status.equals("0")) {
            return false;
        }
        Element ele = root.getChild("policeCheckInfos").getChild("policeCheckInfo");
        status = ele.getChild("message").getChild("status").getText();
        if (status.equals("0")) {
            status = ele.getChild("compStatus").getText();
            if (status.equals("3")) {
                //String addr = ele.getChild("policeadd").getText();
                //String sex = ele.getChild("sex2").getText();
                //sex.equals("男") ? (byte)1 :(byte) 0);
                return true;
            } else
                return false;
        } else
            return false;
    }

}