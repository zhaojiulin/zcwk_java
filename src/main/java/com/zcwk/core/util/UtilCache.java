package com.zcwk.core.util;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.asm.ByteVector;
import com.sun.mail.iap.ByteArray;
import com.zcwk.core.dao.*;
import com.zcwk.core.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

@Service
public class UtilCache {

    @Autowired
    JedisPool jedisPool;
    @Autowired
    AccountMapper accountMapper;
    @Autowired
    UserMapper userMapper;
    @Autowired
    SeoMapper seoMapper;
    @Autowired
    PlatformInfoMapper platformInfoMapper;
    @Autowired
    SupervisorsMapper supervisorsMapper;
    @Autowired
    SystemSetMapper systemSetMapper;
    @Autowired
    DebtMapper debtMapper;
    @Autowired
    InvestMapper investMapper;
    @Autowired
    BillDebtMapper billDebtMapper;
    @Autowired
    ProtocolTemplateMapper protocolTemplateMapper;
    @Autowired
    TemplateMapper templateMapper;
    @Autowired
    WithdrawMapper withdrawMapper;
    @Autowired
    RechargeMapper rechargeMapper;
    @Autowired
    ChannelMapper channelMapper;
    int time = 60 * 60 * 24;


    public void delAccount(long id) {
        if (id > 0) {
            Jedis jedis = jedisPool.getResource();
            String key = "account_" + id;
            jedis.del(key);
            jedis.close();
        }
    }

    public Account getAccount(long id) {
        Jedis jedis = this.jedisPool.getResource();
        try {
            String key = "account_" + id;
            Map map = jedis.hgetAll(key);
            if (map.size() == 0) {
                Account account = this.accountMapper.selectByPrimaryKey(id);
                Map jo = CommonUtil.parseMap(account);
                jedis.hmset(key, jo);
                jedis.expire(key, this.time);
                return account;
            }
            return (Account) CommonUtil.parseObject(map, Account.class);
        } finally {
            jedis.close();
        }
    }

    public void setOrder(String order, Map map) {
        Jedis jedis = this.jedisPool.getResource();
        try {
            jedis.hmset(order, map);
            jedis.expire(order, 60 * 10);
        } finally {
            jedis.close();
        }
    }

    public Map getOrder(String order) {
        Jedis jedis = this.jedisPool.getResource();
        try {
            Map map = jedis.hgetAll(order);
            return map;
        } finally {
            jedis.close();
        }
    }

    public void setCode(String key, String code) {
        Jedis jedis = this.jedisPool.getResource();
        try {
            jedis.set(key, code);
            jedis.expire(key, 60 * 2);
        } finally {
            jedis.close();
        }
    }

    public void delCode(String key) {
        Jedis jedis = jedisPool.getResource();
        jedis.del(key);
        jedis.close();
    }

    public String getCode(String key) {
        Jedis jedis = this.jedisPool.getResource();
        try {
            return jedis.get(key);
        } finally {
            jedis.close();
        }
    }


    public void delDebt(long id) {
        if (id > 0) {
            Jedis jedis = jedisPool.getResource();
            String key = "debt_" + id;
            jedis.del(key);
            jedis.close();
        }
    }

    public Debt getDebt(long id) {
        Jedis jedis = this.jedisPool.getResource();
        try {
            String key = "debt_" + id;
            Map map = jedis.hgetAll(key);
            if (map.size() == 0) {
                Debt debt = this.debtMapper.selectByPrimaryKey(id);
                if (debt == null)
                    return null;
                Map jo = CommonUtil.parseMap(debt);
                jedis.hmset(key, jo);
                jedis.expire(key, this.time);
                return debt;
            }
            return (Debt) CommonUtil.parseObject(map, Debt.class);
        } finally {
            jedis.close();
        }
    }

    public Map getSortInvest(int limit) {
        Jedis jedis = this.jedisPool.getResource();
        try {
            String key = "sort_" + limit;
            Map<String, String> map = jedis.hgetAll(key);
            if (map.size() == 0) {
                Date dt = new Date();
                List<HashMap> dayInvest = this.investMapper.selectSort("%Y%m%d", CommonUtil.formatDate("yyyyMMdd", dt), limit);
                jedis.hset(key, "dayInvest", JSON.toJSONString(dayInvest));
                List<HashMap> monthInvest = this.investMapper.selectSort("%Y%m", CommonUtil.formatDate("yyyyMM", dt), limit);
                jedis.hset(key, "monthInvest", JSON.toJSONString(monthInvest));
                List<HashMap> totalInvest = this.investMapper.selectSort(null, null, limit);
                jedis.hset(key, "totalInvest", JSON.toJSONString(totalInvest));
                jedis.expire(key, 60 * 2);
                map = jedis.hgetAll(key);
            }
            return map;
        } finally {
            jedis.close();
        }
    }

    public void setSendTemplate(Template template) {
        Jedis jedis = this.jedisPool.getResource();
        try {
            String key = "template_" + template.getAction() + "_" + template.getType();
            Map jo = CommonUtil.parseMap(template);
            jedis.hmset(key, jo);
            jedis.expire(key, this.time);
        } finally {
            jedis.close();
        }
    }

    public Template getSendTemplate(int action, int type) {
        Jedis jedis = this.jedisPool.getResource();
        try {
            String key = "template_" + action + "_" + type;
            Map map = jedis.hgetAll(key);
            if (map.size() == 0) {
                List<Template> list = this.templateMapper.selectByParam(String.valueOf(action), String.valueOf(type), String.valueOf(Enums.State.Complete.getIndex()));
                if (list == null || list.size() == 0)
                    return null;
                Map jo = CommonUtil.parseMap(list.get(0));
                jedis.hmset(key, jo);
                jedis.expire(key, this.time);
                return list.get(0);
            }
            return CommonUtil.parseObject(map, Template.class);
        } finally {
            jedis.close();
        }
    }

    public void setProtocol(ProtocolTemplate protocol) {
        Jedis jedis = this.jedisPool.getResource();
        try {
            String key = "protocol_" + protocol.getType();
            jedis.set(key, protocol.getContent());
        } finally {
            jedis.close();
        }
    }

    public String getProtocol(int type) {
        Jedis jedis = this.jedisPool.getResource();
        try {
            String key = "protocol_" + type;
            String protocol = jedis.get(key);
            if (protocol == null || protocol.isEmpty()) {
                List<ProtocolTemplate> list = this.protocolTemplateMapper.selectByParam(String.valueOf(Enums.ProtocolType.Debt.getIndex()));
                if (list.size() > 0) {
                    protocol = list.get(0).getContent();
                    jedis.set(key, protocol);
                }
            }
            return protocol;
        } finally {
            jedis.close();
        }
    }

    public void setTotal(String key, double value) {
        Jedis jedis = this.jedisPool.getResource();
        try {
            String _key = "total_"+ CommonUtil.formatDate("yyyy_MM_dd", new Date());
            Map<String, String> map = jedis.hgetAll(_key);
            if (map.size() == 0)
                this.getTotal();
            jedis.hincrByFloat(_key, key, value);
        } finally {
            jedis.close();
        }
    }


    public Map<String, String> getTotal() {
        Jedis jedis = this.jedisPool.getResource();
        try {
            String key = "total_"+ CommonUtil.formatDate("yyyy_MM_dd", new Date());
            Map<String, String> map = jedis.hgetAll(key);
            if (map.isEmpty()||map.size() == 0) {
                BigDecimal todayDebt = this.debtMapper.selectCountDebt(Enums.State.Complete.getIndex());
                map.put(Enums.TotalItems.TodayDebt.getKey(), String.valueOf(todayDebt));
                jedis.hincrByFloat(key, Enums.TotalItems.TodayDebt.getKey(), todayDebt.doubleValue());

                BigDecimal todayRegister = this.userMapper.selectCountId(Enums.State.Complete.getIndex());
                map.put(Enums.TotalItems.TodayRegister.getKey(), String.valueOf(todayRegister));
                jedis.hincrByFloat(key, Enums.TotalItems.TodayRegister.getKey(), todayRegister.doubleValue());

                BigDecimal todayRechargeAmount = this.rechargeMapper.selectSumAmount(Enums.State.Complete.getIndex());
                map.put(Enums.TotalItems.TodayRechargeAmount.getKey(), String.valueOf(todayRechargeAmount));
                jedis.hincrByFloat(key, Enums.TotalItems.TodayRechargeAmount.getKey(), todayRechargeAmount.doubleValue());

                BigDecimal todayWithdrawAmount = this.withdrawMapper.selectSumAmount(Enums.State.Complete.getIndex());
                map.put(Enums.TotalItems.TodayWithdrawAmount.getKey(), String.valueOf(todayWithdrawAmount));
                jedis.hincrByFloat(key, Enums.TotalItems.TodayWithdrawAmount.getKey(), todayWithdrawAmount.doubleValue());

                BigDecimal todayDebtAmount = this.debtMapper.selectSumAmount(Enums.State.Complete.getIndex());
                map.put(Enums.TotalItems.TodayDebtAmount.getKey(), String.valueOf(todayDebtAmount));
                jedis.hincrByFloat(key, Enums.TotalItems.TodayDebtAmount.getKey(), todayDebtAmount.doubleValue());


                BigDecimal todayInvest = this.investMapper.selectSumAmount(Enums.State.Complete.getIndex());
                map.put(Enums.TotalItems.TodayInvest.getKey(), String.valueOf(todayInvest));
                jedis.hincrByFloat(key, Enums.TotalItems.TodayInvest.getKey(), todayInvest.doubleValue());

                BigDecimal todayPieceAmount = this.investMapper.selectSumPsamount(Enums.State.Complete.getIndex());
                map.put(Enums.TotalItems.TodayPieceAmount.getKey(), String.valueOf(todayPieceAmount));
                jedis.hincrByFloat(key, Enums.TotalItems.TodayPieceAmount.getKey(), todayPieceAmount.doubleValue());

                int registerCount = this.userMapper.selectCount((byte) Enums.AccountType.Investor.getIndex());
                map.put(Enums.TotalItems.TotalRegister.getKey(), String.valueOf(registerCount));
                jedis.hincrBy(key, Enums.TotalItems.TotalRegister.getKey(), registerCount);

                BigDecimal totalInvest = this.investMapper.selectSumAmount(Enums.State.Wait.getIndex());
                map.put(Enums.TotalItems.TotalInvest.getKey(), String.valueOf(totalInvest));
                jedis.hincrByFloat(key, Enums.TotalItems.TotalInvest.getKey(), totalInvest.doubleValue());

                BigDecimal totalPaymentAmount = this.billDebtMapper.selectSumpayAmount((byte) Enums.State.Complete.getIndex());
                map.put(Enums.TotalItems.TotalPaymentAmount.getKey(), String.valueOf(totalPaymentAmount));
                jedis.hincrByFloat(key, Enums.TotalItems.TotalPaymentAmount.getKey(), totalPaymentAmount.doubleValue());



                BigDecimal totalInterest = this.billDebtMapper.selectSumInterest((byte) Enums.State.Complete.getIndex());
                map.put(Enums.TotalItems.TotalInterest.getKey(), String.valueOf(totalInterest));
                jedis.hincrByFloat(key, Enums.TotalItems.TotalInterest.getKey(), totalInterest.doubleValue());

                //统计借款金额和借款笔数
                HashMap totalDebtList = this.debtMapper.selectTotalDebtAmount();
                if(totalDebtList!=null){
                    map.put(Enums.TotalItems.TotalDebt.getKey(),totalDebtList.get(Enums.TotalItems.TotalDebt.getKey()).toString());
                    jedis.hincrBy(key, Enums.TotalItems.TotalDebt.getKey(), Integer.valueOf(totalDebtList.get(Enums.TotalItems.TotalDebt.getKey()).toString()));
                    map.put(Enums.TotalItems.TotalDebtAmount.getKey(),totalDebtList.get(Enums.TotalItems.TotalDebtAmount.getKey()).toString());
                    jedis.hincrByFloat(key, Enums.TotalItems.TotalDebtAmount.getKey(), Double.valueOf(totalDebtList.get(Enums.TotalItems.TotalDebtAmount.getKey()).toString()));
                }else{
                    map.put(Enums.TotalItems.TotalDebt.getKey(),"0");
                    jedis.hincrBy(key, Enums.TotalItems.TotalDebt.getKey(), 0);
                    map.put(Enums.TotalItems.TotalDebtAmount.getKey(),"0");
                    jedis.hincrByFloat(key, Enums.TotalItems.TotalDebtAmount.getKey(), 0);
                }


                List<HashMap> totalBillList = this.billDebtMapper.selectPaymentAll((byte) Enums.State.Wait.getIndex(), "", "", "");
                map.put(Enums.TotalItems.TotalWaitPaymentCount.getKey(), String.valueOf(totalBillList.size()));
                jedis.hincrBy(key, Enums.TotalItems.TotalWaitPaymentCount.getKey(), totalBillList.size());

                BigDecimal totalBillAmount = BigDecimal.ZERO;
                for (int i = 0; i < totalBillList.size(); i++) {
                    totalBillAmount = totalBillAmount.add(new BigDecimal(totalBillList.get(i).get("paymentCorpus").toString())).add(new BigDecimal(totalBillList.get(i).get("paymentInterest").toString()));
                }
                map.put(Enums.TotalItems.TotalWaitPaymentAmount.getKey(), String.valueOf(totalBillAmount));
                jedis.hincrByFloat(key, Enums.TotalItems.TotalWaitPaymentAmount.getKey(), totalBillAmount.doubleValue());

                Byte route=0;
                List<HashMap> totalWithdrawList = this.withdrawMapper.selectByParam("", "", "", String.valueOf(Enums.State.Complete.getIndex()), "", 0,route,"","");
                BigDecimal totalWithdrawAmount = BigDecimal.ZERO;
                for (int i = 0; i < totalWithdrawList.size(); i++) {
                    totalWithdrawAmount = totalWithdrawAmount.add(new BigDecimal(totalWithdrawList.get(i).get("amount").toString()));
                }
                map.put(Enums.TotalItems.TotalWithdrawAmount.getKey(), String.valueOf(totalWithdrawAmount));
                jedis.hincrByFloat(key, Enums.TotalItems.TotalWithdrawAmount.getKey(), totalWithdrawAmount.doubleValue());

                List<HashMap> totalRechargeList = this.rechargeMapper.selectByParam("", "", "", "", "", BigDecimal.valueOf(0), BigDecimal.valueOf(0), 0,route,"1");
                BigDecimal totalRechargeAmount = BigDecimal.ZERO;
                for (int i = 0; i < totalRechargeList.size(); i++) {
                    totalRechargeAmount = totalRechargeAmount.add(new BigDecimal(totalRechargeList.get(i).get("amount").toString()));
                }
                map.put(Enums.TotalItems.TotalRechargeAmount.getKey(), String.valueOf(totalRechargeAmount));
                jedis.hincrByFloat(key, Enums.TotalItems.TotalRechargeAmount.getKey(), totalRechargeAmount.doubleValue());

                //年化率统计
                HashMap rate = this.debtMapper.selectRate();
                jedis.hincrByFloat(key, Enums.TotalItems.StartRate.getKey(), Double.valueOf(rate.get(Enums.TotalItems.StartRate.getKey()).toString()));
                jedis.hincrByFloat(key, Enums.TotalItems.EndRate.getKey(), Double.valueOf(rate.get(Enums.TotalItems.EndRate.getKey()).toString()));
//                int _time = CommonUtil.getSecond();
//                System.out.println(_time);
                jedis.expire(key,24*60*60*2);
            }
            return map;
        } finally {
            jedis.close();
        }
    }


    public List<JSONObject> getTotalInvest() {
        Jedis jedis = this.jedisPool.getResource();
        try {
            String key = "total_invest";
            String invests = jedis.get(key);
            if (invests == null || invests.length() == 0) {
                List<HashMap> map = this.investMapper.selectTotalByDay();
                if (map == null)
                    return null;
                invests = JSON.toJSONString(map);
                jedis.set(key, invests);
                int _time = CommonUtil.getSecond();
                jedis.expire(key, _time);
            }
            return JSON.parseObject(invests, List.class);
        } finally {
            jedis.close();
        }
    }

    public void delParams() {
        Jedis jedis = jedisPool.getResource();
        String key = "params";
        jedis.del(key);
        jedis.close();
    }
    public void delTotal() {
        Jedis jedis = jedisPool.getResource();
        String key = "total";
        jedis.del(key);
        jedis.close();
    }

    public List<JSONObject> getParams() {
        Jedis jedis = this.jedisPool.getResource();
        try {
            String key = "params";
            String sets = jedis.get(key);
            if (sets == null || sets.length() == 0) {
                List<HashMap> map = this.systemSetMapper.selectAll();
                if (map == null)
                    return null;
                sets = JSON.toJSONString(map);
                jedis.set(key, sets);
                jedis.expire(key, this.time);
            }
            return JSON.parseObject(sets, List.class);
        } finally {
            jedis.close();
        }
    }

    public void delUser(long id) {
        if (id > 0) {
            Jedis jedis = jedisPool.getResource();
            String key = "user_" + id;
            jedis.del(key);
            jedis.close();
        }
    }

    public User getUser(long id) {
        Jedis jedis = this.jedisPool.getResource();
        try {
            String key = "user_" + id;
            Map map = jedis.hgetAll(key);
            if (map.size() == 0) {
                User user = this.userMapper.selectByPrimaryKey(id);
                if (user == null)
                    return null;
                Map jo = CommonUtil.parseMap(user);
                jedis.hmset(key, jo);
                jedis.expire(key, this.time);
                return user;
            }
            return (User) CommonUtil.parseObject(map, User.class);
        } finally {
            jedis.close();
        }
    }

    public void delSeo() {
        Jedis jedis = jedisPool.getResource();
        String key = "seo";
        jedis.del(key);
        jedis.close();
    }

    public Seo getSeo() {
        Jedis jedis = this.jedisPool.getResource();
        try {
            String key = "seo";
            Map map = jedis.hgetAll(key);
            if (map.size() == 0) {
                Seo seo = this.seoMapper.selectOne();
                if (seo == null)
                    return null;
                Map jo = CommonUtil.parseMap(seo);
                jedis.hmset(key, jo);
                jedis.expire(key, this.time);
                return seo;
            }
            return (Seo) CommonUtil.parseObject(map, Seo.class);
        } finally {
            jedis.close();
        }
    }

    public void delPlatformInfo() {
        Jedis jedis = jedisPool.getResource();
        String key = "platform";
        jedis.del(key);
        jedis.close();
    }

    public PlatformInfo getPlatformInfo() {
        Jedis jedis = this.jedisPool.getResource();
        try {
            String key = "platform";
            Map map = jedis.hgetAll(key);
            if (map.size() == 0) {
                PlatformInfo platform = this.platformInfoMapper.selectOne();
                if (platform == null)
                    return null;
                Map jo = CommonUtil.parseMap(platform);
                jedis.hmset(key, jo);
                jedis.expire(key, this.time);
                return platform;
            }
            return (PlatformInfo) CommonUtil.parseObject(map, PlatformInfo.class);
        } finally {
            jedis.close();
        }
    }

    /**
     * 设置E签宝参数
     * @param key
     * @param code
     */
    public void setDevId(String key, String code, int time) {
    	if(time <= 0){
    		time = this.time;
    	}
        Jedis jedis = this.jedisPool.getResource();
        try {
            jedis.set(key, code);
            jedis.expire(key, time);
        } finally {
            jedis.close();
        }
    }

    /**
     * 防刷
     * @param key
     * @param second 过期时间   //单位秒
     * @return
     */
    public boolean checkFrequentAction(String key,int second) {
        Jedis jedis = this.jedisPool.getResource();
        try {
            key = "tomcat_Frenquent_" + key;
            Long val = jedis.setnx(key, "ok");
            if (val==1) {
                jedis.expire(key,second);
                return true;
            }
            return false;
        } finally {
            jedis.close();
        }
    }
}
