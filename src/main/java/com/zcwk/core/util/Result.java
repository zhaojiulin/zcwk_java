package com.zcwk.core.util;

import java.util.HashMap;

import com.github.pagehelper.PageInfo;

public class Result {
    private HashMap map;

    public Result() {
        this.map = new HashMap();
    }

    public Result Add(String key, Object value) {
        this.map.put(key, value);
        return this;
    }

    public Result Add(Object value) {
        return this;
    }

    public HashMap Success() {
        this.map.put("code", 1);
        this.map.put("message", "");
        return this.map;
    }

    public HashMap Error(String msg) {
        this.map.put("code", -1);
        this.map.put("message", msg);
        return this.map;
    }


    public Result Page(PageInfo page) {
        this.map.put("pageno", Integer.valueOf(page.getPageNum()));
        this.map.put("pagesize", Integer.valueOf(page.getPageSize()));
        this.map.put("total", Long.valueOf(page.getTotal()));
        this.map.put("list", page.getList());
        return this;
    }
}
