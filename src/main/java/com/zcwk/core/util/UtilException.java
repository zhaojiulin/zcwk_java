package com.zcwk.core.util;

public class UtilException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5644984399668046811L;

	public UtilException() {
		
	}

	public UtilException(String smg) {
		super(smg);
	}
}