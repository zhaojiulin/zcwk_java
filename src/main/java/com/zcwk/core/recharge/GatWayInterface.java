package com.zcwk.core.recharge;

import com.zcwk.core.model.Account;
import com.zcwk.core.model.BankCard;
import com.zcwk.core.util.UtilException;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by pwx on 2016/6/15.
 */
@Service
public interface GatWayInterface {

    HashMap<String, String> createGateWay(BigDecimal money, String bank, String orderNo) throws UtilException;

    boolean callGateWay(Map<String, String> params) throws UtilException;

    HashMap<String, String> createPcAuth(long accountId, int route, BigDecimal money, String orderNo) throws Exception;

    HashMap<String, String> createWapAuth(long accountId, int route, BigDecimal money, String orderNo) throws Exception;

    HashMap<String, String> createAppAuth(long accountId, int route, BigDecimal money, String orderNo) throws Exception;

    boolean callPcAuth(Map<String, String> params) throws UtilException, IOException, NoSuchAlgorithmException, InvalidKeySpecException, InvalidKeyException, SignatureException;

    boolean callWapAuth(Map<String, String> params) throws UtilException, IOException;

    boolean callAppAuth(Map<String, String> params) throws UtilException, IOException;

    boolean bindCard(BankCard bankCard) throws Exception;

    boolean subPay(String order, String code) throws Exception;

    String convertBank(String bank);

    String convertCode(String bank);

    boolean checkGatWayOrder(String orderId) throws Exception;

    boolean checkPcAuthOrder(String orderId) throws Exception;

    boolean checkWapAuthOrder(String orderId) throws Exception;

    String cardBinQuery(String cardNo)  throws Exception;

    String directBindCard(BankCard bankCard,String idCard) throws Exception;

    String preCretifyPay(BigDecimal money, String bindId, String orderNo) throws Exception;

    String certainCretifyPay(String businessNo, String smsCode,String orderNo) throws Exception;
}
