package com.zcwk.core.recharge.GatWays;

import com.alibaba.fastjson.JSON;
import com.zcwk.core.dao.BankCardMapper;
import com.zcwk.core.dao.UserMapper;
import com.zcwk.core.model.BankCard;
import com.zcwk.core.model.User;
import com.zcwk.core.recharge.GatWayInterface;
import com.zcwk.core.service.RechargeService;
import com.zcwk.core.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by pwx on 2016/9/18.
 */
@Service("BfGatWay")
public class BfGatWay extends BaseGatWay implements GatWayInterface {
    @Autowired
    RechargeService rechargeService;
    @Autowired
    BankCardMapper bankCardMapper;
    @Autowired
    UserMapper userMapper;
    @Value("${pay.bf_pfx_path}")
    private String pfxPath;
    @Value("${pay.bf_cer_path}")
    private String cerPath;
    @Value("${payApi.bf_apyApi_pfx_path}")
    private String apipfxPath;
    @Value("${payApi.bf_apyApi_cer_path}")
    private String apicerPath;
    @Override
    public HashMap<String, String> createGateWay(BigDecimal money, String bank, String orderNo) throws UtilException {
        super.init(Enums.GatWay.BF.getIndex());
        String merchantID = this.config.get("merchantID");
        String terminalID = this.config.get("terminalID");
        String merOrderNum = orderNo; // 订单号 ---- 支付流水号
        BigDecimal rate = new BigDecimal(100);
        String tranAmt = Integer.toString(money.multiply(rate).intValue());
        String tranDateTime = CommonUtil.simple(new Date());
        String show_url = this.baseUrl + "/account/result/" + orderNo;
        String back_url = this.apiBaseUrl + "/call/bfGateWay";
        HashMap<String, String> args = new HashMap<String, String>();
        args.put("MemberID", merchantID);
        args.put("TerminalID", terminalID);
        args.put("InterfaceVersion", "4.0");
        args.put("KeyType", "1");
        args.put("PayID", this.convertBank(bank));
        args.put("TradeDate", tranDateTime);
        args.put("TransID", orderNo);
        args.put("OrderMoney", tranAmt);
        args.put("PageUrl", show_url);
        args.put("ReturnUrl", back_url);
        args.put("ProductName", "商品名称");
        args.put("Amount", "1");
        args.put("Username", "用户名称");
        args.put("AdditionalInfo","附加信息");
        String MARK="|";
        String md5 =new String(merchantID+MARK+this.convertBank(bank)+MARK+tranDateTime+MARK+orderNo+MARK+tranAmt+MARK+show_url+MARK+back_url+MARK+"1"+MARK+ this.config.get("key"));
        String signValue = MD5Util.MD5(md5);
        args.put("Signature", signValue.toUpperCase());
        args.put("NoticeType", "1");
        args.put("url", this.config.get("url"));
        System.err.println("宝付网银支付请求报文："+args);
        return args;
    }

    @Override
    public boolean callGateWay(Map<String, String> params) throws UtilException {
        super.init(Enums.GatWay.BF.getIndex());
        String TransID = params.get("TransID");
        String MARK = "~|~";
        String MemberID = params.get("MemberID");
        String TerminalID = params.get("TerminalID");
        String Result = params.get("Result");
        String ResultDesc = params.get("ResultDesc");
        String FactMoney = params.get("FactMoney");
        String AdditionalInfo = params.get("AdditionalInfo");
        String SuccTime = params.get("SuccTime");
        String Md5Sign = params.get("Md5Sign");
        System.out.println("宝付网关=======================signPain===================："+params.toString());
        String md5 = "MemberID=" + MemberID + MARK + "TerminalID=" + TerminalID
                + MARK + "TransID=" + TransID + MARK + "Result=" + Result
                + MARK + "ResultDesc=" + ResultDesc + MARK + "FactMoney="
                + FactMoney + MARK + "AdditionalInfo=" + AdditionalInfo + MARK
                + "SuccTime=" + SuccTime + MARK + "Md5Sign=" + this.config.get("key");
        if (MD5Util.MD5(md5).compareTo(Md5Sign) == 0) {
            if (Result.equals("1")) {
                return this.rechargeService.updateOrderSuccess(TransID, Enums.FundType.OnLineRecharge, "在线支付成功");
            }
        }
        return false;
    }

    @Override
    public HashMap<String, String> createPcAuth(long accountId, int route, BigDecimal money, String orderNo) throws Exception {
        BankCard bankCard = this.bankCardMapper.selectByAccount(accountId);
        User user = this.userMapper.selectByAccountId(bankCard.getAccountId());
        super.init(Enums.GatWay.BF.getIndex());
        String acc_no = bankCard.getCardId();//银行卡号
        String pay_code = bankCard.getBank();//银行编码
        String id_holder = user.getRealName();//姓名
        String mobile = bankCard.getMobile();//手机号
        String id_card = user.getIdCard();//身份证号
        BigDecimal txn_amt_num = money.multiply(BigDecimal.valueOf(100)).setScale(0);
        File pfxFile = new File(this.pfxPath);
        if (!pfxFile.exists()) {
            throw new UtilException("私钥文件不存在！");
        }
        String trade_date = CommonUtil.simple(new Date());//交易日期
        String trans_id = orderNo;//商户订单号
        //=================================================
        String version = "4.0.0.0";//版本号
        String terminal_id = this.config.get("authPcTerminalID");    //终端号
        String member_id = this.config.get("merchantID");    //商户号
        String pfxPwd = this.config.get("pfx_key");//商户私钥证书密码
        String txn_sub_type = "03"; //交易子类
        String txn_type = "03311";
        String biz_type = "0000";
        //======================FORM===========================
        String input_charset = "1";
        String language = "1";
        String data_type = "json"; //加密报文的数据类型（xml/json）
        //=====================================================
        String page_url = this.baseUrl + "/account/result/" + orderNo;
        String return_url = this.apiBaseUrl + "/call/bfPcAuth";//商户改成自已的地址。返回方法在returnurl内请参考
        Map<String, String> ArrayData = new HashMap<String, String>();
        ArrayData.put("txn_sub_type", txn_sub_type);
        ArrayData.put("biz_type", biz_type);
        ArrayData.put("terminal_id", terminal_id);
        ArrayData.put("member_id", member_id);
        ArrayData.put("pay_code", pay_code);
        ArrayData.put("acc_no", acc_no);
        ArrayData.put("id_card_type", "01");
        ArrayData.put("id_card", id_card);
        ArrayData.put("id_holder", id_holder);
        ArrayData.put("mobile", mobile);
        ArrayData.put("trans_id", trans_id);
        ArrayData.put("txn_amt", txn_amt_num.toString());
        ArrayData.put("trade_date", trade_date);
        ArrayData.put("commodity_name", "商品名称");
        ArrayData.put("commodity_amount", "1");//商品数量（默认为1）
        ArrayData.put("user_name", "用户名称");
        ArrayData.put("page_url", page_url);
        ArrayData.put("return_url", return_url);
        ArrayData.put("additional_info", "附加信息");
        ArrayData.put("req_reserved", "保留");
        String XmlOrJson = JSON.toJSONString(ArrayData);
        String base64str = SecurityUtil.Base64Encode(XmlOrJson);
        String data_content = RsaCodingUtil.encryptByPriPfxFile(base64str, this.pfxPath, pfxPwd);
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("version", version);
        map.put("input_charset", input_charset);
        map.put("language", language);
        map.put("terminal_id", terminal_id);
        map.put("txn_type", txn_type);
        map.put("txn_sub_type", txn_sub_type);
        map.put("member_id", member_id);
        map.put("data_type", data_type);
        map.put("data_content", data_content);
        map.put("back_url", page_url);
        map.put("url", this.config.get("authPcPostUrl"));
        System.err.println("宝付PC认证支付请求报文："+map);
        return map;
    }

    @Override
    public HashMap<String, String> createWapAuth(long accountId, int route, BigDecimal money, String orderNo) throws Exception {
        BankCard bankCard = this.bankCardMapper.selectByAccount(accountId);
        User user = this.userMapper.selectByAccountId(accountId);
        super.init(Enums.GatWay.BF.getIndex());
        String acc_no = bankCard.getCardId();//银行卡号
        String pay_code = bankCard.getBank();//银行编码
        String id_holder = user.getRealName();//姓名
        String mobile = bankCard.getMobile();//手机号
        String id_card = user.getIdCard();//身份证号
        BigDecimal txn_amt_num = money.multiply(BigDecimal.valueOf(100)).setScale(0);
        File pfxFile = new File(this.pfxPath);
        if (!pfxFile.exists()) {
            throw new UtilException("私钥文件不存在！");
        }
        String trade_date = CommonUtil.simple(new Date());//交易日期
        String trans_id = orderNo;//商户订单号
        //=================================================
        String version = "4.0.0.0";//版本号
        String terminal_id = this.config.get("authPcTerminalID");    //终端号
        String member_id = this.config.get("merchantID");    //商户号
        String pfxPwd = this.config.get("pfx_key");//商户私钥证书密码
        String txn_sub_type = "01"; //交易子类
        String txn_type = "03311";
        String biz_type = "0000";
        //======================FORM===========================
        String input_charset = "1";
        String language = "1";
        String data_type = "json"; //加密报文的数据类型（xml/json）
        //=====================================================
        String page_url = this.baseUrl + "/account/result/" + orderNo;
        String return_url = this.apiBaseUrl + "/call/bfWapAuth";//商户改成自已的地址。返回方法在returnurl内请参考
        Map<String, String> ArrayData = new HashMap<String, String>();
        ArrayData.put("txn_sub_type", txn_sub_type);
        ArrayData.put("biz_type", biz_type);
        ArrayData.put("terminal_id", terminal_id);
        ArrayData.put("member_id", member_id);
        ArrayData.put("pay_code", pay_code);
        ArrayData.put("acc_no", acc_no);
        ArrayData.put("id_card_type", "01");
        ArrayData.put("id_card", id_card);
        ArrayData.put("id_holder", id_holder);
        ArrayData.put("mobile", mobile);
        ArrayData.put("trans_id", trans_id);
        ArrayData.put("txn_amt", txn_amt_num.toString());
        ArrayData.put("trade_date", trade_date);
        ArrayData.put("commodity_name", "商品名称");
        ArrayData.put("commodity_amount", "1");//商品数量（默认为1）
        ArrayData.put("user_name", "用户名称");
        ArrayData.put("page_url", page_url);
        ArrayData.put("return_url", return_url);
        ArrayData.put("additional_info", "附加信息");
        ArrayData.put("req_reserved", "保留");
        String XmlOrJson = JSON.toJSONString(ArrayData);
        String base64str = SecurityUtil.Base64Encode(XmlOrJson);
        String data_content = RsaCodingUtil.encryptByPriPfxFile(base64str, this.pfxPath, pfxPwd);
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("version", version);
        map.put("input_charset", input_charset);
        map.put("language", language);
        map.put("terminal_id", terminal_id);
        map.put("txn_type", txn_type);
        map.put("txn_sub_type", txn_sub_type);
        map.put("member_id", member_id);
        map.put("data_type", data_type);
        map.put("data_content", data_content);
        map.put("back_url", page_url);
        map.put("url", this.config.get("authWapPostUrl"));
        System.err.println("宝付WAP认证支付请求报文："+map);
        return map;
    }

    @Override
    public HashMap<String, String> createAppAuth(long accountId, int route, BigDecimal money, String orderNo) throws Exception {
        BankCard bankCard = this.bankCardMapper.selectByAccount(accountId);
        User user = this.userMapper.selectByAccountId(accountId);
        super.init(Enums.GatWay.BF.getIndex());
        String acc_no = bankCard.getCardId();//银行卡号
        String pay_code = bankCard.getBank();//银行编码
        String id_holder = user.getRealName();//姓名
        String mobile = bankCard.getMobile();//手机号
        String id_card = user.getIdCard();//身份证号
        BigDecimal txn_amt_num = money.multiply(BigDecimal.valueOf(100)).setScale(0);
        File pfxFile = new File(this.pfxPath);
        if (!pfxFile.exists()) {
            throw new UtilException("私钥文件不存在！");
        }
        String trade_date = CommonUtil.simple(new Date());//交易日期
        String trans_id = orderNo;//商户订单号
        //=================================================
        String version = "4.0.0.0";//版本号
        String terminal_id = this.config.get("authPcTerminalID");    //终端号
        String member_id = this.config.get("merchantID");    //商户号
        String pfxPwd = this.config.get("pfx_key");//商户私钥证书密码
        String txn_sub_type = "01"; //交易子类
        String txn_type = "03311";
        String biz_type = "0000";
        //======================FORM===========================
        String input_charset = "1";
        String language = "1";
        String data_type = "json"; //加密报文的数据类型（xml/json）
        //=====================================================
        String page_url = this.baseUrl + "/api/result/" + orderNo;
        String return_url = this.apiBaseUrl + "/call/bfAppAuth";//商户改成自已的地址。返回方法在returnurl内请参考
        Map<String, String> ArrayData = new HashMap<String, String>();
        ArrayData.put("txn_sub_type", txn_sub_type);
        ArrayData.put("biz_type", biz_type);
        ArrayData.put("terminal_id", terminal_id);
        ArrayData.put("member_id", member_id);
        ArrayData.put("pay_code", pay_code);
        ArrayData.put("acc_no", acc_no);
        ArrayData.put("id_card_type", "01");
        ArrayData.put("id_card", id_card);
        ArrayData.put("id_holder", id_holder);
        ArrayData.put("mobile", mobile);
        ArrayData.put("trans_id", trans_id);
        ArrayData.put("txn_amt", txn_amt_num.toString());
        ArrayData.put("trade_date", trade_date);
        ArrayData.put("commodity_name", "商品名称");
        ArrayData.put("commodity_amount", "1");//商品数量（默认为1）
        ArrayData.put("user_name", "用户名称");
        ArrayData.put("page_url", page_url);
        ArrayData.put("return_url", return_url);
        ArrayData.put("additional_info", "附加信息");
        ArrayData.put("req_reserved", "保留");
        String XmlOrJson = JSON.toJSONString(ArrayData);
        String base64str = SecurityUtil.Base64Encode(XmlOrJson);
        String data_content = RsaCodingUtil.encryptByPriPfxFile(base64str, this.pfxPath, pfxPwd);
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("version", version);
        map.put("input_charset", input_charset);
        map.put("language", language);
        map.put("terminal_id", terminal_id);
        map.put("txn_type", txn_type);
        map.put("txn_sub_type", txn_sub_type);
        map.put("member_id", member_id);
        map.put("data_type", data_type);
        map.put("data_content", data_content);
        map.put("back_url", page_url);
        map.put("url", this.config.get("authWapPostUrl"));
        System.err.println("宝付APP认证支付请求报文："+map);
        return map;
    }

    @Override
    public boolean callPcAuth(Map<String, String> params) throws UtilException, IOException {
        String data_content = params.get("data_content");
        if (data_content.isEmpty()) {
            throw new UtilException("返回数据为空.");
        }
        System.out.println("=====返回数据:" + data_content);
        File cerFile = new File(this.cerPath);
        if (!cerFile.exists()) {
            throw new UtilException("宝付公钥文件不存在!.");
        }
        data_content = RsaCodingUtil.decryptByPubCerFile(data_content, this.cerPath);
        if (data_content.isEmpty()) {
            throw new UtilException("解密公钥是否正确!.");
        }
        data_content = SecurityUtil.Base64Decode(data_content);
        Map<String, String> ArrayData = CommonUtil.parseMap(data_content);//将JSON转化为Map对象。
        System.out.println("宝付PC=======================signPain===================："+ArrayData.toString());
        if (!ArrayData.containsKey("resp_code")) {
            return false;
        } else {
            String resp_code = ArrayData.get("resp_code").toString();
            if (resp_code.equals("0000")) {
                String TransID = ArrayData.get("trans_id");
                return this.rechargeService.updateOrderSuccess(TransID, Enums.FundType.OnLineRecharge, "在线支付成功");//处理完成在页面输出OK（必须）
            }
        }
        return false;
    }

    @Override
    public boolean callWapAuth(Map<String, String> params) throws UtilException, IOException {
        String data_content = params.get("data_content");//回调参数
        if (data_content.isEmpty()) {
            throw new UtilException("返回数据为空.");
        }
        File cerFile = new File(this.cerPath);
        if (!cerFile.exists()) {
            throw new UtilException("宝付公钥文件不存在!.");
        }
        data_content = RsaCodingUtil.decryptByPubCerFile(data_content, this.cerPath);
        if (data_content.isEmpty()) {
            throw new UtilException("解密公钥是否正确!.");
        }
        data_content = SecurityUtil.Base64Decode(data_content);
        Map<String, String> ArrayData = CommonUtil.parseMap(data_content);//将JSON转化为Map对象。
        System.out.println("宝付Wap=======================signPain===================："+ArrayData.toString());
        if (!ArrayData.containsKey("resp_code")) {
            return false;
        } else {
            String resp_code = ArrayData.get("resp_code").toString();
            if (resp_code.equals("0000")) {
                String TransID = ArrayData.get("trans_id");
                return this.rechargeService.updateOrderSuccess(TransID, Enums.FundType.OnLineRecharge, "在线支付成功");//处理完成在页面输出OK（必须）
            }
        }
        return false;
    }

    @Override
    public boolean callAppAuth(Map<String, String> params) throws UtilException, IOException {
        String data_content = params.get("data_content");//回调参数
        if (data_content.isEmpty()) {
            throw new UtilException("返回数据为空.");
        }
        File cerFile = new File(this.cerPath);
        if (!cerFile.exists()) {
            throw new UtilException("宝付公钥文件不存在!.");
        }
        data_content = RsaCodingUtil.decryptByPubCerFile(data_content, this.cerPath);
        if (data_content.isEmpty()) {
            throw new UtilException("解密公钥是否正确!.");
        }
        data_content = SecurityUtil.Base64Decode(data_content);
        Map<String, String> ArrayData = CommonUtil.parseMap(data_content);//将JSON转化为Map对象。
        System.out.println("宝付APP=======================signPain===================："+ArrayData.toString());
        if (!ArrayData.containsKey("resp_code")) {
            return false;
        } else {
            String resp_code = ArrayData.get("resp_code").toString();
            if (resp_code.equals("0000")) {
                String TransID = ArrayData.get("trans_id");
                return this.rechargeService.updateOrderSuccess(TransID, Enums.FundType.OnLineRecharge, "在线支付成功");//处理完成在页面输出OK（必须）
            }
        }
        return false;
    }

    @Override
    public boolean bindCard(BankCard bankCard) {
        return false;
    }

    @Override
    public boolean subPay(String order, String code) {
        return false;
    }

    @Override
    public String convertBank(String bank) {
        Map<String, String> map = new HashMap<>();
        map.put("CMB", "3001");
        map.put("ICBC", "3002");
        map.put("CCB", "3003");
        map.put("SPDB", "3004");
        map.put("ABC", "3005");
        map.put("CMBC", "3006");
        map.put("CIB", "3009");
        map.put("BOCOM", "3020");
        map.put("CEB", "3022");
        map.put("BOCSH", "3026");
        map.put("BCCB", "3032");
        map.put("PAB", "3035");
        map.put("GDB", "3036");
        map.put("CNCB", "3039");
        map.put("BOS", "3059");
        map.put("PSBC", "3038");
        map.put("HXB", "3050");
        map.put("SRCB", "3037");
        return map.get(bank);
    }

    @Override
    public String convertCode(String bank) {
        return "";
    }

    @Override
    public boolean checkGatWayOrder(String orderId) throws Exception {
        super.init(Enums.GatWay.BF.getIndex());;
        //测试：https://tgw.baofoo.com/order/query
        //正式：https://gw.baofoo.com/order/query
        String postUrl="https://gw.baofoo.com/order/query";
        String terminal_id = this.config.get("terminalID");    //终端号
        String member_id = this.config.get("merchantID");    //商户号
        String MD5Sign=MD5Util.MD5(member_id+"|"+terminal_id+"|"+orderId+"|"+this.config.get("key"));
        String rsJson=UtilHttp.sendPost(postUrl, "MemberID=" +member_id+"&TerminalID="+terminal_id+"&TransID="+orderId+"&MD5Sign="+MD5Sign);
        System.err.println("宝付网关补单响应报文："+rsJson);
        String[] result = rsJson.split("\\|");//Y：成功 F：失败 P：处理中 N：没有订单
        if(result!=null && result.length==7 && result[3].equals("Y")){
            return true;
        }else{
            return false;
        }
    }

    @Override
    public boolean checkPcAuthOrder(String orderId) throws Exception {
        super.init(Enums.GatWay.BF.getIndex());
        String data_content="";
        //正式：https://gw.baofoo.com/apipay/queryQuickOrder
        //测试：https://tgw.baofoo.com/apipay/queryQuickOrder
        String postUrl="https://gw.baofoo.com/apipay/queryQuickOrder";
        File pfxFile = new File(this.pfxPath);
        if (!pfxFile.exists()) {
            throw new UtilException("私钥文件不存在！");
        }
        String terminal_id = this.config.get("authPcTerminalID");    //终端号
        String member_id = this.config.get("merchantID");    //商户号
        String pfxKey = this.config.get("pfx_key");//商户私钥证书密码
        Map<String, String> ArrayData = new HashMap<String, String>();
        ArrayData.put("member_id", member_id);
        ArrayData.put("terminal_id", terminal_id);
        ArrayData.put("orig_trans_id",orderId );
        ArrayData.put("trans_serial_no", ""+System.currentTimeMillis());
        ArrayData.put("additional_info", "附加字段");
        ArrayData.put("req_reserved", "保留域");
        String XmlOrJson = JSON.toJSONString(ArrayData);
        System.err.println("宝付PC补单请求报文："+XmlOrJson);
        String base64str = SecurityUtil.Base64Encode(XmlOrJson);
        data_content = RsaCodingUtil.encryptByPriPfxFile(base64str, this.pfxPath, pfxKey);
        String rsJson=UtilHttp.sendPost(postUrl, "member_id=" +member_id+"&terminal_id="+terminal_id+"&data_type=json&version=4.0.0.1&input_charset=1&language=1&data_content="+data_content);
        File cerFile = new File(this.cerPath);
        if (!cerFile.exists()) {
            throw new UtilException("宝付公钥文件不存在！");
        }
        String result = RsaCodingUtil.decryptByPubCerFile(rsJson, this.cerPath);
        result = SecurityUtil.Base64Decode(result);
        Map<String, String> map = CommonUtil.parseMap(result);
        System.err.println("宝付PC补单响应报文："+map);
        String retCode = map.get("resp_code").toString();
        if(map!=null && map.get("resp_code")!=null && map.get("resp_code").equals("0000")){
            return true;
        }else {
            return false;
        }
    }

    @Override
    public boolean checkWapAuthOrder(String orderId) throws Exception {
        super.init(Enums.GatWay.BF.getIndex());
        String data_content="";
        //正式：https://gw.baofoo.com/apipay/queryQuickOrder
        //测试：https://tgw.baofoo.com/apipay/queryQuickOrder
        String postUrl="https://gw.baofoo.com/apipay/queryQuickOrder";
        File pfxFile = new File(this.pfxPath);
        if (!pfxFile.exists()) {
            throw new UtilException("私钥文件不存在！");
        }
        String terminal_id = this.config.get("authPcTerminalID");    //终端号
        String member_id = this.config.get("merchantID");    //商户号
        String pfxKey = this.config.get("pfx_key");//商户私钥证书密码
        Map<String, String> ArrayData = new HashMap<String, String>();
        ArrayData.put("member_id", member_id);
        ArrayData.put("terminal_id", terminal_id);
        ArrayData.put("orig_trans_id",orderId );
        ArrayData.put("trans_serial_no", ""+System.currentTimeMillis());
        ArrayData.put("additional_info", "附加字段");
        ArrayData.put("req_reserved", "保留域");
        String XmlOrJson = JSON.toJSONString(ArrayData);
        System.err.println("宝付Wap补单请求报文："+XmlOrJson);
        String base64str = SecurityUtil.Base64Encode(XmlOrJson);
        data_content = RsaCodingUtil.encryptByPriPfxFile(base64str, this.pfxPath, pfxKey);
        String rsJson=UtilHttp.sendPost(postUrl, "member_id=" +member_id+"&terminal_id="+terminal_id+"&data_type=json&version=4.0.0.1&input_charset=1&language=1&data_content="+data_content);
        File cerFile = new File(this.cerPath);
        if (!cerFile.exists()) {
            throw new UtilException("宝付公钥文件不存在！");
        }
        String result = RsaCodingUtil.decryptByPubCerFile(rsJson, this.cerPath);
        result = SecurityUtil.Base64Decode(result);
        Map<String, String> map = CommonUtil.parseMap(result);
        System.err.println("宝付Wap补单响应报文："+map);
        String retCode = map.get("resp_code").toString();
        if(map!=null && map.get("resp_code")!=null && map.get("resp_code").equals("0000")){
            return true;
        }else {
            return false;
        }
    }

    /**
     *  卡Bin查询接口
     * @param cardNo
     * @return
     * @throws Exception
     */
    public String cardBinQuery(String cardNo)  throws Exception{
        super.init(Enums.GatWay.BF.getIndex());
        //测试 http://tgw.baofoo.com/product/bankcard/v1/bin/info
        //正式:https://api.xinyan.com/product/bankcard/v1/bin/info
        String cardBinUrl="https://api.xinyan.com/product/bankcard/v1/bin/info";
        String merchantID = this.config.get("merchantID");
        String terminalID = this.config.get("terminalID");
        String pfxKey = this.config.get("pfx_key");
        String data_content="";
        String trans_id=""+System.currentTimeMillis();
        File pfxFile = new File(this.pfxPath);
        if (!pfxFile.exists()) {
            throw new UtilException("私钥文件不存在！");
        }
        Map<String, String> ArrayData = new HashMap<String, String>();
        ArrayData.put("member_id", merchantID);
        ArrayData.put("terminal_id", terminalID);
        ArrayData.put("trans_id",trans_id );
        ArrayData.put("trade_date", CommonUtil.getFormatTime(new Date(), "yyyyMMddHHmmss"));
        ArrayData.put("industry_type", "B2");
        ArrayData.put("card_no", cardNo);
        String XmlOrJson = JSON.toJSONString(ArrayData);
        System.err.println("宝付卡Bin请求报文："+XmlOrJson);
        String base64str = SecurityUtil.Base64Encode(XmlOrJson);
        data_content = RsaCodingUtil.encryptByPriPfxFile(base64str, this.pfxPath, pfxKey);
        String rsJson=UtilHttp.sendPost(cardBinUrl, "member_id=" +merchantID+"&terminal_id="+terminalID+"&data_type=json&data_content="+data_content);
        System.err.println("宝付卡Bin响应报文："+rsJson);
        Map<String, String> map=CommonUtil.parseMap(rsJson);
        if(map!=null && map.get("success")!=null && map.get("success").equals("true")  && map.get("data")!=null){
            Map<String, String> data=CommonUtil.parseMap(map.get("data"));
            if(data!=null &&  data.get("bank_id")!=null){
                if(data.get("card_type").equals("1")){
                    if(data.get("pay_id").equals("-1")){
                        throw new UtilException("该卡不支持认证支付.");
                    }else {
                        return data.get("bank_id");
                    }
                }else{
                    throw new UtilException("只支持借记卡.");
                }
            }else{
                throw new UtilException("信息解析错误!");
            }
        }else{
            throw new UtilException("未找到卡bin信息!");
        }
    }

    //直接绑卡
    public String directBindCard(BankCard bankCard,String idCard) throws Exception{
        super.init(Enums.GatWay.BF.getIndex());
        String apiPayPostUrl=this.config.get("apiPayPostUrl");
        if(apiPayPostUrl==null || apiPayPostUrl.isEmpty()){
            apiPayPostUrl="https://public.baofoo.com/cutpayment/api/backTransRequest";
        }
        String merchantID = this.config.get("merchantID");
        String terminalID = this.config.get("terminalApiID");
        String pfxKey = this.config.get("pfx_ApiKey");
        String data_content="";
        String trans_id=""+System.currentTimeMillis();
        File pfxFile = new File(this.apipfxPath);
        if (!pfxFile.exists()) {
            throw new UtilException("私钥文件不存在！");
        }
        Map<String, String> ArrayData = new HashMap<String, String>();
        ArrayData.put("trans_serial_no",bankCard.getAccountId()+trans_id );
        ArrayData.put("trans_id",merchantID+bankCard.getAccountId()+trans_id);
        ArrayData.put("txn_sub_type", "01");
        ArrayData.put("biz_type","0000");
        ArrayData.put("member_id", merchantID);
        ArrayData.put("terminal_id", terminalID);
        ArrayData.put("acc_no",bankCard.getCardId());
        ArrayData.put("id_card_type","01");
        ArrayData.put("id_card",idCard);
        ArrayData.put("valid_date","");
        ArrayData.put("valid_no","");
        ArrayData.put("id_holder",bankCard.getAccountName());
        ArrayData.put("mobile",bankCard.getMobile());
        ArrayData.put("trade_date", CommonUtil.getFormatTime(new Date(), "yyyyMMddHHmmss"));
        ArrayData.put("pay_code",bankCard.getBank());
        ArrayData.put("additional_info","附加字段");
        ArrayData.put("req_reserved","");
        String XmlOrJson = JSON.toJSONString(ArrayData);
        System.err.println("宝付绑卡请求报文："+XmlOrJson);
        String base64str = SecurityUtil.Base64Encode(XmlOrJson);
        data_content = RsaCodingUtil.encryptByPriPfxFile(base64str, this.apipfxPath, pfxKey);
        String rsJson=UtilHttp.sendPost(apiPayPostUrl, "member_id=" +merchantID+"&terminal_id="+terminalID+"&data_type=json&version=4.0.0.0&txn_type=0431&txn_sub_type=01&data_content="+data_content);
        File cerFile = new File(this.apicerPath);
        if (!cerFile.exists()) {
            throw new UtilException("宝付公钥文件不存在！");
        }
        String result = RsaCodingUtil.decryptByPubCerFile(rsJson, this.apicerPath);
        result = SecurityUtil.Base64Decode(result);
        System.err.println("宝付绑卡响应报文："+result);
        Map<String, String> map=CommonUtil.parseMap(result);
        if(map!=null && map.get("resp_code")!=null && map.get("resp_code").equals("0000") && map.get("bind_id")!=null){
            return map.get("bind_id");
        }else{
            throw new UtilException("绑卡失败.");
        }
    }

    //预支付
    public String preCretifyPay(BigDecimal money, String bindId, String orderNo) throws Exception{
        super.init(Enums.GatWay.BF.getIndex());
        String apiPayPostUrl=this.config.get("apiPayPostUrl");
        if(apiPayPostUrl==null || apiPayPostUrl.isEmpty()){
            apiPayPostUrl="https://public.baofoo.com/cutpayment/api/backTransRequest";
        }
        String merchantID = this.config.get("merchantID");
        String terminalID = this.config.get("terminalApiID");
        String pfxKey = this.config.get("pfx_ApiKey");
        String data_content="";
        File pfxFile = new File(this.apipfxPath);
        if (!pfxFile.exists()) {
            throw new UtilException("私钥文件不存在！");
        }
        Map<String, Object> ArrayData = new HashMap<String, Object>();
        ArrayData.put("trans_serial_no",merchantID+orderNo);
        ArrayData.put("trans_id",orderNo);
        ArrayData.put("txn_sub_type", "15");
        ArrayData.put("biz_type","0000");
        ArrayData.put("member_id", merchantID);
        ArrayData.put("terminal_id", terminalID);
        ArrayData.put("bind_id",bindId);
        ArrayData.put("txn_amt", money.multiply(BigDecimal.valueOf(100)).toString());
        ArrayData.put("trade_date", CommonUtil.getFormatTime(new Date(), "yyyyMMddHHmmss"));
        ArrayData.put("additional_info","附加字段");
        ArrayData.put("req_reserved","");
        Map<String,String> ClientIp = new HashMap<String,String>();
        ClientIp.put("client_ip", "100.0.0.0");
        ArrayData.put("risk_content",ClientIp);

        String XmlOrJson = JSON.toJSONString(ArrayData);
        System.err.println("宝付预充值请求报文："+XmlOrJson);
        String base64str = SecurityUtil.Base64Encode(XmlOrJson);
        data_content = RsaCodingUtil.encryptByPriPfxFile(base64str, this.apipfxPath, pfxKey);
        String rsJson=UtilHttp.sendPost(apiPayPostUrl, "member_id=" +merchantID+"&terminal_id="+terminalID+"&data_type=json&version=4.0.0.0&txn_type=0431&txn_sub_type=15&data_content="+data_content);
        File cerFile = new File(this.apicerPath);
        if (!cerFile.exists()) {
            throw new UtilException("宝付公钥文件不存在！");
        }
        String result = RsaCodingUtil.decryptByPubCerFile(rsJson, this.apicerPath);
        result = SecurityUtil.Base64Decode(result);
        System.err.println("宝付预充值响应报文："+result);
        Map<String, String> map=CommonUtil.parseMap(result);
        if(map!=null && map.get("resp_code")!=null && map.get("resp_code").equals("0000") && map.get("business_no")!=null){
            return map.get("business_no");
        }else{
            throw new UtilException("宝付预充值失败.");
        }
    }

    //预支付
    public String certainCretifyPay(String businessNo, String smsCode,String orderNo) throws Exception{
        super.init(Enums.GatWay.BF.getIndex());
        String apiPayPostUrl=this.config.get("apiPayPostUrl");
        if(apiPayPostUrl==null || apiPayPostUrl.isEmpty()){
            apiPayPostUrl="https://public.baofoo.com/cutpayment/api/backTransRequest";
        }
        String merchantID = this.config.get("merchantID");
        String terminalID = this.config.get("terminalApiID");
        String pfxKey = this.config.get("pfx_ApiKey");
        String data_content="";
        File pfxFile = new File(this.apipfxPath);
        if (!pfxFile.exists()) {
            throw new UtilException("私钥文件不存在！");
        }
        Map<String, Object> ArrayData = new HashMap<String, Object>();
        ArrayData.put("trans_serial_no","C"+merchantID+orderNo);
        ArrayData.put("business_no",businessNo);
        ArrayData.put("txn_sub_type", "16");
        ArrayData.put("biz_type","0000");
        ArrayData.put("member_id", merchantID);
        ArrayData.put("terminal_id", terminalID);
        ArrayData.put("sms_code",smsCode);
        ArrayData.put("trade_date", CommonUtil.getFormatTime(new Date(), "yyyyMMddHHmmss"));
        ArrayData.put("additional_info","附加字段");
        ArrayData.put("req_reserved","");
        String XmlOrJson = JSON.toJSONString(ArrayData);
        System.err.println("宝付确定充值请求报文："+XmlOrJson);
        String base64str = SecurityUtil.Base64Encode(XmlOrJson);
        data_content = RsaCodingUtil.encryptByPriPfxFile(base64str, this.apipfxPath, pfxKey);
        System.err.println("postUrl:"+apiPayPostUrl);
        String param="member_id=" +merchantID+"&terminal_id="+terminalID+"&data_type=json&version=4.0.0.0&txn_type=0431&txn_sub_type=16&data_content="+data_content;
        System.err.println("param:"+param);
        String rsJson=UtilHttp.sendPost(apiPayPostUrl, param);
        File cerFile = new File(this.apicerPath);
        if (!cerFile.exists()) {
            throw new UtilException("宝付公钥文件不存在！");
        }
        String result = RsaCodingUtil.decryptByPubCerFile(rsJson, this.apicerPath);
        result = SecurityUtil.Base64Decode(result);
        System.err.println("宝付确定充值响应报文："+result);
        Map<String, String> map=CommonUtil.parseMap(result);
        if(map!=null && map.get("resp_code")!=null && map.get("resp_code").equals("0000") && map.get("business_no")!=null){
            this.rechargeService.updateOrderSuccess(orderNo, Enums.FundType.OnLineRecharge, "在线支付成功");
            return map.get("business_no");
        }else {
            throw new UtilException("宝付确定充值失败.");
        }
    }

}
