package com.zcwk.core.recharge.GatWays;

import com.zcwk.core.model.BankCard;
import com.zcwk.core.recharge.GatWayInterface;
import com.zcwk.core.service.RechargeService;
import com.zcwk.core.util.CommonUtil;
import com.zcwk.core.util.Enums;
import com.zcwk.core.util.MD5Util;
import com.zcwk.core.util.UtilException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by pwx on 2016/9/18.
 */
@Service("HcGatWay")
public class HcGatWay extends BaseGatWay implements GatWayInterface {
    @Autowired
    RechargeService rechargeService;

    @Override
    public HashMap<String, String> createGateWay(BigDecimal money, String bank, String orderNo) throws UtilException {
        HashMap<String, String> args = new HashMap<String, String>();
        super.init(Enums.GatWay.HC.getIndex());
        String merNo = this.config.get("merNo");
        String returnUrl = this.baseUrl + "/account/result/" + orderNo;
        String key = this.config.get("key");
        DecimalFormat currentNumberFormat = new DecimalFormat("#0.00");
        String amount = currentNumberFormat.format(money);
        String time = CommonUtil.simple(new Date());
        String sign = MD5Util.MD5("MerNo=" + merNo + "&" + "BillNo=" + orderNo + "&" + "Amount=" + amount + "&" + "OrderTime=" + time + "&" + "ReturnURL=" + returnUrl + "&" + "AdviceURL=" + (this.baseUrl + "/call/hcAuth") + "&" + key).toUpperCase();
        args.put("url", this.config.get("url"));
        args.put("MerNo", merNo);
        args.put("BillNo", orderNo);
        args.put("Amount", amount);
        args.put("ReturnURL", returnUrl);
        args.put("AdviceURL", this.apiBaseUrl + "/call/hcGatWay");
        args.put("SignInfo", sign);
        args.put("Remark", "");
        args.put("payType", "B2CDebit");
        args.put("defaultBankNumber", "");
        args.put("OrderTime", time);
        args.put("products", "");
        return args;
    }

    @Override
    public boolean callGateWay(Map<String, String> params) throws UtilException {
        super.init(Enums.GatWay.HC.getIndex());
        String merNo = this.config.get("merNo");
        String billNo = params.get("BillNo");
        String succeed = params.get("Succeed");
        String amount = params.get("Amount");
        String orderNo = params.get("OrderNo");
        String signMD5info = params.get("SignInfo");
        String sign = MD5Util.MD5("MerNo=" + merNo + "&" + "BillNo=" + billNo + "&" + "OrderNo=" + orderNo + "&" + "Amount=" + amount + "&" + "Succeed=" + succeed + "&" + this.config.get("key")).toUpperCase();
        if (signMD5info.equals(sign) && succeed.equals("88")) {
            return this.rechargeService.updateOrderSuccess(billNo, Enums.FundType.OnLineRecharge, "在线支付成功");
        }
        return false;
    }

    @Override
    public HashMap<String, String> createPcAuth(long accountId, int route, BigDecimal money, String orderNo) throws Exception {
        return null;
    }

    @Override
    public HashMap<String, String> createWapAuth(long accountId, int route, BigDecimal money, String orderNo) throws Exception {
        return null;
    }

    @Override
    public HashMap<String, String> createAppAuth(long accountId, int route, BigDecimal money, String orderNo) throws Exception {
        return null;
    }

    @Override
    public boolean callPcAuth(Map<String, String> params) throws UtilException, IOException {
        return false;
    }

    @Override
    public boolean callWapAuth(Map<String, String> params) throws UtilException {
        return false;
    }

    @Override
    public boolean callAppAuth(Map<String, String> params) throws UtilException {
        return false;
    }

    @Override
    public boolean bindCard(BankCard bankCard) {
        return false;
    }

    @Override
    public boolean subPay(String order, String code) {
        return false;
    }

    @Override
    public String convertBank(String bank) {
        return bank;
    }

    @Override
    public String convertCode(String bank) {
        return "";
    }

    @Override
    public boolean checkGatWayOrder(String orderId) throws Exception {
        return false;
    }

    @Override
    public boolean checkPcAuthOrder(String orderId) throws Exception {
        return false;
    }

    @Override
    public boolean checkWapAuthOrder(String orderId) throws Exception {
        return false;
    }

    /**
     *  卡Bin查询接口
     * @param cardNo
     * @return
     * @throws Exception
     */
    public String cardBinQuery(String cardNo)  throws Exception{
        return "";
    }

    public String directBindCard(BankCard bankCard,String idCard) throws Exception{
        return "";
    }

    public String preCretifyPay(BigDecimal money, String bindId, String orderNo) throws Exception{
        return "";
    }

    public String certainCretifyPay(String businessNo, String smsCode,String orderNo) throws Exception{
        return "";
    }
}
