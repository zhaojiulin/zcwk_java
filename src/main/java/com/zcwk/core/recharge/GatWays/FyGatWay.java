package com.zcwk.core.recharge.GatWays;

import com.zcwk.core.dao.BankCardMapper;
import com.zcwk.core.dao.RechargeMapper;
import com.zcwk.core.dao.UserMapper;
import com.zcwk.core.model.BankCard;
import com.zcwk.core.model.Recharge;
import com.zcwk.core.model.User;
import com.zcwk.core.recharge.GatWayInterface;
import com.zcwk.core.recharge.Util.DESCoderFUIOU;
import com.zcwk.core.recharge.Util.FyData.*;
import com.zcwk.core.recharge.Util.HttpPostUtil;
import com.zcwk.core.recharge.Util.XmlBeanUtils;
import com.zcwk.core.service.RechargeService;
import com.zcwk.core.util.*;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.io.IOException;
import java.io.StringReader;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by pwx on 2016/11/24.
 */
@Service("FyGatWay")
public class FyGatWay extends BaseGatWay implements GatWayInterface {
    @Autowired
    RechargeMapper rechargeMapper;
    @Autowired
    RechargeService rechargeService;
    @Autowired
    BankCardMapper bankCardMapper;
    @Autowired
    UserMapper userMapper;
    @Autowired
    UtilCache cache;


    @Override
    public HashMap<String, String> createGateWay(BigDecimal money, String bank, String orderNo) throws UtilException {
        super.init(Enums.GatWay.FY.getIndex());
        String mchnt_cd = this.config.get("gatWayMchntCd");
        String order_id = orderNo;
        String order_amt = money.multiply(BigDecimal.valueOf(100)).toString();
        String order_pay_type = "B2C";
        String page_notify_url = this.baseUrl + "/account/result/" + orderNo;
        String back_notify_url = this.baseUrl + "/call/fyGateWay";
        String order_valid_time = "10m";
        String iss_ins_cd = convertBank(bank);
        String goods_name = "";
        String goods_display_url = "";
        String mchnt_key = this.config.get("gatWayKey"); //32位的商户密钥
        String rem = "";
        String ver = "1.0.1";
        String signDataStr = mchnt_cd + "|" + order_id + "|" + order_amt + "|" + order_pay_type + "|" +
                page_notify_url + "|" + back_notify_url + "|" + order_valid_time + "|" +
                iss_ins_cd + "|" + goods_name + "|" + goods_display_url + "|"
                + rem + "|" + ver + "|" + mchnt_key;
        String md5 = MD5Util.MD5(signDataStr);
        System.out.println(signDataStr);
        HashMap<String, String> args = new HashMap<String, String>();
        args.put("md5", md5);
        args.put("mchnt_cd", mchnt_cd);
        args.put("order_id", order_id);
        args.put("order_amt", order_amt);
        args.put("order_pay_type", order_pay_type);
        args.put("page_notify_url", page_notify_url);
        args.put("back_notify_url", back_notify_url);
        args.put("order_valid_time", order_valid_time);
        args.put("iss_ins_cd", iss_ins_cd);
        args.put("goods_name", goods_name);
        args.put("goods_display_url", goods_display_url);
        args.put("rem", rem);
        args.put("ver", ver);
        args.put("url", this.config.get("gatWayUrl"));
        return args;
    }

    @Override
    public boolean callGateWay(Map<String, String> params) throws UtilException {
        super.init(Enums.GatWay.FY.getIndex());
        String mchnt_cd = this.config.get("gatWayMchntCd");
        String order_id = params.get("order_id");
        String order_amt = params.get("order_amt");
        String order_date = params.get("order_date");
        String order_st = params.get("order_st");
        String order_pay_code = params.get("order_pay_code");
        String order_pay_error = params.get("order_pay_error");
        String fy_ssn = params.get("fy_ssn");
        String resv1 = params.get("resv1");
        String md5 = params.get("md5");
        String mchnt_key = this.config.get("gatWayKey");  //32位的商户密钥
        String signDataStr = mchnt_cd + "|" +
                order_id + "|" + order_date + "|" + order_amt + "|" + order_st + "|" + order_pay_code + "|" +
                order_pay_error + "|" + resv1 + "|" +
                fy_ssn + "|" + mchnt_key;
        System.err.println("=======================signDataStr===================" + signDataStr);
        if (MD5Util.MD5(signDataStr).compareTo(md5) == 0) {
            if (order_pay_code.equals("0000")) {
                return this.rechargeService.updateOrderSuccess(order_id, Enums.FundType.OnLineRecharge, "在线支付成功");
            }
        }
        return false;
    }

    @Override
    public HashMap<String, String> createPcAuth(long accountId, int route, BigDecimal money, String orderNo) throws Exception {
        BankCard bankCard = this.bankCardMapper.selectByAccount(accountId);
        User user = this.userMapper.selectByAccountId(bankCard.getAccountId());
        super.init(Enums.GatWay.FY.getIndex());
        String mchnt_cd = this.config.get("authPcMchntCd");
        String order_id = orderNo;
        String order_amt = money.multiply(BigDecimal.valueOf(100)).toString();
        String user_id = user.getId().toString();
        String card_no = bankCard.getCardId();
        String page_notify_url = this.baseUrl + "/account/result/" + orderNo;
        String cert_type = "0";
        String cert_no = user.getIdCard();
        String user_type = "0";
        String cardholder_name = bankCard.getAccountName();
        String back_notify_url = this.apiBaseUrl + "/call/fyPcAuth";
        String pri_key = this.config.get("authPcPrivateKey");
        String signDataStr = mchnt_cd + "|" + user_id + "|" + order_id + "|" + order_amt + "|" +
                card_no + "|" + cardholder_name + "|" + cert_type + "|" + cert_no + "|" +
                page_notify_url + "|" + back_notify_url;
        System.out.println(signDataStr);
        byte[] bytesKey = (new BASE64Decoder()).decodeBuffer(pri_key.trim());
        PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(bytesKey);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PrivateKey priKey = keyFactory.generatePrivate(pkcs8KeySpec);
        Signature signature = Signature.getInstance("MD5WithRSA");
        signature.initSign(priKey);
        signature.update(signDataStr.getBytes("GBK"));
        String RSA = (new BASE64Encoder()).encodeBuffer(signature.sign());
        HashMap<String, String> args = new HashMap<String, String>();
        args.put("RSA", RSA);
        args.put("mchnt_cd", mchnt_cd);
        args.put("order_id", order_id);
        args.put("order_amt", order_amt);
        args.put("user_type", user_type);
        args.put("card_no", card_no);
        args.put("page_notify_url", page_notify_url);
        args.put("back_notify_url", back_notify_url);
        args.put("cert_type", cert_type);
        args.put("cert_no", cert_no);
        args.put("cardholder_name", cardholder_name);
        args.put("user_id", user_id);
        args.put("url", this.config.get("authPcUrl"));
        return args;
    }

    @Override
    public HashMap<String, String> createWapAuth(long accountId, int route, BigDecimal money, String orderNo) throws Exception {
        BankCard bankCard = this.bankCardMapper.selectByAccount(accountId);
        User user = this.userMapper.selectByAccountId(accountId);
        super.init(Enums.GatWay.FY.getIndex());
        String userId = user.getId().toString();
        String amt = money.multiply(BigDecimal.valueOf(100)).toString();
        String cardId = bankCard.getCardId();
        String name = user.getRealName();
        String idType = "0";
        String idNo = user.getIdCard();
        StringBuffer orderPlain = new StringBuffer();
        String orderId = orderNo;
        String Version = "2.0";
        String type = "10";
        String logoTp = "1";
        String mchntCd = this.config.get("authWapMchntCd");
        String key = this.config.get("authWapKey");
        String url = this.config.get("authWapUrl");
        String returnUrl = this.baseUrl + "/account/result/" + orderNo;
        String backUrl = this.apiBaseUrl + "/call/fyWapAuth";
        String errorUrl = this.baseUrl + "/account/result/error";
        String signPlain = type + "|" + Version + "|" + mchntCd + "|" + orderId + "|" + userId
                + "|" + amt + "|" + cardId + "|" + backUrl + "|" + name + "|" + idNo + "|" + idType + "|" + logoTp + "|"
                + returnUrl + "|" + errorUrl + "|" + key;
        String sign = MD5Util.MD5(signPlain);
        System.out.println("[签名明文:]" + signPlain);
        orderPlain.append("<ORDER>")
                .append("<VERSION>" + Version + "</VERSION>")
                .append("<LOGOTP>" + logoTp + "</LOGOTP>")
                .append("<MCHNTCD>").append(mchntCd).append("</MCHNTCD>")
                .append("<TYPE>" + type + "</TYPE>")
                .append("<MCHNTORDERID>").append(orderId).append("</MCHNTORDERID>")
                .append("<USERID>").append(userId).append("</USERID>")
                .append("<AMT>").append(amt).append("</AMT>")
                .append("<BANKCARD>").append(cardId).append("</BANKCARD>")
                .append("<BACKURL>").append(backUrl).append("</BACKURL>")
                .append("<HOMEURL>").append(returnUrl).append("</HOMEURL>")
                .append("<REURL>").append(errorUrl).append("</REURL>")
                .append("<NAME>").append(name).append("</NAME>")
                .append("<IDTYPE>").append(idType).append("</IDTYPE>")
                .append("<IDNO>").append(idNo).append("</IDNO>")
                .append("<REM1></REM1>")
                .append("<REM2></REM2>")
                .append("<REM3></REM3>")
                .append("<SIGNTP>md5</SIGNTP>")
                .append("<SIGN>").append(sign).append("</SIGN>")
                .append("</ORDER>");
        System.out.println("[订单信息:]" + orderPlain.toString());
        HashMap<String, String> param = new HashMap<String, String>();
        param.put("VERSION", Version);
        param.put("ENCTP", "1");
        param.put("LOGOTP", logoTp);
        param.put("MCHNTCD", mchntCd);
        param.put("FM", DESCoderFUIOU.desEncrypt(orderPlain.toString(), DESCoderFUIOU.getKeyLength8(key)));
        param.put("url", url);
        return param;
    }

    @Override
    public HashMap<String, String> createAppAuth(long accountId, int route, BigDecimal money, String orderNo) throws Exception {
        BankCard bankCard = this.bankCardMapper.selectByAccount(accountId);
        User user = this.userMapper.selectByAccountId(accountId);
        super.init(Enums.GatWay.FY.getIndex());
        String userId = user.getId().toString();
        String amt = money.multiply(BigDecimal.valueOf(100)).toString();
        String cardId = bankCard.getCardId();
        String name = user.getRealName();
        String idType = "0";
        String idNo = user.getIdCard();
        StringBuffer orderPlain = new StringBuffer();
        String orderId = orderNo;
        String Version = "2.0";
        String type = "10";
        String logoTp = "1";
        String mchntCd = this.config.get("authWapMchntCd");
        String key = this.config.get("authWapKey");
        String url = this.config.get("authWapUrl");
        String errorUrl = this.baseUrl + "/api/error";
        String returnUrl = this.baseUrl + "/api/result/" + orderNo;
        String backUrl = this.apiBaseUrl + "/call/fyAppAuth";
        String signPlain = type + "|" + Version + "|" + mchntCd + "|" + orderId + "|" + userId
                + "|" + amt + "|" + cardId + "|" + backUrl + "|" + name + "|" + idNo + "|" + idType + "|" + logoTp + "|"
                + returnUrl + "|" + errorUrl + "|" + key;
        String sign = MD5Util.MD5(signPlain);
        System.out.println("[签名明文:]" + signPlain);
        orderPlain.append("<ORDER>")
                .append("<VERSION>" + Version + "</VERSION>")
                .append("<LOGOTP>" + logoTp + "</LOGOTP>")
                .append("<MCHNTCD>").append(mchntCd).append("</MCHNTCD>")
                .append("<TYPE>" + type + "</TYPE>")
                .append("<MCHNTORDERID>").append(orderId).append("</MCHNTORDERID>")
                .append("<USERID>").append(userId).append("</USERID>")
                .append("<AMT>").append(amt).append("</AMT>")
                .append("<BANKCARD>").append(cardId).append("</BANKCARD>")
                .append("<BACKURL>").append(backUrl).append("</BACKURL>")
                .append("<HOMEURL>").append(returnUrl).append("</HOMEURL>")
                .append("<REURL>").append(errorUrl).append("</REURL>")
                .append("<NAME>").append(name).append("</NAME>")
                .append("<IDTYPE>").append(idType).append("</IDTYPE>")
                .append("<IDNO>").append(idNo).append("</IDNO>")
                .append("<REM1></REM1>")
                .append("<REM2></REM2>")
                .append("<REM3></REM3>")
                .append("<SIGNTP>md5</SIGNTP>")
                .append("<SIGN>").append(sign).append("</SIGN>")
                .append("</ORDER>");
        System.out.println("[订单信息:]" + orderPlain.toString());
        HashMap<String, String> param = new HashMap<String, String>();
        param.put("VERSION", Version);
        param.put("ENCTP", "1");
        param.put("LOGOTP", logoTp);
        param.put("MCHNTCD", mchntCd);
        param.put("FM", DESCoderFUIOU.desEncrypt(orderPlain.toString(), DESCoderFUIOU.getKeyLength8(key)));
        param.put("url", url);
        return param;
    }

    @Override
    public boolean callPcAuth(Map<String, String> params) throws UtilException, IOException, NoSuchAlgorithmException, InvalidKeySpecException, InvalidKeyException, SignatureException {
        super.init(Enums.GatWay.FY.getIndex());
        String mchnt_cd = this.config.get("authPcMchntCd");
        String order_id = params.get("order_id");
        String order_amt = params.get("order_amt");
        String order_date = params.get("order_date");
        String order_st = params.get("order_st");
        String order_pay_code = params.get("order_pay_code");
        String order_pay_error = params.get("order_pay_msg");
        String fy_ssn = params.get("fy_ssn");
        String user_id = params.get("user_id");
        String card_no = params.get("card_no");
        String RSA = params.get("RSA");
        String publicKey = this.config.get("authPcPublicKey");
        String signDataStr = mchnt_cd + "|" + user_id + "|" + order_id + "|" + order_amt + "|" +
                order_date + "|" + card_no + "|" + fy_ssn;
        System.err.println("=======================params===================" + params);
        byte[] keyBytes = (new BASE64Decoder()).decodeBuffer(publicKey);
        // 构造X509EncodedKeySpec对象
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(keyBytes);
        // KEY_ALGORITHM 指定的加密算法
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        // 取公钥匙对象
        PublicKey pubKey = keyFactory.generatePublic(keySpec);
        Signature signature = Signature.getInstance("MD5withRSA");
        signature.initVerify(pubKey);
        signature.update(signDataStr.getBytes("gbk"));
        // 验证签名是否正常
        boolean ret = signature.verify((new BASE64Decoder()).decodeBuffer(RSA));
        System.err.println("=======================ret===================" + ret);
        if (ret) {
            if (order_pay_code.equals("0000")) {
                return this.rechargeService.updateOrderSuccess(order_id, Enums.FundType.OnLineRecharge, "在线支付成功");
            }
        }
        return false;
    }

    @Override
    public boolean callWapAuth(Map<String, String> params) throws UtilException {
        super.init(Enums.GatWay.FY.getIndex());
        String version = params.get("VERSION");
        String type = params.get("TYPE");
        String responseCode = params.get("RESPONSECODE");
        String responseMsg = params.get("RESPONSEMSG");
        String mchntCd = this.config.get("authWapMchntCd");
        String mchntOrderId = params.get("MCHNTORDERID");
        String orderId = params.get("ORDERID");
        String bankCard = params.get("BANKCARD");
        String amt = params.get("AMT");
        String sign = params.get("SIGN");
        // 校验签名
        String signPain = new StringBuffer().append(type).append("|").append(version).append("|").append(responseCode)
                .append("|").append(mchntCd).append("|").append(mchntOrderId).append("|").append(orderId).append("|")
                .append(amt).append("|").append(bankCard).append("|").append(this.config.get("authWapKey")).toString();
        System.err.println("=======================signPain===================" + signPain);
        if (MD5Util.MD5(signPain).equals(sign)) {
            if (("0000").equals(responseCode)) {
                return this.rechargeService.updateOrderSuccess(mchntOrderId, Enums.FundType.OnLineRecharge, "在线支付成功");
            } else
                return false;
        } else {
            return false;
        }
    }

    @Override
    public boolean callAppAuth(Map<String, String> params) throws UtilException {
        super.init(Enums.GatWay.FY.getIndex());
        String version = params.get("VERSION");
        String type = params.get("TYPE");
        String responseCode = params.get("RESPONSECODE");
        String responseMsg = params.get("RESPONSEMSG");
        String mchntCd = this.config.get("authWapMchntCd");
        String mchntOrderId = params.get("MCHNTORDERID");
        String orderId = params.get("ORDERID");
        String bankCard = params.get("BANKCARD");
        String amt = params.get("AMT");
        String sign = params.get("SIGN");
        // 校验签名
        String signPain = new StringBuffer().append(type).append("|").append(version).append("|").append(responseCode)
                .append("|").append(mchntCd).append("|").append(mchntOrderId).append("|").append(orderId).append("|")
                .append(amt).append("|").append(bankCard).append("|").append(this.config.get("authWapKey")).toString();
        System.err.println("=======================signPain===================" + signPain);
        if (MD5Util.MD5(signPain).equals(sign)) {
            if (("0000").equals(responseCode)) {
                return this.rechargeService.updateOrderSuccess(mchntOrderId, Enums.FundType.OnLineRecharge, "在线支付成功");
            } else
                return false;
        } else {
            return false;
        }
        //super.init(Enums.GatWay.FY.getIndex());
        //String version = params.get("VERSION");
        //String type = params.get("TYPE");
        //String responseCode = params.get("RESPONSECODE");
        //String responseMsg = params.get("RESPONSEMSG");
        //String mchntCd = params.get("MCHNTCD");
        //String mchntOrderId = params.get("MCHNTORDERID");
        //String orderId = params.get("ORDERID");
        //String bankCard = params.get("BANKCARD");
        //String amt = params.get("AMT");
        //String sign = params.get("SIGN");
        //String key = this.config.get("authApiKey");
        // 校验签名
        //String signPain = new StringBuffer().append(type).append("|").append(version).append("|").append(responseCode)
        //        .append("|").append(mchntCd).append("|").append(mchntOrderId).append("|").append(orderId).append("|")
        //        .append(amt).append("|").append(bankCard).append("|").append(key).toString();
        //if (MD5Util.MD5(signPain).equals(sign)) {
        //    if (("0000").equals(responseCode)) {
        //        return this.rechargeService.updateOrderSuccess(orderId, Enums.FundType.OnLineRecharge, "在线支付成功");
        //    } else {
        //        return false;
        //    }
        //} else {
        //    return false;
        //}
    }

    @Override
    public boolean bindCard(BankCard bankCard) throws Exception {
        super.init(Enums.GatWay.FY.getIndex());
        //BankCard bankCard = this.bankCardMapper.selectByAccount(account.getId());
        User user = this.userMapper.selectByAccountId(bankCard.getAccountId());
        String MchntCd = this.config.get("bindMchntCd");
        String Ono = bankCard.getCardId();
        String Onm = user.getRealName();
        String OCerTp = "0";
        String OCerNo = user.getIdCard();
        String Mno = bankCard.getMobile();
        String Ver = "1.30";
        String OSsn = bankCard.getAccountId() + "" + new Date().getTime();
        String key = this.config.get("bindKey");
        String Sign = MD5Util.MD5(MchntCd + "|" + Ver + "|" + OSsn + "|" + Ono + "|" + OCerTp + "|" + OCerNo + "|" + key);
        StringBuilder xml = new StringBuilder();
        xml.append("<FM>");
        xml.append("<MchntCd>" + MchntCd + "</MchntCd>");
        xml.append("<Ono>" + Ono + "</Ono>");
        xml.append("<Onm>" + Onm + "</Onm>");
        xml.append("<OCerTp>" + OCerTp + "</OCerTp>");
        xml.append("<OCerNo>" + OCerNo + "</OCerNo>");
        xml.append("<Mno>" + Mno + "</Mno>");
        xml.append("<Sign>" + Sign + "</Sign>");
        xml.append("<Ver>" + Ver + "</Ver>");
        xml.append("<OSsn>" + OSsn + "</OSsn>");
        xml.append("</FM>");
        System.out.println("绑卡请求报文:" + xml);
        String rsXml = UtilHttp.sendPost(this.config.get("bindUrl"), "FM=" + xml.toString());
        System.out.println("绑卡响应报文:" + rsXml);
        BindRespData bd = XmlBeanUtils.convertXml2Bean(rsXml, BindRespData.class);
        if (bd.getRcd().equals("0000")) {
            bankCard.setBank(convertCode(bd.getInsCd()));
            return true;
        }
        return false;
    }

    @Override
    public boolean subPay(String order, String code) throws Exception {
        super.init(Enums.GatWay.FY.getIndex());
        String mchntCd = this.config.get("authApiMchntCd");
        String key = this.config.get("authApiKey");
        String payUrl = this.config.get("authApiPayUrl");
        Map map = this.cache.getOrder(order);
        OrderRespData respData = CommonUtil.parseObject(map, OrderRespData.class);
        Recharge recharge = this.rechargeMapper.selectByOrderNo(order);
        User user = this.userMapper.selectByAccountId(recharge.getAccountId());
        BankCard bankCard = this.bankCardMapper.selectByAccount(recharge.getAccountId());
        String userId = user.getId().toString();
        String cardId = bankCard.getCardId();
        String mobile = bankCard.getMobile();
        String userIp = "";
        String version = "2.3";
        String type = "03";
        String rem1 = "";
        String rem2 = "";
        String rem3 = "";
        String mchntorderid = order;
        String orderid = respData.getOrderid();
        String vercd = code;
        String signpay = respData.getSignpay();
        PayReqData payReqData = new PayReqData();
        payReqData.setBankcard(cardId);
        payReqData.setMchntcd(mchntCd);
        payReqData.setMchntorderid(mchntorderid);
        payReqData.setMobile(mobile);
        payReqData.setOrderid(orderid);
        payReqData.setRem1(rem1);
        payReqData.setRem2(rem2);
        payReqData.setRem3(rem3);

        payReqData.setSignpay(signpay);
        payReqData.setSigntp("md5");
        payReqData.setType(type);
        payReqData.setUserid(userId);
        payReqData.setUserip(userIp);
        payReqData.setVercd(vercd);
        payReqData.setVersion(version);
        String signPlain = payReqData.getType() + "|" + payReqData.getVersion() + "|" + mchntCd + "|"
                + payReqData.getOrderid() + "|" + payReqData.getMchntorderid() + "|" + payReqData.getUserid() + "|"
                + payReqData.getBankcard() + "|" + payReqData.getVercd() + "|" + payReqData.getMobile() + "|"
                + payReqData.getUserip() + "|" + key;
        String sign = MD5Util.MD5(signPlain);
        payReqData.setSign(sign);
        System.out.println("[签名明文:]" + signPlain);
        String apiFms = XmlBeanUtils.convertBean2Xml(payReqData);
        System.out.println("[订单信息:]" + apiFms);
        Map<String, String> param = new HashMap<String, String>();
        param.put("MCHNTCD", mchntCd);
        param.put("APIFMS", DESCoderFUIOU.desEncrypt(apiFms, DESCoderFUIOU.getKeyLength8(key)));
        System.out.println("[请求信息:]" + param);
        String respStr = HttpPostUtil.postForward(payUrl, param);
        respStr = DESCoderFUIOU.desDecrypt(respStr, DESCoderFUIOU.getKeyLength8(key));
        System.out.println("返回数据：" + respStr);
        PayRespData orderRespData = XmlBeanUtils.convertXml2Bean(respStr, PayRespData.class);
        // 成功就跳到下单页面
        if (orderRespData.getResponsecode().equals("0000")) {
            this.rechargeService.updateOrderSuccess(order, Enums.FundType.OnLineRecharge, "在线支付成功");
            return true;
        }
        return false;
    }

    @Override
    public String convertBank(String bank) {
        Map<String, String> map = new HashMap<>();
        map.put("ICBC", "0801020000");
        map.put("CCB", "0801050000");
        map.put("CMBC", "0803050000");
        map.put("PSBC", "0801000000");
        map.put("CEB", "0803030000");
        map.put("HXB", "0803040000");
        map.put("CMB", "0803080000");
        map.put("BOC", "0801040000");
        map.put("BCM", "0803010000");
        map.put("SPDB", "0803100000");
        map.put("CIB", "0803090000");
        map.put("CITIC", "0803020000");
        map.put("BOB", "0804031000");
        map.put("CGB", "0803060000");
        map.put("PAB", "0804105840");
        map.put("ABC", "0801030000");
        return map.get(bank);
    }

    @Override
    public String convertCode(String bank) {
        Map<String, String> map = new HashMap<>();
        map.put("080102", "ICBC");
        map.put("080105", "CCB");
        map.put("080305", "CMBC");
        map.put("080100", "PSBC");
        map.put("080303", "CEB");
        map.put("080304", "HXB");
        map.put("080308", "CMB");
        map.put("080104", "BOC");
        map.put("080301", "BCM");
        map.put("080310", "SPDB");
        map.put("080309", "CIB");
        map.put("080302", "CITIC");
        map.put("080403", "BOB");
        map.put("080306", "CGB");
        map.put("080410", "PAB");
        map.put("080103", "ABC");
        return map.get(bank.substring(0, 6));
    }

    //网关补单
    @Override
    public boolean checkGatWayOrder(String orderId) throws Exception {
        super.init(Enums.GatWay.FY.getIndex());
        String postUrl="https://pay.fuiou.com/smpAQueryGate.do";
        String mchnt_cd =this.config.get("authPcMchntCd");
        String mchnt_key =this.config.get("gatWayKey");
        String signDataStr = mchnt_cd + "|" + orderId+"|"+mchnt_key;
        String sign = MD5Util.MD5(signDataStr);
        String rsXml=UtilHttp.sendPost(postUrl, "md5=" + sign.toString()+"&mchnt_cd="+mchnt_cd+"&order_id="+orderId);
        System.err.println("网关支付补单响应：rsXml="+rsXml);
        SAXBuilder builder = new SAXBuilder();
        Document doc = builder.build(new StringReader(rsXml));
        Element root = doc.getRootElement();
        String status =root.getChild("plain").getChild("order_st").getText();
        if (status.equals("11")) {
            return true;
        }else{
            return false;
        }
    }

    //pc快捷订单查询
    @Override
    public boolean checkPcAuthOrder(String orderId) throws Exception {
        super.init(Enums.GatWay.FY.getIndex());
        String postUrl="https://pay.fuiou.com/newSynDirPayQueryServlet.do";
        String pri_key = this.config.get("authPcPrivateKey");
        String mchnt_cd = this.config.get("authPcMchntCd");
        String order_id = orderId;
        String signDataStr =mchnt_cd + "|"+order_id;
        System.out.println(signDataStr);

        byte[] bytesKey = (new BASE64Decoder()).decodeBuffer(pri_key.trim());
        PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(bytesKey);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PrivateKey priKey = keyFactory.generatePrivate(pkcs8KeySpec);
        Signature signature = Signature.getInstance("MD5WithRSA");
        signature.initSign(priKey);
        signature.update(signDataStr.getBytes("GBK"));
        String RSA = (new BASE64Encoder()).encodeBuffer(signature.sign());
        String param="RSA=" + URLEncoder.encode(RSA, "UTF-8")+"&mchnt_cd="+mchnt_cd+"&order_id="+order_id;
        System.err.println(param);
        String rsXml=UtilHttp.sendPost(postUrl,param);

        System.err.println("PC补单响应：rsXml="+rsXml);
        SAXBuilder builder = new SAXBuilder();
        Document doc = builder.build(new StringReader(rsXml));
        Element root = doc.getRootElement();
        String status = root.getChild("plain").getChild("order_st").getText();
        if (status.equals("11")) {
            return true;
        }else{
            return false;
        }
    }

    //wap快捷订单查询
    @Override
    public boolean checkWapAuthOrder(String orderId) throws Exception {
        String url="https://mpay.fuiou.com:16128/checkInfo/checkResult.pay";
        super.init(Enums.GatWay.FY.getIndex());
        String mchnt_cd =this.config.get("authPcMchntCd");
        String mchnt_key =this.config.get("authWapKey");
        String signDataStr = "1.0|" + mchnt_cd + "|" + orderId+"|"+mchnt_key;
        String sign = MD5Util.MD5(signDataStr);
        StringBuilder xml = new StringBuilder();
        System.err.println(signDataStr);
        xml.append("<ORDER>");
        xml.append("<VERSION>1.0</VERSION>");
        xml.append("<MCHNTCD>" + mchnt_cd + "</MCHNTCD>");
        xml.append("<MCHNTORDERID>"+orderId+"</MCHNTORDERID>");
        xml.append("<REM1></REM1><REM2></REM2><REM3></REM3>");
        xml.append("<SIGN>" + sign + "</SIGN>");
        xml.append("</ORDER>");
        String rsXml=UtilHttp.sendPost(url, "FM=" + xml.toString());
        System.err.println("手机端补单响应：rsXml="+rsXml);
        SAXBuilder builder = new SAXBuilder();
        Document doc = builder.build(new StringReader(rsXml));
        Element root = doc.getRootElement();
        String status = root.getChild("RESPONSECODE").getText();
        if (status.equals("0000")) {
            return true;
        }else{
            return false;
        }
    }

    /**
     *  卡Bin查询接口
     * @param cardNo
     * @return
     * @throws Exception
     */
    public String cardBinQuery(String cardNo)  throws Exception{
        super.init(Enums.GatWay.FY.getIndex());
        String mchnt_key =this.config.get("authWapKey"); //32位的商户密钥
        String mchnt_cd =this.config.get("authPcMchntCd");
        String cardBinUrl=this.config.get("cardBinUrl");
        if(cardBinUrl==null || cardBinUrl.isEmpty()){
            cardBinUrl="https://mpay.fuiou.com:16128/findPay/cardBinQuery.pay";
        }
        String signDataStr = mchnt_cd + "|" + cardNo + "|" + mchnt_key;
        String sign = MD5Util.MD5(signDataStr);
        StringBuilder xml = new StringBuilder();
        xml.append("<FM>");
        xml.append("<MchntCd>" + mchnt_cd + "</MchntCd>");
        xml.append("<Ono>" + cardNo + "</Ono>");
        xml.append("<Sign>" + sign + "</Sign>");
        xml.append("</FM>");
        String rsXml=UtilHttp.sendPost(cardBinUrl, "FM=" + xml.toString());
        System.err.println("rsXml="+rsXml);
        if(rsXml!=null && rsXml!=""){
            CardBinRespData bd = XmlBeanUtils.convertXml2Bean(rsXml, CardBinRespData.class);
            if(bd.getRcd().equals("0000")){
                if(bd.getCtp().equals("01")){
                    return convertCode(bd.getInsCd());
                }else{
                    throw new UtilException("只支持借记卡.");
                }
            }else{
                throw new UtilException(bd.getRDesc());
            }
        }else{
            throw new UtilException("卡Bin查询失败.");
        }
    }

    public String directBindCard(BankCard bankCard,String idCard) throws Exception{
        return "";
    }

    public String preCretifyPay(BigDecimal money, String bindId, String orderNo) throws Exception{
        return "";
    }

    public String certainCretifyPay(String businessNo, String smsCode,String orderNo) throws Exception{
        return "";
    }
}
