package com.zcwk.core.recharge.Util;

import java.io.UnsupportedEncodingException;
import java.security.SecureRandom;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

import com.zcwk.core.util.MD5Util;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

/**
 * Created by pwx on 2016/11/24.
 */
public class DESCoderFUIOU {
    private static String CHATSET = "UTF-8";

    public DESCoderFUIOU() {
    }

    public static String desEncrypt(String input, String keystr) throws Exception {
        try {
            byte[] e = input.getBytes(CHATSET);
            SecureRandom random = new SecureRandom();
            DESKeySpec desKey = new DESKeySpec(keystr.getBytes(CHATSET));
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
            SecretKey securekey = keyFactory.generateSecret(desKey);
            Cipher cipher = Cipher.getInstance("DES");
            cipher.init(1, securekey, random);
            return (new BASE64Encoder()).encode(cipher.doFinal(e));
        } catch (Throwable var8) {
            var8.printStackTrace();
            return null;
        }
    }

    public static String desDecrypt(String cipherText, String strkey) throws Exception {
        SecureRandom random = new SecureRandom();
        DESKeySpec desKey = new DESKeySpec(strkey.getBytes(CHATSET));
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
        SecretKey securekey = keyFactory.generateSecret(desKey);
        Cipher cipher = Cipher.getInstance("DES");
        cipher.init(2, securekey, random);
        BASE64Decoder decoder = new BASE64Decoder();
        return new String(cipher.doFinal(decoder.decodeBuffer(cipherText)), CHATSET);
    }

    public static String padding(String str) {
        try {
            byte[] oldByteArray = str.getBytes(CHATSET);
            int e = 8 - oldByteArray.length % 8;
            byte[] newByteArray = new byte[oldByteArray.length + e];
            System.arraycopy(oldByteArray, 0, newByteArray, 0, oldByteArray.length);

            for (int i = oldByteArray.length; i < newByteArray.length; ++i) {
                newByteArray[i] = 0;
            }

            return new String(newByteArray, CHATSET);
        } catch (UnsupportedEncodingException var5) {
            System.out.println("Crypter.padding UnsupportedEncodingException");
            return null;
        }
    }

    public static byte[] removePadding(byte[] oldByteArray) {
        int numberPaded = 0;

        for (int newByteArray = oldByteArray.length; newByteArray >= 0; --newByteArray) {
            if (oldByteArray[newByteArray - 1] != 0) {
                numberPaded = oldByteArray.length - newByteArray;
                break;
            }
        }

        byte[] var3 = new byte[oldByteArray.length - numberPaded];
        System.arraycopy(oldByteArray, 0, var3, 0, var3.length);
        return var3;
    }

    public static String getKey(String name) {
        StringBuffer buffer = new StringBuffer();
        buffer.append(MD5Util.MD5(MD5Util.MD5(name + "fuiou"))).append(MD5Util.MD5(name + "20160112")).append(MD5Util.MD5(name + "mpay")).append(MD5Util.MD5(MD5Util.MD5(name)));
        return buffer.toString();
    }

    public static String getKeyLength8(String key) {
        key = key == null ? "" : key.trim();
        int tt = key.length() % 64;
        String temp = "";

        for (int i = 0; i < 64 - tt; ++i) {
            temp = temp + "D";
        }

        return key + temp;
    }
}
