package com.zcwk.core.recharge.Util.FyData;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by pwx on 2017/3/27.
 */
@XmlRootElement(name = "FM")
public class CardBinRespData {
    private String rcd;
    private String rDesc;
    private String ctp;
    private String cnm;
    private String insCd;
    private String sign;

    @XmlElement(name = "Rcd")
    public void setRcd(String rcd) {
        this.rcd = rcd;
    }

    public String getRcd() {
        return rcd;
    }

    @XmlElement(name = "RDesc")
    public void setRDesc(String rDesc) {
        this.rDesc = rDesc;
    }

    public String getRDesc() {
        return rDesc;
    }

    @XmlElement(name = "Ctp")
    public void setCtp(String ctp) {
        this.ctp = ctp;
    }

    public String getCtp() {
        return ctp;
    }

    @XmlElement(name = "Sign")
    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getSign() {
        return sign;
    }

    @XmlElement(name = "Cnm")
    public void setCnm(String cnm) {
        this.cnm = cnm;
    }

    public String getCnm() {
        return cnm;
    }

    @XmlElement(name = "InsCd")
    public void setInsCd(String insCd) {
        this.insCd = insCd;
    }

    public String getInsCd() {
        return insCd;
    }

}
