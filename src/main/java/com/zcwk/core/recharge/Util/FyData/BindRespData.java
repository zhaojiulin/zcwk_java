package com.zcwk.core.recharge.Util.FyData;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by pwx on 2016/11/24.
 */
@XmlRootElement(name = "FM")
public class BindRespData {
    private String rcd;
    private String rDesc;
    private String ossn;
    private String cardNo;
    private String mchntCd;
    private String ver;
    private String cnm;
    private String insCd;
    private String sign;

    @XmlElement(name = "Rcd")
    public void setRcd(String rcd) {
        this.rcd = rcd;
    }

    public String getRcd() {
        return rcd;
    }

    @XmlElement(name = "RDesc")
    public void setRDesc(String rDesc) {
        this.rDesc = rDesc;
    }

    public String getRDesc() {
        return rDesc;
    }

    @XmlElement(name = "OSsn")
    public void setOSsn(String ossn) {
        this.ossn = ossn;
    }

    public String getOSsn() {
        return ossn;
    }

    @XmlElement(name = "CardNo")
    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getCardNo() {
        return cardNo;
    }

    @XmlElement(name = "MchntCd")
    public void setMchntCd(String mchntCd) {
        this.mchntCd = mchntCd;
    }

    public String getMchntCd() {
        return mchntCd;
    }

    @XmlElement(name = "Ver")
    public void setVer(String ver) {
        this.ver = ver;
    }

    public String getVer() {
        return ver;
    }

    @XmlElement(name = "Cnm")
    public void setCnm(String cnm) {
        this.cnm = cnm;
    }

    public String getCnm() {
        return cnm;
    }

    @XmlElement(name = "InsCd")
    public void setInsCd(String insCd) {
        this.insCd = insCd;
    }

    public String getInsCd() {
        return insCd;
    }

    @XmlElement(name = "Sign")
    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getSign() {
        return sign;
    }
}
