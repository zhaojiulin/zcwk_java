package com.zcwk.core.recharge;

import com.zcwk.core.util.Enums;
import com.zcwk.core.util.UtilBean;

/**
 * Created by pwx on 2016/9/18.
 */
public class GatWayFactory {
    public static GatWayInterface create(int index) {
        GatWayInterface gateWay = null;
        if (index == Enums.GatWay.BF.getIndex())
            gateWay = (GatWayInterface) UtilBean.applicationContext.getBean("BfGatWay");
        else if (index == Enums.GatWay.HC.getIndex())
            gateWay = (GatWayInterface) UtilBean.applicationContext.getBean("HcGatWay");
        else if (index == Enums.GatWay.FY.getIndex())
            gateWay = (GatWayInterface) UtilBean.applicationContext.getBean("FyGatWay");
        return gateWay;
    }
}
