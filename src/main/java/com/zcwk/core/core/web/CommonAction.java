package com.zcwk.core.core.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.zcwk.core.util.StringUtil;

/**
 *
 * @author zp
 * @since 2017年3月17日
 */
@Controller
@Scope("prototype")
public class CommonAction{
	
	protected HttpServletRequest request;
	protected HttpServletResponse response;
	
	protected int paramInt(String str) {
		String obj = request.getParameter(str);
		if (StringUtil.isBlank(obj))
			return 0;
		return StringUtil.toInt(obj);
	}
	
	protected long paramLong(String str) {
		String obj = request.getParameter(str);
		if (StringUtil.isBlank(obj))
			return 0l;
		return StringUtil.toLong(obj);
	}

	/*protected double paramDouble(String str) {
		String obj = request.getParameter(str);
		if (StringUtil.isBlank(obj))
			return 0;
		return BigDecimalUtil.round(obj);
	}*/

	/*private Object paramValue(Class<?> type, String name) {
		Object v = null;
		if (type.equals(Long.class) || type.equals(long.class)) {
			v = this.paramLong(name);
		} else if (type == Integer.class || type == int.class || type == Byte.class || type == Short.class
				|| type == short.class) {
			v = this.paramInt(name);
		} else if (type == Boolean.class || type == boolean.class) {
			int i = this.paramInt(name);
			if (i == 1)
				v = true;
			v = false;
		} else if (type.equals(Double.class) || type.equals(double.class) || type.equals(Float.class)
				|| type.equals(float.class)) {
			v = this.paramDouble(name);
		} else {
			v = this.paramString(name);
		}
		return v;
	}*/
	
	/*protected Object paramModel(Class<?> clazz) {
		Object model;
		try {
			model = clazz.newInstance();
		} catch (Exception e) {
			return null;
		}
		Field[] fs = clazz.getDeclaredFields();
		Object v = null;

		for (Field f : fs) {
			v = paramValue(f.getType(), f.getName());
			ReflectUtil.invokeSetMethod(Class, model, f.getName(), f.getType(), v);
		}
		return model;
	}*/
	
	
	protected String paramString(String str) {
		return StringUtil.isNull(request.getParameter(str));
	}

	public HttpServletRequest getRequest() {
		return ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	public HttpServletResponse getResponse() {
		return response;
	}

	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}
	
}
