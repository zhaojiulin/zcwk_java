package com.zcwk.core.core.model;

import java.util.ArrayList;
import java.util.List;

import com.zcwk.core.util.StringUtil;

/**
 * 工具类-查询条件处理
 * 
 * @author zp
 * @since 2017年3月16日
 *
 */
public class QueryParam{
	
	public enum OrderType {
		DESC, ASC
	}
	
	protected String orderName;
	
	protected OrderType order;
	
    protected String orderByClause;
    
    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    /**
	 * 初始化实列
	 * 
	 * @return
	 */
	public static QueryParam getInstance() {
		return new QueryParam();
	}
    
    public QueryParam() {
        oredCriteria = new ArrayList<Criteria>();
    }
    
    public void addOrder(String orderName,OrderType order){
    	orderName = StringUtil.humpToLine2(orderName);
    	this.setOrderByClause((this.orderByClause==null?"":this.orderByClause + ",") + orderName + " " + order);
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

	public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andParamIsNull(String param) {
        	param = StringUtil.humpToLine2(param);
            addCriterion(StringUtil.humpToLine2(param) + " is null");
            return (Criteria) this;
        }

        public Criteria andParamIsNotNull(String param) {
        	param = StringUtil.humpToLine2(param);
            addCriterion(param + " is not null");
            return (Criteria) this;
        }

        public Criteria andParamEqualTo(String param ,Object value) {
        	param = StringUtil.humpToLine2(param);
            addCriterion(param + " =", value, param);
            return (Criteria) this;
        }

        public Criteria andParamNotEqualTo(String param ,Object value) {
        	param = StringUtil.humpToLine2(param);
            addCriterion(param + " <>", value, param);
            return (Criteria) this;
        }

        public Criteria andParamGreaterThan(String param ,Object value) {
        	param = StringUtil.humpToLine2(param);
            addCriterion(param + " >", value, param);
            return (Criteria) this;
        }

        public Criteria andParamGreaterThanOrEqualTo(String param ,Object value) {
        	param = StringUtil.humpToLine2(param);
            addCriterion(param + " >=", value, param);
            return (Criteria) this;
        }

        public Criteria andParamLessThan(String param ,Object value) {
        	param = StringUtil.humpToLine2(param);
            addCriterion(param + " <", value, param);
            return (Criteria) this;
        }

        public Criteria andParamLessThanOrEqualTo(String param ,Object value) {
        	param = StringUtil.humpToLine2(param);
            addCriterion(param + " <=", value, param);
            return (Criteria) this;
        }
        
        public Criteria andParamLike(String param ,Object value) {
        	param = StringUtil.humpToLine2(param);
            addCriterion(param + " like", "%" + value + "%", param);
            return (Criteria) this;
        }

        public Criteria andParamNotLike(String param ,Object value) {
        	param = StringUtil.humpToLine2(param);
            addCriterion(param + " not like", value, param);
            return (Criteria) this;
        }

        public Criteria andParamIn(String param ,List<Object> values) {
        	param = StringUtil.humpToLine2(param);
            addCriterion(param + " in", values, param);
            return (Criteria) this;
        }

        public Criteria andParamNotIn(String param ,List<Object> values) {
        	param = StringUtil.humpToLine2(param);
            addCriterion(param + " not in", values, param);
            return (Criteria) this;
        }

        public Criteria andParamBetween(String param ,Object value1, Object value2) {
        	param = StringUtil.humpToLine2(param);
            addCriterion(param + " between", value1, value2, param);
            return (Criteria) this;
        }

        public Criteria andParamNotBetween(String param ,Object value1, Object value2) {
        	param = StringUtil.humpToLine2(param);
            addCriterion(param + " not between", value1, value2, param);
            return (Criteria) this;
        }

    }

    /**
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}