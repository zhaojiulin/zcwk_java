package com.zcwk.core.queue;

import com.zcwk.core.util.Enums;
import com.zcwk.core.util.UtilBean;

/**
 * Created by pwx on 2016/7/14.
 */
public class ActionFactory {
    public static ActionInterface create(int index) {
        ActionInterface action = null;
        if (index == Enums.Actions.Register.getIndex())
            action = (ActionInterface) UtilBean.applicationContext.getBean("RegisterAction");
        else if (index == Enums.Actions.Buy.getIndex())
            action = (ActionInterface) UtilBean.applicationContext.getBean("BuyAction");
        else if (index == Enums.Actions.ValidEmail.getIndex())
            action = (ActionInterface) UtilBean.applicationContext.getBean("ValidEmailAction");
        else if (index == Enums.Actions.Login.getIndex())
            action = (ActionInterface) UtilBean.applicationContext.getBean("LoginAction");
        else if (index == Enums.Actions.RealName.getIndex())
            action = (ActionInterface) UtilBean.applicationContext.getBean("RealNameAction");
        else if (index == Enums.Actions.PayPassword.getIndex())
            action = (ActionInterface) UtilBean.applicationContext.getBean("PayPasswordAction");
        else if (index == Enums.Actions.BindCard.getIndex())
            action = (ActionInterface) UtilBean.applicationContext.getBean("BindCardAction");
        else if (index == Enums.Actions.Withdraw.getIndex())
            action = (ActionInterface) UtilBean.applicationContext.getBean("WithdrawAction");
        else if (index == Enums.Actions.Recharge.getIndex())
            action = (ActionInterface) UtilBean.applicationContext.getBean("RechargeAction");
        else if (index == Enums.Actions.Payment.getIndex())
            action = (ActionInterface) UtilBean.applicationContext.getBean("PaymentAction");
        else if (index == Enums.Actions.WithdrawFail.getIndex())
            action = (ActionInterface) UtilBean.applicationContext.getBean("WithdrawFailAction");
         else if (index == Enums.Actions.FullDebtAward.getIndex())
           action = (ActionInterface) UtilBean.applicationContext.getBean("FullDebtAwardAction");
        return action;
    }
}
