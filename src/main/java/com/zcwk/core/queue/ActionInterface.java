package com.zcwk.core.queue;

import org.springframework.stereotype.Service;

import java.util.HashMap;

/**
 * Created by pwx on 2016/7/14.
 */
@Service
public interface ActionInterface {
    void exec(HashMap map) throws Exception;
}
