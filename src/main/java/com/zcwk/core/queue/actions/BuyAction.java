package com.zcwk.core.queue.actions;

import com.alibaba.fastjson.JSON;
import com.zcwk.core.dao.*;
import com.zcwk.core.model.*;
import com.zcwk.core.queue.ActionInterface;
import com.zcwk.core.util.Enums;
import com.zcwk.core.util.UtilCache;
import com.zcwk.core.util.UtilException;
import com.zcwk.core.util.UtilHttp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.URLEncoder;
import java.util.HashMap;

/**
 * Created by pwx on 2016/7/15.
 */
@Service("BuyAction")
public class BuyAction extends BaseAction implements ActionInterface {
    @Autowired
    AccountMapper accountMapper;
    @Autowired
    UserMapper userMapper;
    @Autowired
    UserLevelMapper userLevelMapper;
    @Autowired
    UtilCache cache;
    @Autowired
    YoumiMapper youmiMapper;

    @Override
    public void exec(HashMap map) throws Exception {
        super.init(map);
        Invest invest = JSON.parseObject(this.data.toString(), Invest.class);
        Account account = this.accountMapper.selectByPrimaryKey(invest.getAccountId());
        //更新用户等级
        UserLevel level = this.userLevelMapper.selectLevelByAmount(account.getTotalInvestAmount());
        if (level != null) {
            User u = new User();
            u.setId(this.user.getId());
            u.setLevel(level.getId());
            this.userMapper.updateByPrimaryKeySelective(u);
            this.cache.delUser(this.user.getId());
        }
        super.must();
        this.postYoumi(account);
    }

    /**
     * 有米对接请求
     * @param accountYoumi
     * @throws com.zcwk.core.util.UtilException
     */
    private void postYoumi(Account accountYoumi) throws UtilException {
        if(accountYoumi!=null && accountYoumi.getInvestCount()!=null && accountYoumi.getInvestCount()==1){
            if(this.user!=null && this.user.getRoute()== Enums.Route.IOS.getIndex() && ((this.user.getSimulateIDFA()!=null && this.user.getSimulateIDFA()!="")||(this.user.getAdIdentifier()!=null && this.user.getAdIdentifier()!=""))){
                Youmi youmi=new Youmi();
                youmi.setAdIdentifier(this.user.getAdIdentifier());
                youmi.setSimulateIDFA(this.user.getSimulateIDFA());
                Youmi dbYoumi=youmiMapper.selectYoumiByKey(youmi);
                if(dbYoumi!=null && dbYoumi.getCallbackUrl()!=null && dbYoumi.getCallbackUrl()!="" && ((dbYoumi.getSimulateIDFA()!=null && dbYoumi.getSimulateIDFA()!="")||(dbYoumi.getAdIdentifier()!=null && dbYoumi.getAdIdentifier()!=""))){
                    youmiMapper.updateUserIdByPrimaryKey(dbYoumi.getId(),this.user.getId());
                    String param = "c=0" ;
                    String url=dbYoumi.getCallbackUrl();
                    if(dbYoumi.getCallbackUrl().indexOf("?")>-1){
                        String[] urls = dbYoumi.getCallbackUrl().split("[?]");
                        if(urls!= null || urls.length==2){
                            url=urls[0];
                            param=urls[1]+"&c=0";
                        }
                    }
                    UtilHttp.sendGet(url, param);
                }else{
                    System.err.println("有米通知3...."+this.user.getMobile());
                }
            }else{
                System.err.println("有米通知2..."+accountYoumi.getId());
            }
        }else{
            System.err.println("有米购买1。。");
        }
    }
}
