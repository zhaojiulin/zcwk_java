package com.zcwk.core.queue.actions;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.zcwk.core.dao.TemplateMapper;
import com.zcwk.core.dao.UserMapper;
import com.zcwk.core.model.User;
import com.zcwk.core.service.*;
import com.zcwk.core.util.CommonUtil;
import com.zcwk.core.util.Enums;
import com.zcwk.core.util.UtilCache;
import com.zcwk.core.util.UtilException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.MessagingException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by pwx on 2016/7/14.
 */
public class BaseAction {
    @Autowired
    BonusService bonusService;
    @Autowired
    CouponService couponService;
    @Autowired
    UserService userService;
    @Autowired
    IntegralService integralService;
    @Autowired
    UtilCache cache;
    @Autowired
    CommonService commonService;
    public int action;
    public User user;
    public Object data;
    public BigDecimal amount;


    public void init(HashMap map) {
        this.action = (int) map.get("action");
        long userId=Long.parseLong(map.get("userId").toString());
        this.user = userService.getUserById(userId);
        if(user==null){
            System.err.println(map);
            System.err.println("action="+action);
            System.err.println("map.get(userId)="+map.get("userId").toString());
            System.err.println("userId="+userId);
            System.err.println("队列数据异常。。。");
        }
        this.data = map.get("data");
        this.amount = new BigDecimal(map.get("amount").toString());
    }

    public void must() throws UtilException, UnsupportedEncodingException, MessagingException, JsonProcessingException {
        //发送红包
        this.bonusService.sendBonus(this.action, this.user, this.amount);
        //发送加息券
        this.couponService.sendCoupon(this.action, this.user, this.amount);
        //发送积分
        this.integralService.sendIntegral(this.action, this.user, this.amount);
        //发送短信,邮件
        if(this.user!=null){
            Map param = new HashMap<>();
            param = CommonUtil.extendMap(param, CommonUtil.parseMap(this.user));
            if (this.data != null)
                param = CommonUtil.extendMap(param, CommonUtil.parseMap(this.data));
            if (this.amount.compareTo(BigDecimal.ZERO) >= 0)
                param.put("amount", this.amount);
            if (param.containsKey("state"))
                param.put("state", Enums.State.Cancel.getIndex() == Integer.valueOf(param.get("state").toString()) ? "失败" : "成功");
            this.commonService.sendMsg(this.action, Enums.SendType.Sms.getIndex(), param);
            if (this.user.getValidEmail())
                this.commonService.sendMsg(this.action, Enums.SendType.Email.getIndex(), param);
            this.cache.delUser(this.user.getId());
        }
    }
}
