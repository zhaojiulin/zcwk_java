package com.zcwk.core.queue.actions;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.zcwk.core.queue.ActionInterface;
import com.zcwk.core.util.UtilException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.MessagingException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;

/**
 * Created by pwx on 2016/7/14.
 */
@Service("RegisterAction")
public class RegisterAction extends BaseAction implements ActionInterface {
    @Override
    public void exec(HashMap map) throws UtilException, UnsupportedEncodingException, MessagingException, JsonProcessingException {
        super.init(map);
        super.must();
    }
}
