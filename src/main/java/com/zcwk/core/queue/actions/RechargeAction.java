package com.zcwk.core.queue.actions;

import com.zcwk.core.queue.ActionInterface;
import org.springframework.stereotype.Service;

import java.util.HashMap;

/**
 * Created by pwx on 2016/10/19.
 */
@Service("RechargeAction")
public class RechargeAction  extends BaseAction implements ActionInterface {
    @Override
    public void exec(HashMap map) throws Exception {
        super.init(map);
        super.must();
    }
}
