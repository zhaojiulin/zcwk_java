package com.zcwk.core.queue.actions;


import com.zcwk.core.queue.ActionInterface;
import com.zcwk.core.service.SystemSetService;
import com.zcwk.core.util.Enums;
import com.zcwk.core.util.MD5Util;
import com.zcwk.core.util.UtilEmail;
import com.zcwk.core.util.UtilException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;

/**
 * Created by pwx on 2016/7/15.
 */
@Service("ValidEmailAction")
public class ValidEmailAction extends BaseAction implements ActionInterface {
    @Autowired
    SystemSetService systemSetService;

    @Override
    public void exec(HashMap map) throws Exception {
        super.init(map);
        super.must();
    }
}
