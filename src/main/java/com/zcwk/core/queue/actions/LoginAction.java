package com.zcwk.core.queue.actions;

import com.zcwk.core.queue.ActionInterface;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;

/**
 * Created by pwx on 2016/8/23.
 */
@Service("LoginAction")
public class LoginAction extends BaseAction implements ActionInterface {
    @Override
    public void exec(HashMap map) throws Exception {
        super.init(map);
        super.must();
    }
}
