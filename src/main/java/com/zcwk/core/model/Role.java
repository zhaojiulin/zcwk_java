package com.zcwk.core.model;

import java.util.Date;

/**
 * 
 * @author dengqun
 * 
 *         下午6:32:11
 */
public class Role {
	private Long id;

	private String name;

	private Date time;

	private Byte status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public Byte getStatus() {
		return status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}
}