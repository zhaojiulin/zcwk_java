package com.zcwk.core.model;

import java.util.Date;

public class User {
    private Long id;

    private String realName;

    private String idCard;

    private String mobile;

    private String password;

    private String payPassword;

    private String email;

    private Boolean validEmail;

    private Byte realCount;

    private Long score;

    private Boolean lock;

    private String nick;

    private Date time;

    private Long accountId;

    private Long inviteId;

    private String inviteCode;

    private Date lastLogin;

    /** 账户类型：0投资用户；1借款用户 **/
    private Byte type;
    
    /** 用户类别：0个人用户；1企业用户 **/
    private Byte userNature;

    private Byte effect;

    private Byte route;

    private Long channel;

    private Long subChannel;

    private Long level;

    private Byte sex;

    private Integer loginFail;

    private Date lockTime;

    private Date failTime;

    private String headImg;

    /**
     * 辅助字段*
     */
    private String invite; //邀请人
    private String inviteMobile; //邀请人手机号码
    private String levelName;
    

    /** E签宝账户唯一标识 **/
    private String eSignAccountId;
    
    /** E签宝电子印章数据 **/
    private String eSignSealData;
    
    /** 企业法人姓名 **/
    private String legalName;
    
    /** 企业法人身份证/护照号 **/
    private String legalIdNo;

    /**
     * 广告标识符
     */
    private String adIdentifier;

    /**
     * 设备标识符
     */
    private String simulateIDFA;

    //签到次数
    private Integer signInNo;

    //签到时间
    private Date signInTime;

    public void setSignInNo(Integer signInNo){
        this.signInNo=signInNo;
    }

    public Integer getSignInNo(){
        return signInNo;
    }

    public void setSignInTime(Date signInTime){
        this.signInTime=signInTime;
    }

    public Date getSignInTime(){
        return signInTime;
    }

    public String geteSignAccountId() {
		return eSignAccountId;
	}

	public void seteSignAccountId(String eSignAccountId) {
		this.eSignAccountId = eSignAccountId;
	}

	public String geteSignSealData() {
		return eSignSealData;
	}

	public void seteSignSealData(String eSignSealData) {
		this.eSignSealData = eSignSealData;
	}

	public String getLegalName() {
		return legalName;
	}

	public void setLegalName(String legalName) {
		this.legalName = legalName;
	}

	public String getLegalIdNo() {
		return legalIdNo;
	}

	public void setLegalIdNo(String legalIdNo) {
		this.legalIdNo = legalIdNo;
	}

	public String getInvite() {
        return invite;
    }

    public void setInvite(String invite) {
        this.invite = invite;
    }
    public String getInviteMobile() {
        return inviteMobile;
    }

    public void setInviteMobile(String inviteMobile) {
        this.inviteMobile = inviteMobile;
    }


    public String getLevelName() {
        return levelName;
    }

    public void setLevelName(String levelName) {
        this.levelName = levelName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName == null ? null : realName.trim();
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard == null ? null : idCard.trim();
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    public String getPayPassword() {
        return payPassword;
    }

    public void setPayPassword(String payPassword) {
        this.payPassword = payPassword == null ? null : payPassword.trim();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    public Boolean getValidEmail() {
        return validEmail;
    }

    public void setValidEmail(Boolean validEmail) {
        this.validEmail = validEmail;
    }

    public Byte getRealCount() {
        return realCount;
    }

    public void setRealCount(Byte realCount) {
        this.realCount = realCount;
    }

    public Long getScore() {
        return score;
    }

    public void setScore(Long score) {
        this.score = score;
    }

    public Boolean getLock() {
        return lock;
    }

    public void setLock(Boolean lock) {
        this.lock = lock;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick == null ? null : nick.trim();
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public Long getInviteId() {
        return inviteId;
    }

    public void setInviteId(Long inviteId) {
        this.inviteId = inviteId;
    }

    public String getInviteCode() {
        return inviteCode;
    }

    public void setInviteCode(String inviteCode) {
        this.inviteCode = inviteCode == null ? null : inviteCode.trim();
    }

    public Date getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Date lastLogin) {
        this.lastLogin = lastLogin;
    }

    public Byte getType() {
        return type;
    }

    public void setType(Byte type) {
        this.type = type;
    }

    public Byte getEffect() {
        return effect;
    }

    public void setEffect(Byte effect) {
        this.effect = effect;
    }

    public Byte getRoute() {
        return route;
    }

    public void setRoute(Byte route) {
        this.route = route;
    }

    public Long getChannel() {
        return channel;
    }

    public void setChannel(Long channel) {
        this.channel = channel;
    }

    public Long getSubChannel() {
        return subChannel;
    }

    public void setSubChannel(Long subChannel) {
        this.subChannel = subChannel;
    }

    public Long getLevel() {
        return level;
    }

    public void setLevel(Long level) {
        this.level = level;
    }

    public Byte getSex() {
        return sex;
    }

    public void setSex(Byte sex) {
        this.sex = sex;
    }

    public Integer getLoginFail() {
        return loginFail;
    }

    public void setLoginFail(Integer loginFail) {
        this.loginFail = loginFail;
    }

    public Date getLockTime() {
        return lockTime;
    }

    public void setLockTime(Date lockTime) {
        this.lockTime = lockTime;
    }

    public Date getFailTime() {
        return failTime;
    }

    public void setFailTime(Date failTime) {
        this.failTime = failTime;
    }

    public String getHeadImg() {
        return headImg;
    }

    public void setHeadImg(String headImg) {
        this.headImg = headImg == null ? null : headImg.trim();
    }
    
    public Byte getUserNature() {
		return userNature;
	}

	public void setUserNature(Byte userNature) {
		this.userNature = userNature;
	}

    public String getAdIdentifier() {
        return adIdentifier;
    }

    public void setAdIdentifier(String adIdentifier) {
        this.adIdentifier = adIdentifier;
    }

    public String getSimulateIDFA(){
        return simulateIDFA;
    }

    public void setSimulateIDFA(String simulateIDFA){
        this.simulateIDFA=simulateIDFA;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        User other = (User) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
                && (this.getRealName() == null ? other.getRealName() == null : this.getRealName().equals(other.getRealName()))
                && (this.getIdCard() == null ? other.getIdCard() == null : this.getIdCard().equals(other.getIdCard()))
                && (this.getMobile() == null ? other.getMobile() == null : this.getMobile().equals(other.getMobile()))
                && (this.getPassword() == null ? other.getPassword() == null : this.getPassword().equals(other.getPassword()))
                && (this.getPayPassword() == null ? other.getPayPassword() == null : this.getPayPassword().equals(other.getPayPassword()))
                && (this.getEmail() == null ? other.getEmail() == null : this.getEmail().equals(other.getEmail()))
                && (this.getValidEmail() == null ? other.getValidEmail() == null : this.getValidEmail().equals(other.getValidEmail()))
                && (this.getRealCount() == null ? other.getRealCount() == null : this.getRealCount().equals(other.getRealCount()))
                && (this.getScore() == null ? other.getScore() == null : this.getScore().equals(other.getScore()))
                && (this.getLock() == null ? other.getLock() == null : this.getLock().equals(other.getLock()))
                && (this.getNick() == null ? other.getNick() == null : this.getNick().equals(other.getNick()))
                && (this.getTime() == null ? other.getTime() == null : this.getTime().equals(other.getTime()))
                && (this.getAccountId() == null ? other.getAccountId() == null : this.getAccountId().equals(other.getAccountId()))
                && (this.getInviteId() == null ? other.getInviteId() == null : this.getInviteId().equals(other.getInviteId()))
                && (this.getInviteCode() == null ? other.getInviteCode() == null : this.getInviteCode().equals(other.getInviteCode()))
                && (this.getLastLogin() == null ? other.getLastLogin() == null : this.getLastLogin().equals(other.getLastLogin()))
                && (this.getType() == null ? other.getType() == null : this.getType().equals(other.getType()))
                && (this.getEffect() == null ? other.getEffect() == null : this.getEffect().equals(other.getEffect()))
                && (this.getRoute() == null ? other.getRoute() == null : this.getRoute().equals(other.getRoute()))
                && (this.getChannel() == null ? other.getChannel() == null : this.getChannel().equals(other.getChannel()))
                && (this.getSubChannel() == null ? other.getSubChannel() == null : this.getSubChannel().equals(other.getSubChannel()))
                && (this.getLevel() == null ? other.getLevel() == null : this.getLevel().equals(other.getLevel()))
                && (this.getSex() == null ? other.getSex() == null : this.getSex().equals(other.getSex()))
                && (this.getLoginFail() == null ? other.getLoginFail() == null : this.getLoginFail().equals(other.getLoginFail()))
                && (this.getLockTime() == null ? other.getLockTime() == null : this.getLockTime().equals(other.getLockTime()))
                && (this.getFailTime() == null ? other.getFailTime() == null : this.getFailTime().equals(other.getFailTime()))
                && (this.getHeadImg() == null ? other.getHeadImg() == null : this.getHeadImg().equals(other.getHeadImg()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getRealName() == null) ? 0 : getRealName().hashCode());
        result = prime * result + ((getIdCard() == null) ? 0 : getIdCard().hashCode());
        result = prime * result + ((getMobile() == null) ? 0 : getMobile().hashCode());
        result = prime * result + ((getPassword() == null) ? 0 : getPassword().hashCode());
        result = prime * result + ((getPayPassword() == null) ? 0 : getPayPassword().hashCode());
        result = prime * result + ((getEmail() == null) ? 0 : getEmail().hashCode());
        result = prime * result + ((getValidEmail() == null) ? 0 : getValidEmail().hashCode());
        result = prime * result + ((getRealCount() == null) ? 0 : getRealCount().hashCode());
        result = prime * result + ((getScore() == null) ? 0 : getScore().hashCode());
        result = prime * result + ((getLock() == null) ? 0 : getLock().hashCode());
        result = prime * result + ((getNick() == null) ? 0 : getNick().hashCode());
        result = prime * result + ((getTime() == null) ? 0 : getTime().hashCode());
        result = prime * result + ((getAccountId() == null) ? 0 : getAccountId().hashCode());
        result = prime * result + ((getInviteId() == null) ? 0 : getInviteId().hashCode());
        result = prime * result + ((getInviteCode() == null) ? 0 : getInviteCode().hashCode());
        result = prime * result + ((getLastLogin() == null) ? 0 : getLastLogin().hashCode());
        result = prime * result + ((getType() == null) ? 0 : getType().hashCode());
        result = prime * result + ((getEffect() == null) ? 0 : getEffect().hashCode());
        result = prime * result + ((getRoute() == null) ? 0 : getRoute().hashCode());
        result = prime * result + ((getChannel() == null) ? 0 : getChannel().hashCode());
        result = prime * result + ((getSubChannel() == null) ? 0 : getSubChannel().hashCode());
        result = prime * result + ((getLevel() == null) ? 0 : getLevel().hashCode());
        result = prime * result + ((getSex() == null) ? 0 : getSex().hashCode());
        result = prime * result + ((getLoginFail() == null) ? 0 : getLoginFail().hashCode());
        result = prime * result + ((getLockTime() == null) ? 0 : getLockTime().hashCode());
        result = prime * result + ((getFailTime() == null) ? 0 : getFailTime().hashCode());
        result = prime * result + ((getHeadImg() == null) ? 0 : getHeadImg().hashCode());
        return result;
    }
}