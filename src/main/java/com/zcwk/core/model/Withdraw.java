package com.zcwk.core.model;

import java.math.BigDecimal;
import java.util.Date;

public class Withdraw {
    /** 主键Id **/
    private Long id;

    /** 账号Id**/
    private Long accountId;

    /** 金额 **/
    private BigDecimal amount;

    /** 续投金额**/
    private BigDecimal pieceAmount;

    /** 银行卡Id**/
    private Long bankcardId;

    /** 审核状态**/
    private Byte auditState;

    /** 审批状态**/
    private Byte payState;

    /** 备注**/
    private String memo;

    /** 创建时间**/
    private Date time;

    /** 提现端 PC:1,IOS:2,Andrio:3，Wap：4**/
    private Byte route;

    /** 审核时间**/
    private Date auditTime;

    /** 审批时间**/
    private Date payTime;

    /** 审核管理员Id**/
    private Long auditUserId;

    /** 审批管理员Id**/
    private Long payUserId;

    /** 记录状态**/
    private Byte state;

    /**
     * 审批意见
     */
    private String payMemo;

    //手续费
    private BigDecimal fee;

    //到账金额
    private BigDecimal arriveAmount;

    //其他手续费
    private BigDecimal otherFee;

    //未投资扣除手续费
    private BigDecimal noInvestedFee;

    //到账时间
    private Date arriveTime;

    //订单号
    private String orderNo;

    private Byte gateWay;

    public Byte getGateWay() {
        return gateWay;
    }

    public void setGateWay(Byte gateWay) {
        this.gateWay = gateWay;
    }

    public void setFee(BigDecimal fee){
        this.fee=fee;
    }
    public BigDecimal getFee(){
        return fee;
    }
    public void setArriveAmount(BigDecimal arriveAmount){
        this.arriveAmount=arriveAmount;
    }
    public BigDecimal getArriveAmount(){
        return arriveAmount;
    }
    public void setOtherFee(BigDecimal otherFee){
        this.otherFee=otherFee;
    }
    public BigDecimal getOtherFee(){
        return otherFee;
    }
    public void setNoInvestedFee(BigDecimal noInvestedFee){
        this.noInvestedFee=noInvestedFee;
    }
    public BigDecimal getNoInvestedFee(){
        return noInvestedFee;
    }
    public void setArriveTime(Date arriveTime){
        this.arriveTime=arriveTime;
    }
    public Date getArriveTime(){
        return arriveTime;
    }

    public void setOrderNo(String orderNo){
        this.orderNo=orderNo;
    }
    public String getOrderNo(){
        return orderNo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getPieceAmount() {
        return pieceAmount;
    }

    public void setPieceAmount(BigDecimal pieceAmount) {
        this.pieceAmount = pieceAmount;
    }

    public Long getBankcardId() {
        return bankcardId;
    }

    public void setBankcardId(Long bankcardId) {
        this.bankcardId = bankcardId;
    }

    public Byte getAuditState() {
        return auditState;
    }

    public void setAuditState(Byte auditState) {
        this.auditState = auditState;
    }

    public Byte getState() {
        return state;
    }

    public void setState(Byte state) {
        this.state = state;
    }

    public Byte getPayState() {
        return payState;
    }

    public void setPayState(Byte payState) {
        this.payState = payState;
    }

    public String getPayMemo() {
        return payMemo;
    }

    public void setPayMemo(String payMemo) {
        this.payMemo = payMemo == null ? null : payMemo.trim();
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo == null ? null : memo.trim();
    }
    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Date getAuditTime() {
        return auditTime;
    }

    public void setAuditTime(Date auditTime) {
        this.auditTime = auditTime;
    }

    public Date getPayTime() {
        return payTime;
    }

    public void setPayTime(Date payTime) {
        this.payTime = payTime;
    }

    public Long getAuditUserId() {
        return auditUserId;
    }

    public void setAuditUserId(Long auditUserId) {
        this.auditUserId = auditUserId;
    }

    public Long getPayUserId() {
        return payUserId;
    }

    public void setPayUserId(Long payUserId) {
        this.payUserId = payUserId;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        Withdraw other = (Withdraw) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
                && (this.getAccountId() == null ? other.getAccountId() == null : this.getAccountId().equals(other.getAccountId()))
                && (this.getAmount() == null ? other.getAmount() == null : this.getAmount().equals(other.getAmount()))
                && (this.getBankcardId() == null ? other.getBankcardId() == null : this.getBankcardId().equals(other.getBankcardId()))
                && (this.getMemo() == null ? other.getMemo() == null : this.getMemo().equals(other.getMemo()))
                && (this.getTime() == null ? other.getTime() == null : this.getTime().equals(other.getTime()))
                && (this.getAuditTime() == null ? other.getAuditTime() == null : this.getAuditTime().equals(other.getAuditTime()))
                && (this.getPayTime() == null ? other.getPayTime() == null : this.getPayTime().equals(other.getPayTime()))
                && (this.getAuditUserId() == null ? other.getAuditUserId() == null : this.getAuditUserId().equals(other.getAuditUserId()))
                && (this.getPayUserId() == null ? other.getPayUserId() == null : this.getPayUserId().equals(other.getPayUserId()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getAccountId() == null) ? 0 : getAccountId().hashCode());
        result = prime * result + ((getAmount() == null) ? 0 : getAmount().hashCode());
        result = prime * result + ((getBankcardId() == null) ? 0 : getBankcardId().hashCode());
        result = prime * result + ((getMemo() == null) ? 0 : getMemo().hashCode());
        result = prime * result + ((getTime() == null) ? 0 : getTime().hashCode());
        result = prime * result + ((getAuditTime() == null) ? 0 : getAuditTime().hashCode());
        result = prime * result + ((getPayTime() == null) ? 0 : getPayTime().hashCode());
        result = prime * result + ((getAuditUserId() == null) ? 0 : getAuditUserId().hashCode());
        result = prime * result + ((getPayUserId() == null) ? 0 : getPayUserId().hashCode());
        return result;
    }

    public Byte getRoute() {
        return route;
    }

    public void setRoute(Byte route) {
        this.route = route;
    }
}