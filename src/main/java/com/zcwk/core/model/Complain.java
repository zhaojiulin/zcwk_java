package com.zcwk.core.model;

import java.util.Date;


/**
 * 意见反馈
 * @author zp
 * @since 2017年2月9日
 */
public class Complain {
	
	/** 主键id **/
    private Long id;
    
    /** 用户id **/
    private Long userId;
    
    /** 内容 **/
    private String content;;
    
    /** 添加时间 **/
    private Date addTime;

    
    
	public Complain() {
		super();
	}
	
	public Complain(Long userId, String content, Date addTime) {
		super();
		this.userId = userId;
		this.content = content;
		this.addTime = addTime;
	}



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getAddTime() {
		return addTime;
	}

	public void setAddTime(Date addTime) {
		this.addTime = addTime;
	}

}