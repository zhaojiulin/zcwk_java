package com.zcwk.core.model;

import java.math.BigDecimal;
import java.util.Date;

public class Stat {
    private Long id;

    private Long channel;

    private Long subChannel;

    private Integer gatway;

    private Integer route;

    private Long userId;

    private Integer regCount;

    private Integer realCount;

    private Integer emailCount;

    private Integer bindCount;

    private Integer rechargeCount;

    private BigDecimal rechargeAmount;

    private Integer withdrawCount;

    private BigDecimal withdrawAmount;

    private BigDecimal withdrawFee;

    private Integer buyCount;

    private BigDecimal buyAmount;

    private BigDecimal collect;

    private Date time;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getChannel() {
        return channel;
    }

    public void setChannel(Long channel) {
        this.channel = channel;
    }

    public Long getSubChannel() {
        return subChannel;
    }

    public void setSubChannel(Long subChannel) {
        this.subChannel = subChannel;
    }

    public Integer getGatway() {
        return gatway;
    }

    public void setGatway(Integer gatway) {
        this.gatway = gatway;
    }

    public Integer getRoute() {
        return route;
    }

    public void setRoute(Integer route) {
        this.route = route;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Integer getRegCount() {
        return regCount;
    }

    public void setRegCount(Integer regCount) {
        this.regCount = regCount;
    }

    public Integer getRealCount() {
        return realCount;
    }

    public void setRealCount(Integer realCount) {
        this.realCount = realCount;
    }

    public Integer getEmailCount() {
        return emailCount;
    }

    public void setEmailCount(Integer emailCount) {
        this.emailCount = emailCount;
    }

    public Integer getBindCount() {
        return bindCount;
    }

    public void setBindCount(Integer bindCount) {
        this.bindCount = bindCount;
    }

    public Integer getRechargeCount() {
        return rechargeCount;
    }

    public void setRechargeCount(Integer rechargeCount) {
        this.rechargeCount = rechargeCount;
    }

    public BigDecimal getRechargeAmount() {
        return rechargeAmount;
    }

    public void setRechargeAmount(BigDecimal rechargeAmount) {
        this.rechargeAmount = rechargeAmount;
    }

    public Integer getWithdrawCount() {
        return withdrawCount;
    }

    public void setWithdrawCount(Integer withdrawCount) {
        this.withdrawCount = withdrawCount;
    }

    public BigDecimal getWithdrawAmount() {
        return withdrawAmount;
    }

    public void setWithdrawAmount(BigDecimal withdrawAmount) {
        this.withdrawAmount = withdrawAmount;
    }

    public BigDecimal getWithdrawFee() {
        return withdrawFee;
    }

    public void setWithdrawFee(BigDecimal withdrawFee) {
        this.withdrawFee = withdrawFee;
    }

    public Integer getBuyCount() {
        return buyCount;
    }

    public void setBuyCount(Integer buyCount) {
        this.buyCount = buyCount;
    }

    public BigDecimal getBuyAmount() {
        return buyAmount;
    }

    public void setBuyAmount(BigDecimal buyAmount) {
        this.buyAmount = buyAmount;
    }

    public BigDecimal getCollect() {
        return collect;
    }

    public void setCollect(BigDecimal collect) {
        this.collect = collect;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        Stat other = (Stat) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getChannel() == null ? other.getChannel() == null : this.getChannel().equals(other.getChannel()))
            && (this.getSubChannel() == null ? other.getSubChannel() == null : this.getSubChannel().equals(other.getSubChannel()))
            && (this.getGatway() == null ? other.getGatway() == null : this.getGatway().equals(other.getGatway()))
            && (this.getRoute() == null ? other.getRoute() == null : this.getRoute().equals(other.getRoute()))
            && (this.getUserId() == null ? other.getUserId() == null : this.getUserId().equals(other.getUserId()))
            && (this.getRegCount() == null ? other.getRegCount() == null : this.getRegCount().equals(other.getRegCount()))
            && (this.getRealCount() == null ? other.getRealCount() == null : this.getRealCount().equals(other.getRealCount()))
            && (this.getEmailCount() == null ? other.getEmailCount() == null : this.getEmailCount().equals(other.getEmailCount()))
            && (this.getBindCount() == null ? other.getBindCount() == null : this.getBindCount().equals(other.getBindCount()))
            && (this.getRechargeCount() == null ? other.getRechargeCount() == null : this.getRechargeCount().equals(other.getRechargeCount()))
            && (this.getRechargeAmount() == null ? other.getRechargeAmount() == null : this.getRechargeAmount().equals(other.getRechargeAmount()))
            && (this.getWithdrawCount() == null ? other.getWithdrawCount() == null : this.getWithdrawCount().equals(other.getWithdrawCount()))
            && (this.getWithdrawAmount() == null ? other.getWithdrawAmount() == null : this.getWithdrawAmount().equals(other.getWithdrawAmount()))
            && (this.getWithdrawFee() == null ? other.getWithdrawFee() == null : this.getWithdrawFee().equals(other.getWithdrawFee()))
            && (this.getBuyCount() == null ? other.getBuyCount() == null : this.getBuyCount().equals(other.getBuyCount()))
            && (this.getBuyAmount() == null ? other.getBuyAmount() == null : this.getBuyAmount().equals(other.getBuyAmount()))
            && (this.getCollect() == null ? other.getCollect() == null : this.getCollect().equals(other.getCollect()))
            && (this.getTime() == null ? other.getTime() == null : this.getTime().equals(other.getTime()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getChannel() == null) ? 0 : getChannel().hashCode());
        result = prime * result + ((getSubChannel() == null) ? 0 : getSubChannel().hashCode());
        result = prime * result + ((getGatway() == null) ? 0 : getGatway().hashCode());
        result = prime * result + ((getRoute() == null) ? 0 : getRoute().hashCode());
        result = prime * result + ((getUserId() == null) ? 0 : getUserId().hashCode());
        result = prime * result + ((getRegCount() == null) ? 0 : getRegCount().hashCode());
        result = prime * result + ((getRealCount() == null) ? 0 : getRealCount().hashCode());
        result = prime * result + ((getEmailCount() == null) ? 0 : getEmailCount().hashCode());
        result = prime * result + ((getBindCount() == null) ? 0 : getBindCount().hashCode());
        result = prime * result + ((getRechargeCount() == null) ? 0 : getRechargeCount().hashCode());
        result = prime * result + ((getRechargeAmount() == null) ? 0 : getRechargeAmount().hashCode());
        result = prime * result + ((getWithdrawCount() == null) ? 0 : getWithdrawCount().hashCode());
        result = prime * result + ((getWithdrawAmount() == null) ? 0 : getWithdrawAmount().hashCode());
        result = prime * result + ((getWithdrawFee() == null) ? 0 : getWithdrawFee().hashCode());
        result = prime * result + ((getBuyCount() == null) ? 0 : getBuyCount().hashCode());
        result = prime * result + ((getBuyAmount() == null) ? 0 : getBuyAmount().hashCode());
        result = prime * result + ((getCollect() == null) ? 0 : getCollect().hashCode());
        result = prime * result + ((getTime() == null) ? 0 : getTime().hashCode());
        return result;
    }
}