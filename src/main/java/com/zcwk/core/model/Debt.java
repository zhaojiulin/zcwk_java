package com.zcwk.core.model;

import java.math.BigDecimal;
import java.util.Date;

public class Debt {
    private Long id;

    private String title;

    private Long accountId;

    private BigDecimal amount;

    private String no;

    private Byte productType;
    private  Byte debtProperty;
    private Byte interestType;

    private Byte paymentType;

    private Byte periodUnit;

    private int period;

    private BigDecimal minInvestAmount;

    private BigDecimal maxInvestAmount;

    private int investPeriod;

    private Date fullTime;

    private BigDecimal rate;

    private Byte awardType;

    private BigDecimal awardNumber;

    private Byte showType;

    private Byte state;

    private BigDecimal schedule;

    private BigDecimal investedAmount;

    private Date beginTime;

    private Date cutTime;

    private Date auditTime;

    private int version;

    private Boolean isNew;

    private Boolean isCut;

    private Byte investType;

    private BigDecimal investAmount;

    private BigDecimal overdueRate;

    private int overdueDay;

    private String imgFiles;

    private String safeguard;

    private String auditMemo;

    private String cancelMemo;

    private Date time;

    private Date lastAutoTime;

    private int periodDay;

    private int closedPeriod;

    private String mobile;
    
    /** 是否推荐标 0普通标; 1推荐标 **/
    private Integer isRecommend;
    
    /** 推荐时间 **/
    private Date recommendTime;

    /**
     * 加息标识
     */
    private String increaseRate;

    /**
     * 月标密码
     */
    private String debtPwd;

    /**
     * 抵押物
     */
    private String collateral;

    public void setIncreaseRate(String increaseRate){
        this.increaseRate=increaseRate;
    }

    public String getIncreaseRate(){
        return increaseRate;
    }

    public void setDebtPwd(String debtPwd){
        this.debtPwd=debtPwd;
    }

    public String getDebtPwd(){
        return debtPwd;
    }

    public void setCollateral(String collateral){
        this.collateral=collateral;
    }

    public String getCollateral(){
        return collateral;
    }

    public Integer getIsRecommend() {
		return isRecommend;
	}

	public void setIsRecommend(Integer isRecommend) {
		this.isRecommend = isRecommend;
	}

	public Date getRecommendTime() {
		return recommendTime;
	}

	public void setRecommendTime(Date recommendTime) {
		this.recommendTime = recommendTime;
	}

	public String getAuditMemo() {
        return auditMemo;
    }

    public void setAuditMemo(String auditMemo) {
        this.auditMemo = auditMemo;
    }

    public String getCancelMemo() {
        return cancelMemo;
    }

    public void setCancelMemo(String cancelMemo) {
        this.cancelMemo = cancelMemo;
    }

    public int getPeriodDay() {
        return periodDay;
    }

    public void setPeriodDay(int periodDay) {
        this.periodDay = periodDay;
    }

    public int getClosedPeriod() {
        return closedPeriod;
    }

    public void setClosedPeriod(int closedPeriod) {
        this.closedPeriod = closedPeriod;
    }

    public void setLastAutoTime(Date lastAutoTime) {
        this.lastAutoTime = lastAutoTime;
    }

    public Date getLastAutoTime() {
        return this.lastAutoTime;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getMobile() {
        return this.mobile;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Date getTime() {
        return this.time;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no == null ? null : no.trim();
    }

    public Byte getProductType() {
        return productType;
    }

    public void setProductType(Byte productType) {
        this.productType = productType;
    }

    public Byte getInterestType() {
        return interestType;
    }

    public void setInterestType(Byte interestType) {
        this.interestType = interestType;
    }

    public Byte getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(Byte paymentType) {
        this.paymentType = paymentType;
    }

    public Byte getPeriodUnit() {
        return periodUnit;
    }

    public void setPeriodUnit(Byte periodUnit) {
        this.periodUnit = periodUnit;
    }

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    public BigDecimal getMinInvestAmount() {
        return minInvestAmount;
    }

    public void setMinInvestAmount(BigDecimal minInvestAmount) {
        this.minInvestAmount = minInvestAmount;
    }

    public BigDecimal getMaxInvestAmount() {
        return maxInvestAmount;
    }

    public void setMaxInvestAmount(BigDecimal maxInvestAmount) {
        this.maxInvestAmount = maxInvestAmount;
    }

    public int getInvestPeriod() {
        return investPeriod;
    }

    public void setInvestPeriod(int investPeriod) {
        this.investPeriod = investPeriod;
    }

    public Date getFullTime() {
        return fullTime;
    }

    public void setFullTime(Date fullTime) {
        this.fullTime = fullTime;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public Byte getAwardType() {
        return awardType;
    }

    public void setAwardType(Byte awardType) {
        this.awardType = awardType;
    }

    public BigDecimal getAwardNumber() {
        return awardNumber;
    }

    public void setAwardNumber(BigDecimal awardNumber) {
        this.awardNumber = awardNumber;
    }

    public Byte getShowType() {
        return showType;
    }

    public void setShowType(Byte showType) {
        this.showType = showType;
    }

    public Byte getState() {
        return state;
    }

    public void setState(Byte state) {
        this.state = state;
    }

    public BigDecimal getSchedule() {
        return schedule;
    }

    public void setSchedule(BigDecimal schedule) {
        this.schedule = schedule;
    }

    public BigDecimal getInvestedAmount() {
        return investedAmount;
    }

    public void setInvestedAmount(BigDecimal investedAmount) {
        this.investedAmount = investedAmount;
    }

    public Date getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(Date beginTime) {
        this.beginTime = beginTime;
    }

    public Date getAuditTime() {
        return auditTime;
    }

    public void setAuditTime(Date auditTime) {
        this.auditTime = auditTime;
    }

    public Date getCutTime() {
        return cutTime;
    }

    public void setCutTime(Date cutTime) {
        this.cutTime = cutTime;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public Boolean getIsNew() {
        return isNew;
    }

    public void setIsNew(Boolean isNew) {
        this.isNew = isNew;
    }

    public Boolean getIsCut() {
        return isCut;
    }

    public void setIsCut(Boolean isCut) {
        this.isCut = isCut;
    }

    public Byte getInvestType() {
        return investType;
    }

    public void setInvestType(Byte investType) {
        this.investType = investType;
    }

    public BigDecimal getInvestAmount() {
        return investAmount;
    }

    public void setInvestAmount(BigDecimal investAmount) {
        this.investAmount = investAmount;
    }

    public BigDecimal getOverdueRate() {
        return overdueRate;
    }

    public void setOverdueRate(BigDecimal overdueRate) {
        this.overdueRate = overdueRate;
    }

    public int getOverdueDay() {
        return this.overdueDay;
    }

    public void setOverdueDay(int overdueDay) {
        this.overdueDay = overdueDay;
    }

    public String getImgFiles() {
        return imgFiles;
    }

    public void setImgFiles(String imgFiles) {
        this.imgFiles = imgFiles == null ? null : imgFiles.trim();
    }

    public String getSafeguard() {
        return safeguard;
    }

    public void setSafeguard(String safeguard) {
        this.safeguard = safeguard == null ? null : safeguard.trim();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        Debt other = (Debt) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
                && (this.getTitle() == null ? other.getTitle() == null : this.getTitle().equals(other.getTitle()))
                && (this.getAccountId() == null ? other.getAccountId() == null : this.getAccountId().equals(other.getAccountId()))
                && (this.getAmount() == null ? other.getAmount() == null : this.getAmount().equals(other.getAmount()))
                && (this.getNo() == null ? other.getNo() == null : this.getNo().equals(other.getNo()))
                && (this.getProductType() == null ? other.getProductType() == null : this.getProductType().equals(other.getProductType()))
                && (this.getDebtProperty() == null ? other.getDebtProperty() == null : this.getDebtProperty().equals(other.getDebtProperty()))
                && (this.getInterestType() == null ? other.getInterestType() == null : this.getInterestType().equals(other.getInterestType()))
                && (this.getPaymentType() == null ? other.getPaymentType() == null : this.getPaymentType().equals(other.getPaymentType()))
                && (this.getPeriodUnit() == null ? other.getPeriodUnit() == null : this.getPeriodUnit().equals(other.getPeriodUnit()))
                && (this.getMinInvestAmount() == null ? other.getMinInvestAmount() == null : this.getMinInvestAmount().equals(other.getMinInvestAmount()))
                && (this.getMaxInvestAmount() == null ? other.getMaxInvestAmount() == null : this.getMaxInvestAmount().equals(other.getMaxInvestAmount()))
                && (this.getFullTime() == null ? other.getFullTime() == null : this.getFullTime().equals(other.getFullTime()))
                && (this.getRate() == null ? other.getRate() == null : this.getRate().equals(other.getRate()))
                && (this.getAwardType() == null ? other.getAwardType() == null : this.getAwardType().equals(other.getAwardType()))
                && (this.getAwardNumber() == null ? other.getAwardNumber() == null : this.getAwardNumber().equals(other.getAwardNumber()))
                && (this.getShowType() == null ? other.getShowType() == null : this.getShowType().equals(other.getShowType()))
                && (this.getState() == null ? other.getState() == null : this.getState().equals(other.getState()))
                && (this.getSchedule() == null ? other.getSchedule() == null : this.getSchedule().equals(other.getSchedule()))
                && (this.getInvestedAmount() == null ? other.getInvestedAmount() == null : this.getInvestedAmount().equals(other.getInvestedAmount()))
                && (this.getBeginTime() == null ? other.getBeginTime() == null : this.getBeginTime().equals(other.getBeginTime()))
                && (this.getAuditTime() == null ? other.getAuditTime() == null : this.getAuditTime().equals(other.getAuditTime()))
                && (this.getIsNew() == null ? other.getIsNew() == null : this.getIsNew().equals(other.getIsNew()))
                && (this.getInvestType() == null ? other.getInvestType() == null : this.getInvestType().equals(other.getInvestType()))
                && (this.getInvestAmount() == null ? other.getInvestAmount() == null : this.getInvestAmount().equals(other.getInvestAmount()))
                && (this.getOverdueRate() == null ? other.getOverdueRate() == null : this.getOverdueRate().equals(other.getOverdueRate()))
                && (this.getImgFiles() == null ? other.getImgFiles() == null : this.getImgFiles().equals(other.getImgFiles()))
                && (this.getSafeguard() == null ? other.getSafeguard() == null : this.getSafeguard().equals(other.getSafeguard()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getTitle() == null) ? 0 : getTitle().hashCode());
        result = prime * result + ((getAccountId() == null) ? 0 : getAccountId().hashCode());
        result = prime * result + ((getAmount() == null) ? 0 : getAmount().hashCode());
        result = prime * result + ((getNo() == null) ? 0 : getNo().hashCode());
        result = prime * result + ((getProductType() == null) ? 0 : getProductType().hashCode());
        result = prime * result + ((getDebtProperty() == null) ? 0 : getDebtProperty().hashCode());
        result = prime * result + ((getInterestType() == null) ? 0 : getInterestType().hashCode());
        result = prime * result + ((getPaymentType() == null) ? 0 : getPaymentType().hashCode());
        result = prime * result + ((getPeriodUnit() == null) ? 0 : getPeriodUnit().hashCode());
        result = prime * result + ((getMinInvestAmount() == null) ? 0 : getMinInvestAmount().hashCode());
        result = prime * result + ((getMaxInvestAmount() == null) ? 0 : getMaxInvestAmount().hashCode());
        result = prime * result + ((getFullTime() == null) ? 0 : getFullTime().hashCode());
        result = prime * result + ((getRate() == null) ? 0 : getRate().hashCode());
        result = prime * result + ((getAwardType() == null) ? 0 : getAwardType().hashCode());
        result = prime * result + ((getAwardNumber() == null) ? 0 : getAwardNumber().hashCode());
        result = prime * result + ((getShowType() == null) ? 0 : getShowType().hashCode());
        result = prime * result + ((getState() == null) ? 0 : getState().hashCode());
        result = prime * result + ((getSchedule() == null) ? 0 : getSchedule().hashCode());
        result = prime * result + ((getInvestedAmount() == null) ? 0 : getInvestedAmount().hashCode());
        result = prime * result + ((getBeginTime() == null) ? 0 : getBeginTime().hashCode());
        result = prime * result + ((getAuditTime() == null) ? 0 : getAuditTime().hashCode());
        result = prime * result + ((getIsNew() == null) ? 0 : getIsNew().hashCode());
        result = prime * result + ((getInvestType() == null) ? 0 : getInvestType().hashCode());
        result = prime * result + ((getInvestAmount() == null) ? 0 : getInvestAmount().hashCode());
        result = prime * result + ((getOverdueRate() == null) ? 0 : getOverdueRate().hashCode());
        result = prime * result + ((getImgFiles() == null) ? 0 : getImgFiles().hashCode());
        result = prime * result + ((getSafeguard() == null) ? 0 : getSafeguard().hashCode());
        return result;
    }

    public Byte getDebtProperty() {
        return debtProperty;
    }

    public void setDebtProperty(Byte debtProperty) {
        this.debtProperty = debtProperty;
    }
}