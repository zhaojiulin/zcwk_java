package com.zcwk.core.model;

import java.math.BigDecimal;
import java.util.Date;

public class Transfer {
    private Long id;

    private Long investId;

    private Long debtId;

    private Long accountId;

    private BigDecimal amount;

    private BigDecimal award;

    private BigDecimal investedAmount;

    private BigDecimal schedule;

    private Boolean isCut;

    private Date cutTime;

    private Byte state;

    private Date time;

    private int periods;

    public int getPeriods() {
        return periods;
    }

    public void setPeriods(int periods) {
        this.periods = periods;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getInvestId() {
        return investId;
    }

    public void setInvestId(Long investId) {
        this.investId = investId;
    }

    public Long getDebtId() {
        return debtId;
    }

    public void setDebtId(Long debtId) {
        this.debtId = debtId;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getAward() {
        return award;
    }

    public void setAward(BigDecimal award) {
        this.award = award;
    }

    public BigDecimal getInvestedAmount() {
        return investedAmount;
    }

    public void setInvestedAmount(BigDecimal investedAmount) {
        this.investedAmount = investedAmount;
    }

    public BigDecimal getSchedule() {
        return schedule;
    }

    public void setSchedule(BigDecimal schedule) {
        this.schedule = schedule;
    }

    public Boolean getIsCut() {
        return isCut;
    }

    public void setIsCut(Boolean isCut) {
        this.isCut = isCut;
    }

    public Date getCutTime() {
        return cutTime;
    }

    public void setCutTime(Date cutTime) {
        this.cutTime = cutTime;
    }

    public Byte getState() {
        return state;
    }

    public void setState(Byte state) {
        this.state = state;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        Transfer other = (Transfer) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
                && (this.getInvestId() == null ? other.getInvestId() == null : this.getInvestId().equals(other.getInvestId()))
                && (this.getDebtId() == null ? other.getDebtId() == null : this.getDebtId().equals(other.getDebtId()))
                && (this.getAccountId() == null ? other.getAccountId() == null : this.getAccountId().equals(other.getAccountId()))
                && (this.getAmount() == null ? other.getAmount() == null : this.getAmount().equals(other.getAmount()))
                && (this.getAward() == null ? other.getAward() == null : this.getAward().equals(other.getAward()))
                && (this.getInvestedAmount() == null ? other.getInvestedAmount() == null : this.getInvestedAmount().equals(other.getInvestedAmount()))
                && (this.getSchedule() == null ? other.getSchedule() == null : this.getSchedule().equals(other.getSchedule()))
                && (this.getIsCut() == null ? other.getIsCut() == null : this.getIsCut().equals(other.getIsCut()))
                && (this.getCutTime() == null ? other.getCutTime() == null : this.getCutTime().equals(other.getCutTime()))
                && (this.getState() == null ? other.getState() == null : this.getState().equals(other.getState()))
                && (this.getTime() == null ? other.getTime() == null : this.getTime().equals(other.getTime()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getInvestId() == null) ? 0 : getInvestId().hashCode());
        result = prime * result + ((getDebtId() == null) ? 0 : getDebtId().hashCode());
        result = prime * result + ((getAccountId() == null) ? 0 : getAccountId().hashCode());
        result = prime * result + ((getAmount() == null) ? 0 : getAmount().hashCode());
        result = prime * result + ((getAward() == null) ? 0 : getAward().hashCode());
        result = prime * result + ((getInvestedAmount() == null) ? 0 : getInvestedAmount().hashCode());
        result = prime * result + ((getSchedule() == null) ? 0 : getSchedule().hashCode());
        result = prime * result + ((getIsCut() == null) ? 0 : getIsCut().hashCode());
        result = prime * result + ((getCutTime() == null) ? 0 : getCutTime().hashCode());
        result = prime * result + ((getState() == null) ? 0 : getState().hashCode());
        result = prime * result + ((getTime() == null) ? 0 : getTime().hashCode());
        return result;
    }
}