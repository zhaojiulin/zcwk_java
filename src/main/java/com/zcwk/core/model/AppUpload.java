package com.zcwk.core.model;

import java.util.Date;

/**
 * Created by Administrator on 2017/2/28.
 */
public class AppUpload {

    private  long id;//表id

    private  long verSion;//上传版本ID
    private  String versionName;//上传版本号
    private String Content;//上传版本内容
    private int isForce;//是否强制更新
    private Date time;//上传时间
    private  String downloadUrl;//下载链接


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }



    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }


    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }


    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }




    public String getContent() {
        return Content;
    }

    public void setContent(String content) {
        Content = content;
    }


    public int getIsForce() {
        return isForce;
    }

    public void setIsForce(int isForce) {
        this.isForce = isForce;
    }


    public long getVerSion() {
        return verSion;
    }

    public void setVerSion(long verSion) {
        this.verSion = verSion;
    }
}
