package com.zcwk.core.model;

import sun.security.util.BigInt;

import java.math.BigDecimal;
import java.util.Date;

public class BillDebt {
    private Long id;

    private Long borrowerId;

    private Long investorId;

    private BigDecimal paymentCorpus;

    private BigDecimal paymentInterest;

    private Date paymentTime;

    private Long debtId;

    private Boolean isTransfer;

    private Long transferId;

    private Byte periods;

    private Byte maxPeriods;

    private Byte state;

    private Date time;

    private Boolean isExperience;

    private Date realPaymentTime;

    /*** 加息券利息**/
    private BigDecimal paymentCouponInterest;

    private BigDecimal realPaymentInterest;

    private Boolean isOverdue;

    private BigDecimal overdueFine;

    private byte overdueDay;

    private int periodDay;

    private String group;

    private Long investId;

    private BigDecimal realPaymentCorpus;

    public Boolean getIsTransfer() {
        return isTransfer;
    }

    public void setIsTransfer(Boolean isTransfer) {
        this.isTransfer = isTransfer;
    }

    public Long getTransferId() {
        return transferId;
    }

    public void setTransferId(Long transferId) {
        this.transferId = transferId;
    }

    public Long getInvestId() {
        return investId;
    }

    public void setInvestId(Long investId) {
        this.investId = investId;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public int getPeriodDay() {
        return periodDay;
    }

    public void setPeriodDay(int periodDay) {
        this.periodDay = periodDay;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBorrowerId() {
        return this.borrowerId;
    }

    public void setBorrowerId(Long borrowerId) {
        this.borrowerId = borrowerId;
    }

    public Long getInvestorId() {
        return investorId;
    }

    public void setInvestorId(Long investorId) {
        this.investorId = investorId;
    }

    public BigDecimal getPaymentCorpus() {
        return paymentCorpus;
    }

    public void setPaymentCorpus(BigDecimal paymentCorpus) {
        this.paymentCorpus = paymentCorpus;
    }

    public BigDecimal getPaymentInterest() {
        return paymentInterest;
    }

    public void setPaymentInterest(BigDecimal paymentInterest) {
        this.paymentInterest = paymentInterest;
    }

    public Date getPaymentTime() {
        return paymentTime;
    }

    public void setPaymentTime(Date paymentTime) {
        this.paymentTime = paymentTime;
    }

    public Long getDebtId() {
        return debtId;
    }

    public void setDebtId(Long debtId) {
        this.debtId = debtId;
    }


    public Byte getPeriods() {
        return periods;
    }

    public void setPeriods(Byte periods) {
        this.periods = periods;
    }

    public Byte getMaxPeriods() {
        return maxPeriods;
    }

    public void setMaxPeriods(Byte maxPeriods) {
        this.maxPeriods = maxPeriods;
    }

    public Byte getState() {
        return state;
    }

    public void setState(Byte state) {
        this.state = state;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Boolean getIsExperience() {
        return isExperience;
    }

    public void setIsExperience(Boolean isExperience) {
        this.isExperience = isExperience;
    }

    public Date getRealPaymentTime() {
        return realPaymentTime;
    }

    public void setRealPaymentTime(Date realPaymentTime) {
        this.realPaymentTime = realPaymentTime;
    }

    public BigDecimal getRealPaymentCorpus() {
        return realPaymentCorpus;
    }

    public void setRealPaymentCorpus(BigDecimal realPaymentCorpus) {
        this.realPaymentCorpus = realPaymentCorpus;
    }

    public BigDecimal getRealPaymentInterest() {
        return realPaymentInterest;
    }

    public void setRealPaymentInterest(BigDecimal realPaymentInterest) {
        this.realPaymentInterest = realPaymentInterest;
    }

    public Boolean getIsOverdue() {
        return isOverdue;
    }

    public void setIsOverdue(Boolean isOverdue) {
        this.isOverdue = isOverdue;
    }

    public BigDecimal getOverdueFine() {
        return overdueFine;
    }

    public void setOverdueFine(BigDecimal overdueFine) {
        this.overdueFine = overdueFine;
    }

    public Byte getOverdueDay() {
        return overdueDay;
    }

    public void setOverdueDay(byte overdueDay) {
        this.overdueDay = overdueDay;
    }

    /**加息券利息**/
    public BigDecimal getPaymentCouponInterest() {
        return paymentCouponInterest;
    }

    /**加息券利息**/
    public void setPaymentCouponInterest(BigDecimal paymentCouponInterest) {
        this.paymentCouponInterest = paymentCouponInterest;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        BillDebt other = (BillDebt) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
                && (this.getPaymentCorpus() == null ? other.getPaymentCorpus() == null : this.getPaymentCorpus().equals(other.getPaymentCorpus()))
                && (this.getPaymentInterest() == null ? other.getPaymentInterest() == null : this.getPaymentInterest().equals(other.getPaymentInterest()))
                && (this.getPaymentCouponInterest() == null ? other.getPaymentCouponInterest() == null : this.getPaymentCouponInterest().equals(other.getPaymentCouponInterest()))
                && (this.getPaymentTime() == null ? other.getPaymentTime() == null : this.getPaymentTime().equals(other.getPaymentTime()))
                && (this.getDebtId() == null ? other.getDebtId() == null : this.getDebtId().equals(other.getDebtId()))
                && (this.getPeriods() == null ? other.getPeriods() == null : this.getPeriods().equals(other.getPeriods()))
                && (this.getMaxPeriods() == null ? other.getMaxPeriods() == null : this.getMaxPeriods().equals(other.getMaxPeriods()))
                && (this.getState() == null ? other.getState() == null : this.getState().equals(other.getState()))
                && (this.getTime() == null ? other.getTime() == null : this.getTime().equals(other.getTime()))
                && (this.getIsExperience() == null ? other.getIsExperience() == null : this.getIsExperience().equals(other.getIsExperience()))
                && (this.getRealPaymentTime() == null ? other.getRealPaymentTime() == null : this.getRealPaymentTime().equals(other.getRealPaymentTime()))
                && (this.getRealPaymentCorpus() == null ? other.getRealPaymentCorpus() == null : this.getRealPaymentCorpus().equals(other.getRealPaymentCorpus()))
                && (this.getRealPaymentInterest() == null ? other.getRealPaymentInterest() == null : this.getRealPaymentInterest().equals(other.getRealPaymentInterest()))
                && (this.getIsOverdue() == null ? other.getIsOverdue() == null : this.getIsOverdue().equals(other.getIsOverdue()))
                && (this.getOverdueFine() == null ? other.getOverdueFine() == null : this.getOverdueFine().equals(other.getOverdueFine()))
                && (this.getOverdueDay() == null ? other.getOverdueDay() == null : this.getOverdueDay().equals(other.getOverdueDay()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getPaymentCorpus() == null) ? 0 : getPaymentCorpus().hashCode());
        result = prime * result + ((getPaymentInterest() == null) ? 0 : getPaymentInterest().hashCode());
        result = prime * result + ((getPaymentCouponInterest() == null) ? 0 : getPaymentCouponInterest().hashCode());
        result = prime * result + ((getPaymentTime() == null) ? 0 : getPaymentTime().hashCode());
        result = prime * result + ((getDebtId() == null) ? 0 : getDebtId().hashCode());
        result = prime * result + ((getPeriods() == null) ? 0 : getPeriods().hashCode());
        result = prime * result + ((getMaxPeriods() == null) ? 0 : getMaxPeriods().hashCode());
        result = prime * result + ((getState() == null) ? 0 : getState().hashCode());
        result = prime * result + ((getTime() == null) ? 0 : getTime().hashCode());
        result = prime * result + ((getIsExperience() == null) ? 0 : getIsExperience().hashCode());
        result = prime * result + ((getRealPaymentTime() == null) ? 0 : getRealPaymentTime().hashCode());
        result = prime * result + ((getRealPaymentCorpus() == null) ? 0 : getRealPaymentCorpus().hashCode());
        result = prime * result + ((getRealPaymentInterest() == null) ? 0 : getRealPaymentInterest().hashCode());
        result = prime * result + ((getIsOverdue() == null) ? 0 : getIsOverdue().hashCode());
        result = prime * result + ((getOverdueFine() == null) ? 0 : getOverdueFine().hashCode());
        result = prime * result + ((getOverdueDay() == null) ? 0 : getOverdueDay().hashCode());
        return result;
    }
}