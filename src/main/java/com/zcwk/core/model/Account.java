package com.zcwk.core.model;

import java.math.BigDecimal;

public class Account {
    /*主键Id*/
    private Long id;

    /*用户id*/
    private Long userId;

    /**余额**/
    private BigDecimal balance;

    private BigDecimal pieceBalance;

    private BigDecimal freeze;

    private Boolean isNew;

    private Boolean bindCard;

    private BigDecimal experienceAmount;

    private BigDecimal collectCorpus;

    private BigDecimal collectInterest;

    private BigDecimal totalBorrowAmount;

    private BigDecimal totalReceivedCorpus;

    private BigDecimal totalReceivedInterest;

    private BigDecimal totalInvestAmount;

    private BigDecimal totalRewardAmount;

    private BigDecimal totalWithdrawAmount;

    private BigDecimal totalRechargeAmount;

    private Integer investCount;

    private String sign;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getInvestCount() {
        return investCount;
    }

    public void setInvestCount(Integer investCount) {
        this.investCount = investCount;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public BigDecimal getPieceBalance() {
        return pieceBalance;
    }

    public void setPieceBalance(BigDecimal pieceBalance) {
        this.pieceBalance = pieceBalance;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public BigDecimal getFreeze() {
        return freeze;
    }

    public void setFreeze(BigDecimal freeze) {
        this.freeze = freeze;
    }

    public Boolean getBindCard() {
        return bindCard;
    }

    public void setBindCard(Boolean bindCard) {
        this.bindCard = bindCard;
    }

    public Boolean getIsNew() {
        return isNew;
    }

    public void setIsNew(Boolean isNew) {
        this.isNew = isNew;
    }

    public BigDecimal getExperienceAmount() {
        return experienceAmount;
    }

    public void setExperienceAmount(BigDecimal experienceAmount) {
        this.experienceAmount = experienceAmount;
    }

    public BigDecimal getCollectCorpus() {
        return collectCorpus;
    }

    public void setCollectCorpus(BigDecimal collectCorpus) {
        this.collectCorpus = collectCorpus;
    }

    public BigDecimal getCollectInterest() {
        return collectInterest;
    }

    public void setCollectInterest(BigDecimal collectInterest) {
        this.collectInterest = collectInterest;
    }

    public BigDecimal getTotalBorrowAmount() {
        return totalBorrowAmount;
    }

    public void setTotalBorrowAmount(BigDecimal totalBorrowAmount) {
        this.totalBorrowAmount = totalBorrowAmount;
    }

    public BigDecimal getTotalReceivedCorpus() {
        return totalReceivedCorpus;
    }

    public void setTotalReceivedCorpus(BigDecimal totalReceivedCorpus) {
        this.totalReceivedCorpus = totalReceivedCorpus;
    }

    public BigDecimal getTotalReceivedInterest() {
        return totalReceivedInterest;
    }

    public void setTotalReceivedInterest(BigDecimal totalReceivedInterest) {
        this.totalReceivedInterest = totalReceivedInterest;
    }

    public BigDecimal getTotalInvestAmount() {
        return totalInvestAmount;
    }

    public void setTotalInvestAmount(BigDecimal totalInvestAmount) {
        this.totalInvestAmount = totalInvestAmount;
    }

    public BigDecimal getTotalRewardAmount() {
        return totalRewardAmount;
    }

    public void setTotalRewardAmount(BigDecimal totalRewardAmount) {
        this.totalRewardAmount = totalRewardAmount;
    }

    public BigDecimal getTotalWithdrawAmount() {
        return totalWithdrawAmount;
    }

    public void setTotalWithdrawAmount(BigDecimal totalWithdrawAmount) {
        this.totalWithdrawAmount = totalWithdrawAmount;
    }

    public BigDecimal getTotalRechargeAmount() {
        return totalRechargeAmount;
    }

    public void setTotalRechargeAmount(BigDecimal totalRechargeAmount) {
        this.totalRechargeAmount = totalRechargeAmount;
    }


    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign == null ? null : sign.trim();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        Account other = (Account) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
                && (this.getUserId() == null ? other.getUserId() == null : this.getUserId().equals(other.getUserId()))
                && (this.getBalance() == null ? other.getBalance() == null : this.getBalance().equals(other.getBalance()))
                && (this.getFreeze() == null ? other.getFreeze() == null : this.getFreeze().equals(other.getFreeze()))
                && (this.getIsNew() == null ? other.getIsNew() == null : this.getIsNew().equals(other.getIsNew()))
                && (this.getExperienceAmount() == null ? other.getExperienceAmount() == null : this.getExperienceAmount().equals(other.getExperienceAmount()))
                && (this.getCollectCorpus() == null ? other.getCollectCorpus() == null : this.getCollectCorpus().equals(other.getCollectCorpus()))
                && (this.getCollectInterest() == null ? other.getCollectInterest() == null : this.getCollectInterest().equals(other.getCollectInterest()))
                && (this.getTotalBorrowAmount() == null ? other.getTotalBorrowAmount() == null : this.getTotalBorrowAmount().equals(other.getTotalBorrowAmount()))
                && (this.getTotalReceivedCorpus() == null ? other.getTotalReceivedCorpus() == null : this.getTotalReceivedCorpus().equals(other.getTotalReceivedCorpus()))
                && (this.getTotalReceivedInterest() == null ? other.getTotalReceivedInterest() == null : this.getTotalReceivedInterest().equals(other.getTotalReceivedInterest()))
                && (this.getTotalInvestAmount() == null ? other.getTotalInvestAmount() == null : this.getTotalInvestAmount().equals(other.getTotalInvestAmount()))
                && (this.getTotalRewardAmount() == null ? other.getTotalRewardAmount() == null : this.getTotalRewardAmount().equals(other.getTotalRewardAmount()))
                && (this.getTotalWithdrawAmount() == null ? other.getTotalWithdrawAmount() == null : this.getTotalWithdrawAmount().equals(other.getTotalWithdrawAmount()))
                && (this.getTotalRechargeAmount() == null ? other.getTotalRechargeAmount() == null : this.getTotalRechargeAmount().equals(other.getTotalRechargeAmount()))
                && (this.getSign() == null ? other.getSign() == null : this.getSign().equals(other.getSign()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getUserId() == null) ? 0 : getUserId().hashCode());
        result = prime * result + ((getBalance() == null) ? 0 : getBalance().hashCode());
        result = prime * result + ((getFreeze() == null) ? 0 : getFreeze().hashCode());
        result = prime * result + ((getIsNew() == null) ? 0 : getIsNew().hashCode());
        result = prime * result + ((getExperienceAmount() == null) ? 0 : getExperienceAmount().hashCode());
        result = prime * result + ((getCollectCorpus() == null) ? 0 : getCollectCorpus().hashCode());
        result = prime * result + ((getCollectInterest() == null) ? 0 : getCollectInterest().hashCode());
        result = prime * result + ((getTotalBorrowAmount() == null) ? 0 : getTotalBorrowAmount().hashCode());
        result = prime * result + ((getTotalReceivedCorpus() == null) ? 0 : getTotalReceivedCorpus().hashCode());
        result = prime * result + ((getTotalReceivedInterest() == null) ? 0 : getTotalReceivedInterest().hashCode());
        result = prime * result + ((getTotalInvestAmount() == null) ? 0 : getTotalInvestAmount().hashCode());
        result = prime * result + ((getTotalRewardAmount() == null) ? 0 : getTotalRewardAmount().hashCode());
        result = prime * result + ((getTotalWithdrawAmount() == null) ? 0 : getTotalWithdrawAmount().hashCode());
        result = prime * result + ((getTotalRechargeAmount() == null) ? 0 : getTotalRechargeAmount().hashCode());
        result = prime * result + ((getSign() == null) ? 0 : getSign().hashCode());
        return result;
    }
}