package com.zcwk.core.model;

import java.util.Date;

/**
 * Created by pwx on 2017/5/27.
 */
public class BaoFoo {

    /** 主键Id **/
    private Long id;

    /** 银行 **/
    private String bank;

    /** 账户id **/
    private Long accountId;

    /** 账户id **/
    private String cardId;

    /** 户名 **/
    private String accountName;

    /** 预留手机号 **/
    private String mobile;

    /** 创建时间 **/
    private Date time;

    /** 绑定id **/
    private String bindId;

    public void setBindId(String bindId){
        this.bindId=bindId;
    }

    public String getBindId(){
        return bindId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank == null ? null : bank.trim();
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName == null ? null : accountName.trim();
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId == null ? null : cardId.trim();
    }

}
