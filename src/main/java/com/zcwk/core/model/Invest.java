package com.zcwk.core.model;

import java.math.BigDecimal;
import java.util.Date;

public class Invest {
    private Long id;

    private Long accountId;

    private Long debtId;

    private Boolean isTransfer;

    private Long transferId;

    private BigDecimal amount;

    private BigDecimal bonusAmount;

    private BigDecimal pieceAmount;

    private Boolean isAutomaticInvest;
 private Byte route;
    private Date time;

    private Boolean isExperience;

    private Byte debtPeriodUnit;

    private int debtPeriod;

    private BigDecimal debtRate;

    private BigDecimal couponRate;

    private Byte awardType;

    private BigDecimal awardNumber;

    private Byte state;

    private String protocol;

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public Long getDebtId() {
        return debtId;
    }

    public void setDebtId(Long debtId) {
        this.debtId = debtId;
    }

    public Boolean getIsTransfer() {
        return isTransfer;
    }

    public void setIsTransfer(Boolean isTransfer) {
        this.isTransfer = isTransfer;
    }

    public Long getTransferId() {
        return transferId;
    }

    public void setTransferId(Long transferId) {
        this.transferId = transferId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getBonusAmount() {
        return bonusAmount;
    }

    public void setBonusAmount(BigDecimal bonusAmount) {
        this.bonusAmount = bonusAmount;
    }

    public BigDecimal getPieceAmount() {
        return pieceAmount;
    }

    public void setPieceAmount(BigDecimal pieceAmount) {
        this.pieceAmount = pieceAmount;
    }

    public Boolean getIsAutomaticInvest() {
        return isAutomaticInvest;
    }

    public void setIsAutomaticInvest(Boolean isAutomaticInvest) {
        this.isAutomaticInvest = isAutomaticInvest;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Boolean getIsExperience() {
        return isExperience;
    }

    public void setIsExperience(Boolean isExperience) {
        this.isExperience = isExperience;
    }

    public Byte getDebtPeriodUnit() {
        return debtPeriodUnit;
    }

    public void setDebtPeriodUnit(Byte debtPeriodUnit) {
        this.debtPeriodUnit = debtPeriodUnit;
    }

    public int getDebtPeriod() {
        return debtPeriod;
    }

    public void setDebtPeriod(int debtPeriod) {
        this.debtPeriod = debtPeriod;
    }

    public BigDecimal getDebtRate() {
        return debtRate;
    }

    public void setDebtRate(BigDecimal debtRate) {
        this.debtRate = debtRate;
    }

    public BigDecimal getCouponRate() {
        return couponRate;
    }

    public void setCouponRate(BigDecimal couponRate) {
        this.couponRate = couponRate;
    }

    public Byte getAwardType() {
        return awardType;
    }

    public void setAwardType(Byte awardType) {
        this.awardType = awardType;
    }

    public BigDecimal getAwardNumber() {
        return awardNumber;
    }

    public void setAwardNumber(BigDecimal awardNumber) {
        this.awardNumber = awardNumber;
    }

    public Byte getState() {
        return state;
    }

    public void setState(Byte state) {
        this.state = state;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        Invest other = (Invest) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
                && (this.getAccountId() == null ? other.getAccountId() == null : this.getAccountId().equals(other.getAccountId()))
                && (this.getDebtId() == null ? other.getDebtId() == null : this.getDebtId().equals(other.getDebtId()))
                && (this.getIsTransfer() == null ? other.getIsTransfer() == null : this.getIsTransfer().equals(other.getIsTransfer()))
                && (this.getTransferId() == null ? other.getTransferId() == null : this.getTransferId().equals(other.getTransferId()))
                && (this.getAmount() == null ? other.getAmount() == null : this.getAmount().equals(other.getAmount()))
                && (this.getIsAutomaticInvest() == null ? other.getIsAutomaticInvest() == null : this.getIsAutomaticInvest().equals(other.getIsAutomaticInvest()))
                && (this.getTime() == null ? other.getTime() == null : this.getTime().equals(other.getTime()))
                && (this.getIsExperience() == null ? other.getIsExperience() == null : this.getIsExperience().equals(other.getIsExperience()))
                && (this.getDebtPeriodUnit() == null ? other.getDebtPeriodUnit() == null : this.getDebtPeriodUnit().equals(other.getDebtPeriodUnit()))
                && (this.getDebtRate() == null ? other.getDebtRate() == null : this.getDebtRate().equals(other.getDebtRate()))
                && (this.getAwardType() == null ? other.getAwardType() == null : this.getAwardType().equals(other.getAwardType()))
                && (this.getAwardNumber() == null ? other.getAwardNumber() == null : this.getAwardNumber().equals(other.getAwardNumber()))
                && (this.getState() == null ? other.getState() == null : this.getState().equals(other.getState()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getAccountId() == null) ? 0 : getAccountId().hashCode());
        result = prime * result + ((getDebtId() == null) ? 0 : getDebtId().hashCode());
        result = prime * result + ((getIsTransfer() == null) ? 0 : getIsTransfer().hashCode());
        result = prime * result + ((getTransferId() == null) ? 0 : getTransferId().hashCode());
        result = prime * result + ((getAmount() == null) ? 0 : getAmount().hashCode());
        result = prime * result + ((getIsAutomaticInvest() == null) ? 0 : getIsAutomaticInvest().hashCode());
        result = prime * result + ((getTime() == null) ? 0 : getTime().hashCode());
        result = prime * result + ((getIsExperience() == null) ? 0 : getIsExperience().hashCode());
        result = prime * result + ((getDebtPeriodUnit() == null) ? 0 : getDebtPeriodUnit().hashCode());
        result = prime * result + ((getDebtRate() == null) ? 0 : getDebtRate().hashCode());
        result = prime * result + ((getAwardType() == null) ? 0 : getAwardType().hashCode());
        result = prime * result + ((getAwardNumber() == null) ? 0 : getAwardNumber().hashCode());
        result = prime * result + ((getState() == null) ? 0 : getState().hashCode());
        return result;
    }

    public Byte getRoute() {
        return route;
    }

    public void setRoute(Byte route) {
        this.route = route;
    }
}