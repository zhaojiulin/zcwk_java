package com.zcwk.core.model;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by pwx on 2016/12/6.
 */
public class CouponSet {
    private Integer id;

    private String name;

    private BigDecimal rate;

    private Date sendBeginTime;

    private Date sendEndTime;

    private Byte sendAction;

    private BigDecimal sendBuyAmount;

    private BigDecimal sendTotalAmount;

    private BigDecimal sendCollect;

    private Byte sendInvestCount;

    private Boolean sendInvite;

    private Date useBeginTime;

    private Date useEndTime;

    private BigDecimal useBuyAmount;

    private Byte useProductType;

    private Byte usePeriodUnit;

    private Byte sendCount;

    private Byte usePeriod;

    private Byte state;

    private Date time;

    private Boolean sendFirst;

    public Boolean getSendFirst() {
        return sendFirst;
    }

    public void setSendFirst(Boolean sendFirst) {
        this.sendFirst = sendFirst;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Byte getSendCount() {
        return sendCount;
    }

    public void setSendCount(Byte sendCount) {
        this.sendCount = sendCount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public Date getSendBeginTime() {
        return sendBeginTime;
    }

    public void setSendBeginTime(Date sendBeginTime) {
        this.sendBeginTime = sendBeginTime;
    }

    public Date getSendEndTime() {
        return sendEndTime;
    }

    public void setSendEndTime(Date sendEndTime) {
        this.sendEndTime = sendEndTime;
    }

    public Byte getSendAction() {
        return sendAction;
    }

    public void setSendAction(Byte sendAction) {
        this.sendAction = sendAction;
    }

    public BigDecimal getSendBuyAmount() {
        return sendBuyAmount;
    }

    public void setSendBuyAmount(BigDecimal sendBuyAmount) {
        this.sendBuyAmount = sendBuyAmount;
    }

    public BigDecimal getSendTotalAmount() {
        return sendTotalAmount;
    }

    public void setSendTotalAmount(BigDecimal sendTotalAmount) {
        this.sendTotalAmount = sendTotalAmount;
    }

    public BigDecimal getSendCollect() {
        return sendCollect;
    }

    public void setSendCollect(BigDecimal sendCollect) {
        this.sendCollect = sendCollect;
    }

    public Byte getSendInvestCount() {
        return sendInvestCount;
    }

    public void setSendInvestCount(Byte sendInvestCount) {
        this.sendInvestCount = sendInvestCount;
    }

    public Boolean getSendInvite() {
        return sendInvite;
    }

    public void setSendInvite(Boolean sendInvite) {
        this.sendInvite = sendInvite;
    }

    public Date getUseBeginTime() {
        return useBeginTime;
    }

    public void setUseBeginTime(Date useBeginTime) {
        this.useBeginTime = useBeginTime;
    }

    public Date getUseEndTime() {
        return useEndTime;
    }

    public void setUseEndTime(Date useEndTime) {
        this.useEndTime = useEndTime;
    }

    public BigDecimal getUseBuyAmount() {
        return useBuyAmount;
    }

    public void setUseBuyAmount(BigDecimal useBuyAmount) {
        this.useBuyAmount = useBuyAmount;
    }

    public Byte getUseProductType() {
        return useProductType;
    }

    public void setUseProductType(Byte useProductType) {
        this.useProductType = useProductType;
    }

    public Byte getUsePeriodUnit() {
        return usePeriodUnit;
    }

    public void setUsePeriodUnit(Byte usePeriodUnit) {
        this.usePeriodUnit = usePeriodUnit;
    }

    public Byte getUsePeriod() {
        return usePeriod;
    }

    public void setUsePeriod(Byte usePeriod) {
        this.usePeriod = usePeriod;
    }

    public Byte getState() {
        return state;
    }

    public void setState(Byte state) {
        this.state = state;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        BonusSet other = (BonusSet) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
                && (this.getName() == null ? other.getName() == null : this.getName().equals(other.getName()))
                && (this.getRate() == null ? other.getAmount() == null : this.getRate().equals(other.getAmount()))
                && (this.getSendBeginTime() == null ? other.getSendBeginTime() == null : this.getSendBeginTime().equals(other.getSendBeginTime()))
                && (this.getSendEndTime() == null ? other.getSendEndTime() == null : this.getSendEndTime().equals(other.getSendEndTime()))
                && (this.getSendAction() == null ? other.getSendAction() == null : this.getSendAction().equals(other.getSendAction()))
                && (this.getSendBuyAmount() == null ? other.getSendBuyAmount() == null : this.getSendBuyAmount().equals(other.getSendBuyAmount()))
                && (this.getSendTotalAmount() == null ? other.getSendTotalAmount() == null : this.getSendTotalAmount().equals(other.getSendTotalAmount()))
                && (this.getSendCollect() == null ? other.getSendCollect() == null : this.getSendCollect().equals(other.getSendCollect()))
                && (this.getSendInvestCount() == null ? other.getSendInvestCount() == null : this.getSendInvestCount().equals(other.getSendInvestCount()))
                && (this.getSendInvite() == null ? other.getSendInvite() == null : this.getSendInvite().equals(other.getSendInvite()))
                && (this.getUseBeginTime() == null ? other.getUseBeginTime() == null : this.getUseBeginTime().equals(other.getUseBeginTime()))
                && (this.getUseEndTime() == null ? other.getUseEndTime() == null : this.getUseEndTime().equals(other.getUseEndTime()))
                && (this.getUseBuyAmount() == null ? other.getUseBuyAmount() == null : this.getUseBuyAmount().equals(other.getUseBuyAmount()))
                && (this.getUseProductType() == null ? other.getUseProductType() == null : this.getUseProductType().equals(other.getUseProductType()))
                && (this.getUsePeriodUnit() == null ? other.getUsePeriodUnit() == null : this.getUsePeriodUnit().equals(other.getUsePeriodUnit()))
                && (this.getUsePeriod() == null ? other.getUsePeriod() == null : this.getUsePeriod().equals(other.getUsePeriod()))
                && (this.getState() == null ? other.getState() == null : this.getState().equals(other.getState()))
                && (this.getTime() == null ? other.getTime() == null : this.getTime().equals(other.getTime()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
        result = prime * result + ((getRate() == null) ? 0 : getRate().hashCode());
        result = prime * result + ((getSendBeginTime() == null) ? 0 : getSendBeginTime().hashCode());
        result = prime * result + ((getSendEndTime() == null) ? 0 : getSendEndTime().hashCode());
        result = prime * result + ((getSendAction() == null) ? 0 : getSendAction().hashCode());
        result = prime * result + ((getSendBuyAmount() == null) ? 0 : getSendBuyAmount().hashCode());
        result = prime * result + ((getSendTotalAmount() == null) ? 0 : getSendTotalAmount().hashCode());
        result = prime * result + ((getSendCollect() == null) ? 0 : getSendCollect().hashCode());
        result = prime * result + ((getSendInvestCount() == null) ? 0 : getSendInvestCount().hashCode());
        result = prime * result + ((getSendInvite() == null) ? 0 : getSendInvite().hashCode());
        result = prime * result + ((getUseBeginTime() == null) ? 0 : getUseBeginTime().hashCode());
        result = prime * result + ((getUseEndTime() == null) ? 0 : getUseEndTime().hashCode());
        result = prime * result + ((getUseBuyAmount() == null) ? 0 : getUseBuyAmount().hashCode());
        result = prime * result + ((getUseProductType() == null) ? 0 : getUseProductType().hashCode());
        result = prime * result + ((getUsePeriodUnit() == null) ? 0 : getUsePeriodUnit().hashCode());
        result = prime * result + ((getUsePeriod() == null) ? 0 : getUsePeriod().hashCode());
        result = prime * result + ((getState() == null) ? 0 : getState().hashCode());
        result = prime * result + ((getTime() == null) ? 0 : getTime().hashCode());
        return result;
    }
}
