package com.zcwk.core.model;

import java.util.Date;

/**
 * 有米
 * Created by pwx on 2017/3/14.
 */
public class Youmi {

    private Long id;

    /**
     * 广告标识符
     */
    private String adIdentifier;

    /**
     *  Mac地址
     */
    private String mac;

    /**
     * 回调地址
     */
    private String callbackUrl;

    /**
     * 设备标识符
     */
    private String simulateIDFA;

    private Date time;

    private Long userId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAdIdentifier() {
        return adIdentifier;
    }

    public void setAdIdentifier(String adIdentifier) {
        this.adIdentifier = adIdentifier;
    }

    public String getMac(){
        return mac;
    }

    public void setMac(String mac){
        this.mac=mac;
    }

    public String getCallbackUrl(){
        return callbackUrl;
    }

    public void setCallbackUrl(String callbackUrl){
        this.callbackUrl=callbackUrl;
    }

    public String getSimulateIDFA(){
        return simulateIDFA;
    }

    public void setSimulateIDFA(String simulateIDFA){
        this.simulateIDFA=simulateIDFA;
    }

    public Date getTime(){
        return time;
    }

    public void setTime(Date time){
        this.time=time;
    }

    public Long getUserId(){
        return userId;
    }

    public void setUserId(Long userId){
        this.userId=userId;
    }
}
