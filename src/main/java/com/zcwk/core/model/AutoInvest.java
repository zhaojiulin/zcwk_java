package com.zcwk.core.model;

import java.math.BigDecimal;
import java.util.Date;

public class AutoInvest {
    private Long id;

    private Long accountId;

    private String productType;

    private BigDecimal minBuy;

    private BigDecimal maxBuy;

    private BigDecimal minBalance;

    private BigDecimal minRate;

    private BigDecimal maxRate;

    private Date lastAutoTime;

    private Byte state;

    private int minPeriodDay;

    private int maxPeriodDay;


    public int getMinPeriodDay() {
        return this.minPeriodDay;
    }

    public void setMinPeriodDay(int minPeriodDay) {
        this.minPeriodDay = minPeriodDay;
    }

    public int getMaxPeriodDay() {
        return this.maxPeriodDay;
    }

    public void setMaxPeriodDay(int maxPeriodDay) {
        this.maxPeriodDay = maxPeriodDay;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType == null ? null : productType.trim();
    }

    public BigDecimal getMinBuy() {
        return minBuy;
    }

    public void setMinBuy(BigDecimal minBuy) {
        this.minBuy = minBuy;
    }

    public BigDecimal getMaxBuy() {
        return maxBuy;
    }

    public void setMaxBuy(BigDecimal maxBuy) {
        this.maxBuy = maxBuy;
    }

    public BigDecimal getMinBalance() {
        return minBalance;
    }

    public void setMinBalance(BigDecimal minBalance) {
        this.minBalance = minBalance;
    }

    public BigDecimal getMinRate() {
        return minRate;
    }

    public void setMinRate(BigDecimal minRate) {
        this.minRate = minRate;
    }

    public BigDecimal getMaxRate() {
        return maxRate;
    }

    public void setMaxRate(BigDecimal maxRate) {
        this.maxRate = maxRate;
    }

    public Date getLastAutoTime() {
        return lastAutoTime;
    }

    public void setLastAutoTime(Date lastAutoTime) {
        this.lastAutoTime = lastAutoTime;
    }

    public Byte getState() {
        return state;
    }

    public void setState(Byte state) {
        this.state = state;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        AutoInvest other = (AutoInvest) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
                && (this.getAccountId() == null ? other.getAccountId() == null : this.getAccountId().equals(other.getAccountId()))
                && (this.getProductType() == null ? other.getProductType() == null : this.getProductType().equals(other.getProductType()))
                && (this.getMinBuy() == null ? other.getMinBuy() == null : this.getMinBuy().equals(other.getMinBuy()))
                && (this.getMaxBuy() == null ? other.getMaxBuy() == null : this.getMaxBuy().equals(other.getMaxBuy()))
                && (this.getMinBalance() == null ? other.getMinBalance() == null : this.getMinBalance().equals(other.getMinBalance()))
                && (this.getMinRate() == null ? other.getMinRate() == null : this.getMinRate().equals(other.getMinRate()))
                && (this.getMaxRate() == null ? other.getMaxRate() == null : this.getMaxRate().equals(other.getMaxRate()))
                && (this.getLastAutoTime() == null ? other.getLastAutoTime() == null : this.getLastAutoTime().equals(other.getLastAutoTime()))
                && (this.getState() == null ? other.getState() == null : this.getState().equals(other.getState()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getAccountId() == null) ? 0 : getAccountId().hashCode());
        result = prime * result + ((getProductType() == null) ? 0 : getProductType().hashCode());
        result = prime * result + ((getMinBuy() == null) ? 0 : getMinBuy().hashCode());
        result = prime * result + ((getMaxBuy() == null) ? 0 : getMaxBuy().hashCode());
        result = prime * result + ((getMinBalance() == null) ? 0 : getMinBalance().hashCode());
        result = prime * result + ((getMinRate() == null) ? 0 : getMinRate().hashCode());
        result = prime * result + ((getMaxRate() == null) ? 0 : getMaxRate().hashCode());
        result = prime * result + ((getLastAutoTime() == null) ? 0 : getLastAutoTime().hashCode());
        result = prime * result + ((getState() == null) ? 0 : getState().hashCode());
        return result;
    }
}