package com.zcwk.core.model;

import java.util.Date;

/**
 * app标识
 * @author zp
 * @since 2017年2月16日
 *
 */
public class AppClient {
	/** 主键id **/
    private Long id;

    /** 用户id **/
    private Long userId;

    /** app标识 **/
    private String clientId;
    
    /** app渠道 **/
    private int route;
    
    private Date addTime;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public int getRoute() {
		return route;
	}

	public void setRoute(int route) {
		this.route = route;
	}

	public Date getAddTime() {
		return addTime;
	}

	public void setAddTime(Date addTime) {
		this.addTime = addTime;
	}
    
}