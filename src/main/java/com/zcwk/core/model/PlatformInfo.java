package com.zcwk.core.model;

public class PlatformInfo {
    private Integer id;

    private String platformName;

    private String webLogo;

    private String adminLogo;

    private String companyName;

    private String address;

    private String phone1;

    private String phone2;

    private String qq1;

    private String qq2;

    private String qq3;

    private String record;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPlatformName() {
        return platformName;
    }

    public void setPlatformName(String platformName) {
        this.platformName = platformName == null ? null : platformName.trim();
    }

    public String getWebLogo() {
        return webLogo;
    }

    public void setWebLogo(String webLogo) {
        this.webLogo = webLogo == null ? null : webLogo.trim();
    }

    public String getAdminLogo() {
        return adminLogo;
    }

    public void setAdminLogo(String adminLogo) {
        this.adminLogo = adminLogo == null ? null : adminLogo.trim();
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName == null ? null : companyName.trim();
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    public String getPhone1() {
        return phone1;
    }

    public void setPhone1(String phone1) {
        this.phone1 = phone1 == null ? null : phone1.trim();
    }

    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2 == null ? null : phone2.trim();
    }

    public String getQq1() {
        return qq1;
    }

    public void setQq1(String qq1) {
        this.qq1 = qq1 == null ? null : qq1.trim();
    }

    public String getQq2() {
        return qq2;
    }

    public void setQq2(String qq2) {
        this.qq2 = qq2 == null ? null : qq2.trim();
    }

    public String getQq3() {
        return qq3;
    }

    public void setQq3(String qq3) {
        this.qq3 = qq3 == null ? null : qq3.trim();
    }

    public String getRecord() {
        return record;
    }

    public void setRecord(String record) {
        this.record = record == null ? null : record.trim();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        PlatformInfo other = (PlatformInfo) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getPlatformName() == null ? other.getPlatformName() == null : this.getPlatformName().equals(other.getPlatformName()))
            && (this.getWebLogo() == null ? other.getWebLogo() == null : this.getWebLogo().equals(other.getWebLogo()))
            && (this.getAdminLogo() == null ? other.getAdminLogo() == null : this.getAdminLogo().equals(other.getAdminLogo()))
            && (this.getCompanyName() == null ? other.getCompanyName() == null : this.getCompanyName().equals(other.getCompanyName()))
            && (this.getAddress() == null ? other.getAddress() == null : this.getAddress().equals(other.getAddress()))
            && (this.getPhone1() == null ? other.getPhone1() == null : this.getPhone1().equals(other.getPhone1()))
            && (this.getPhone2() == null ? other.getPhone2() == null : this.getPhone2().equals(other.getPhone2()))
            && (this.getQq1() == null ? other.getQq1() == null : this.getQq1().equals(other.getQq1()))
            && (this.getQq2() == null ? other.getQq2() == null : this.getQq2().equals(other.getQq2()))
            && (this.getQq3() == null ? other.getQq3() == null : this.getQq3().equals(other.getQq3()))
            && (this.getRecord() == null ? other.getRecord() == null : this.getRecord().equals(other.getRecord()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getPlatformName() == null) ? 0 : getPlatformName().hashCode());
        result = prime * result + ((getWebLogo() == null) ? 0 : getWebLogo().hashCode());
        result = prime * result + ((getAdminLogo() == null) ? 0 : getAdminLogo().hashCode());
        result = prime * result + ((getCompanyName() == null) ? 0 : getCompanyName().hashCode());
        result = prime * result + ((getAddress() == null) ? 0 : getAddress().hashCode());
        result = prime * result + ((getPhone1() == null) ? 0 : getPhone1().hashCode());
        result = prime * result + ((getPhone2() == null) ? 0 : getPhone2().hashCode());
        result = prime * result + ((getQq1() == null) ? 0 : getQq1().hashCode());
        result = prime * result + ((getQq2() == null) ? 0 : getQq2().hashCode());
        result = prime * result + ((getQq3() == null) ? 0 : getQq3().hashCode());
        result = prime * result + ((getRecord() == null) ? 0 : getRecord().hashCode());
        return result;
    }
}