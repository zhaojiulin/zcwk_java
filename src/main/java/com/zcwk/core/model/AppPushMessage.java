package com.zcwk.core.model;

import java.util.Date;

/**
 * app推送消息
 * @author zp
 * @since 2017年2月16日
 *
 */
public class AppPushMessage {
	/** 主键id **/
    private Long id;

    /** 标题 **/
    private String title;
    
    /** 类容 **/
    private String content;
    
    /** 透传类容 **/
    private String transmissionContent;
    
    /** 状态：1:成功；-1：失败 **/
    private int status;
    
    /** 备注 **/
    private String remark;
    
    private Date addTime;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getTransmissionContent() {
		return transmissionContent;
	}

	public void setTransmissionContent(String transmissionContent) {
		this.transmissionContent = transmissionContent;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Date getAddTime() {
		return addTime;
	}

	public void setAddTime(Date addTime) {
		this.addTime = addTime;
	}

}