package com.zcwk.core.service;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.*;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zcwk.core.dao.*;
import com.zcwk.core.model.*;
import com.zcwk.core.util.*;
import com.zcwk.core.util.Enums.*;




@Service
public class DebtService {

    @Autowired
    AccountMapper accountMapper;
    @Autowired
    AccountService accountService;
    @Autowired
    DebtMapper debtMapper;
    @Autowired
    UtilCache cache;
    @Autowired
    CashflowService cashflowService;
    @Autowired
    FreezeService freezeService;
    @Autowired
    InvestService investService;
    @Autowired
    InviteService inviteService;
    @Autowired
    BillDebtService billDebtService;
    @Autowired
    BillDebtMapper billDebtMapper;
    @Autowired
    DataSafety dataSafety;
    @Autowired
    UserService userService;
    @Autowired
    QueueService queueService;
    @Autowired
    BonusRecordMapper bonusRecordMapper;
    @Autowired
    BonusService bonusService;
    @Autowired
    CouponRecordMapper couponRecordMapper;
    @Autowired
    CouponService couponService;
    @Autowired
    InvestMapper investMapper;
    @Autowired
    SystemSetService systemSetService;

    /**
     * 添加
     *
     * @param debt
     * @return
     */
    @Transactional(rollbackFor = {Exception.class})
    public void save(Debt debt) throws UtilException {
        Debt dbDebt = null;
        if (debt.getTitle().isEmpty() || debt.getTitle() == null)
            throw new UtilException("借款标题不得为空");
        if (debt.getProductType() <= 0)
            throw new UtilException("产品类型不能为空");
        if (debt.getAwardType() == AwardType.Scale.getIndex() && debt.getAwardNumber().compareTo(BigDecimal.ZERO) > 0)
            debt.setAwardNumber(debt.getAwardNumber().divide(new BigDecimal(100), 4, BigDecimal.ROUND_HALF_UP));
        if (debt.getId() != null)
            dbDebt = this.debtMapper.selectByPrimaryKey(debt.getId());
        if (debt.getId() == null || dbDebt.getState() == DebtState.InConfirm.getIndex()) {
            if (debt.getRate() != null)
                debt.setRate(debt.getRate().divide(new BigDecimal(100), 4, BigDecimal.ROUND_HALF_UP));
            if (debt.getOverdueRate() != null)
                debt.setOverdueRate(debt.getOverdueRate().divide(new BigDecimal(100), 4, BigDecimal.ROUND_HALF_UP));
            if (debt.getMobile().isEmpty() || debt.getMobile() == null)
                throw new UtilException("借款人不得为空");
            if (debt.getPeriod() == 0)
                throw new UtilException("借款周期不得为0");
            if (debt.getAmount().compareTo(BigDecimal.ZERO) <= 0)
                throw new UtilException("借款金额输入不正确");
            if (debt.getRate().compareTo(BigDecimal.ZERO) <= 0)
                throw new UtilException("年化输入不正确");
            if (debt.getPeriodUnit() == PeriodUnitType.Day.getIndex() && (debt.getPaymentType() == PaymentType.Principal.getIndex() || debt.getPaymentType() == PaymentType.Interest.getIndex() || debt.getPaymentType() == PaymentType.Front.getIndex()))
                throw new UtilException("此回款方式不能为天标");
            if (debt.getPaymentType() == PaymentType.Front.getIndex() && debt.getInterestType() == InterestType.Now.getIndex())
                throw new UtilException("此回款方式计息方式不能为即投即计息");
            User user = this.userService.getUserByMobile(debt.getMobile());
            if (user == null)
                throw new UtilException("借款人不存在");
            if (user.getType() != (byte) AccountType.Borrower.getIndex())
                throw new UtilException("此账户不可用");
            Account account = this.accountMapper.selectByPrimaryKey(user.getAccountId());
            if (account == null)
                throw new UtilException("借款账户不存在");
            if (!this.dataSafety.checkAccount(account.getId()))
                throw new UtilException("借款账户异常");
            debt.setLastAutoTime(debt.getBeginTime());
            debt.setCutTime(CommonUtil.addDay(debt.getBeginTime(), debt.getInvestPeriod()));
            if (debt.getPeriodUnit() == (byte) PeriodUnitType.Month.getIndex()) {
                debt.setPeriodDay((CommonUtil.compareDay(debt.getBeginTime(), CommonUtil.addMonth(debt.getBeginTime(), debt.getPeriod()))));
            } else
                debt.setPeriodDay(debt.getPeriod());
            if (debt.getMaxInvestAmount().compareTo(BigDecimal.ZERO) <= 0)
                debt.setMaxInvestAmount(debt.getAmount());
            debt.setAccountId(account.getId());
        }
        if (debt.getId() == null) {
            debt.setState((byte) Enums.DebtState.InConfirm.getIndex());
            debt.setTime(new Date());
            this.debtMapper.insertSelective(debt);
            this.cache.delDebt(debt.getId());
        } else {
            if (debt.getState() != null)
                throw new UtilException("状态不允许更改");
            this.debtMapper.updateByPrimaryKeySelective(debt);
            this.cache.delDebt(debt.getId());
        }

    }

    /**
     * 获取标信息
     *
     * @param id
     * @return
     */
    public Debt getDebt(long id) throws UtilException {
        if (id <= 0)
            throw new UtilException("标编号不正确");
        Debt debt = this.cache.getDebt(id);
        return debt;
    }

    /**
     * 投标
     *
     * @return
     */
    @Transactional(rollbackFor = {Exception.class})
    public Long buy(long debtId, BigDecimal amount, long accountId, Boolean isAuto, String bonusId, String couponId,int route) throws UtilException {
        Debt debt = this.debtMapper.selectByPrimaryKey(debtId);
        // 判断标状态为可投状态
        if (debt.getState() != Enums.DebtState.ApprovalPass.getIndex())
            throw new UtilException("标的状态为不可投资状态");
        if (debt.getAccountId() == accountId)
            throw new UtilException("不能投自己的借款标");
        if(amount==null || amount.compareTo(BigDecimal.ZERO)<= 0) {
            throw new UtilException("购买金额不正确.");
        }
        // 判断标剩余金额
        if (debt.getInvestedAmount().add(amount).compareTo(debt.getAmount()) == 1) {
            if (!isAuto)
                throw new UtilException("超出可投金额");
            else
                amount = debt.getAmount().subtract(debt.getInvestedAmount());
        }
        if (debt.getBeginTime() != null && debt.getBeginTime().compareTo(new Date()) > 0)
            throw new UtilException("此标还未到可投时间");
        // 判断是否已截标
        if (debt.getCutTime() != null && debt.getCutTime().compareTo(new Date()) < 0)
            throw new UtilException("此标已过有效期");
        //判断剩余可投金额
        BigDecimal minInvest = debt.getMinInvestAmount();
        if (minInvest.compareTo(BigDecimal.ZERO) > 0 && minInvest.compareTo(debt.getAmount().subtract(debt.getInvestedAmount())) > 0)
            minInvest = debt.getAmount().subtract(debt.getInvestedAmount());
        if (minInvest.compareTo(BigDecimal.ZERO) > 0 && minInvest.compareTo(amount) == 1)
            throw new UtilException("最低可投金额为" + minInvest);
        BigDecimal investedAmount = this.investMapper.selectSumByDebt(debtId, accountId, (byte) State.Wait.getIndex());
        if (debt.getMaxInvestAmount().compareTo(BigDecimal.ZERO) > 0 && (investedAmount.add(amount)).compareTo(debt.getMaxInvestAmount()) > 0)
            throw new UtilException("此标您还可投金额为" + debt.getMaxInvestAmount().subtract(investedAmount) + "元");
        Account account = this.accountMapper.selectByPrimaryKey(accountId);
        if (account == null)
            throw new UtilException("投资人不存在");
        if (debt.getBeginTime() != null && debt.getBeginTime().compareTo(new Date()) > 0)
            throw new UtilException("投资时间未到");
        if (!this.dataSafety.checkAccount(account.getId()))
            throw new UtilException("账户出现异常，请联系管理员");
        //判断是否为新手
        if (!account.getIsNew() && debt.getIsNew())
            throw new UtilException("您已经不是新手了");
        //获取红包金额
        BigDecimal bonusAmount = BigDecimal.ZERO;
        if (bonusId != null && bonusId != "") {
            BonusRecord record = this.bonusRecordMapper.selectByPrimaryKey(Long.valueOf(bonusId));
            if (record != null)
                bonusAmount = record.getAmount();
        }
        //获加息券利率
        BigDecimal couponRate = BigDecimal.ZERO;
        if (couponId != null && couponId != "") {
            CouponRecord record = this.couponRecordMapper.selectByPrimaryKey(Long.valueOf(couponId));
            if (record != null)
                couponRate = record.getRate();
        }
        // 判断余额
        if ((account.getBalance().add(bonusAmount)).compareTo(amount) < 0)
            throw new UtilException("账户余额不足，请充值");
        // 更新已投金额
        int buyResult = this.debtMapper.buyDebt(debt.getId(), amount);
        if (buyResult < 1)
            throw new UtilException("项目可投金额不足");
        // 增加投资记录
        Invest invest = this.investService.addDebt(account, debt, amount, bonusAmount, couponRate,route, isAuto, false);
        //使用红包
        if (bonusId != null && bonusId != "")
            if (!this.bonusService.useBonus(Long.valueOf(bonusId), invest.getId(), amount, debt))
                throw new UtilException("使用红包异常，请核查红包使用条件");
        //使用加息券
        if (couponId != null && couponId != "")
            if (!this.couponService.useCoupon(Long.valueOf(couponId), invest.getId(), amount, debt))
                throw new UtilException("使用加息券异常，请核查加息券使用条件");
        // 购买成功后处理
        this.buySuccess(invest, debt, account, amount);
        this.cache.delDebt(debt.getId());
        this.cache.getDebt(debt.getId());
        //添加queue
        this.queueService.insertQueue(Actions.Buy.getIndex(), account.getUserId(), invest, amount);
        //添加统计
        if(amount!=null){
            this.cache.setTotal(TotalItems.TotalInvest.getKey(), amount.doubleValue());
            this.cache.setTotal(TotalItems.TodayInvest.getKey(), amount.doubleValue());
        }
        if(invest!=null && invest.getPieceAmount()!=null){
            this.cache.setTotal(TotalItems.TodayPieceAmount.getKey(), invest.getPieceAmount().doubleValue());
        }
        // 返回投资记录
        return invest.getId();
    }

    /**
     * 购买成功后处理
     *
     * @return
     */
    @Transactional(rollbackFor = {Exception.class})
    private void buySuccess(Invest invest, Debt debt, Account account,
                            BigDecimal amount) throws UtilException {
        Account investAccount = new Account();
        investAccount.setId(invest.getAccountId());
        investAccount.setInvestCount(1); //购买次数加1

        // 判断计息方式，如果为即投即计息，立刻生成回款计划
        List<Invest> list = new ArrayList<Invest>();
        if (debt.getInterestType() == InterestType.Now.getIndex()) {
            list.add(invest);
            createBill(debt, list); // 生成回款计划
        } else {
            // 增加冻结金额
            this.freezeService.save(account.getId(), Enums.FundType.Buy, invest.getId(), amount);
            investAccount.setFreeze(invest.getAmount());
        }
        // 如果是新手标
        if (account.getIsNew()) {
            Account acc = new Account();
            acc.setIsNew(false);
            acc.setId(account.getId());
            this.accountMapper.updateByPrimaryKeySelective(acc);
        }

        // 更新用户余额
        investAccount.setBalance(invest.getAmount().multiply(new BigDecimal(-1)));
        investAccount.setPieceBalance(invest.getPieceAmount().multiply(new BigDecimal(-1)));
        investAccount.setTotalInvestAmount(amount);
        this.accountService.updateBalance(investAccount, invest.getAmount().multiply(new BigDecimal(-1)), Enums.FundType.Buy, invest.getId(), "扣除投资金额");
        Account upAcc = this.accountMapper.selectByPrimaryKey(investAccount.getId());
        if (upAcc.getBalance().compareTo(BigDecimal.ZERO) < 0) {
            throw new UtilException("余额不足");
        }

    }

    /**
     * 发放投标奖励
     *
     * @return
     * @throws UtilException
     */
    public void sendInvestAward(List<Invest> list) throws UtilException {
        for (Invest invest : list) {
            if (invest.getAwardType() > 0) {
                if (invest.getAwardNumber().compareTo(BigDecimal.ZERO) == 1) {
                    BigDecimal award = BigDecimal.ZERO;
                    Account acc = new Account();
                    acc.setId(invest.getAccountId());
                    if (invest.getAwardType() == Enums.AwardType.Scale.getIndex())
                        award = invest.getAwardNumber().multiply(invest.getAmount());
                    else
                        award = invest.getAwardNumber();
                    award = award.setScale(2, BigDecimal.ROUND_HALF_UP);
                    if (award.compareTo(BigDecimal.ZERO) > 0) {
                        acc.setBalance(award);
                        acc.setTotalRewardAmount(award);
                        this.accountService.updateBalance(acc, award, FundType.InvestAward, invest.getId(), "增加投标奖励");
                    }
                }
            }
        }
    }

    /**
     * 发放续投奖励
     *
     * @return
     * @throws UtilException
     */
    public void sendPieceAward(List<Invest> list) throws UtilException {
        Object piece = this.systemSetService.getParamVal(Params.Piece.getKey());
        if (piece != null) {
            //System.out.println("续投奖励金额:");
            BigDecimal _piece = new BigDecimal(piece.toString());
            for (Invest invest : list) {
                BigDecimal award = _piece.multiply(invest.getPieceAmount());
                if (award.compareTo(BigDecimal.ZERO) > 0) {
                    Account acc = new Account();
                    acc.setId(invest.getAccountId());
                    acc.setBalance(award);
                    acc.setTotalRewardAmount(award);
                    this.accountService.updateBalance(acc, award, FundType.PieceAward, invest.getId(), "增加续投奖励");
                }
            }
        }
    }

    /**
     * 预期收益
     *
     * @return
     * @throws UtilException
     */
    public List<HashMap> getExpect(long debtId, BigDecimal amount) {
        List<HashMap> returnMap = new ArrayList<>();
        try {
            Debt debt = this.cache.getDebt(debtId);
            if (debt != null && amount.compareTo(BigDecimal.ZERO) > 0) {
                HashMap<Date, Object[]> map = this.getPeriods(debt, amount);
                for (Date dt : map.keySet()) {
                    HashMap<String, Object> _map = new HashMap<>();
                    _map.put("time", dt.getTime());
                    _map.put("corpus", map.get(dt)[0]);
                    _map.put("interest", map.get(dt)[1]);
                    returnMap.add(_map);
                }
            }
            return returnMap;
        } catch (Exception ex) {
            return returnMap;
        }
    }

    /**
     * 回款期数
     *
     * @return
     * @throws UtilException
     */
    private HashMap<Date, Object[]> getPeriods(Debt debt, BigDecimal amount) {
        LinkedHashMap<Date, Object[]> map = new LinkedHashMap<Date, Object[]>();
        Date preDate = new Date();
        Date _currDate = preDate;
        Date currDate = null;
        BigDecimal corpus = BigDecimal.ZERO;
        BigDecimal interest = BigDecimal.ZERO;
        if (debt.getPaymentType() == (byte) PaymentType.Once.getIndex()) {
            int periods = 1;
            corpus = amount;
            if (debt.getPeriodUnit() == (byte) PeriodUnitType.Month.getIndex()) {
                currDate = CommonUtil.addMonth(preDate, debt.getPeriod());
                interest = (amount.multiply(debt.getRate().divide(BigDecimal.valueOf(12), 8, BigDecimal.ROUND_HALF_UP)).multiply(BigDecimal.valueOf(debt.getPeriod()))).setScale(2, BigDecimal.ROUND_HALF_UP);
            } else {
                currDate = CommonUtil.addDay(preDate, debt.getPeriod());
                interest = (amount.multiply(debt.getRate().divide(BigDecimal.valueOf(365), 8, BigDecimal.ROUND_HALF_UP)).multiply(BigDecimal.valueOf(debt.getPeriod()))).setScale(2, BigDecimal.ROUND_HALF_UP);
            }
            map.put(currDate, new Object[]{corpus, interest, CommonUtil.compareDay(preDate, currDate), periods});
        } else if (debt.getPaymentType() == (byte) PaymentType.Interest.getIndex()) {
            double monthRate = debt.getRate().doubleValue() / 12;
            // 每月利息  = 剩余本金 x 贷款月利率
            BigDecimal monthIncome = amount.multiply(new BigDecimal(monthRate * Math.pow(1 +
                    monthRate, debt.getPeriod()))).divide(new BigDecimal(Math.pow(1 + monthRate, debt.getPeriod()) - 1), 2,
                    BigDecimal.ROUND_HALF_UP);
            BigDecimal capital = amount;
            for (int i = 1; i < debt.getPeriod() + 1; i++) {
                capital = capital.subtract(corpus);
                interest = capital.multiply(new BigDecimal(monthRate)).setScale(2, BigDecimal.ROUND_HALF_UP);
                corpus = (i == debt.getPeriod() ? capital : monthIncome.subtract(interest));
                currDate = CommonUtil.addMonth(_currDate, i);
                map.put(currDate, new Object[]{corpus, interest, CommonUtil.compareDay(preDate, currDate), i});
                preDate = currDate;
            }
        } else if (debt.getPaymentType() == (byte) PaymentType.Principal.getIndex()) {
            double monthRate = debt.getRate().doubleValue() / 12;
            BigDecimal capital = BigDecimal.ZERO;
            for (int i = 1; i < debt.getPeriod() + 1; i++) {
                interest = ((amount.subtract(capital)).multiply(BigDecimal.valueOf(monthRate))).setScale(2, BigDecimal.ROUND_HALF_UP);
                corpus = (i == debt.getPeriod() ? amount.subtract(capital) : amount.divide(BigDecimal.valueOf(debt.getPeriod()), 2, BigDecimal.ROUND_HALF_UP));
                capital = capital.add(amount.divide(BigDecimal.valueOf(debt.getPeriod()), 2, BigDecimal.ROUND_HALF_UP));
                currDate = CommonUtil.addMonth(_currDate, i);
                map.put(currDate, new Object[]{corpus, interest, CommonUtil.compareDay(preDate, currDate), i});
                preDate = currDate;
            }
        } else {
            if (debt.getPeriodUnit() == (byte) PeriodUnitType.Month.getIndex()) {
                interest = (amount.multiply(debt.getRate().divide(BigDecimal.valueOf(12), 8, BigDecimal.ROUND_HALF_UP))).setScale(2, BigDecimal.ROUND_HALF_UP);
                int month = debt.getPeriod();
                if (debt.getPaymentType() == (byte) PaymentType.Front.getIndex())
                    month++;
                for (int i = 1; i <= month; i++) {
                    if (i == month && debt.getPaymentType() == (byte) PaymentType.Front.getIndex())
                        interest = BigDecimal.ZERO;
                    if (i == month)
                        corpus = amount;
                    if (debt.getPaymentType() == (byte) PaymentType.Front.getIndex())
                        currDate = CommonUtil.addMonth(_currDate, i - 1);
                    else
                        currDate = CommonUtil.addMonth(_currDate, i);
                    map.put(currDate, new Object[]{corpus, interest, CommonUtil.compareDay(preDate, currDate), i});
                    preDate = currDate;
                }
            } else {
                int days = debt.getPeriodDay();
                int i = 1;
                while (days > 0) {
                    currDate = CommonUtil.addMonth(_currDate, i);
                    int diffDay = CommonUtil.compareDay(preDate, currDate);
                    if (days - diffDay <= 0) {
                        corpus = amount;
                        currDate = CommonUtil.addDay(preDate, days);
                    }
                    interest = (amount.multiply(debt.getRate().divide(BigDecimal.valueOf(365), 8, BigDecimal.ROUND_HALF_UP)).multiply(BigDecimal.valueOf(CommonUtil.compareDay(preDate, currDate)))).setScale(2, BigDecimal.ROUND_HALF_UP);
                    map.put(currDate, new Object[]{corpus, interest, CommonUtil.compareDay(preDate, currDate), i});
                    preDate = currDate;
                    days -= diffDay;
                    i++;
                }
            }
        }
        return map;
    }

    /**
     * 生成回款计划
     *
     * @return
     */
    @Transactional(rollbackFor = {Exception.class})
    public void createBill(Debt debt, List<Invest> list) throws UtilException {
        BigDecimal amount = BigDecimal.ZERO;
        BigDecimal debtAmount = BigDecimal.ZERO;
        BigDecimal corpus = BigDecimal.ZERO;
        BigDecimal interest = BigDecimal.ZERO;
        BigDecimal couponInterest = BigDecimal.ZERO;
        int periodDay = 0;
        int periods = 0;
        Account debtAccount = new Account();
        Account investAccount;
        for (Invest invest : list) {
            BigDecimal totalInterest = BigDecimal.ZERO;
            amount = invest.getAmount();
            investAccount = new Account();
            HashMap<Date, Object[]> map = this.getPeriods(debt, amount);
            if (invest.getCouponRate()!=null && invest.getCouponRate().compareTo(BigDecimal.ZERO) > 0){
                Debt _debt=new Debt();
                _debt.setId(debt.getId());
                _debt.setRate(invest.getCouponRate());
                _debt.setPaymentType(debt.getPaymentType());
                _debt.setPeriod(debt.getPeriod());
                _debt.setPeriodUnit(debt.getPeriodUnit());
                HashMap<Date, Object[]> _map = this.getPeriods(_debt, amount);
                for (Date dt : _map.keySet()) {
                    couponInterest = (BigDecimal) _map.get(dt)[1];
                }
            }
            for (Date dt : map.keySet()) {
                corpus = (BigDecimal) map.get(dt)[0];
                interest = (BigDecimal) map.get(dt)[1];
                periodDay = (int) map.get(dt)[2];
                periods = (int) map.get(dt)[3];
                this.billDebtService.saveDebt(debt.getAccountId(), invest.getAccountId(), corpus, interest,couponInterest, dt, debt, (byte) periods, (byte) map.size(), periodDay, invest.getId(), false);
                totalInterest = totalInterest.add(interest).add(couponInterest);

                //统计待还款金额 2017-3-25
                BigDecimal todayPayment=corpus.add(interest);
                this.cache.setTotal(TotalItems.TotalWaitPaymentCount.getKey(),1);
                if(todayPayment!=null){
                    this.cache.setTotal(TotalItems.TotalWaitPaymentAmount.getKey(), todayPayment.doubleValue());
                }
            }
            debtAmount = debtAmount.add(invest.getAmount());
            investAccount.setId(invest.getAccountId());
            investAccount.setCollectCorpus(amount);
            investAccount.setCollectInterest(totalInterest);
            this.accountService.updateAccountTotal(investAccount);
        }
        debtAccount.setId(debt.getAccountId());
        debtAccount.setBalance(debtAmount);
        debtAccount.setTotalBorrowAmount(debtAmount);
        this.accountService.updateBalance(debtAccount, debtAmount, Enums.FundType.Debt, debt.getId(), "增加借款金额");
    }

    /**
     * 还款
     *
     * @return
     * @throws Exception
     */
    @Transactional(rollbackFor = {Exception.class})
    public void payment(long debtId, String group, boolean advance) throws Exception {
        if (group.isEmpty() || group == null)
            throw new UtilException("回款时间不得为空");
        List<BillDebt> list = this.billDebtService.getPayment(group);
        Debt debt = this.debtMapper.selectByPrimaryKey(debtId);
        if (debt.getState() != Enums.DebtState.InPayment.getIndex() && debt.getState() != DebtState.Overdue.getIndex())
            throw new UtilException("标状态为不可还款状态");
        BillDebt billDebt = new BillDebt();
        Account debtAccount = new Account();
        Account investAccount = new Account();
        BigDecimal realCorpus = null;
        BigDecimal realInterest = null;
        BigDecimal realCouponInterest=BigDecimal.ZERO;;
        BigDecimal totalAmount = BigDecimal.ZERO;
        BigDecimal totalInterest = BigDecimal.ZERO;
        BigDecimal todayPayment=BigDecimal.ZERO;//今日还款金额
        int todayPaymentCount=0;
        BigDecimal overdueAmount = null;
        User user = null;
        Invest invest = new Invest();
        for (BillDebt bill : list) {
            todayPayment=todayPayment.add(bill.getPaymentCorpus()).add(bill.getPaymentInterest());
            todayPaymentCount=todayPaymentCount-1;
            realCorpus = bill.getPaymentCorpus();
            realInterest = CommonUtil.calculateInterest(bill.getPaymentTime(), bill.getPaymentInterest(), bill.getPeriodDay(), advance);
            if(bill.getPaymentCouponInterest()!=null){
                realCouponInterest = CommonUtil.calculateInterest(bill.getPaymentTime(), bill.getPaymentCouponInterest(), bill.getPeriodDay(), advance);
            }
             overdueAmount = bill.getOverdueFine();
            totalAmount = totalAmount.add(realInterest).add(realCorpus).add(overdueAmount);
            totalInterest = totalInterest.add(realInterest);
            billDebt.setId(bill.getId());
            billDebt.setRealPaymentCorpus(realCorpus);
            billDebt.setRealPaymentInterest(realInterest.add(realCouponInterest));
            billDebt.setRealPaymentTime(new Date());
            bill.setRealPaymentCorpus(realCorpus);
            bill.setRealPaymentInterest(realInterest.add(realCouponInterest));
            bill.setRealPaymentTime(new Date());
            billDebt.setState((byte) Enums.State.Complete.getIndex());

            //更新回款计划实际还款本金和利息
            this.billDebtMapper.updateByPrimaryKeySelective(billDebt);

            investAccount.setId(bill.getInvestorId());
            investAccount.setBalance(realCorpus.add(realInterest).add(overdueAmount));
            investAccount.setPieceBalance(realCorpus.add(realInterest).add(overdueAmount));
            investAccount.setTotalReceivedCorpus(realCorpus);
            investAccount.setTotalReceivedInterest(realInterest.add(realCouponInterest));

            //减去待收
            investAccount.setCollectCorpus(bill.getPaymentCorpus().multiply(BigDecimal.valueOf(-1)));
            if(bill.getPaymentCouponInterest()!=null && bill.getPaymentCouponInterest().compareTo(BigDecimal.ZERO) > 0){
                investAccount.setCollectInterest(bill.getPaymentInterest().add(bill.getPaymentCouponInterest()).multiply(BigDecimal.valueOf(-1)));
            }else{
                investAccount.setCollectInterest(bill.getPaymentInterest().multiply(BigDecimal.valueOf(-1)));
            }
           
            //加息券收益
            if(realCouponInterest.compareTo(BigDecimal.ZERO) > 0){
                CouponRecord couponRecord=new CouponRecord();
                couponRecord=couponRecordMapper.selectByInvestId(bill.getInvestId());
                if (couponRecord==null){
                    throw new UtilException("加息券记录异常");
                }
                Account couponInvestAccount = new Account();
                couponInvestAccount.setId(bill.getInvestorId());
                couponInvestAccount.setBalance(realCouponInterest);
                this.accountService.updateBalance(couponInvestAccount, realCouponInterest, FundType.CouponIterest, couponRecord.getId(), "加息券收益");
            }

            //更新投资账户资金
            if ((realCorpus.add(realInterest).add(overdueAmount)).compareTo(BigDecimal.ZERO) > 0){
                this.accountService.updateBalance(investAccount, realCorpus.add(realInterest).add(overdueAmount), Enums.FundType.PaymentInvest, billDebt.getId(), advance ? "提前接收还款" :((overdueAmount!=null && overdueAmount.compareTo(BigDecimal.ZERO)>0)?"逾期接收还款":"正常接收还款"));
            }
            //判断是否完成回款
            if (bill.getPeriods() == bill.getMaxPeriods()) {
                invest.setId(bill.getInvestId());
                invest.setState((byte) State.Complete.getIndex());
                this.investMapper.updateByPrimaryKeySelective(invest);
            }
        }
        Account acc = this.accountMapper.selectByPrimaryKey(debt.getAccountId());
        if (acc.getBalance().compareTo(totalAmount) < 0)
            throw new UtilException("借款人账户余额不足，请充值");
        debtAccount.setId(debt.getAccountId());
        debtAccount.setBalance(totalAmount.multiply(new BigDecimal(-1)));
        this.accountService.updateBalance(debtAccount, totalAmount.multiply(new BigDecimal(-1)), Enums.FundType.PaymentDebt, billDebt.getId(), "扣除还款金额");
        //更新标状态为还款中
        List<BillDebt> listWait = this.billDebtMapper.selectPaymentByState(debtId, (byte) State.Wait.getIndex());
        if (listWait.size() == 0) {
            Debt updateDebt = new Debt();
            updateDebt.setId(debt.getId());
            updateDebt.setState((byte) DebtState.EndPayment.getIndex());
            this.debtMapper.updateByPrimaryKeySelective(updateDebt);
            this.cache.delDebt(updateDebt.getId());
        }
        //发送回款短信
        for (BillDebt bill : list) {
            user = this.userService.getUserByAccountId(bill.getInvestorId());
            this.queueService.insertQueue(Actions.Payment.getIndex(), user.getId(), bill, bill.getRealPaymentCorpus().add(bill.getRealPaymentInterest()).add(bill.getOverdueFine()));
        }
        if(totalInterest!=null){
            this.cache.setTotal(TotalItems.TotalInterest.getKey(), totalInterest.doubleValue());
        }

        //统计待还款金额 2017-3-25
        if(todayPayment!=null) {
            this.cache.setTotal(TotalItems.TotalPaymentAmount.getKey(), todayPayment.doubleValue());
            this.cache.setTotal(TotalItems.TotalWaitPaymentAmount.getKey(), todayPayment.multiply(new BigDecimal(-1)).doubleValue());
        }
        this.cache.setTotal(TotalItems.TotalWaitPaymentCount.getKey(),todayPaymentCount);

    }


    /**
     * 撤标
     *
     * @return
     * @throws Exception
     */
    @Transactional(rollbackFor = {Exception.class})
    public void cancelDebt(long debtId, String memo) throws Exception {
        Debt debt = this.debtMapper.selectByPrimaryKey(debtId);
        if (debt.getState() != (byte) Enums.DebtState.ApprovalPass.getIndex())
            throw new UtilException("此借款状态不允许撤标");
        //List<BillDebt> listComplete = this.billDebtMapper.selectPaymentByState(
        //        debtId, (byte) Enums.State.Complete.getIndex());
        //if (listComplete.size() > 0)
        //    throw new UtilException("此借款已有还款");
        List<BillDebt> listWait = this.billDebtMapper.selectPaymentByState(
                debtId, (byte) State.Wait.getIndex());
        List<Invest> listInvest = this.investService.getListByDebtId(debtId);
        Account investAccount = new Account();
        Invest upInvest = new Invest();
        BigDecimal totalAmount = BigDecimal.ZERO;
        if (debt.getInterestType() == Enums.InterestType.Full.getIndex() && debt.getState() == (byte) Enums.DebtState.ApprovalPass.getIndex())
            this.freezeService.thawBuy(listInvest);
        for (Invest invest : listInvest) {
            upInvest.setId(invest.getId());
            upInvest.setState((byte) State.Cancel.getIndex());
            this.investMapper.updateByPrimaryKeySelective(upInvest);
            investAccount.setId(invest.getAccountId());
            investAccount.setBalance(invest.getAmount());
            investAccount.setPieceBalance(invest.getPieceAmount());
            BigDecimal totalCollectCorpus = BigDecimal.ZERO;
            BigDecimal totalCollectInterest = BigDecimal.ZERO;
            for (BillDebt bill : listWait) {
                if (bill.getInvestId().compareTo(invest.getId()) == 0) {
                    totalCollectCorpus = totalCollectCorpus.add(bill.getPaymentCorpus().multiply(BigDecimal.valueOf(-1)));
                    totalCollectInterest = totalCollectInterest.add(bill.getPaymentInterest().multiply(BigDecimal.valueOf(-1)));
                }
                if(bill.getPaymentCouponInterest()!=null && bill.getPaymentCouponInterest().compareTo(BigDecimal.ZERO) > 0){
                    CouponRecord couponRecord=this.couponRecordMapper.selectByInvestId(bill.getInvestId());
                    if(couponRecord==null){
                        throw new UtilException("加息券记录异常！");
                    }
                    totalCollectInterest = totalCollectInterest.add(bill.getPaymentCouponInterest().multiply(BigDecimal.valueOf(-1)));
                    Account couponInvestAccount = new Account();
                    couponInvestAccount.setId(invest.getAccountId());
                    couponInvestAccount.setBalance(bill.getPaymentCouponInterest());
                    this.accountService.updateBalance(couponInvestAccount, bill.getPaymentCouponInterest(), FundType.CouponIterest, couponRecord.getId(), "标的撤销，补偿加息券收益");
                }
            }
            investAccount.setCollectCorpus(totalCollectCorpus);
            investAccount.setCollectInterest(totalCollectInterest);
            investAccount.setTotalInvestAmount(invest.getAmount().multiply(BigDecimal.valueOf(-1)));
            totalAmount = totalAmount.add(invest.getAmount());
            this.accountService.updateBalance(investAccount, invest.getAmount(), Enums.FundType.CancelInvest, invest.getId(), "标的撤销，返还投标金额");
        }
        if (debt.getInterestType() == InterestType.Now.getIndex()) {
            Account debtAccount = new Account();
            debtAccount.setId(debt.getAccountId());
            debtAccount.setBalance(totalAmount.multiply(BigDecimal.valueOf(-1)));
            debtAccount.setTotalBorrowAmount(totalAmount.multiply(BigDecimal.valueOf(-1)));
            this.accountService.updateBalance(debtAccount, totalAmount.multiply(BigDecimal.valueOf(-1)), Enums.FundType.CancelDebt, debt.getId(), "标的撤销，扣除已筹金额");
        }
        this.billDebtMapper.updateStateByDebtId(debtId, (byte) Enums.State.Cancel.getIndex());
        Debt upDebt = new Debt();
        upDebt.setInvestedAmount(BigDecimal.ZERO);
        upDebt.setSchedule(BigDecimal.ZERO);
        upDebt.setState((byte) Enums.DebtState.Cancel.getIndex());
        upDebt.setCancelMemo(memo);
        upDebt.setId(debt.getId());
        this.debtMapper.updateByPrimaryKeySelective(upDebt);
        this.cache.delDebt(debt.getId());

        //撤标减去借款金额和借款笔数
        this.cache.setTotal(TotalItems.TotalDebt.getKey(), -1);
        this.cache.setTotal(TotalItems.TodayDebt.getKey(), -1);
        if(debt!=null && debt.getAmount()!=null){
            this.cache.setTotal(TotalItems.TotalDebtAmount.getKey(), debt.getAmount().multiply(BigDecimal.valueOf(-1)).doubleValue());
            this.cache.setTotal(TotalItems.TodayDebtAmount.getKey(), debt.getAmount().multiply(BigDecimal.valueOf(-1)).doubleValue());
        }
    }

    /**
     *  截标
     *
     * @return
     * @throws Exception
     */
    @Transactional(rollbackFor = {Exception.class})
    public void cutDebt(long debtId)throws Exception{
        List<Invest> list = new ArrayList<Invest>();
        Debt debt = this.debtMapper.selectByPrimaryKey(debtId);
        Date dt=new Date();
        if (debt.getState() != (byte) DebtState.ApprovalPass.getIndex())
            throw new UtilException("此借款状态不允许截标！");
        if (debt.getCutTime()!=null && dt.compareTo(debt.getCutTime())>0)
            throw new UtilException("此借款标已截标，请等待自动审核满标！");
        if(!debt.getIsNew())
            throw new UtilException("此借款标不是新手标！");
        Debt debts=new Debt();
        debts.setId(debt.getId());
        debts.setCutTime(new Date());
        int updateState = this.debtMapper.updateByPrimaryKeySelective(debts);
        if (updateState < 1)
            throw new UtilException("手动截标失败");
        this.cache.delDebt(debt.getId());
    }

    /**
     * 审核
     *
     * @return
     * @throws Exception
     */
    @Transactional(rollbackFor = {Exception.class})
    public void audit(long id, int state, String memo) throws UtilException {
        Debt cacheDebt = this.debtMapper.selectByPrimaryKey(id);
        if (cacheDebt.getState() != DebtState.InConfirm.getIndex())
            throw new UtilException("当前状态不允许审核");
        Debt debt = new Debt();
        if (state == State.Complete.getIndex())
            debt.setState((byte) DebtState.ApprovalPass.getIndex());
        else if (state == State.Cancel.getIndex())
            debt.setState((byte) DebtState.ApprovalReject.getIndex());
        debt.setId(id);
        debt.setAuditTime(new Date());
        debt.setAuditMemo(memo);
        this.debtMapper.updateByPrimaryKeySelective(debt);
        this.cache.delDebt(debt.getId());
        if(state == State.Complete.getIndex()){
            this.cache.setTotal(TotalItems.TotalDebt.getKey(), 1);
            this.cache.setTotal(TotalItems.TodayDebt.getKey(), 1);
            if(cacheDebt!=null && cacheDebt.getAmount()!=null){
                this.cache.setTotal(TotalItems.TotalDebtAmount.getKey(), cacheDebt.getAmount().doubleValue());
                this.cache.setTotal(TotalItems.TodayDebtAmount.getKey(), cacheDebt.getAmount().doubleValue());
            }
        }
    }

    /**
     * 根据条件获取标列表
     *
     * @return
     * @throws Exception
     */
    public PageInfo getDebtsByParam(int pageNo, int pageSize, byte state, String param, String bTime, String eTime) {
        PageHelper.startPage(pageNo, pageSize);
        List list = this.debtMapper.selectByParam(state, param, bTime, eTime);
        PageInfo page = new PageInfo(list);
        return page;
    }

    /**
     * 根据条件获取所有标列表
     *
     * @return
     * @throws Exception
     */
    public PageInfo getDebtsAllByParam(int pageNo, int pageSize, String param, String bTime, String eTime) {
        PageHelper.startPage(pageNo, pageSize);
        List list = this.debtMapper.selectAllByParam(param, bTime, eTime);
        PageInfo page = new PageInfo(list);
        return page;
    }


    /**
     * 获取进行中的借款标
     *
     * @return
     * @throws Exception
     */
    public PageInfo getInDebts(int pageNo, int pageSize, byte productType, int containNovice) {
        PageHelper.startPage(pageNo, pageSize);
        List list = this.debtMapper.selectInDebts(productType, containNovice, (byte) DebtState.InConfirm.getIndex());
        PageInfo page = new PageInfo(list);
        return page;
    }

    public PageInfo getDebtList(int pageNo, int pageSize, byte state,byte isReady, byte productType,byte paymentType,byte containNovice){
        PageHelper.startPage(pageNo, pageSize);
        List list = this.debtMapper.getDebtList(state,isReady, productType, paymentType,containNovice);
        PageInfo page = new PageInfo(list);
        return page;
    }

    /**
     * 根据条件获取所有回款计划
     *
     * @return
     * @throws Exception
     */
    public PageInfo getPaymentByParam(int pageNo, int pageSize, String param, String bTime, String eTime, State state) throws UtilException {
        PageHelper.startPage(pageNo, pageSize);
        List list = this.billDebtMapper.selectPaymentAll((byte) state.getIndex(), bTime, eTime, param);
        PageInfo page = new PageInfo(list);
        return page;
    }

    /**
     * 根据条件获取回款本金和利息
     *
     * @return
     * @throws Exception
     */
    public PageInfo statPaymentIng( String param, String bTime, String eTime, State state) throws UtilException {
        PageHelper.startPage(1, 10);
        List list = this.billDebtMapper.statPayment((byte) state.getIndex(), bTime, eTime, param);
        PageInfo page = new PageInfo(list);
        return page;
    }

    /**
     * 根据条件获投资列表
     *
     * @return
     * @throws Exception
     */
    public PageInfo getInvests(int pageNo, int pageSize, String debtId, String accountId, String beginTime, String endTime,Byte route, String param,int channel,int bAmount,int eAmount,String invite) throws UtilException {
        PageHelper.startPage(pageNo, pageSize);
        List list = this.investService.getAll(debtId, accountId, beginTime, endTime,route, param, channel,bAmount,eAmount,invite);
        PageInfo page = new PageInfo(list);
        return page;
    }

    /**
     * 获取回款计划详情
     *
     * @return
     * @throws Exception
     */
    public HashMap getPaymentByDebt(String group, boolean advance) throws ParseException, UtilException {
        HashMap map = this.billDebtMapper.selectPaymentByDebt((byte) State.Wait.getIndex(), group);
        List<BillDebt> list = this.billDebtService.getPayment(group);
        BigDecimal sumCorpus = BigDecimal.ZERO;
        BigDecimal sumInterest = BigDecimal.ZERO;
        for (BillDebt bill : list) {
            sumCorpus = sumCorpus.add(bill.getPaymentCorpus());
            sumInterest = sumInterest.add(CommonUtil.calculateInterest(bill.getPaymentTime(), bill.getPaymentInterest(), bill.getPeriodDay(), advance));
        }
        map.put("realPaymentCorpus", sumCorpus);
        map.put("realPaymentInterest", sumInterest);
        return map;
    }

    /**
     * 根据条件获取所有回款计划
     *
     * @return
     * @throws Exception
     */
    public PageInfo getPaymentDetailList(int pageNo, int pageSize, String group) throws UtilException {
        PageHelper.startPage(pageNo, pageSize);
        List list = this.billDebtMapper.selectPaymentDetailByGroup((byte) State.Wait.getIndex(), group);
        PageInfo page = new PageInfo(list);
        return page;
    }

    /**
     * 获取新手标
     *
     * @return
     * @throws Exception
     */
    public List<Debt> getNoviceDebt(int limit) {
        return this.debtMapper.selectNoviceDebt(limit);
    }
    
    /**
     * 推荐
     *
     * @return
     * @throws Exception
     */
    public void recommend(Debt debt) {
        this.debtMapper.updateByPrimaryKeySelective(debt);
        this.cache.delDebt(debt.getId());
    }

}
