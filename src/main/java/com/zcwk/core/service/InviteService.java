package com.zcwk.core.service; 

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zcwk.core.dao.DebtMapper;
import com.zcwk.core.dao.InviteAwardMapper;
import com.zcwk.core.dao.InviteSetMapper;
import com.zcwk.core.dao.UserMapper;
import com.zcwk.core.model.*;
import com.zcwk.core.util.Enums;
import com.zcwk.core.util.UtilException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by pwx on 2016/8/24.
 */
@Service
public class InviteService {
    @Autowired
    InviteSetMapper inviteSetMapper;
    @Autowired
    InviteAwardMapper inviteAwardMapper;
    @Autowired
    DebtMapper debtMapper;
    @Autowired
    UserMapper userMapper;
    @Autowired
    AccountService accountService;

    public void saveSet(InviteSet set) {
        InviteSet _set = this.inviteSetMapper.selectOne();
        if (_set != null)
            this.inviteSetMapper.updateByPrimaryKeySelective(set);
        else
            this.inviteSetMapper.insertSelective(set);
    }

    public InviteSet getSet() {
        return this.inviteSetMapper.selectOne();
    }

    /**
     * 根据条件获取推荐奖励
     *
     * @return
     * @throws Exception
     */
    public PageInfo getAllInviteAward(int pageNo, int pageSize, String param, String bTime, String eTime, String inviteId) {
        PageHelper.startPage(pageNo, pageSize);
        List<HashMap> list = this.inviteAwardMapper.selectByParam(bTime, eTime, param, inviteId);
        PageInfo page = new PageInfo(list);
        return page;
    }

    /**
     * 获取奖励总和
     *
     * @return
     * @throws Exception
     */
    public BigDecimal getSum(String bTime, String eTime, String inviteId) {
        BigDecimal amount = this.inviteAwardMapper.selectSumAmount(bTime, eTime, inviteId);
        return amount;
    }


    @Transactional(rollbackFor = {Exception.class})
    public void sendInviteAward(List<Invest> list) throws UtilException {
        InviteSet set = this.inviteSetMapper.selectOne();
        if (set != null) {
            if (set.getState() == Enums.State.Complete.getIndex()) {
                User user = null;
                User inviteUser = null;
                Account account = new Account();
                for (Invest invest : list) {
                    user = this.userMapper.selectByAccountId(invest.getAccountId());
                    if (user.getInviteId() > 0) {
                        InviteAward inviteAward = new InviteAward();
                        inviteAward.setInviteId(user.getInviteId());
                        inviteAward.setBeInviteId(user.getId());
                        inviteAward.setBuyAmount(invest.getAmount());
                        inviteAward.setPeriodUnit(invest.getDebtPeriodUnit());
                        inviteAward.setPeriod(invest.getDebtPeriod());
                        inviteAward.setDebtId(invest.getDebtId());
                        BigDecimal rate = BigDecimal.ZERO;
                        BigDecimal amount = BigDecimal.ZERO;
                        if (invest.getDebtPeriodUnit() == Enums.PeriodUnitType.Month.getIndex()) {
                            rate = set.getMonthRate().multiply(BigDecimal.valueOf(invest.getDebtPeriod()));
                            amount = invest.getAmount().multiply(rate).setScale(2, BigDecimal.ROUND_HALF_UP);
                        } else if (invest.getDebtPeriodUnit() == Enums.PeriodUnitType.Day.getIndex()) {
                            rate = set.getDayRate().multiply(BigDecimal.valueOf(invest.getDebtPeriod()));
                            amount = invest.getAmount().multiply(rate).setScale(2, BigDecimal.ROUND_HALF_UP);
                        }
                        inviteAward.setRate(rate);
                        inviteAward.setAmount(amount);
                        inviteAward.setTime(invest.getTime());
                        if (amount.compareTo(BigDecimal.ZERO) > 0) {
                            this.inviteAwardMapper.insertSelective(inviteAward);
                            inviteUser = this.userMapper.selectByPrimaryKey(user.getInviteId());
                            account.setId(inviteUser.getAccountId());
                            account.setBalance(amount);
                            account.setTotalRewardAmount(amount);
                            this.accountService.updateBalance(account, amount, Enums.FundType.InviteAward, inviteAward.getId(), "增加邀请奖励");
                        }
                    }
                }
            }
        }
    }
}
