package com.zcwk.core.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.zcwk.core.model.User;
import com.zcwk.core.util.Enums;
import net.rubyeye.xmemcached.MemcachedClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;

/**
 * Created by pwx on 2016/7/15.
 */
@Service
public class QueueService {
    @Autowired
    @Qualifier("mqClient")
    MemcachedClient mq;

    public void insertQueue(int action, long userId, Object data, BigDecimal amount) {
        try {
            HashMap map = new HashMap();
            if (data != null)
                map.put("data", data);
            map.put("action", action);
            map.put("userId", userId);
            map.put("amount", amount);
            JSONObject jo = (JSONObject) JSON.toJSON(map);
            this.mq.set("Actions", 0, JSON.toJSONString(jo));
            System.out.println("insert queue " + JSON.toJSONString(jo));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
