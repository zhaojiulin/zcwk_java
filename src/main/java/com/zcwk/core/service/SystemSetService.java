package com.zcwk.core.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.zcwk.core.dao.SystemSetMapper;
import com.zcwk.core.model.SystemSet;
import com.zcwk.core.util.Enums;
import com.zcwk.core.util.UtilCache;
import com.zcwk.core.util.UtilException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

/**
 * Created by pwx on 2016/8/26.
 */
@Service
public class SystemSetService {
    @Autowired
    UtilCache cache;
    @Autowired
    SystemSetMapper systemSetMapper;

    public void save(SystemSet set) {
        if (set.getId() > 0)
            this.systemSetMapper.updateByPrimaryKeySelective(set);
        this.cache.delParams();
    }

    @Transactional(rollbackFor = {Exception.class})
    public Object getParamVal(String key) throws UtilException {
        Object value = null;
        try {
            List<JSONObject> map = this.cache.getParams();
            for (int i = 0; i < map.size(); i++) {
                if (map.get(i).get("key").equals(key) && map.get(i).get("state").equals(Enums.State.Complete.getIndex()))
                    value = map.get(i).get("value");
            }
            return value;
        } catch (Exception ex) {
            throw new UtilException("请求异常");
        }
    }

    public List<JSONObject> getAll() {
        List<JSONObject> map = this.cache.getParams();
        return map;
    }
}
