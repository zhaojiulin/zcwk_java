package com.zcwk.core.service;

import com.zcwk.core.dao.ProductMapper;
import com.zcwk.core.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by pwx on 2016/10/10.
 */
@Service
public class ProductService {
    @Autowired
    ProductMapper productMapper;

    public List getAll(String state) {
        return this.productMapper.selectAll(state);
    }

    public void save(Product product) {
        if (product.getId() > 0)
            this.productMapper.updateByPrimaryKeySelective(product);
        else
            this.productMapper.insertSelective(product);
    }

    public Product get(int id) {
        return this.productMapper.selectByPrimaryKey(id);
    }
}
