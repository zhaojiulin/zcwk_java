package com.zcwk.core.service;

import com.zcwk.core.dao.BaoFooMapper;
import com.zcwk.core.model.BankCard;
import com.zcwk.core.model.BaoFoo;
import com.zcwk.core.model.Recharge;
import com.zcwk.core.model.User;
import com.zcwk.core.recharge.GatWayFactory;
import com.zcwk.core.recharge.GatWayInterface;
import com.zcwk.core.util.Enums;
import com.zcwk.core.util.Result;
import com.zcwk.core.util.UtilCache;
import com.zcwk.core.util.UtilException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.regex.Pattern;

/**
 * Created by pwx on 2017/5/27.
 */
@Service
public class BaoFooService {

    @Autowired
    UserService userService;
    @Autowired
    BankCardService bankCardService;
    @Autowired
    UtilCache cache;
    @Autowired
    RechargeService rechargeService;
    @Autowired
    BaoFooMapper baoFooMapper;
    @Autowired
    AccountService accountService;

    /**
     * 绑卡
     * @param accountId
     * @return
     * @throws Exception
     */
    public String directBindCard(long accountId) throws Exception{
        User user=userService.getUserByAccountId(accountId);
        BankCard bankCard=bankCardService.getBankCardByAccountId(accountId);
        if(bankCard==null){
            throw new UtilException("银行卡未添加");
        }
        GatWayInterface _gatWay = GatWayFactory.create(Enums.GatWay.BF.getIndex());
        if(bankCard.getMobile()==null || bankCard.getMobile().isEmpty()){
            bankCard.setMobile(user.getMobile());
        }
        String bindId= _gatWay.directBindCard(bankCard,user.getIdCard());
        BaoFoo _baoFoo=baoFooMapper.selectByAccount(accountId);
        if(_baoFoo==null && bindId!=null && !bindId.isEmpty()){
            BaoFoo baoFoo=new BaoFoo();
            baoFoo.setAccountId(accountId);
            baoFoo.setAccountName(bankCard.getAccountName());
            baoFoo.setBank(bankCard.getBank());
            baoFoo.setCardId(bankCard.getCardId());
            baoFoo.setMobile(bankCard.getMobile());
            baoFoo.setTime(new Date());
            baoFoo.setBindId(bindId);
            baoFooMapper.insertSelective(baoFoo);
            accountService.bindCard(accountId,user.getId());
        }
        return bindId;
    }

    /**
     * 预充值
     * @param accountId
     * @param money
     * @param route
     * @return
     * @throws Exception
     */
    public HashMap preCretifyPay(long accountId,BigDecimal money,int route) throws Exception{
        String bindId="";
        BaoFoo _baoFoo=baoFooMapper.selectByAccount(accountId);
        if(_baoFoo!=null){
            bindId=_baoFoo.getBindId();
        }else{
            bindId=directBindCard(accountId);
        }
        if (accountId <= 0)
            return new Result().Error("充值用户不存在");
        if (!(Pattern.compile("^\\+?[1-9][0-9]*$").matcher(String.valueOf(money)).matches()) || money.compareTo(BigDecimal.ZERO) <= 0)
            return new Result().Error("请输入正确的金额");
        if (money.compareTo(BigDecimal.valueOf(99999999))> 0)
            throw new UtilException("充值金额不正确");
        boolean flag= cache.checkFrequentAction("preCretifyPay_"+accountId,2);
        if(!flag){
            return new Result().Error("请勿频繁操作");
        }
        Recharge recharge = this.rechargeService.createOrder(accountId, money, Enums.GatWay.BF.getIndex(),route, "认证支付");
        if(recharge==null){
            return new Result().Error("订单信息错误.");
        }
        GatWayInterface _gatWay = GatWayFactory.create(Enums.GatWay.BF.getIndex());
        String businessNo=_gatWay.preCretifyPay(money,bindId,recharge.getOrderNo());
        BankCard bankCard=bankCardService.getBankCardByAccountId(accountId);
        HashMap map= new HashMap();
        map.put("businessNo", businessNo);
        map.put("orderNo", recharge.getOrderNo());
        map.put("amount", recharge.getAmount());
        map.put("bankCard",bankCard);
        return map;
    }

    /**
     * 确认充值
     * @param accountId
     * @param businessNo
     * @param orderNo
     * @param smsCode
     * @return
     * @throws Exception
     */
    public String certainCretifyPay(long accountId,String businessNo,String orderNo,String smsCode) throws Exception{
        GatWayInterface _gatWay = GatWayFactory.create(Enums.GatWay.BF.getIndex());
        return _gatWay.certainCretifyPay(businessNo, smsCode, orderNo);
    }
}
