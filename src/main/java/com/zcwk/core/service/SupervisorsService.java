package com.zcwk.core.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zcwk.core.dao.RightsMapper;
import com.zcwk.core.dao.RoleRightsMapper;
import com.zcwk.core.dao.SupervisorsMapper;
import com.zcwk.core.model.Rights;
import com.zcwk.core.model.RoleRights;
import com.zcwk.core.model.Supervisors;
import com.zcwk.core.util.CommonUtil;
import com.zcwk.core.util.Enums;
import com.zcwk.core.util.FormatUtil;
import com.zcwk.core.util.GoogleAuthenticator;
import com.zcwk.core.util.MD5Util;
import com.zcwk.core.util.Result;
import com.zcwk.core.util.UtilCache;
import com.zcwk.core.util.UtilException;

/**
 * @author dengqun
 *         <p/>
 *         上午11:12:45
 */

@Service
public class SupervisorsService {

    @Autowired
    SupervisorsMapper supervisorsMapper;
    @Autowired
    UserService userService;
    @Autowired
    UtilCache utilCache;
    @Autowired
    RightsMapper rightsMapper;
    @Autowired
    RoleRightsMapper roleRightsMapper;
    @Autowired
    SystemSetService systemSetService;

    /**
     * 添加supervisors
     *
     * @param supervisors
     * @return
     * @throws UtilException
     */
    @Transactional(rollbackFor = {Exception.class})
    public void save(Supervisors supervisors) throws UtilException {
        Object key = this.systemSetService.getParamVal(Enums.Params.Key.getKey());
        if (key == null)
            throw new UtilException("请求异常");
        if (supervisors.getId() > 0) {
            if (supervisors.getPassword() != null && !supervisors.getPassword().isEmpty())
                supervisors.setPassword(MD5Util.MD5(supervisors.getPassword() + key.toString()));
            this.supervisorsMapper.updateByPrimaryKeySelective(supervisors);
        } else {
            if (supervisors.getPassword() == null || supervisors.getPassword().isEmpty())
                throw new UtilException("密码长度至少6位");
            else
                supervisors.setPassword(MD5Util.MD5(supervisors.getPassword() + key.toString()));
            Supervisors sp = new Supervisors();
            sp.setName(supervisors.getName());
            List<HashMap> list = this.supervisorsMapper.selectSupervisors(sp);
            if (list.size() > 0)
                throw new UtilException("系统中已有该注册帐号，不能注册，请更换帐号注册");
            try {
                supervisors.setTime(new Date());
                this.supervisorsMapper.insertSelective(supervisors);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                throw new UtilException("异常情况发生正在检查");
            }
        }
    }

    /**
     * 根据id删除supervisors
     *
     * @param id
     * @return
     * @throws UtilException
     */
    @Transactional(rollbackFor = {Exception.class})
    public void delete(long id) throws UtilException {

        try {
            this.supervisorsMapper.deleteByPrimaryKey(id);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw new UtilException("删除时异常情况发生请检查");
        }

    }

    /**
     * 根据id查询
     *
     * @param id
     * @return
     * @throws UtilException
     */
    public Supervisors selectSupervisorsById(long id) throws UtilException {

        Supervisors supervisors;
        try {
            supervisors = this.supervisorsMapper.selectByPrimaryKey(id);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw new UtilException("查询时出现异常");
        }

        if (supervisors != null) {
            return supervisors;
        } else {
            return null;
        }
    }

    /**
     * 查询管理员列表
     *
     * @param supervisors
     * @return
     * @throws UtilException
     */
    public List<HashMap> selectSupervisorsList(Supervisors supervisors)
            throws UtilException {
        List<HashMap> list;
        try {
            list = supervisorsMapper.selectSupervisors(supervisors);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw new UtilException("查询时出现异常");
        }

        return list;
    }

    /**
     * 管理员列表分页查询
     *
     * @param pageNo
     * @param pageSize
     * @param supervisors
     * @return
     * @throws UtilException
     */
    public PageInfo selectSupervisorsPage(int pageNo, int pageSize,
                                          Supervisors supervisors) throws UtilException {
        PageHelper.startPage(pageNo, pageSize);
        List<HashMap> list;
        try {
            list = supervisorsMapper.selectSupervisors(supervisors);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw new UtilException("查询时发生异常");
        }
        PageInfo page = new PageInfo(list);
        return page;
    }

    /**
     * 后台登录
     *
     * @return
     * @throws UtilException
     */
    public Supervisors login(String name, String password,String googleCode) throws UtilException {
        if (name == null) {
            throw new UtilException("帐号为空,请检查后再次输入");
        }
        if (password == null) {
            throw new UtilException("请输入密码");
        }
        if (!(Pattern.compile("[0-9A-Za-z]{5,12}").matcher(
                name).matches())) {
            throw new UtilException("帐号的用户名只能是以5到12位字母和数字的组合");
        }
        Supervisors sp = new Supervisors();
        sp.setName(name);
        List<HashMap> list = supervisorsMapper.selectSupervisors(sp);
        if (list.size() == 0)
            throw new UtilException("此账户不存在");
        sp = CommonUtil.parseObject(list.get(0), Supervisors.class);
        //谷歌验证身份
    	Object o = systemSetService.getParamVal(Enums.Params.googleAuthenticator.getKey());
    	if(o != null){
    		if(FormatUtil.toInt(googleCode) == 0 || StringUtils.isEmpty(sp.getGoogleKey()) || !GoogleAuthenticator.authcode(googleCode, sp.getGoogleKey())){
    			throw new UtilException("谷歌身份验证码错误");
    		};
    	}
        if (sp.getLock())
            throw new UtilException("此账户已被锁定，请联系管理员");
        Object key = this.systemSetService.getParamVal(Enums.Params.Key.getKey());
        if (key == null)
            throw new UtilException("请求异常");
        if (sp.getPassword().equals(MD5Util.MD5(password + key.toString()))) {
            Date dt = new Date();
            sp.setLastLogin(dt);
            sp.setId(sp.getId());
            Supervisors supervisors = new Supervisors();
            supervisors.setId(sp.getId());
            supervisors.setLastLogin(dt);
            this.supervisorsMapper.updateByPrimaryKeySelective(supervisors);
            return sp;
        } else {

            throw new UtilException("密码错误");
        }
    }


    /**
     * 获取权限列表
     *
     * @return
     * @throws UtilException
     */
    public List<Rights> getRightsList(long roleId) {
        List<Rights> list = this.rightsMapper.selectAllRights();
        if (roleId > 0) {
            HashMap<String, RoleRights> map = new HashMap<String, RoleRights>();
            List<RoleRights> rList = this.roleRightsMapper.selectByRole(roleId);
            for (RoleRights r : rList)
                map.put(r.getRightsId().toString(), r);
            for (Rights r : list) {
                if (map.containsKey(r.getId().toString()))
                    r.setCheck(true);
                else
                    r.setCheck(false);
            }
        }
        return F(list, Long.valueOf(0));
    }

    private List<Rights> F(List<Rights> list, long parentId) {
        List<Rights> childList = this.getChildList(list, parentId);
        if (childList.size() > 0) {
            for (int i = 0; i < childList.size(); i++) {
                childList.get(i).setChildList(F(list, childList.get(i).getId()));
            }
            return childList;
        } else {
            return null;
        }
    }

    private List<Rights> getChildList(List<Rights> list, long parentId) {
        List<Rights> childList = new ArrayList<Rights>();
        for (Rights r : list) {
            if (r.getParentId() == parentId)
                childList.add(r);
        }
        return childList;
    }


    /**
     * 生成谷歌身份验证key
     * @param id
     * @return
     * @throws UtilException 
     */
    public HashMap genSecret(long id,String name) throws UtilException{
    	//获取配置
    	Object o = systemSetService.getParamVal(Enums.Params.googleAuthenticator.getKey());
    	if(o == null){
    		return new Result().Error("配置未开启");
    	}
    	Map<String, String> config = CommonUtil.parseMap(o.toString());
    	String namePrefix = config.get("name_prefix");//用户名前缀
    	name = namePrefix + "_" + name;
    	//生成key
    	String key = GoogleAuthenticator.genSecret(name,namePrefix);
    	
    	Supervisors supervisors = new Supervisors();
    	supervisors.setId(id);
    	supervisors.setGoogleName(name);
    	supervisors.setGoogleKey(key);
    	supervisorsMapper.updateByPrimaryKeySelective(supervisors);
    	return new Result().Success();
    }
}
