package com.zcwk.core.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zcwk.core.dao.ActivityMapper;
import com.zcwk.core.model.Activity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by pwx on 2016/10/28.
 */
@Service
public class ActivityService {
    @Autowired
    ActivityMapper activityMapper;

    /**
     * 根据条件获取所有活动
     *
     * @return
     * @throws Exception
     */
    public PageInfo getActivityByParam(int pageNo, int pageSize, String bTime, String eTime, String state, String showType) {
        PageHelper.startPage(pageNo, pageSize);
        List list = this.activityMapper.selectByParam(state, bTime, eTime, showType);
        PageInfo page = new PageInfo(list);
        return page;
    }

    /**
     * 获取活动
     *
     * @return
     * @throws Exception
     */
    public Activity getActivityById(long id) {
        return this.activityMapper.selectByPrimaryKey(id);
    }

    /**
     * 保存活动
     *
     * @return
     * @throws Exception
     */
    public void save(Activity activity) {
        if (activity.getId() > 0)
            this.activityMapper.updateByPrimaryKeySelective(activity);
        else {
            activity.setTime(new Date());
            this.activityMapper.insertSelective(activity);
        }
    }
}
