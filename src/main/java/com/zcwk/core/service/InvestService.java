package com.zcwk.core.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.zcwk.core.model.Transfer;
import com.zcwk.core.util.CommonUtil;
import com.zcwk.core.util.Enums;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zcwk.core.dao.InvestMapper;
import com.zcwk.core.model.Account;
import com.zcwk.core.model.Debt;
import com.zcwk.core.model.Invest;
import com.zcwk.core.util.UtilException;

@Service
public class InvestService {
    @Autowired
    InvestMapper investMapper;

    public Invest addTransfer(Account account, Debt debt, BigDecimal amount, BigDecimal bonusAmount, BigDecimal couponRate,
                              Boolean isAuto, Boolean isExperience, Transfer transfer) throws UtilException {
        BigDecimal pieceAmount = CommonUtil.getPieceAmount(amount, account, Enums.FundType.Buy);
        Invest invest = new Invest();
        invest.setAccountId(account.getId());
        invest.setDebtId(debt.getId());
        invest.setDebtPeriod(debt.getPeriod());
        invest.setDebtPeriodUnit(debt.getPeriodUnit());
        invest.setDebtRate(debt.getRate());
        invest.setCouponRate(couponRate);
        invest.setAmount(amount);
        invest.setPieceAmount(pieceAmount);
        invest.setAwardType((byte) Enums.AwardType.Scale.getIndex());
        invest.setAwardNumber(transfer.getAward());
        invest.setIsAutomaticInvest(isAuto);
        invest.setTime(new Date());
        invest.setIsExperience(isExperience);
        invest.setAwardType(debt.getAwardType());
        invest.setBonusAmount(bonusAmount);
        invest.setAwardType(debt.getAwardType());
        invest.setTransferId(transfer.getId());
        invest.setState((byte) Enums.State.Wait.getIndex());
        this.investMapper.insertSelective(invest);
        if (invest.getId() == null)
            throw new UtilException("投资失败,请联系管理员");
        return invest;
    }

    public Invest addDebt(Account account, Debt debt, BigDecimal amount, BigDecimal bonusAmount, BigDecimal couponRate,int route,
                          Boolean isAuto, Boolean isExperience) throws UtilException {
        BigDecimal pieceAmount = CommonUtil.getPieceAmount(amount, account, Enums.FundType.Buy);
        Invest invest = new Invest();
        invest.setAccountId(account.getId());
        invest.setDebtId(debt.getId());
        invest.setDebtPeriod(debt.getPeriod());
        invest.setDebtPeriodUnit(debt.getPeriodUnit());
        invest.setDebtRate(debt.getRate());
        invest.setCouponRate(couponRate);
        invest.setAmount(amount);
        invest.setPieceAmount(pieceAmount);
        invest.setAwardType(debt.getAwardType());
        invest.setAwardNumber(debt.getAwardNumber());
        invest.setRoute((byte) route);
        invest.setIsAutomaticInvest(isAuto);
        invest.setTime(new Date());
        invest.setIsExperience(isExperience);
        invest.setAwardType(debt.getAwardType());
        invest.setBonusAmount(bonusAmount);
        invest.setAwardType(debt.getAwardType());
        invest.setState((byte) Enums.State.Wait.getIndex());
        this.investMapper.insertSelective(invest);
        if (invest.getId() == null)
            throw new UtilException("投资失败,请联系管理员");
        return invest;
    }

    public List<Invest> getListByDebtId(long debtId) {
        return this.investMapper.selectByDebtId(debtId);
    }

    public List<HashMap> getAll(String debtId, String accountId, String beginTime, String endTime,Byte route, String param,int channel,int bAmount,int eAmount,String invite) {
        return this.investMapper.selectAll(debtId, accountId, beginTime, endTime,route, param,channel,bAmount,eAmount,invite);
    }
}
