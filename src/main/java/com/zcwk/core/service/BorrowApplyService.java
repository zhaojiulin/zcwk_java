package com.zcwk.core.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zcwk.core.dao.BorrowApplyMapper;
import com.zcwk.core.model.BorrowApply;
import com.zcwk.core.util.Enums;
import com.zcwk.core.util.UtilException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by pwx on 2016/8/11.
 */
@Service
public class BorrowApplyService {
    @Autowired
    BorrowApplyMapper borrowApplyMapper;

    /**
     * 保存借款申请
     */
    public void save(BorrowApply borrowApply) {
        borrowApply.setTime(new Date());
        borrowApply.setState((byte) Enums.State.Wait.getIndex());
        this.borrowApplyMapper.insertSelective(borrowApply);
    }

    /**
     * 审核借款申请
     */
    public void audit(long id, byte state, String memo) {
        BorrowApply borrowApply = new BorrowApply();
        borrowApply.setId(id);
        borrowApply.setMemo(memo);
        borrowApply.setState(state);
        this.borrowApplyMapper.updateByPrimaryKeySelective(borrowApply);
    }

    /**
     * 根据条件获取所有回款计划
     *
     * @return
     * @throws Exception
     */
    public PageInfo getApplyByParam(int pageNo, int pageSize, String param) throws UtilException {
        PageHelper.startPage(pageNo, pageSize);
        List list = this.borrowApplyMapper.selectByParam(param);
        PageInfo page = new PageInfo(list);
        return page;
    }
}
