package com.zcwk.core.service;

import com.alibaba.druid.support.json.JSONUtils;
import com.alibaba.fastjson.JSONObject;

import com.antgroup.zmxy.openplatform.api.ZhimaApiException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zcwk.core.dao.TemplateMapper;
import com.zcwk.core.model.PlatformInfo;
import com.zcwk.core.model.Template;
import com.zcwk.core.util.*;
import org.apache.cxf.jaxrs.client.WebClient;

import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.*;

import com.antgroup.zmxy.openplatform.api.DefaultZhimaClient;


import com.antgroup.zmxy.openplatform.api.request.ZhimaCreditIvsDetailGetRequest;
import com.antgroup.zmxy.openplatform.api.response.ZhimaCreditIvsDetailGetResponse;

/**
 * Created by pwx on 2016/10/3.
 */
@Service
public class CommonService {
    @Autowired
    TemplateMapper templateMapper;
    @Autowired
    SystemSetService systemSetService;
    @Autowired
    UtilCache cache;
    @Autowired
    PlatformService platformService;
    private static ObjectMapper objectMapper = new ObjectMapper();

    //芝麻信用返回数据姓名与身份证号匹配
    String zhimaCodes[] = {"CERTNO_Match_Sharing_Bad", "CERTNO_Match_Trust_Self_Sharing_Bad",
            "CERTNO_Match_Sharing_Good", "CERTNO_Match_Trust_Self_Sharing_Good",
            "CERTNO_Match_Recency_Bad", "CERTNO_Match_Trust_Self_Recency_Bad",
            "CERTNO_Match_Recency_Good", "CERTNO_Match_Trust_Self_Recency_Good",
            "CERTNO_Match_Reliability_Bad", "CERTNO_Match_Trust_Self_Reliability_Bad",
            "CERTNO_Match_Reliability_Good", "CERTNO_Match_Trust_Self_Reliability_Good",
            "CERTNO_History_NegativeList", "NAME_Match_Sharing_Bad", "NAME_Match_Sharing_Good",
            "NAME_Match_Recency_Bad", "NAME_Match_Recency_Good",
            "NAME_Match_Reliability_Bad", "NAME_Match_Reliability_Bad"
    };
    private static String fformatStr = "https://api4.udcredit.com/dsp-front/4.1/dsp-front/default/pubkey/%s/product_code/%s/out_order_id/%s/signature/%s";//有盾新接口地址

    public boolean sendMsg(int action, int type, Map map) throws UtilException, UnsupportedEncodingException, MessagingException, JsonProcessingException {
        Template template = this.cache.getSendTemplate(action, type);
        if (template != null && template.getState() == Enums.State.Complete.getIndex()) {
            String content = this.replaceParam(template.getContent(), map);
            if (type == Enums.SendType.Sms.getIndex())
                return sendSmsMsg(map.get("mobile").toString(), content);
            else if (type == Enums.SendType.Email.getIndex())
                return sendEmailMsg(map.get("email").toString(), Enums.getActionDesc(action), content);
        }
        return false;
    }

    public boolean sendEmailMsg(String email, String title, String content) throws UtilException, MessagingException, JsonProcessingException, UnsupportedEncodingException {
        PlatformInfo platformInfo = this.platformService.get();
        Object obj = this.systemSetService.getParamVal(Enums.Params.Email.getKey());
        if (obj != null) {
            Map<String, String> config = CommonUtil.parseMap(obj.toString());
            String param = "api_user=" + config.get("user") + "&api_key=" + config.get("key") + "&to=" + email + "&subject=" + title + "&html=" + URLEncoder.encode(content, "UTF-8") + "&from="+((config.get("from")!=null && config.get("from")!="")?config.get("from"):"sendcloud@sendcloud.org")+"&fromname=" + URLEncoder.encode(platformInfo.getPlatformName(), "UTF-8");
            UtilHttp.sendPost(config.get("url"), param);
            return true;
        }
        return false;
    }

    //搜索编号：1487908346
    public boolean sendSmsMsg(String mobile, String content) throws UtilException {
        try {
            Object obj = this.systemSetService.getParamVal(Enums.Params.SmsWay.getKey());
            if (obj != null) {
                String SmsWay = obj.toString();
                System.out.println(SmsWay);
                System.out.println(content);
                if (SmsWay.equals("facaiyu")) {
                    boolean result = this.fcsendSmsMsg(mobile, content);
                    return result;
                }
                else if (SmsWay.equals("chuanglan")) {
                    boolean result = this.clsendSmsMsg(mobile, content);
                    return result;
                }
                else if(SmsWay.equals("jianzhou")){
                    boolean result = this.jzsendSmsMsg(mobile, content);
                    return result;
                }else if(SmsWay.equals("emay")){
                    boolean result = this.emaySmsMsg(mobile, content);
                    return result;
                }
                else{
                    return false;
                }
            }
            throw new UtilException("请求异常！");
        } catch (Exception ex) {
            throw new UtilException("出现异常！");
        }
    }

    /** 建周短信 **/
    public boolean jzsendSmsMsg(String mobile, String content) throws UtilException,IOException {
        //需要手动添加签名
        try {
            Object obj = this.systemSetService.getParamVal(Enums.Params.JianzhouSms.getKey());
            if (obj != null) {
                Map<String, String> config = CommonUtil.parseMap(obj.toString());
                if(config!=null && config.get("username")!=null && config.get("password")!=null && config.get("sign")!=null){
                    JianZhouUtil.readContentFromPost(config.get("url"), config.get("username"), config.get("password"), mobile, content+config.get("sign"));
                    return true;
                }
            }
            return false;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return false;
        }
    }

    /** 创兰短信 **/
    public boolean clsendSmsMsg(String mobile, String content) throws UtilException {
        try {
            Object obj = this.systemSetService.getParamVal(Enums.Params.ChuanglanSms.getKey());
            if (obj != null) {
                Map<String, String> config = CommonUtil.parseMap(obj.toString());
                String returnString = HttpSender.batchSend(config.get("url"), config.get("un"), config.get("pwd"), mobile, content, "0", null);
                System.out.println(returnString);
                return true;
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /** 发财鱼短信 **/
    public boolean fcsendSmsMsg(String mobile, String content) throws UtilException {
        try {
            Object obj = this.systemSetService.getParamVal(Enums.Params.Sms.getKey());
            if (obj != null) {
                PlatformInfo platformInfo = this.platformService.get();
                Map<String, String> config = CommonUtil.parseMap(obj.toString());
                WebClient client = WebClient.create(config.get("url"));
                client.type("application/json;charset=UTF-8");
                String tradeNo = AESUtil.getTradeNo();
                Map<String, Object> parameters = new HashMap<String, Object>();
                parameters.put("tradeNo", tradeNo);// 必填参数
                parameters.put("userName", config.get("name"));// 必填参数
                parameters.put("userPassword", config.get("pwd"));// 必填参数
                parameters.put("phones", mobile);
                //parameters.put("content", content + "【" + platformInfo.getPlatformName() + "】");
                parameters.put("content", content);
                parameters.put("etnumber", "");
                String sign = AESUtil.encrypt(objectMapper.writeValueAsString(parameters), config.get("key"));
                parameters.put("sign", sign);
                parameters.put("userPassword", AESUtil.MD5(config.get("pwd")));// 必填参数
                String body = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(parameters);
                Response response = client.post(body);
                System.out.println(response);
                return true;
            }
            return false;

        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return false;
        }
    }

    /** 发财鱼短信 **/
    public boolean emaySmsMsg(String mobile, String content) throws UtilException,IOException,UnsupportedEncodingException {
        try {
            Object obj = this.systemSetService.getParamVal(Enums.Params.EmaySms.getKey());
            if (obj != null) {
                Map<String, String> config = CommonUtil.parseMap(obj.toString());
                if(config!=null && config.get("cdkey")!=null && config.get("password")!=null && config.get("sign")!=null){
                    content=config.get("sign")+content;
                    String param="cdkey="+config.get("cdkey")+"&password="+config.get("password")+"&phone="+mobile+"&message="+URLEncoder.encode(content, "UTF-8")+"&seqid="+System.currentTimeMillis()+"&smspriority=1";;
                    String xmlJson=UtilHttp.sendPost("http://hprpt2.eucp.b2m.cn:8080/sdkproxy/sendsms.action", param);
                    System.err.println("xmlJson="+xmlJson);
                    SAXBuilder builder = new SAXBuilder();
                    Document doc = builder.build(new StringReader(xmlJson));
                    Element root = doc.getRootElement();
                    String status =root.getChild("error").getText();
                    if (status.equals("0")) {
                        return true;
                    }else{
                        return false;
                    }
                }
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private String replaceParam(String content, Map map) {
        String param = "";
        for (Object key : map.keySet()) {
            param = "" + key.toString() + "";
            if (content.indexOf(param) > -1)
                content = content.replaceAll(param, map.get(key.toString()).toString());
        }
        content = content.replaceAll("date", CommonUtil.getFormatTime(new Date(), "yyyy-MM-dd HH:mm"));
        return content;
    }

    public boolean realName(String name, String idCard) throws UtilException {
        try {
            Object obj = this.systemSetService.getParamVal(Enums.Params.RealNameWay.getKey());
            if (obj != null) {
                String realWay = obj.toString();
                System.out.println(realWay);
                if (realWay.equals("udcredit")) {
                    boolean result = this.UrealName(name, idCard);
                    return result;
                }
                else if (realWay.equals("zhima")) {
                    boolean result = this.zhimarealName(name, idCard);
                    return result;
                }else if(realWay.equals("gzt")){
                    boolean result = this.gztRealName(name, idCard);
                    return result;
                }else{
                    return false;
                }
            }
            throw new UtilException("请求异常！");
        } catch (Exception ex) {
            throw new UtilException("出现异常！");
        }
    }

    public boolean UrealName(String name, String idCard) throws UtilException {//有盾实名认证
        String outOrderId = new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date()) + (int) (Math.random() * (1000 - 100) + 100);
        try {
            Object obj = this.systemSetService.getParamVal(Enums.Params.RealName.getKey());
            if (obj != null) {
                Map<String, String> config = CommonUtil.parseMap(obj.toString());
                if(config.get("wayVersion")!=null && config.get("wayVersion").equals("1")){
                    //友盾新实名接口
                    Map<String, String> body = new HashMap<String, String>();
                    body.put("id_no", idCard);
                    body.put("id_name", name);
                    String result = apiCall(config.get("newUrl"), config.get("pubkey"),//调用有盾demo
                            config.get("seckey"),config.get("product"),
                            outOrderId, body);
                    System.out.println(result);
                    Object retCode = (((HashMap) ((HashMap) JSONUtils.parse(result)).get("header")).get("ret_code"));
                    Object retMsg = (((HashMap) ((HashMap) JSONUtils.parse(result)).get("header")).get("ret_msg"));
                    System.out.println(retCode);
                    if(retCode.equals("000000")){
                        Object status = (((HashMap) ((HashMap) JSONUtils.parse(result)).get("body")).get("status"));
                        if(status.equals("1")){
                            return true;
                        }
                        throw new UtilException("实名信息错误！");
                    }
                    throw new UtilException("请求异常！");
                } else {
                    JSONObject body = new JSONObject(true);
                    body.put("name_card", name); //api接口传参 详见api文档
                    body.put("id_card", idCard); //api接口传参 详见api文档
                    JSONObject header = new JSONObject(true);
                    header.put("version", "1.0");//固定1.0
                    header.put("merchant", config.get("merchant"));//商户号 运营开户后发出
                    header.put("product", config.get("product"));//业务类型
                    header.put("outOrderId", outOrderId);//订单号 <32位且不重复 没有格式要求
                    System.out.println("外部订单号：" + outOrderId);
                    JSONObject signJson = new JSONObject(true);
                    signJson.put("body", body);
                    signJson.put("header", header);
                    JSONObject common = new JSONObject(true);
                    common.put("request", signJson);
                    String orginSign = signJson.toJSONString();
                    String sign = MD5Util.MD5(orginSign + config.get("key")); //替换MD5_key 加签
                    common.put("sign", sign);
                    HttpRequestSimple http = new HttpRequestSimple();
                    String out = http.postSendHttp(config.get("url"), common.toString());
                    Object status = ((HashMap) ((HashMap) ((HashMap) JSONUtils.parse(out)).get("response")).get("body")).get("status");
                    System.out.println(out);
                    if (status != null && status.equals("1"))
                        return true;
                    return false;
                }
            }
            return false;
        } catch (Exception ex) {
            return false;
        }
    }

    public boolean zhimarealName(String name, String cert_no) throws UtilException {//芝麻信用实名认
        Object realName = this.systemSetService.getParamVal(Enums.Params.ZhimaRealName.getKey());
        if (realName != null) {
            Map<String, String> config = CommonUtil.parseMap(realName.toString());
            System.out.println(config);
            System.out.println(config.get("url"));
            ZhimaCreditIvsDetailGetRequest req = new ZhimaCreditIvsDetailGetRequest();
            String transactionId = new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date()) + UUID.randomUUID().toString();
            req.setProductCode("w1010100000000000103");
            req.setTransactionId(transactionId);
            req.setCertType("100");
            req.setName(name);
            req.setCertNo(cert_no);
            req.setApiVersion("1.0");
            req.setChannel("apppc");
            req.setPlatform("zmop");
            DefaultZhimaClient client = new DefaultZhimaClient(config.get("url"), config.get("appId"), config.get("privateKey"),
                    config.get("zhimaPublicKey"));
            try {
                ZhimaCreditIvsDetailGetResponse response = client.execute(req);
                System.out.println(response.getIvsScore());
                String code = response.getIvsDetail().get(0).getCode();
                System.out.println(code);
                System.out.println(response.getErrorCode());
                System.out.println(response.getErrorMessage());
//                if (code.equals("CERTNO_Match_Reliability_Good")||code.equals("CERTNO_Match_Sharing_Good")||code.equals("NAME_Match_Sharing_Good")||code.equals("NAME_Match_Reliability_Good")) {
//                    return true;
//                }
                for (String c : zhimaCodes) {
                    if (code.equals(c)) {
                        return true;
                    }
                }
                return false;
            } catch (ZhimaApiException e) {
                e.printStackTrace();
                return false;
            }
        }
        return  false;
    }

    /**
     * 国政通实名认证
     * @param name
     * @param idCard
     * @return
     * @throws UtilException
     */
    public boolean gztRealName(String name,String idCard) throws Exception{
        Object realName = this.systemSetService.getParamVal(Enums.Params.GZTRealName.getKey());
        if (realName != null) {
            Map<String, String> config = CommonUtil.parseMap(realName.toString());
            System.err.println(realName);
            if(config.get("gztUrl")!=null && !config.get("gztUrl").isEmpty()&&config.get("gztAccount")!=null && !config.get("gztAccount").isEmpty()&&config.get("gztPwd")!=null && !config.get("gztPwd").isEmpty()){
                return GZTUtil.checkPersonID(name,idCard,config.get("gztUrl"),config.get("gztAccount"),config.get("gztPwd"));
            }
            return false;
        }
        return false;
    }

    public static String apiCall(String url, String pub_key, String secretkey, String product_code, String out_order_id, Map<String, String> parameter) throws Exception {
        if (parameter == null || parameter.isEmpty())
            throw new Exception("error ! the parameter Map can't be null.");
        StringBuffer bodySb = new StringBuffer("{");
        for (Map.Entry<String, String> entry : parameter.entrySet()) {
            bodySb.append("'").append(entry.getKey()).append("':'").append(entry.getValue()).append("',");
        }
        String bodyStr = bodySb.substring(0, bodySb.length() - 1) + "}";
        String signature = MD5Util.md5(bodyStr + "|" + secretkey);
        url = String.format(fformatStr, pub_key, product_code,out_order_id+"", signature);
        System.out.println(url);
        System.out.println("request parameter body=>" + bodyStr);
        HttpResponse r =UtilHttp.makePostRequest(url, bodyStr);
        System.out.println(r);
        return EntityUtils.toString(r.getEntity());
    }

}
