/**
 *
 */
package com.zcwk.core.service;

import com.zcwk.core.dao.AccountMapper;
import com.zcwk.core.dao.BankCardMapper;
import com.zcwk.core.model.Account;
import com.zcwk.core.model.BankCard;
import com.zcwk.core.model.Recharge;
import com.zcwk.core.model.User;
import com.zcwk.core.util.CommonUtil;
import com.zcwk.core.util.Enums;
import com.zcwk.core.util.UtilCache;
import com.zcwk.core.util.UtilException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zcwk.core.dao.RechargeMapper;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

/**
 * @author dengqun
 *         <p/>
 *         上午11:11:52
 */

@Service
public class RechargeService {
    @Autowired
    RechargeMapper rechargeMapper;
    @Autowired
    AccountService accountService;
    @Autowired
    AccountMapper accountMapper;
    @Autowired
    BankCardMapper bankCardMapper;
    @Autowired
    QueueService queueService;
    @Autowired
    UtilCache cache;
    @Autowired
    SystemSetService systemSetService;

    public Recharge createOrder(long accountId, BigDecimal amount, int gatWay, int route, String memo) {
        Account acc = this.accountMapper.selectByPrimaryKey(accountId);
        Recharge recharge = new Recharge();
        recharge.setAccountId(accountId);
        recharge.setOrderNo(recharge.createOrderNo());
        recharge.setAmount(amount);
        recharge.setBalance(acc.getBalance());
        recharge.setGatway((byte) gatWay);
        recharge.setTime(new Date());
        recharge.setMemo(memo);
        recharge.setRoute((byte) route);
        this.rechargeMapper.insertSelective(recharge);
        return recharge;
    }

    @Transactional(rollbackFor = {Exception.class})
    public boolean updateOrderSuccess(String orderNo, Enums.FundType type, String memo) throws UtilException {
        Recharge recharge = this.rechargeMapper.selectByOrderNo(orderNo);
        if (recharge != null) {
            if (recharge.getIsCompleted())
                return true;
            Account acc = this.accountMapper.selectByPrimaryKey(recharge.getAccountId());

            Object key=this.systemSetService.getParamVal(Enums.Params.bindCard.getKey());
            Map<String, String> config =null;
            if(key!=null){
                config= CommonUtil.parseMap(key.toString());
            }
            if(config!=null && config.get("noNeedBind").equals("1") && recharge.getMemo()!=null &&recharge.getMemo()!="" && recharge.getMemo().indexOf("快捷支付")>-1){
                accountService.bindCard(recharge.getAccountId(),acc.getUserId());
            }
            if (this.rechargeMapper.updateSuccess(recharge.getId(), acc.getBalance().add(recharge.getAmount())) > 0) {
                Account account = new Account();
                account.setId(recharge.getAccountId());
                account.setBalance(recharge.getAmount());
                account.setTotalRechargeAmount(recharge.getAmount());
                this.accountService.updateBalance(account, recharge.getAmount(), type, recharge.getId(), memo);
                this.queueService.insertQueue(Enums.Actions.Recharge.getIndex(), acc.getUserId(), recharge, recharge.getAmount());
                if(recharge!=null && recharge.getAmount()!=null){
                    this.cache.setTotal(Enums.TotalItems.TotalRechargeAmount.getKey(), recharge.getAmount().doubleValue());
                    this.cache.setTotal(Enums.TotalItems.TodayRechargeAmount.getKey(), recharge.getAmount().doubleValue());
                }
                return true;
            }
        }
        return false;
    }

    public long lineRecharge(User user, BigDecimal amount, int gatWay, String memo) throws UtilException {
        Recharge recharge = this.createOrder(user.getAccountId(), amount, gatWay,Enums.Route.RG.getIndex(),memo);
        this.updateOrderSuccess(recharge.getOrderNo(), Enums.FundType.LineRecharge, memo);
        return recharge.getId();
    }
}
