package com.zcwk.core.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.zcwk.core.util.CommonUtil;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zcwk.core.dao.BillDebtMapper;
import com.zcwk.core.model.BillDebt;
import com.zcwk.core.model.Debt;
import com.zcwk.core.util.Enums;

@Service
public class BillDebtService {
    @Autowired
    BillDebtMapper billDebtMapper;

    public long saveDebt(long borrowerId, long investorId, BigDecimal corpus, BigDecimal interest,BigDecimal couponInterest,
                         Date time, Debt debt, byte period, byte maxPeriod, int periodDay, long investId, boolean isExperience) {
        BillDebt bill = new BillDebt();
        bill.setBorrowerId(borrowerId);
        bill.setInvestorId(investorId);
        bill.setPaymentCorpus(corpus);
        bill.setPaymentInterest(interest);
        bill.setPaymentCouponInterest(couponInterest);
        bill.setPaymentTime(time);
        bill.setDebtId(debt.getId());
        bill.setPeriods(period);
        bill.setMaxPeriods(maxPeriod);
        bill.setIsExperience(isExperience);
        bill.setTime(new Date());
        bill.setPeriodDay(periodDay);
        bill.setInvestId(investId);
        bill.setIsTransfer(false);
        bill.setState((byte) Enums.State.Wait.getIndex());
        bill.setGroup(CommonUtil.getFormatTime(new Date(), "yyyyMMdd") + "-" + debt.getId() + "-" + period);
        this.billDebtMapper.insertSelective(bill);
        return bill.getId();
    }

    public long saveTransfer(long borrowerId, long investorId, BigDecimal corpus, BigDecimal interest,BigDecimal couponInterest,
                             Date time, long debtId, byte period, byte maxPeriod, int periodDay, long investId, boolean isExperience, long transferId, String group) {
        BillDebt bill = new BillDebt();
        bill.setBorrowerId(borrowerId);
        bill.setInvestorId(investorId);
        bill.setPaymentCorpus(corpus);
        bill.setPaymentInterest(interest);
        bill.setPaymentCouponInterest(couponInterest);
        bill.setPaymentTime(time);
        bill.setDebtId(debtId);
        bill.setPeriods(period);
        bill.setMaxPeriods(maxPeriod);
        bill.setIsExperience(isExperience);
        bill.setTime(new Date());
        bill.setPeriodDay(periodDay);
        bill.setInvestId(investId);
        bill.setTransferId(transferId);
        bill.setGroup(group);
        bill.setState((byte) Enums.State.Wait.getIndex());
        this.billDebtMapper.insertSelective(bill);
        return bill.getId();
    }

    public List<BillDebt> getPayment(String group) {
        return this.billDebtMapper.selectByPayment(group);
    }
}
