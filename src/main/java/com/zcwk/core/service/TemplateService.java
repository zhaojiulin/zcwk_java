package com.zcwk.core.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zcwk.core.dao.TemplateMapper;
import com.zcwk.core.model.Template;
import com.zcwk.core.util.Enums;
import com.zcwk.core.util.UtilCache;
import com.zcwk.core.util.UtilException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by pwx on 2016/9/30.
 */
@Service
public class TemplateService {
    @Autowired
    TemplateMapper templateMapper;
    @Autowired
    UtilCache cache;

    public void save(Template template) throws UtilException {
        List<Template> list = this.templateMapper.selectByParam(String.valueOf(template.getAction()), String.valueOf(template.getType()), String.valueOf(Enums.State.Complete.getIndex()));
        if (list.size() > 0 && template.getState() == Enums.State.Complete.getIndex() && list.get(0).getId() != template.getId())
            throw new UtilException("只允许存在一条此模版");
        if (template.getId() > 0)
            this.templateMapper.updateByPrimaryKeySelective(template);
        else
            this.templateMapper.insertSelective(template);
        this.cache.setSendTemplate(template);
    }

    public PageInfo getAll(int pageNo, int pageSize, String type, String action, String state) {
        PageHelper.startPage(pageNo, pageSize);
        List<Template> list = this.templateMapper.selectByParam(action, type, state);
        PageInfo page = new PageInfo(list);
        return page;
    }

    public Template get(int id) {
        return this.templateMapper.selectByPrimaryKey(id);
    }
}
