package com.zcwk.core.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zcwk.core.dao.ProtocolTemplateMapper;
import com.zcwk.core.model.ProtocolTemplate;
import com.zcwk.core.util.UtilCache;
import com.zcwk.core.util.UtilException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by pwx on 2016/10/6.
 */
@Service
public class ProtocolTemplateService {
    @Autowired
    ProtocolTemplateMapper protocolTemplateMapper;
    @Autowired
    UtilCache cache;

    public void save(ProtocolTemplate template) throws UtilException {
        List<ProtocolTemplate> list = this.protocolTemplateMapper.selectByParam(template.getType().toString());
        if (list.size() > 0 && list.get(0).getId() != template.getId())
            throw new UtilException("只允许存在一条此模版");
        if (template.getId() > 0)
            this.protocolTemplateMapper.updateByPrimaryKeySelective(template);
        else
            this.protocolTemplateMapper.insertSelective(template);
        this.cache.setProtocol(template);
    }

    public PageInfo getAll(int pageNo, int pageSize, String type) {
        PageHelper.startPage(pageNo, pageSize);
        List<ProtocolTemplate> list = this.protocolTemplateMapper.selectByParam(type);
        PageInfo page = new PageInfo(list);
        return page;
    }

    public ProtocolTemplate get(int id) {
        return this.protocolTemplateMapper.selectByPrimaryKey(id);
    }
}
