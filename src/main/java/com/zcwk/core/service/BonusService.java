package com.zcwk.core.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zcwk.core.dao.AccountMapper;
import com.zcwk.core.dao.BonusRecordMapper;
import com.zcwk.core.dao.BonusSetMapper;
import com.zcwk.core.dao.UserMapper;
import com.zcwk.core.model.*;
import com.zcwk.core.util.CommonUtil;
import com.zcwk.core.util.Enums;
import com.zcwk.core.util.UtilCache;
import com.zcwk.core.util.UtilException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by pwx on 2016/7/25.
 */
@Service
public class BonusService {
    @Autowired
    BonusSetMapper bonusSetMapper;
    @Autowired
    AccountMapper accountMapper;
    @Autowired
    AccountService accountService;
    @Autowired
    UserMapper userMapper;
    @Autowired
    UtilCache cache;
    @Autowired
    BonusRecordMapper bonusRecordMapper;

    public void sendBonus(int bonusId, String mobile) throws UtilException {
        if (bonusId <= 0)
            throw new UtilException("请选择红包");
        if (mobile.isEmpty())
            throw new UtilException("请选择发放对象");
        BonusSet b = this.bonusSetMapper.selectByPrimaryKey(bonusId);
        if (b == null)
            throw new UtilException("红包设置不存在或未启用");
        User user = this.userMapper.selectUserByMobile(mobile);
        if (user == null)
            throw new UtilException("发放对象不存在");
        if (user.getRealName().isEmpty() || user.getIdCard().isEmpty())
            throw new UtilException("此用户未实名");
        BonusRecord bonusRecord = new BonusRecord();
        bonusRecord.setAccountId(user.getAccountId());
        bonusRecord.setAction(b.getSendAction());
        bonusRecord.setName(b.getName());
        bonusRecord.setAmount(b.getAmount());
        bonusRecord.setBeginTime(b.getUseBeginTime());
        bonusRecord.setType(b.getType());
        bonusRecord.setEndTime(b.getUseEndTime());
        bonusRecord.setBuyAmount(b.getUseBuyAmount());
        bonusRecord.setProductType(b.getUseProductType());
        bonusRecord.setPeriodUnit(b.getUsePeriodUnit());
        bonusRecord.setBonusId(b.getId());
        bonusRecord.setPeriod(b.getUsePeriod());
        bonusRecord.setState((byte) Enums.State.Wait.getIndex());
        bonusRecord.setTime(new Date());
        if (b.getSendFirst()) {
            int sendCount = this.bonusRecordMapper.selectCountByAccount(user.getAccountId(), b.getId());
            if (sendCount > 0)
                throw new UtilException("此红包不允许发送多个");
        }
        if (b.getType() == Enums.BonusType.Cash.getIndex()) {
            bonusRecord.setUseTime(new Date());
            bonusRecord.setState((byte) Enums.State.Complete.getIndex());
        }
        long id = this.bonusRecordMapper.insertSelective(bonusRecord);
        Account upAcc = new Account();
        if (b.getType() == Enums.BonusType.Cash.getIndex()) {
            upAcc.setBalance(b.getAmount());
            upAcc.setId(user.getAccountId());
            this.accountService.updateBalance(upAcc, b.getAmount(), Enums.FundType.Bonus, id, b.getName());
            this.cache.delAccount(user.getAccountId());
        }
    }

    @Transactional(rollbackFor = {Exception.class})
    public void sendBonus(int action, User user, BigDecimal amount) throws UtilException {
        List<BonusSet> bonusSets = this.bonusSetMapper.selectByParam(String.valueOf(action), String.valueOf(Enums.State.Complete.getIndex()), true, null, null);
        if (user != null) {
            Account account = this.accountMapper.selectByPrimaryKey(user.getAccountId());
            if (account != null) {
                BonusRecord bonusRecord = null;
                for (BonusSet b : bonusSets) {
                    boolean isAdd = true;
                    long sendAccountId = account.getId();
                    if (b.getSendBuyAmount().compareTo(BigDecimal.ZERO) > 0 && b.getSendBuyAmount().compareTo(amount) > 0)  //判断购买金额
                        isAdd = false;
                    if (b.getSendTotalAmount().compareTo(BigDecimal.ZERO) > 0 && b.getSendTotalAmount().compareTo(account.getTotalInvestAmount()) > 0)  //判断投资总额
                        isAdd = false;
                    if (b.getSendCollect().compareTo(BigDecimal.ZERO) > 0 && b.getSendCollect().compareTo(account.getCollectCorpus().add(account.getCollectInterest())) > 0)  //判断待收
                        isAdd = false;
                    if (b.getSendInvestCount() > 0 && b.getSendInvestCount() != (byte) account.getInvestCount().intValue())  //判断购买次数
                        isAdd = false;
                    if (b.getSendFirst() && !b.getSendInvite()) {
                        int sendCount = this.bonusRecordMapper.selectCountByAccount(sendAccountId, b.getId());
                        if (sendCount > 0)
                            isAdd = false;
                    }
                    if (b.getSendInvite()) {
                        User inviteUser = this.userMapper.selectByPrimaryKey(user.getInviteId());
                        if (inviteUser != null)
                            sendAccountId = inviteUser.getAccountId();
                        else
                            isAdd = false;
                    }
                    if (isAdd) {
                        Account upAcc = new Account();
                        for (int i = 0; i < b.getSendCount(); i++) {
                            bonusRecord = new BonusRecord();
                            bonusRecord.setAccountId(sendAccountId);
                            bonusRecord.setAction((byte) action);
                            bonusRecord.setName(b.getName());
                            bonusRecord.setType(b.getType());
                            bonusRecord.setAmount(b.getAmount());
                            bonusRecord.setBeginTime(b.getUseBeginTime());
                            bonusRecord.setEndTime(b.getUseEndTime());
                            bonusRecord.setBuyAmount(b.getUseBuyAmount());
                            bonusRecord.setBonusId(b.getId());
                            bonusRecord.setProductType(b.getUseProductType());
                            bonusRecord.setPeriodUnit(b.getUsePeriodUnit());
                            bonusRecord.setPeriod(b.getUsePeriod());
                            bonusRecord.setState((byte) Enums.State.Wait.getIndex());
                            bonusRecord.setTime(new Date());
                            if (b.getType() == Enums.BonusType.Cash.getIndex()) {
                                bonusRecord.setUseTime(new Date());
                                bonusRecord.setState((byte) Enums.State.Complete.getIndex());
                            }
                            long id = this.bonusRecordMapper.insertSelective(bonusRecord);
                            if (b.getType() == Enums.BonusType.Cash.getIndex()) {
                                upAcc.setBalance(b.getAmount());
                                upAcc.setId(sendAccountId);
                                this.accountService.updateBalance(upAcc, b.getAmount(), Enums.FundType.Bonus, id, b.getName());
                                this.cache.delAccount(sendAccountId);
                            }
                        }
                    }
                }
            }
        }
    }

    @Transactional(rollbackFor = {Exception.class})
    public Boolean useBonus(long id, long investId, BigDecimal amount, Debt debt) throws UtilException {
        try {
            BonusRecord record = this.bonusRecordMapper.selectByPrimaryKey(id);
            if (record != null) {
                if (record.getBeginTime().compareTo(new Date()) > 0 || record.getEndTime().compareTo(new Date()) < 0)
                    return false;
                if (record.getPeriodUnit() > 0 && record.getPeriodUnit() != debt.getPeriodUnit())
                    return false;
                if (record.getPeriod() > 0 && record.getPeriod() > debt.getPeriod())
                    return false;
                if (record.getProductType() > 0 && record.getProductType() != debt.getProductType())
                    return false;
                if (record.getBuyAmount().compareTo(amount) > 0)
                    return false;
                BonusRecord upRecord = new BonusRecord();
                upRecord.setId(record.getId());
                upRecord.setUseTime(new Date());
                upRecord.setInvestId(investId);
                upRecord.setState((byte) Enums.State.Complete.getIndex());
                if (this.bonusRecordMapper.useRecord(upRecord) <= 0)
                    throw new UtilException("红包使用异常");
                Account account = new Account();
                account.setId(record.getAccountId());
                account.setBalance(record.getAmount());
                this.accountService.updateBalance(account, record.getAmount(), Enums.FundType.Bonus, record.getId(), "激活抵扣红包");
                return true;
            }
            return false;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            return false;
        }
    }

    /**
     * 保存红包设置
     *
     * @return
     * @throws Exception
     */
    public void saveSet(BonusSet bonusSet) {
        if (bonusSet.getId() > 0)
            this.bonusSetMapper.updateByPrimaryKeySelective(bonusSet);
        else {
            bonusSet.setTime(new Date());
            this.bonusSetMapper.insertSelective(bonusSet);
        }
    }


    /**
     * 根据条件获取红包设置列表
     *
     * @return
     * @throws Exception
     */
    public PageInfo getBonusSetByParam(int pageNo, int pageSize, String bTime, String eTime, String state) throws UtilException {
        PageHelper.startPage(pageNo, pageSize);
        List<BonusSet> list = this.bonusSetMapper.selectByParam(null, state, false, bTime, eTime);
        PageInfo page = new PageInfo(list);
        return page;
    }

    /**
     * 根据条件获取红包列表
     *
     * @return
     * @throws Exception
     */
    public PageInfo getBonusRecordByParam(int pageNo, int pageSize, String accountId, boolean isExpiry, String productType, String periodUnit, String period, String state, String param, String bTime, String eTime, String type,BigDecimal buyAmount) throws UtilException {
        PageHelper.startPage(pageNo, pageSize);
        List<HashMap> list = this.bonusRecordMapper.selectByParam(accountId, isExpiry, productType, periodUnit, period, state, param, bTime, eTime, type,buyAmount);
        PageInfo page = new PageInfo(list);
        return page;
    }


    public BonusSet getBonusSetById(int id) {
        return this.bonusSetMapper.selectByPrimaryKey(id);
    }
}
