/**
 *
 */
package com.zcwk.core.service;

import com.zcwk.core.util.UtilCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zcwk.core.dao.SeoMapper;
import com.zcwk.core.model.Seo;
import com.zcwk.core.util.Result;
import com.zcwk.core.util.UtilException;

/**
 * @author dengqun
 *         <p/>
 *         上午11:12:14
 */

@Service
public class SeoService {
    @Autowired
    SeoMapper seoMapper;
    @Autowired
    UtilCache cache;

    /**
     * 保存Seo信息
     *
     * @return
     * @throws Exception
     */
    public void save(Seo seo) {
        if (seo.getId() > 0)
            this.seoMapper.updateByPrimaryKeySelective(seo);
        else
            this.seoMapper.insertSelective(seo);
        this.cache.delSeo();
    }

    /**
     * 获取Seo信息
     *
     * @return
     * @throws Exception
     */
    public Seo get() {
        return this.cache.getSeo();
    }
}
