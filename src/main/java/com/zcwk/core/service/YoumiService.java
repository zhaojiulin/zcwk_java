package com.zcwk.core.service;

import com.zcwk.core.dao.UserMapper;
import com.zcwk.core.dao.YoumiMapper;
import com.zcwk.core.model.User;
import com.zcwk.core.model.Youmi;
import com.zcwk.core.util.CommonUtil;
import com.zcwk.core.util.Enums;
import com.zcwk.core.util.UtilException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.regex.Pattern;

/**
 * 有米
 * Created by pwx on 2017/3/14.
 */
@Service
public class YoumiService {

    @Autowired
    YoumiMapper youmiMapper;

    public Youmi getYoumi(long id) {
        Youmi youmi = this.youmiMapper.selectByPrimaryKey(id);
        return youmi;
    }

    /**
     * 有米数据保存
     *
     * @param youmi
     * @throws com.zcwk.core.util.UtilException
     */
    @Transactional(rollbackFor = {Exception.class})
    public void save(Youmi youmi) throws UtilException {
        if (youmi == null)
            throw new UtilException("提交参数不能为空");
        if (youmi.getCallbackUrl() == null || youmi.getCallbackUrl().isEmpty())
            throw new UtilException("回调地址不对");
        youmi.setTime(new Date());
        if((youmi.getAdIdentifier()!=null && youmi.getAdIdentifier()!="") || (youmi.getSimulateIDFA()!=null && youmi.getSimulateIDFA()!="")){
            Youmi _youmi=new Youmi();
            _youmi.setAdIdentifier(youmi.getAdIdentifier());
            _youmi.setSimulateIDFA(youmi.getSimulateIDFA());
            Youmi dbYoumi=dbYoumi=youmiMapper.selectYoumiByKey(_youmi);
            if(dbYoumi==null){
                try {
                    youmiMapper.insert(youmi);
                } catch (Exception e) {
                    e.printStackTrace();
                    throw new UtilException("有米数据保存异常，请稍后尝试");
                }
            }else{
                if(dbYoumi!=null && dbYoumi.getUserId()==null){
                    youmiMapper.updateCallbackUrl(dbYoumi.getId(),youmi.getCallbackUrl());
                }else {
                    throw new UtilException("已存在设备信息，请稍后尝试");
                }
            }
        }else{
            throw new UtilException("无法解析设备，请稍后尝试");
        }
    }

}
