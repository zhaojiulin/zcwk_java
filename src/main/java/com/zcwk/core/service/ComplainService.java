package com.zcwk.core.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zcwk.core.dao.ComplainMapper;
import com.zcwk.core.model.Complain;
import com.zcwk.core.util.DateUtil;
import com.zcwk.core.util.UtilException;

/**
 * 意见反馈
 * @author zp
 * @since 2017年2月9日
 */
@Service
public class ComplainService {
    @Autowired
    ComplainMapper complainMapper;

    /**
     * 保存
     *
     * @return
     * @throws UtilException
     */
    public void save(long userId ,String content) throws UtilException {
    	Complain complain = new Complain(userId, content, DateUtil.getNow());
    	complainMapper.save(complain);
    }

    /**
     * 删除
     *
     * @param id
     * @return
     * @throws UtilException
     */
    public void delById(long id) throws UtilException {
    	complainMapper.delById(id);
    }

    /**
     * 根据id查询
     *
     * @param id
     * @return
     * @throws UtilException
     */
    public Complain findById(long id) throws UtilException {
    	return complainMapper.findById(id);
    }

    /**
     * 按照条件查询
     * @param pageNo 页码
     * @param pageSize 每页数
     * @param param moblie or realName
     * @param bTime 开始时间
     * @param eTime 结束时间
     * @return
     * @throws UtilException
     */
    public PageInfo findByParam(int pageNo, int pageSize, String param, String bTime,String eTime) throws UtilException {
        PageHelper.startPage(pageNo, pageSize);
        List<HashMap> list = complainMapper.findByParam(param, bTime, eTime);
        PageInfo page = new PageInfo(list);
        return page;
    }
}
