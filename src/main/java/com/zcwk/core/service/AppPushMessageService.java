package com.zcwk.core.service;


import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.zcwk.core.dao.AppUploadMapper;
import com.zcwk.core.model.AppUpload;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zcwk.core.dao.AppPushMessageMapper;
import com.zcwk.core.dao.ComplainMapper;
import com.zcwk.core.model.AppClient;
import com.zcwk.core.model.AppPushMessage;
import com.zcwk.core.model.Complain;
import com.zcwk.core.util.AppPushUtil;
import com.zcwk.core.util.CommonUtil;
import com.zcwk.core.util.DateUtil;
import com.zcwk.core.util.Enums;
import com.zcwk.core.util.Result;
import com.zcwk.core.util.UtilException;
import com.zcwk.core.view.AppPushMessageView;

/**
 * 个推记录
 *
 * @author zp
 * @since 2017年2月16日
 */
@Service
public class AppPushMessageService {
    @Autowired
    ComplainMapper complainMapper;
    @Autowired
    AppPushMessageMapper appPushMessageMapper;
    @Autowired
    SystemSetService systemSetService;
    @Autowired
    AppUploadMapper appUploadMapper;

    /**
     * 保存app标识
     *
     * @param
     */
    public void saveAppClient(String clientId, Long userId, int route) {
        AppClient appClient = new AppClient();
        appClient.setClientId(clientId);
        List<AppClient> list = appPushMessageMapper.findAppClient(appClient);
        appClient.setUserId(userId);
        for (AppClient ac : list) {
            if (ac.getUserId() == 0 && appClient.getUserId() > 0) {
                appClient.setId(ac.getId());
                appPushMessageMapper.updateAppClient(appClient);
                return;
            } else if (appClient.getUserId() == 0 || (ac.getUserId() > 0 && ac.getUserId() == appClient.getUserId())) {
                return;
            }
        }

        appClient.setRoute(route);
        appClient.setAddTime(DateUtil.getNow());

        appPushMessageMapper.saveAppClient(appClient);
        return;
    }

    /**
     * 保存app
     *
     * @return
     * @throws UtilException
     */
    public HashMap saveApp(AppUpload appUpload) throws UtilException {

        long vids = this.appUploadMapper.selecrMaxVid();
        if (vids != 0) {
            if (vids > appUpload.getVerSion()) {
                return new Result().Error("版本Id输入错误！");
            }
        }
        appUpload.setTime(new Date());
        int result = this.appUploadMapper.insertSelective(appUpload);
        if (result < 1)
            return new Result().Error("保存失败！");
        return new Result().Success();
    }


    /**
     * 保存
     *
     * @return
     * @throws UtilException
     */
    public PageInfo AllAppList(int pageNo, int pageSize) throws UtilException {
        PageHelper.startPage(pageNo, pageSize);
        List<HashMap> list = this.appUploadMapper.selectAll();
        PageInfo page = new PageInfo(list);
        return page;
    }


    /**
     * 保存
     *
     * @return
     * @throws UtilException
     */
    public void save(long userId, String content) throws UtilException {
        Complain complain = new Complain(userId, content, DateUtil.getNow());
        complainMapper.save(complain);
    }

    /**
     * 删除
     *
     * @param id
     * @return
     * @throws UtilException
     */
    public void delById(long id) throws UtilException {
        complainMapper.delById(id);
    }

    /**
     * 根据id查询
     *
     * @param id
     * @return
     * @throws UtilException
     */
    public Complain findById(long id) throws UtilException {
        return complainMapper.findById(id);
    }

    /**
     * 按照条件查询
     *
     * @param pageNo   页码
     * @param pageSize 每页数
     * @param status   状态
     * @param bTime    开始时间
     * @param eTime    结束时间
     * @return
     * @throws UtilException
     */
    public PageInfo findByParam(int pageNo, int pageSize, int status, String bTime, String eTime) throws UtilException {
        PageHelper.startPage(pageNo, pageSize);
        List<HashMap> list = appPushMessageMapper.findByParam(pageNo, pageSize, status, bTime, eTime);
        PageInfo page = new PageInfo(list);
        return page;
    }

    /**
     * 个推推送
     *
     * @param view AppPushMessageView
     * @return
     * @throws UtilException
     */
    public HashMap appPushMessage(AppPushMessageView view) throws UtilException {
        //获取个推配置
        Object o = systemSetService.getParamVal(Enums.Params.GeTui.getKey());
        if (o == null) {
            return new Result().Error("个推配置未开启");
        }
        Map<String, String> config = CommonUtil.parseMap(o.toString());
        String appId = config.get("appId");//接收应用
        String appKey = config.get("appKey");//鉴定身份是否合法
        String masterSecret = config.get("masterSecret");//第三方客户端个推集成鉴权码，用于验证第三方合法性
        String host = config.get("host");//推送os域名, host可选填，如果host不填程序自动检测用户网络，选择最快的域名连接下发

        Date nowTime = DateUtil.getNow();
        String title = view.getTitle();
        String content = view.getContent();
        String transmissionContent = view.getTransmissionContent();
        //透传内容
        String tc = "";
        JSONObject body = new JSONObject();
        body.put("url", transmissionContent);
        body.put("transmissionType", view.getTransmissionType());
        body.put("title", title);
        body.put("content", content);
        tc = body.toJSONString();

        //推送结果
        HashMap result = null;

        //查询推送app列表
        List<HashMap> list = appPushMessageMapper.findAppClientByParam(view);
        if (list.size() <= 0) {
            return new Result().Error("推送用户不存在");
        }
        List<HashMap> subList = null;
        int size = 1;
        int pageSize = 500;
        if(list.size() > pageSize){
            size = list.size() / pageSize;
            if(list.size() % pageSize > 0){
                size += 1;
            }
        }
        for (int i = 0; i < size; i++) {
            int fromIndex = i * pageSize;
            int toIndex = (i + 1) * pageSize;
            if(i == size - 1 || list.size() <= pageSize){
                toIndex = list.size();
            }
            subList = list.subList(fromIndex , toIndex);
            String appClients[] = new String[subList.size()];
            for (int j = 0; j < subList.size(); j++) {
                appClients[j] = subList.get(j).get("clientId").toString();
            }
            result = AppPushUtil.pushMessage(appId, appKey, masterSecret, host, tc, content, title, appClients);

            if (Integer.parseInt(result.get("code").toString()) != 1) {
                System.err.println(result);
            }
        }

        //备注信息
        view.initRemark();
        //保存推送记录
        AppPushMessage appPushMessage = view.prototype();
        appPushMessage.setAddTime(nowTime);
        appPushMessage.setStatus(Enums.State.Complete.getIndex());
        appPushMessageMapper.saveAppPushMessage(appPushMessage);
        return new Result().Success();
    }





    /**
     *
     *
     * @param
     * @return
     */
    public AppUpload getNewestApp() {
        AppUpload appUpload = this.appUploadMapper.selectNewestApp();
        return appUpload;
    }
}
