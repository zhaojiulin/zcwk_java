package com.zcwk.core.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zcwk.core.dao.AdsMapper;
import com.zcwk.core.model.Ads;
import com.zcwk.core.util.UtilException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Created by pwx on 2016/7/22.
 */
@Service
public class AdsService {
    @Autowired
    AdsMapper adsMapper;

    /**
     * 保存Ads
     *
     * @return
     * @throws UtilException
     */
    public void save(Ads ads) throws UtilException {
        try {
            if (ads.getId() > 0)
                this.adsMapper.updateByPrimaryKeySelective(ads);
            else {
                ads.setTime(new Date());
                this.adsMapper.insertSelective(ads);
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw new UtilException("添加时出现异常");
        }
    }

    /**
     * 根据id删除Ads
     *
     * @param id
     * @return
     * @throws UtilException
     */
    public void delete(long id) throws UtilException {
        try {
            this.adsMapper.deleteByPrimaryKey(id);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw new UtilException("删除时异常情况发生请检查");
        }

    }

    /**
     * 根据id查询
     *
     * @param id
     * @return
     * @throws UtilException
     */
    public Ads get(long id) throws UtilException {
        Ads ads;
        try {
            ads = this.adsMapper.selectByPrimaryKey(id);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw new UtilException("查询异常");
        }

        if (ads != null) {
            return ads;
        } else {
            return null;
        }
    }

    /**
     * 查询Ads
     *
     * @param pageNo
     * @param pageSize
     * @return
     * @throws UtilException
     */
    public PageInfo selectAll(int pageNo, int pageSize, String state, String showType,String itemType)
            throws UtilException {
        PageHelper.startPage(pageNo, pageSize);
        List<Ads> list;
        try {
            list = this.adsMapper.selectAll(state, showType,itemType);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw new UtilException("查询时发生异常");
        }
        PageInfo page = new PageInfo(list);
        return page;
    }
}
