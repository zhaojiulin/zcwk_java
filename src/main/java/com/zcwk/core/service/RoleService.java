/**
 *
 */
package com.zcwk.core.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.alibaba.druid.util.LRUCache;
import com.zcwk.core.dao.RoleRightsMapper;
import com.zcwk.core.model.RoleRights;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zcwk.core.dao.RoleMapper;
import com.zcwk.core.model.Role;
import com.zcwk.core.util.UtilException;

/**
 * @author dengqun
 *         <p/>
 *         下午6:36:38
 */
@Service
public class RoleService {
    @Autowired
    RoleMapper roleMapper;
    @Autowired
    RoleRightsMapper roleRightsMapper;

    /**
     * 添加role
     *
     * @return
     * @throws UtilException
     */
    public void save(Role role) throws UtilException {
        if (role.getName() == null || role.getStatus() == null) {
            throw new UtilException("缺少参数");
        }
        if (role.getId() > 0)
            this.roleMapper.updateByPrimaryKeySelective(role);
        else {
            role.setTime(new Date());
            this.roleMapper.insertSelective(role);
        }
    }

    /**
     * 根据id删除Role
     *
     * @param id
     * @return
     * @throws UtilException
     */
    public void delete(long id) throws UtilException {

        try {
            roleMapper.deleteByPrimaryKey(id);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw new UtilException("删除时异常情况发生请检查");
        }

    }

    /**
     * 根据id查询
     *
     * @param id
     * @return
     * @throws UtilException
     */
    public Role get(long id) throws UtilException {

        Role role;
        try {
            role = roleMapper.selectByPrimaryKey(id);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw new UtilException("查询异常");
        }

        if (role != null) {
            return role;
        } else {
            return null;
        }
    }

    /**
     * @param pageNo
     * @param pageSize
     * @return
     * @throws UtilException
     */
    public PageInfo selectRolePage(int pageNo, int pageSize, String state)
            throws UtilException {
        PageHelper.startPage(pageNo, pageSize);
        List<Role> list;
        try {
            list = this.roleMapper.selectRolePage(state);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw new UtilException("查询时发生异常");
        }
        PageInfo page = new PageInfo(list);
        return page;
    }


    public void saveRights(long roleId, String rightsIds)  throws UtilException{
        if(rightsIds==null || rightsIds.isEmpty()){
            throw new UtilException("权限参数错误");
        }
        String[] ids = rightsIds.split(",");
        List<RoleRights> list = this.roleRightsMapper.selectByRole(roleId);
        HashMap<String, RoleRights> map = new HashMap<String, RoleRights>();
        RoleRights roleRights = new RoleRights();
        for (RoleRights r : list) {
            map.put(r.getRightsId().toString(), r);
        }
        HashMap<String, String> map1 = new HashMap<String, String>();
        for (String id : ids) {
            map1.put(id, id);
        }
        for (RoleRights r : map.values()) {
            if (!map1.containsKey(r.getRightsId().toString())) {
                this.roleRightsMapper.deleteByPrimaryKey(r.getId());
            }
        }
        for (String id : ids) {
            if (!map.containsKey(id)) {
                roleRights.setRightsId(Long.valueOf(id));
                roleRights.setRoleId(roleId);
                this.roleRightsMapper.insertSelective(roleRights);
            }
        }

    }
}
