package com.zcwk.core.service;

import com.zcwk.core.dao.AccountMapper;
import com.zcwk.core.dao.WithdrawMapper;
import com.zcwk.core.model.Account;
import com.zcwk.core.model.BankCard;
import com.zcwk.core.model.Withdraw;
import com.zcwk.core.util.*;
import com.zcwk.core.withdraw.GateWayFactory;
import com.zcwk.core.withdraw.GateWayInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by pwx on 2016/6/8.
 */
@Service
public class WithdrawService {
    @Autowired
    WithdrawMapper withdrawMapper;
    @Autowired
    AccountMapper accountMapper;
    @Autowired
    AccountService accountService;
    @Autowired
    FreezeService freezeService;
    @Autowired
    UtilCache cache;
    @Autowired
    SystemSetService  systemSetService;
    @Autowired
    BankCardService bankCardService;

    public Withdraw save(long accountId, BigDecimal amount, long bankCardId,int route) throws UtilException {
        Account acc = this.accountMapper.selectByPrimaryKey(accountId);
        BigDecimal pieceAmount = CommonUtil.getPieceAmount(amount, acc, Enums.FundType.Buy);
        Withdraw withdraw = new Withdraw();
        withdraw.setAccountId(accountId);
        withdraw.setAuditState((byte) Enums.State.Wait.getIndex());
        withdraw.setPayState((byte) Enums.State.Wait.getIndex());
        withdraw.setAmount(amount);
        withdraw.setArriveAmount(amount);
        withdraw.setPieceAmount(pieceAmount);
        withdraw.setBankcardId(bankCardId);
        withdraw.setTime(new Date());
        withdraw.setRoute((byte) route);
        Object key=this.systemSetService.getParamVal(Enums.Params.withdrawConfig.getKey());
        Map<String, String> config =null;
        if(key!=null){
            config=CommonUtil.parseMap(key.toString());
        }
        int count=0;
        if(config!=null && config.get("feeWithdrawCount")!=null && !config.get("feeWithdrawCount").isEmpty()){
            try {
                count = Integer.parseInt(config.get("feeWithdrawCount"));
            } catch (NumberFormatException e) {
                count=0;
            }
        }
        if(count>0){
            int withdrawCount=this.withdrawMapper.selectWithdrawCountByThisMonth(accountId, CommonUtil.getFormatTime(new Date(), "yyyy-MM-01"));
            if(withdrawCount>=count){
                withdraw.setFee(BigDecimal.valueOf(2));
                withdraw.setArriveAmount(amount.subtract(BigDecimal.valueOf(2)));
            }
        }
        this.withdrawMapper.insertSelective(withdraw);
        if (withdraw.getId() == null)
            throw new UtilException("提现失败,请联系管理员");
        return withdraw;
    }


    /**
     * 打款
     * @param withdrawId
     * @param accountId
     * @param amount
     */
    public void payWithdraw(long withdrawId,String orderNo, long accountId,BigDecimal amount,String gateway) throws Exception{
        BankCard bankCard=bankCardService.getBankCardByAccountId(accountId);
        if(bankCard==null){
            throw new UtilException("请添加银行卡.");
        }
        GateWayInterface _gatWay = GateWayFactory.create(Integer.parseInt(gateway));
        if (_gatWay == null)
            throw new UtilException("网关异常");
        boolean check = false;
        check = _gatWay.payQuick(bankCard.getBank(),bankCard.getCity(),amount,bankCard.getCardId(),bankCard.getAccountName(),orderNo);
        if (!check) {
            throw new UtilException("处理失败");
        }
    }

    public boolean checkpayQuickOrder(long withdrawId) throws Exception{
        Withdraw withdraw=withdrawMapper.selectByPrimaryKey(withdrawId);
        if(withdraw==null){
            throw new UtilException("订单信息错误.");
        }
        if(withdraw.getOrderNo()==null || withdraw.getOrderNo().isEmpty()){
            throw new UtilException("订单号不存在.");
        }
        if(withdraw.getArriveTime()!=null){
            throw new UtilException("订单已完成,无需查询.");
        }
        if (withdraw.getGateWay()==0)
            throw new UtilException("网关异常");
        GateWayInterface _gatWay = GateWayFactory.create(withdraw.getGateWay());
        if (_gatWay==null)
            throw new UtilException("网关异常");
        Date currentDate=new Date();
        String endDt=CommonUtil.getFormatTime(currentDate, "yyyyMMdd");
        String startDt=CommonUtil.getFormatTime(new Date(currentDate.getTime()-14*24*60*60*1000), "yyyyMMdd");
        boolean check= _gatWay.checkpayQuickOrder(withdraw.getOrderNo(),startDt,endDt);
        if(check){
            Withdraw _withdraw=new Withdraw();
            _withdraw.setId(withdrawId);
            _withdraw.setArriveTime(new Date());
            withdrawMapper.updateByPrimaryKeySelective(_withdraw);
        }
        return check;
    }

    public boolean callPaySucess(String orderNo,byte gateWay) throws Exception{
        Withdraw withdraw=withdrawMapper.selectByOrderNo(orderNo, gateWay);
        if(withdraw==null){
            throw new UtilException("订单信息错误.");
        }
        if(withdraw.getArriveTime()!=null){
            throw new UtilException("订单已完成,无需查询.");
        }
        Withdraw _withdraw=new Withdraw();
        _withdraw.setId(withdraw.getId());
        _withdraw.setArriveTime(new Date());
        int count=withdrawMapper.updateByPrimaryKeySelective(_withdraw);
        return count>0?true:false;
    }
}
