package com.zcwk.core.service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;

import com.zcwk.core.dao.BillDebtMapper;
import com.zcwk.core.dao.ChannelMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zcwk.core.dao.UserMapper;
import com.zcwk.core.model.Account;
import com.zcwk.core.model.User;
import com.zcwk.core.util.CommonUtil;
import com.zcwk.core.util.Enums;
import com.zcwk.core.util.Enums.AccountType;
import com.zcwk.core.util.Enums.UserNature;
import com.zcwk.core.util.MD5Util;
import com.zcwk.core.util.UtilCache;
import com.zcwk.core.util.UtilException;

/**
 * Created by pwx on 2016/6/30.
 */
@Service
public class UserService {
    @Autowired
    UserMapper userMapper;
    @Autowired
    AccountService accountService;
    @Autowired
    UtilCache utilCache;
    @Autowired
    QueueService queueService;
    @Autowired
    SystemSetService systemSetService;
    @Autowired
    CommonService commonService;
    @Autowired
    ChannelMapper channelMapper;
    @Autowired
    UtilCache cache;
    @Autowired
    BillDebtMapper billDebtMapper;
    @Autowired
    IntegralService integralService;

    /**
     * 根据手机帐号查询
     *
     * @param mobile
     * @return
     */
    public User getUserByMobile(String mobile) {

        User user = this.userMapper.selectUserByMobile(mobile);
        return user;
    }

    /**
     * 根据编号查询用户信息
     *
     * @return User
     * @throws UtilException
     */
    public User getUserById(long id) {
        User user = this.utilCache.getUser(id);
        return user;
    }

    /**
     * 根据手机帐号查询
     *
     * @param id
     * @return
     */
    public User getUserByAccountId(long id) {
        User user = this.userMapper.selectByAccountId(id);
        return user;
    }

    /**
     * 查询投资人
     *
     * @param pageNo
     * @param pageSize
     * @param user
     * @return
     * @throws UtilException
     */
    public PageInfo queryUsers(int pageNo, int pageSize, User user) throws UtilException {
        PageHelper.startPage(pageNo, pageSize);
        List<User> list;
        try {
            list = this.userMapper.selectUsers(user);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw new UtilException("查询时发生异常");
        }
        PageInfo page = new PageInfo(list);
        return page;
    }

    /**
     * 查询邀请人
     *
     * @param pageNo
     * @param pageSize
     * @return
     * @throws UtilException
     */
    public PageInfo getInviteUser(int pageNo, int pageSize, String inviteId, String param, String bTime, String eTime) throws UtilException {
        PageHelper.startPage(pageNo, pageSize);
        List<HashMap> list;
        try {
            list = this.userMapper.selectInviteUser(inviteId, param, bTime, eTime);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw new UtilException("查询时发生异常");
        }
        PageInfo page = new PageInfo(list);
        return page;
    }

    /**
     * 用户注册
     *
     * @param user
     * @throws UtilException
     */
    @Transactional(rollbackFor = {Exception.class})
    public void save(User user) throws UtilException {
        if (user == null)
            throw new UtilException("提交参数不能为空");
        if (user.getMobile() != null && !(Pattern.compile("^1[34578]{1}[0-9]{9}").matcher(user.getMobile()).matches()))
            throw new UtilException("手机号码格式不对，请修改");
        if (user.getEmail() != null && !(Pattern.compile("([0-9a-zA-Z_-]+)@(.+)\\.(\\w+)").matcher(user.getEmail()).matches()))
            throw new UtilException("邮箱格式不对，请重新填写正确的邮箱");
        if (user.getUserNature() != null && user.getUserNature() == (byte) UserNature.Organize.getIndex() && user.getLegalName() == null)
        	throw new UtilException("企业法人姓名不能为空");
        if (user.getUserNature() != null && user.getUserNature() == (byte) UserNature.Organize.getIndex() && user.getLegalIdNo() == null)
        	throw new UtilException("企业法人证件号不能为空");
        boolean flag= cache.checkFrequentAction("register_"+user.getMobile(),1);
        if(!flag){
            throw new UtilException("请勿频繁操作");
        }
        User dbUser = this.getUserByMobile(user.getMobile());
        Object key = this.systemSetService.getParamVal(Enums.Params.Key.getKey());
        if (user.getId() != null && user.getId() > 0) {
            if (dbUser != null && dbUser.getId().compareTo(user.getId()) != 0)
                throw new UtilException("系统已有该注册帐号，无法再注册");
            User idUser = this.userMapper.selectUserByIdCard(user.getIdCard());
            if (user.getIdCard() != null && !user.getIdCard().isEmpty() && idUser != null && idUser.getId().compareTo(user.getId()) != 0)
                throw new UtilException("该身份信息已被认证");
            if (user.getPassword() != null)
                user.setPassword(CommonUtil.createPwd(user.getPassword(), key.toString()));
            this.userMapper.updateByPrimaryKeySelective(user);
            this.utilCache.delUser(user.getId());
        } else {
            if (dbUser != null)
                throw new UtilException("系统已有该注册帐号，无法再注册");
            if (user.getPassword().length() < 6)
                throw new UtilException("密码长度不够，至少需要6位");
            if (user.getMobile() == null)
                throw new UtilException("手机号码不能为空");
            if (user.getInvite() != null && user.getInvite() != "") {
                User inviteUser = this.userMapper.selectUserByInviteCode(user.getInvite());
                if (inviteUser != null)
                    user.setInviteId(inviteUser.getId());
                else
                    throw new UtilException("推荐人不存在");
            }
            if (key == null)
                throw new UtilException("请求异常");
            user.setInviteCode(this.createInviteCode(user.getMobile()));
            user.setPassword(CommonUtil.createPwd(user.getPassword(), key.toString()));
            user.setTime(new Date());
            try {
                this.userMapper.insertSelective(user);
                Account account = this.accountService.save(user);
                user.setAccountId(account.getId());
                this.userMapper.updateByPrimaryKeySelective(user);
                if (user.getType() == Enums.AccountType.Investor.getIndex()) {
                    this.utilCache.setTotal(Enums.TotalItems.TotalRegister.getKey(), 1);
                    this.utilCache.setTotal(Enums.TotalItems.TodayRegister.getKey(), 1);
                    this.utilCache.getUser(user.getId());
                    this.queueService.insertQueue(Enums.Actions.Register.getIndex(), user.getId(), null, BigDecimal.ZERO);
                }
            } catch (Exception e) {
                e.printStackTrace();
                throw new UtilException("注册时发现异常情况，请稍后尝试");
            }
        }
    }

    /**
     * 生成邀请码
     *
     * @throws UtilException
     */
    private String createInviteCode(String mobile) throws UtilException{
        Object key=this.systemSetService.getParamVal(Enums.Params.userInviteCode.getKey());
        Map<String, String> config =null;
        if(key!=null){
            config=CommonUtil.parseMap(key.toString());
        }
        if(config!=null && config.get("useMobile").equals("1") && mobile!=null && !mobile.isEmpty()){
            if (this.userMapper.selectUserByInviteCode(mobile) == null)
                return mobile;
            return createInviteCode(null);
        }else{
            String a = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            StringBuffer result = new StringBuffer();
            for (int i = 0; i < 6; i++) {
                int rand = (int) (Math.random() * a.length());
                result = result.append(a.charAt(rand));
            }
            if (this.userMapper.selectUserByInviteCode(result.toString()) == null)
                return result.toString();
            return createInviteCode(null);
        }
    }

    /**
     * 用户登录
     *
     * @param user
     * @throws UtilException
     */
    public User login(User user) throws UtilException {
        if (user.getMobile() == null)
            throw new UtilException("请输入手机帐号");
        if (user.getPassword() == null)
            throw new UtilException("请输入密码");
        User dbUser = this.getUserByMobile(user.getMobile());
        if (dbUser == null)
            throw new UtilException("此账户不存在");
        if (dbUser.getType() != (byte) Enums.AccountType.Investor.getIndex())
            throw new UtilException("此账户不允许登录");
        if (dbUser.getLock())
            throw new UtilException("此账户已被锁定，请联系管理员");
        Object key = this.systemSetService.getParamVal(Enums.Params.Key.getKey());
        if (key == null)
            throw new UtilException("请求异常");
        if (dbUser.getPassword().equals(CommonUtil.createPwd(user.getPassword(), key.toString()))) {
            // FIXME 锁状态关闭登录 登录成功开始业务

            User _dbUser=new User();
            _dbUser.setLastLogin(new Date());
            _dbUser.setId(dbUser.getId());
            this.userMapper.updateByPrimaryKeySelective(_dbUser);

            dbUser.setLastLogin(new Date());
            this.queueService.insertQueue(Enums.Actions.Login.getIndex(), dbUser.getId(), null, BigDecimal.ZERO);
            return dbUser;
            // FIXME 锁状态关闭登录 登录成功结束业务
        } else {
            throw new UtilException("密码输入错误");
        }
    }

    /**
     * 实名
     *
     * @param user
     * @throws UtilException
     */
    public boolean saveRealName(User user) throws UtilException {
        if (user.getRealName() == null || user.getRealName().isEmpty())
            throw new UtilException("真实姓名不能为空");
        if (user.getIdCard() == null || user.getIdCard().isEmpty())
            throw new UtilException("身份证号不能为空");
        if (this.userMapper.selectUserByIdCard(user.getIdCard()) != null)
            throw new UtilException("该身份信息已被认证");
        User dbUser = this.userMapper.selectByPrimaryKey(user.getId());
        Object realCount = this.systemSetService.getParamVal(Enums.Params.RealCount.getKey());
        if (realCount != null && dbUser.getRealCount() >= Integer.valueOf(realCount.toString()))
            throw new UtilException("实名不可超过" + realCount + " 次,请联系在线客服");

        boolean result = this.commonService.realName(user.getRealName(), user.getIdCard());
        User upUser = new User();
        upUser.setId(user.getId());
        upUser.setRealCount((byte) (dbUser.getRealCount() + 1));
        if (result) {
            upUser.setRealName(user.getRealName());
            upUser.setIdCard(user.getIdCard());
            this.queueService.insertQueue(Enums.Actions.RealName.getIndex(), user.getId(), null, BigDecimal.ZERO);
        }
        this.userMapper.updateByPrimaryKeySelective(upUser);
        this.utilCache.delUser(user.getId());
        return result;
    }

    /**
     * 邮箱认证
     *
     * @param user
     * @throws UtilException
     */
    public void validEmail(User user) throws Exception {
        if (user.getEmail() != null && !(Pattern.compile("([a-zA-Z0-9_\\.\\-])+\\@(([a-zA-Z0-9\\-])+\\.)+([a-zA-Z0-9]{2,4})+$").matcher(user.getEmail()).matches()))
            throw new UtilException("邮箱格式不对，请重新填写正确的邮箱");
        user.setValidEmail(false);
        this.userMapper.updateByPrimaryKeySelective(user);
        // 发送认证邮件
        this.utilCache.delUser(user.getId());
        //this.queueService.insertQueue(Enums.Actions.ValidEmail.getIndex(), user.getId(), null, BigDecimal.ZERO);
        Object key = this.systemSetService.getParamVal(Enums.Params.Key.getKey());
        if (key == null)
            throw new UtilException("请求异常");
        Object base = this.systemSetService.getParamVal(Enums.Params.Base.getKey());
        long time = new Date().getTime();
        String content = "<a href=\"" + base + "/validEmailSuccess?uid=" + user.getId() + "&time=" + time + "&sign=" + MD5Util.MD5(user.getId() + time + key.toString()) + "\">请点击此处进行认证</a>";

        this.commonService.sendEmailMsg(user.getEmail(), "邮箱认证", content);

    }

    /**
     * 设置头像
     *
     * @throws UtilException
     */
    public void saveHeadImg(long userId, String img) throws Exception {
        User user = new User();
        user.setId(userId);
        user.setHeadImg(img);
        this.userMapper.updateByPrimaryKeySelective(user);
        this.utilCache.delUser(user.getId());
    }

    /**
     * 锁定用户
     *
     * @throws UtilException
     */
    public void lock(long userId, boolean lock) throws UtilException {
        if (userId > 0) {
            User user = new User();
            user.setId(userId);
            user.setLock(lock);
            this.userMapper.updateByPrimaryKeySelective(user);
            this.utilCache.delUser(userId);
        } else
            throw new UtilException("请选择用户");
    }

    /**
     * 邮箱认证
     *
     * @param userId
     * @throws UtilException
     */
    public void validEmailCall(long userId, long time, String sign) throws UtilException {
        Object key = this.systemSetService.getParamVal(Enums.Params.Key.getKey());
        if (key == null)
            throw new UtilException("请求异常");
        long t = new Date().getTime();
        if (t - time > (1000 * 60 * 60 * 24))
            throw new UtilException("认证时间超出24小时，请重新认证");
        if (!MD5Util.MD5(userId + time + key.toString()).equals(sign))
            throw new UtilException("认证地址出错");
        if (this.userMapper.validEmailSuccess(userId) > 0) {
            //this.userMapper.updateByPrimaryKeySelective(user);
            this.queueService.insertQueue(Enums.Actions.ValidEmail.getIndex(), userId, null, BigDecimal.ZERO);
            this.utilCache.delUser(userId);
        } else
            throw new UtilException("此邮箱已认证");
    }

    /**
     * 设置登录密码
     *
     * @throws UtilException
     */
    public void saveLoginPassword(long userId, String password) throws UtilException {
        Object key = this.systemSetService.getParamVal(Enums.Params.Key.getKey());
        if (key == null)
            throw new UtilException("请求异常");
        User cacheUser = this.userMapper.selectByPrimaryKey(userId);
        if (cacheUser.getPayPassword().equals(CommonUtil.createPwd(password, key.toString())))
            throw new UtilException("登录密码和交易密码不能相同");
        User user = new User();
        user.setId(userId);
        user.setPassword(CommonUtil.createPwd(password, key.toString()));
        this.userMapper.updateByPrimaryKeySelective(user);
        this.utilCache.delUser(user.getId());
    }

    /**
     * 设置交易密码
     *
     * @param user
     * @throws UtilException
     */
    public void savePayPassword(User user) throws UtilException {
        if (user.getPayPassword().isEmpty())
            throw new UtilException("交易密码输入不能为空");
        Object key = this.systemSetService.getParamVal(Enums.Params.Key.getKey());
        if (key == null)
            throw new UtilException("请求异常");
        User cacheUser = this.userMapper.selectByPrimaryKey(user.getId());
        if (cacheUser.getPassword().equals(CommonUtil.createPwd(user.getPayPassword(), key.toString())))
            throw new UtilException("登录密码和交易密码不能相同");
        user.setPayPassword(CommonUtil.createPwd(user.getPayPassword(), key.toString()));
        this.userMapper.updateByPrimaryKeySelective(user);
        this.queueService.insertQueue(Enums.Actions.PayPassword.getIndex(), user.getId(), null, BigDecimal.ZERO);
        this.utilCache.delUser(user.getId());
    }

    /**
     * 根据条件获取账户信息
     *
     * @return
     * @throws Exception
     */
    public PageInfo getUserByParam(int pageNo, int pageSize, String param, String bTime, String eTime,int route, String type,int channel,int bInvestCount,int eInvestCount,String invite) throws UtilException {
        PageHelper.startPage(pageNo, pageSize);
        List<HashMap> list = this.userMapper.selectByParam(type, param, bTime, eTime,route,channel,bInvestCount,eInvestCount,invite);
        PageInfo page = new PageInfo(list);
        return page;
    }

    /**
     * 更新积分
     *
     * @return
     * @throws Exception
     */
    public int updateScore(long userId, long score) {
        User user = new User();
        user.setId(userId);
        user.setScore(score);
        int result = this.userMapper.updateScore(user);
        this.utilCache.delUser(userId);
        return result;
    }

    public void lockBorrow(long userId) throws UtilException{
        if (userId > 0) {
            User user = userMapper.selectByPrimaryKey(userId);
            if(user!=null){
                if(user.getMobile().indexOf("-")>-1){
                    throw new UtilException("该借款人已经锁定，无需重复锁定");
                }
                int count= billDebtMapper.selectCountByBorrowerId(user.getAccountId());
                if(count>0){
                    throw new UtilException("该借款人还有未完成的还款，暂时不能锁定");
                }
                User userTemp=new User();
                userTemp.setId(userId);
                userTemp.setMobile("-" + user.getMobile());
                this.userMapper.updateByPrimaryKeySelective(userTemp);
                this.utilCache.delUser(userId);
            }else{
                throw new UtilException("借款人不存在");
            }

        } else
            throw new UtilException("请选择用户");
    }

    public void unlockBorrow(long userId) throws UtilException{
        if (userId > 0) {
            User user = userMapper.selectByPrimaryKey(userId);
            if(user!=null){
                if(user.getMobile().indexOf("-")<0){
                    throw new UtilException("该借款人没锁定，无需解锁");
                }
                String mobile= user.getMobile().substring(1, user.getMobile().length());
                User userTemp=userMapper.selectUserByMobile(mobile);
                if(userTemp!=null){
                    throw new UtilException("该借款人不能解锁，已存该号投资用户");
                }
                User _userTemp=new User();
                _userTemp.setId(userId);
                _userTemp.setMobile(mobile);
                this.userMapper.updateByPrimaryKeySelective(_userTemp);
                this.utilCache.delUser(userId);
            }else{
                throw new UtilException("借款人不存在");
            }

        } else
            throw new UtilException("请选择用户");
    }

    /**
     * 设置登录密码
     *
     * @throws UtilException
     */
    public void saveLoginPassword(String mobile, String password) throws UtilException {
        if (mobile == null || mobile.isEmpty() || !(Pattern.compile("^1[34578]{1}[0-9]{9}").matcher(mobile).matches()))
            throw new UtilException("手机号码格式不对，请修改");
        User dbUser = this.getUserByMobile(mobile);
        if(dbUser==null){
            throw new UtilException("该用户不存在");
        }
        if(dbUser.getPassword()==null || dbUser.getPassword().isEmpty())
            this.saveLoginPassword(dbUser.getId(),password);
    }

    /**
     * 签到
     *
     * @param userId
     * @throws Exception
     */
    @Transactional(rollbackFor = {Exception.class})
    public int signIn(long userId) throws Exception {
        boolean flag = cache.checkFrequentAction("signIn_" + userId, 2);
        if (!flag) {
            throw new UtilException("请勿频繁操作");
        }
        User user = this.userMapper.selectByPrimaryKey(userId);
        if (user == null) {
            throw new UtilException("没有此用户.");
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date todayStart = sdf.parse(CommonUtil.getFormatTime(new Date(), "yyyy-MM-dd 00:00:00"));
        Date todayEnd = new Date(todayStart.getTime() + 24 * 60 * 60 * 1000);
        Date tomorowStart = new Date(todayStart.getTime() - 24 * 60 * 60 * 1000);
        if (user.getSignInTime() != null && user.getSignInTime().getTime() >= todayStart.getTime() && user.getSignInTime().getTime() < todayEnd.getTime()) {
            throw new UtilException("亲,您今日已经签到过了.");
        } else {
            int count = integralService.countSignInNum(userId);
            if (count > 0) {
                throw new UtilException("亲,您今日已经签到过了.");
            }
            int integral = 0;
            User _user = new User();
            _user.setId(userId);
            _user.setSignInNo(1);
            _user.setSignInTime(new Date());
            if (user.getSignInTime() != null && user.getSignInTime().getTime() >= tomorowStart.getTime() && user.getSignInTime().getTime() < todayStart.getTime()) {
                this.userMapper.updateSignInInfor(_user);
                integral = getSignIntegral(user.getSignInNo() + 1);
            } else {
                integral = getSignIntegral(1);
                this.userMapper.updateByPrimaryKeySelective(_user);
            }
            integralService.sendIntegral(Enums.Actions.SignIn.getIndex(), userId, integral, "签到奖励");
            return integral;
        }

    }

    private int getSignIntegral(int day) throws Exception {
        Object key = this.systemSetService.getParamVal(Enums.Params.signInConfig.getKey());
        Map<String, String> config = null;
        if (key != null) {
            config = CommonUtil.parseMap(key.toString());
        }
        int baseIntegral = 0, maxIntegral = 0;
        if (config != null && config.get("baseIntegral") != null && !config.get("baseIntegral").isEmpty() && config.get("maxIntegral") != null && !config.get("maxIntegral").isEmpty()) {
            try {
                baseIntegral = Integer.parseInt(config.get("baseIntegral"));
            } catch (NumberFormatException e) {
                baseIntegral = 0;
            }
            try {
                maxIntegral = Integer.parseInt(config.get("maxIntegral"));
            } catch (NumberFormatException e) {
                maxIntegral = 0;
            }
            return (2 * day - 1) * baseIntegral > maxIntegral ? maxIntegral : (2 * day - 1) * baseIntegral;
        } else {
            throw new UtilException("签到积分奖励未配置.");
        }
    }
}
