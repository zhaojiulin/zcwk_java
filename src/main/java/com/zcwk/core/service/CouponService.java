package com.zcwk.core.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zcwk.core.dao.AccountMapper;
import com.zcwk.core.dao.CouponRecordMapper;
import com.zcwk.core.dao.CouponSetMapper;
import com.zcwk.core.dao.UserMapper;
import com.zcwk.core.model.*;
import com.zcwk.core.util.Enums;
import com.zcwk.core.util.UtilCache;
import com.zcwk.core.util.UtilException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by pwx on 2016/12/6.
 */
@Service
public class CouponService {
    @Autowired
    CouponSetMapper couponSetMapper;
    @Autowired
    AccountMapper accountMapper;
    @Autowired
    AccountService accountService;
    @Autowired
    UserMapper userMapper;
    @Autowired
    UtilCache cache;
    @Autowired
    CouponRecordMapper couponRecordMapper;

    public void sendCoupon(int couponId, String mobile) throws UtilException {
        if (couponId <= 0)
            throw new UtilException("请选择红包");
        if (mobile.isEmpty())
            throw new UtilException("请选择发放对象");
        CouponSet b = this.couponSetMapper.selectByPrimaryKey(couponId);
        if (b == null)
            throw new UtilException("红包设置不存在或未启用");
        User user = this.userMapper.selectUserByMobile(mobile);
        if (user == null)
            throw new UtilException("发放对象不存在");
        if (user.getRealName().isEmpty() || user.getIdCard().isEmpty())
            throw new UtilException("此用户未实名");
        CouponRecord couponRecord = new CouponRecord();
        couponRecord.setAction(b.getSendAction());
        couponRecord.setName(b.getName());
        couponRecord.setRate(b.getRate());
        couponRecord.setBeginTime(b.getUseBeginTime());
        couponRecord.setEndTime(b.getUseEndTime());
        couponRecord.setBuyAmount(b.getUseBuyAmount());
        couponRecord.setProductType(b.getUseProductType());
        couponRecord.setPeriodUnit(b.getUsePeriodUnit());
        couponRecord.setCouponId(b.getId());
        couponRecord.setPeriod(b.getUsePeriod());
        couponRecord.setState((byte) Enums.State.Wait.getIndex());
        couponRecord.setTime(new Date());
        if (b.getSendBeginTime().compareTo(new Date()) > 0 || b.getSendEndTime().compareTo(new Date()) < 0){
            throw new UtilException("加息券已过期或者发放时间未到");
        }
        if (b.getSendInvite()) {
            User inviteUser = this.userMapper.selectByPrimaryKey(user.getInviteId());
            if (inviteUser != null)
                couponRecord.setAccountId(inviteUser.getAccountId());
            else
                throw new UtilException("邀请人不存在");
        }else{
            couponRecord.setAccountId(user.getAccountId());
        }
        if (b.getSendFirst()) {
            int sendCount = this.couponRecordMapper.selectCountByAccount(couponRecord.getAccountId(), b.getId());
            if (sendCount > 0)
                throw new UtilException("此加息券不允许发送多个");
        }
        this.couponRecordMapper.insertSelective(couponRecord);
    }

    @Transactional(rollbackFor = {Exception.class})
    public void sendCoupon(int action, User user, BigDecimal amount) throws UtilException {
        List<CouponSet> couponSets = this.couponSetMapper.selectByParam(String.valueOf(action), String.valueOf(Enums.State.Complete.getIndex()), true, null, null);
        if (user != null) {
            Account account = this.accountMapper.selectByPrimaryKey(user.getAccountId());
            if (account != null) {
                CouponRecord couponRecord = null;
                for (CouponSet b : couponSets) {
                    boolean isAdd = true;
                    long sendAccountId = account.getId();
                    if (b.getSendBuyAmount().compareTo(BigDecimal.ZERO) > 0 && b.getSendBuyAmount().compareTo(amount) > 0)  //判断购买金额
                        isAdd = false;
                    if (b.getSendTotalAmount().compareTo(BigDecimal.ZERO) > 0 && b.getSendTotalAmount().compareTo(account.getTotalInvestAmount()) > 0)  //判断投资总额
                        isAdd = false;
                    if (b.getSendCollect().compareTo(BigDecimal.ZERO) > 0 && b.getSendCollect().compareTo(account.getCollectCorpus().add(account.getCollectInterest())) > 0)  //判断待收
                        isAdd = false;
                    if (b.getSendInvestCount() > 0 && b.getSendInvestCount() != (byte) account.getInvestCount().intValue())  //判断购买次数
                        isAdd = false;
                    if (b.getSendBeginTime().compareTo(new Date()) > 0 || b.getSendEndTime().compareTo(new Date()) < 0){
                        isAdd =  false;
                    }
                    if (b.getSendInvite()) {
                        User inviteUser = this.userMapper.selectByPrimaryKey(user.getInviteId());
                        if (inviteUser != null)
                            sendAccountId = inviteUser.getAccountId();
                        else
                            isAdd = false;
                    }
                    if (b.getSendFirst()) {
                        int sendCount = this.couponRecordMapper.selectCountByAccount(sendAccountId, b.getId());
                        if (sendCount > 0)
                            isAdd = false;
                    }
                    if (isAdd) {
                        for (int i = 0; i < b.getSendCount(); i++) {
                            couponRecord = new CouponRecord();
                            couponRecord.setAccountId(sendAccountId);
                            couponRecord.setAction((byte) action);
                            couponRecord.setName(b.getName());
                            couponRecord.setRate(b.getRate());
                            couponRecord.setBeginTime(b.getUseBeginTime());
                            couponRecord.setEndTime(b.getUseEndTime());
                            couponRecord.setBuyAmount(b.getUseBuyAmount());
                            couponRecord.setCouponId(b.getId());
                            couponRecord.setProductType(b.getUseProductType());
                            couponRecord.setPeriodUnit(b.getUsePeriodUnit());
                            couponRecord.setPeriod(b.getUsePeriod());
                            couponRecord.setState((byte) Enums.State.Wait.getIndex());
                            couponRecord.setTime(new Date());
                            this.couponRecordMapper.insertSelective(couponRecord);
                        }
                    }
                }
            }
        }
    }

    @Transactional(rollbackFor = {Exception.class})
    public Boolean useCoupon(long id, long investId, BigDecimal amount, Debt debt) throws UtilException {
        try {
            CouponRecord record = this.couponRecordMapper.selectByPrimaryKey(id);
            if (record != null) {
                if (record.getBeginTime().compareTo(new Date()) > 0 || record.getEndTime().compareTo(new Date()) < 0)
                    return false;
                if (record.getPeriodUnit() > 0 && record.getPeriodUnit() != debt.getPeriodUnit())
                    return false;
                if (record.getPeriod() > 0 && record.getPeriod() > debt.getPeriod())
                    return false;
                if (record.getProductType() > 0 && record.getProductType() != debt.getProductType())
                    return false;
                if (record.getBuyAmount().compareTo(amount) > 0)
                    return false;
                CouponRecord upRecord = new CouponRecord();
                upRecord.setId(record.getId());
                upRecord.setUseTime(new Date());
                upRecord.setInvestId(investId);
                upRecord.setState((byte) Enums.State.Complete.getIndex());
                if (this.couponRecordMapper.useRecord(upRecord) <= 0)
                    throw new UtilException("加息券使用异常");
                return true;
            }
            return false;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            return false;
        }
    }

    /**
     * 保存加息券设置
     *
     * @return
     * @throws Exception
     */
    public void saveSet(CouponSet couponSet)throws UtilException  {
        BigDecimal num = new BigDecimal(100);
        if (couponSet.getRate().subtract(num).compareTo(BigDecimal.ZERO) >= 0){
            throw new UtilException("加息券利率不正确！");
        }
        if (couponSet.getRate().compareTo(BigDecimal.ZERO) > 0)
            couponSet.setRate(couponSet.getRate().divide(BigDecimal.valueOf(100)));
        if (couponSet.getId() > 0)
            this.couponSetMapper.updateByPrimaryKeySelective(couponSet);
        else {
            couponSet.setTime(new Date());
            this.couponSetMapper.insertSelective(couponSet);
        }
    }


    /**
     * 根据条件获取加息券设置列表
     *
     * @return
     * @throws Exception
     */
    public PageInfo getCouponSetByParam(int pageNo, int pageSize, String bTime, String eTime, String state) throws UtilException {
        PageHelper.startPage(pageNo, pageSize);
        List<CouponSet> list = this.couponSetMapper.selectByParam(null, state, false, bTime, eTime);
        PageInfo page = new PageInfo(list);
        return page;
    }

    /**
     * 根据条件获取加息券列表
     *
     * @return
     * @throws Exception
     */
    public PageInfo getCouponRecordByParam(int pageNo, int pageSize, String accountId, boolean isExpiry, String productType, String periodUnit, String period, String state, String param, String bTime, String eTime,BigDecimal buyAmount) throws UtilException {
        PageHelper.startPage(pageNo, pageSize);
        List<HashMap> list = this.couponRecordMapper.selectByParam(accountId, isExpiry, productType, periodUnit, period, state, param, bTime, eTime,buyAmount);
        PageInfo page = new PageInfo(list);
        return page;
    }


    public CouponSet getCouponSetById(int id) {
        return this.couponSetMapper.selectByPrimaryKey(id);
    }
}
