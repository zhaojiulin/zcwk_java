package com.zcwk.core.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zcwk.core.dao.ChannelMapper;
import com.zcwk.core.dao.UserMapper;
import com.zcwk.core.model.Channel;
import com.zcwk.core.util.Result;
import com.zcwk.core.util.UtilCache;
import com.zcwk.core.util.UtilException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Administrator on 2017/3/15.
 */
@Service
public class ChannelService {
@Autowired
    ChannelMapper channelMapper;
    @Autowired
    UserMapper userMapper;
    @Autowired
    UtilCache cache;

    public List<HashMap<String,Object>> selectChannel(String sTime, String bTime, String eTime, long channelType) throws UtilException {
        List<HashMap<String, Object>> list = new ArrayList<>();
        HashMap<String,Object> m = new HashMap<>();
        if (channelType>1||channelType==0) {
            m=this.SelectCount(sTime,bTime, eTime, channelType);
            list.add(m);
        } else {
            List<HashMap> channels = this.channelMapper.selectchanels();
            if(channels ==null)
                throw new UtilException("查询是出现异常！");
            for (int i = 0; i <channels.size(); i++) {
                HashMap<String, Object> map = new HashMap<>();
                channelType =Long.valueOf(channels.get(i).get("id").toString()) ;
                map=this.SelectCount(sTime,bTime, eTime, channelType);
                list.add(map);
            }
        }
        return list;
    }

    public HashMap<String, Object> SelectCount(String sTime,String bTime, String eTime, long channelType) {

        HashMap<String, Object> map = new HashMap<>();
        String channelName=this.channelMapper.selectChanenl(channelType);
        map.put("channelName",channelName);

        List<HashMap> Rechargelist = this.userMapper.selectRecharge(sTime,bTime, eTime, channelType);
        double totalRecharge = Double.parseDouble(Rechargelist.get(0).get("totalRecharge").toString());
        map.put("totalRecharge", totalRecharge);
        double todayRecharge = Double.parseDouble(Rechargelist.get(0).get("todayRecharge").toString());
        map.put("todayRecharge", todayRecharge);
        double totalRechargeNum = Double.parseDouble(Rechargelist.get(0).get("totalRechargeNum").toString());
        map.put("totalRechargeNum", totalRechargeNum);
        double todayRechargeNum = Double.parseDouble(Rechargelist.get(0).get("todayRechargeNum").toString());
        map.put("todayRechargeNum", todayRechargeNum);

        List<HashMap> Tradelist = this.userMapper.selectTrade(sTime,bTime, eTime, channelType);
        double totalTradeNum = Double.parseDouble(Tradelist.get(0).get("totalTradeNum").toString());
        map.put("totalTradeNum", totalTradeNum);
        double todayTradeNum = Double.parseDouble(Tradelist.get(0).get("todayTradeNum").toString());
        map.put("todayTradeNum", todayTradeNum);
        double totalTrade = Double.parseDouble(Tradelist.get(0).get("totalTrade").toString());
        map.put("totalTrade", totalTrade);
        double todayTrade = Double.parseDouble(Tradelist.get(0).get("todayTrade").toString());
        map.put("todayTrade", todayTrade);

        List<HashMap> Withdrawlist = this.userMapper.selectWithdraw(sTime,bTime, eTime, channelType);
        double totalWithdraw = Double.parseDouble(Withdrawlist.get(0).get("totalWithdraw").toString());
        map.put("totalWithdraw", totalWithdraw);
        double todayWithdraw = Double.parseDouble(Withdrawlist.get(0).get("todayWithdraw").toString());
        map.put("todayWithdraw", todayWithdraw);

        List<HashMap> BindCardlist = this.userMapper.selectBindCard(sTime,bTime, eTime, channelType);
        double todayBindCard = Double.parseDouble(BindCardlist.get(0).get("todayBindCard").toString());
        map.put("todayBindCard", todayBindCard);
        double totalBindCard = Double.parseDouble(BindCardlist.get(0).get("totalBindCard").toString());
        map.put("totalBindCard", totalBindCard);

        List<HashMap> Registerlist = this.userMapper.selectRegister(sTime,bTime, eTime, channelType);
        double totalRegister = Double.parseDouble(Registerlist.get(0).get("totalRegister").toString());
        map.put("totalRegister", totalRegister);
        double todayRegister = Double.parseDouble(Registerlist.get(0).get("todayRegister").toString());
        map.put("todayRegister", todayRegister);

        List<HashMap> RealNamelist = this.userMapper.selectRealName(sTime,bTime, eTime, channelType);
        double totalRealName = Double.parseDouble(RealNamelist.get(0).get("totalRealName").toString());
        map.put("totalRealName", totalRealName);
        double todayRealName = Double.parseDouble(RealNamelist.get(0).get("todayRealName").toString());
        map.put("todayRealName", todayRealName);

        List<HashMap> Payment = this.userMapper.selectPayment(sTime,channelType);
        double paymentCorpus = Double.parseDouble(Payment.get(0).get("paymentCorpus").toString());
        map.put("paymentCorpus", paymentCorpus);
        double paymentInterest = Double.parseDouble(Payment.get(0).get("paymentInterest").toString());
        map.put("paymentInterest", paymentInterest);

        return  map;
    }

    /**
     * 保存
     *搜索编号：：1489626435
     * @return
     * @throws UtilException
     */
    public HashMap saveChannel(Channel channelstr) throws UtilException {

        if(channelstr.getName()==null  ||channelstr.getName()=="" ){
           return new Result().Error("请输入渠道名称！");
        }
        if(channelstr.getName().length()>20){
            return new Result().Error("渠道名称限制20字符！");
        }
        if(channelstr.getCode()==null ||channelstr.getCode().isEmpty() ){
            return new Result().Error("请输入渠道编码！");
        }
        channelstr.setTime(new Date());
        int results=0;

        Channel code = this.channelMapper.selectCode(channelstr.getCode());
        if (code == null || (code!=null && channelstr.getId()!=null && code.getId().equals(channelstr.getId()))) {
            if(channelstr.getId()!=null && channelstr.getId()>=0){
                results=this.channelMapper.updateByPrimaryKeySelective(channelstr);
            }else{
                results=this.channelMapper.insertSelective(channelstr);
            }
            if (results < 1)
                return new Result().Error("渠道保存失败！");
            return new Result().Success();
        }else{
            return new Result().Error("该渠道渠道名称已存在！");
        }

    }

    /**
     * 查询渠道列表
     * @return
     * @throws UtilException
     */
    public PageInfo SelectAllchannelList(int pageNo, int pageSize,String bTime,String eTime) throws UtilException {
        PageHelper.startPage(pageNo, pageSize);
        List<HashMap> list = this.channelMapper.selectAll(bTime,eTime);
        PageInfo page = new PageInfo(list);
        return page;
    }

    /**
     * 根据id获取渠道信息
     * @param id
     * @return
     * @throws UtilException
     */
    public Channel getChannel(long id) throws UtilException {
        if (id < 0)
            throw new UtilException(" 渠道编号不正确");
        return channelMapper.selectByPrimaryKey(id);
    }

    /**
     * 根据code查询
     * @param code
     * @return
     * @throws UtilException
     */
    public Channel getChannelByCode(String code) throws UtilException{
        if(code==null && code!=""){
            throw new UtilException(" 渠道编码不正确");
        }
        return channelMapper.selectCode(code);
    }


}
