package com.zcwk.core.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zcwk.core.dao.IntegralRecordMapper;
import com.zcwk.core.dao.IntegralRuleMapper;
import com.zcwk.core.model.IntegralRecord;
import com.zcwk.core.model.IntegralRule;
import com.zcwk.core.model.User;
import com.zcwk.core.util.CommonUtil;
import com.zcwk.core.util.Enums;
import com.zcwk.core.util.UtilCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

/**
 * Created by pwx on 2016/8/22.
 */
@Service
public class IntegralService {

    @Autowired
    IntegralRuleMapper integralRuleMapper;
    @Autowired
    IntegralRecordMapper integralRecordMapper;
    @Autowired
    UserService userService;
    @Autowired
    UtilCache cache;

    public void saveRule(IntegralRule rule) {
        if (rule.getAwardType() == Enums.AwardType.Scale.getIndex())
            rule.setNumber(rule.getNumber().divide(new BigDecimal(100), 4, BigDecimal.ROUND_HALF_UP));
        if (rule.getId() > 0)
            this.integralRuleMapper.updateByPrimaryKeySelective(rule);
        else {
            rule.setTime(new Date());
            this.integralRuleMapper.insertSelective(rule);
        }
    }

    public IntegralRule getRule(Long id) {
        return this.integralRuleMapper.selectByPrimaryKey(id);
    }

    @Transactional(rollbackFor = {Exception.class})
    public void sendIntegral(int action, User user, BigDecimal amount) {
        if (user != null) {
            List<IntegralRule> list = this.integralRuleMapper.selectByParam(String.valueOf(action), String.valueOf(Enums.State.Complete.getIndex()));
            IntegralRecord record = new IntegralRecord();
            for (IntegralRule rule : list) {
                int number = 0;
                boolean isAdd = true;
                if (rule.getSendFirst()) {
                    int sendCount = this.integralRecordMapper.selectCountByAccount(user.getId(), action);
                    if (sendCount > 0)
                        isAdd = false;
                }
                if (isAdd) {
                    if (rule.getAwardType() == Enums.AwardType.Scale.getIndex())
                        number = (amount.multiply(rule.getNumber())).intValue();
                    else
                        number = rule.getNumber().intValue();
                    if (number > 0) {
                        if (this.userService.updateScore(user.getId(), number) > 0) {
                            record.setTime(new Date());
                            record.setAction((byte) action);
                            record.setUserId(user.getId());
                            record.setNumber(number);
                            this.integralRecordMapper.insertSelective(record);
                        }else{
                            System.err.println("积分漏洞....."+user.getId());
                        }
                    }
                }
            }
        }
    }

    public int countSignInNum(long userId) throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date todayStart = sdf.parse(CommonUtil.getFormatTime(new Date(), "yyyy-MM-dd 00:00:00"));
        Date todayEnd = new Date(todayStart.getTime() + 24 * 60 * 60 * 1000);
        return integralRecordMapper.selectTodayCountByAccount(userId, Enums.Actions.SignIn.getIndex(), todayStart, todayEnd);
    }

    @Transactional(rollbackFor = {Exception.class})
    public void sendIntegral(int action, long userId, int integral, String memo) throws Exception {
        System.err.print(integral);
        IntegralRecord record = new IntegralRecord();
        if (integral != 0) {
            if (this.userService.updateScore(userId, integral) > 0) {
                record.setTime(new Date());
                record.setAction((byte) action);
                record.setUserId(userId);
                record.setNumber(integral);
                record.setMemo(memo);
                this.integralRecordMapper.insertSelective(record);
            } else {
                throw new Exception("积分不足.");
            }
        }
    }

    /**
     * 根据条件获取积分记录规则
     *
     * @return
     * @throws Exception
     */
    public PageInfo getIntegralRuleByParam(int pageNo, int pageSize, String action, String state) {
        PageHelper.startPage(pageNo, pageSize);
        List<IntegralRule> list = this.integralRuleMapper.selectByParam(action, state);
        PageInfo page = new PageInfo(list);
        return page;
    }

    /**
     * 根据条件获取积分记录
     *
     * @return
     * @throws Exception
     */
    public PageInfo getIntegralRecordByParam(int pageNo, int pageSize, String param, String bTime, String eTime, String userId, String action) {
        PageHelper.startPage(pageNo, pageSize);
        List<HashMap> list = this.integralRecordMapper.selectByParam(bTime, eTime, param, userId, action);
        PageInfo page = new PageInfo(list);
        return page;
    }

}
