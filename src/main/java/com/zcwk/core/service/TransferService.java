package com.zcwk.core.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zcwk.core.dao.*;
import com.zcwk.core.model.*;
import com.zcwk.core.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

/**
 * Created by pwx on 2016/12/8.
 */
@Service
public class TransferService {
    @Autowired
    TransferMapper transferMapper;
    @Autowired
    AccountMapper accountMapper;
    @Autowired
    AccountService accountService;
    @Autowired
    DebtMapper debtMapper;
    @Autowired
    UtilCache cache;
    @Autowired
    CashflowService cashflowService;
    @Autowired
    FreezeService freezeService;
    @Autowired
    InvestService investService;
    @Autowired
    BillDebtService billDebtService;
    @Autowired
    BillDebtMapper billDebtMapper;
    @Autowired
    DataSafety dataSafety;
    @Autowired
    UserService userService;
    @Autowired
    QueueService queueService;
    @Autowired
    BonusRecordMapper bonusRecordMapper;
    @Autowired
    BonusService bonusService;
    @Autowired
    CouponRecordMapper couponRecordMapper;
    @Autowired
    CouponService couponService;
    @Autowired
    InvestMapper investMapper;
    @Autowired
    SystemSetService systemSetService;

    //用户可转让列表
    public PageInfo getSureTransfer(long accountId, int pageNo, int pageSize) throws UtilException {
        PageHelper.startPage(pageNo, pageSize);
        Object obj = this.systemSetService.getParamVal(Enums.Params.TransferDay.getKey());
        int day = obj == null ? 7 : Integer.valueOf(obj.toString());
        List<HashMap> list = this.investMapper.selectTransfer(accountId, day);
        PageInfo page = new PageInfo(list);
        return page;
    }

    public PageInfo getAll(String accountId, int pageNo, int pageSize) {
        PageHelper.startPage(pageNo, pageSize);
        List<HashMap> list = this.transferMapper.selectAll(accountId);
        PageInfo page = new PageInfo(list);
        return page;
    }


    @Transactional(rollbackFor = {Exception.class})
    public boolean save(long investId, BigDecimal award) throws UtilException {
        Invest invest = this.investMapper.selectByPrimaryKey(investId);
        if (invest == null)
            throw new UtilException("投资记录不存在");
        Debt debt = this.cache.getDebt(invest.getDebtId());
        if (CommonUtil.compareDay(debt.getFullTime(), new Date()) < debt.getClosedPeriod())
            throw new UtilException("此投资记录还在封闭期");
        //债转只能为一次还款或按月付息到期还本的标的
        //if (debt.getPaymentType() != Enums.PaymentType.Once.getIndex() && debt.getPaymentType() != Enums.PaymentType.Monthly.getIndex())
        //    return false;
        BigDecimal amount = BigDecimal.ZERO;
        BillDebt upBill = new BillDebt();
        List<BillDebt> billList = this.billDebtMapper.selectByInvestId(investId, String.valueOf(Enums.State.Wait.getIndex()));
        Object obj = this.systemSetService.getParamVal(Enums.Params.TransferDay.getKey());
        int day = obj == null ? 7 : Integer.valueOf(obj.toString());
        for (BillDebt bill : billList) {
            if (CommonUtil.compareDay(new Date(), bill.getPaymentTime()) < (day + 1))
                throw new UtilException("回款日前" + (day + 1) + "天不能转让");
            amount = amount.add(bill.getPaymentCorpus());
        }
        Transfer transfer = new Transfer();
        transfer.setPeriods(billList.size());
        transfer.setState((byte) Enums.DebtState.ApprovalPass.getIndex());
        transfer.setAccountId(invest.getAccountId());
        transfer.setAmount(amount);
        transfer.setAward(award);
        transfer.setCutTime(CommonUtil.addDay(new Date(), day));
        transfer.setDebtId(debt.getId());
        transfer.setTime(new Date());
        transfer.setInvestId(investId);
        long transferId = this.transferMapper.insertSelective(transfer);
        for (BillDebt bill : billList) {
            upBill.setId(bill.getId());
            upBill.setIsTransfer(true);
            upBill.setTransferId(transferId);
            upBill.setState((byte) Enums.State.Complete.getIndex());
            this.billDebtMapper.updateByPrimaryKeySelective(upBill);
        }
        Invest upInvest = new Invest();
        upInvest.setId(investId);
        upInvest.setIsTransfer(true);
        upInvest.setTransferId(transferId);
        this.investMapper.updateByPrimaryKeySelective(upInvest);
        return true;
    }

    @Transactional(rollbackFor = {Exception.class})
    public Long buy(long transferId, BigDecimal amount, long accountId, String bonusId) throws UtilException {
        Transfer transfer = this.transferMapper.selectByPrimaryKey(transferId);
        Debt debt = this.cache.getDebt(transfer.getDebtId());
        // 判断标状态为可投状态
        if (transfer.getState() != Enums.DebtState.ApprovalPass.getIndex())
            throw new UtilException("标的状态为不可投资状态");

        if (transfer.getAccountId() == accountId)
            throw new UtilException("不能投自己的借款标");
        // 判断是否已截标
        if (transfer.getCutTime() != null && transfer.getCutTime().compareTo(new Date()) < 0)
            throw new UtilException("此标已过有效期");
        //判断剩余可投金额
        Account account = this.accountService.getAccount(accountId);
        if (account == null)
            throw new UtilException("投资人不存在");
        if (!this.dataSafety.checkAccount(account.getId()))
            throw new UtilException("账户出现异常，请联系管理员");
        //判断是否为新手
        if (!account.getIsNew() && debt.getIsNew())
            throw new UtilException("您已经不是新手了");
        //获取红包金额
        BigDecimal bonusAmount = BigDecimal.ZERO;
        if (bonusId != null && bonusId != "") {
            BonusRecord record = this.bonusRecordMapper.selectByPrimaryKey(Long.valueOf(bonusId));
            if (record != null)
                bonusAmount = record.getAmount();
        }
        //获加息券利率
        BigDecimal couponRate = BigDecimal.ZERO;
        // 判断余额
        if ((account.getBalance().add(bonusAmount)).compareTo(amount) < 0)
            throw new UtilException("账户余额不足，请充值");
        // 更新已投金额
        int buyResult = this.transferMapper.buyTransfer(transferId, amount);
        if (buyResult < 1)
            throw new UtilException("项目可投金额不足");
        // 增加投资记录
        Invest invest = this.investService.addTransfer(account, debt, amount, bonusAmount, couponRate, false, false, transfer);
        //使用红包
        if (bonusId != null && bonusId != "")
            if (!this.bonusService.useBonus(Long.valueOf(bonusId), invest.getId(), amount, debt))
                throw new UtilException("使用红包异常，请核查红包使用条件");
        // 购买成功后处理
        this.buySuccess(invest, debt, account, amount);
        //添加queue
        this.queueService.insertQueue(Enums.Actions.Buy.getIndex(), account.getUserId(), invest, amount);
        //添加统计
        if(amount!=null){
            this.cache.setTotal(Enums.TotalItems.TotalInvest.getKey(), amount.doubleValue());
            this.cache.setTotal(Enums.TotalItems.TodayInvest.getKey(), amount.doubleValue());
        }
        if(invest!=null && invest.getPieceAmount()!=null){
            this.cache.setTotal(Enums.TotalItems.TodayPieceAmount.getKey(), invest.getPieceAmount().doubleValue());
        }
        // 返回投资记录
        return invest.getId();
    }

    /**
     * 购买成功后处理
     *
     * @return
     */
    @Transactional(rollbackFor = {Exception.class})
    private void buySuccess(Invest invest, Debt debt, Account account,
                            BigDecimal amount) throws UtilException {
        Account investAccount = new Account();
        investAccount.setId(invest.getAccountId());
        investAccount.setInvestCount(1); //购买次数加1
        // 增加冻结金额
        this.freezeService.save(account.getId(), Enums.FundType.Buy, invest.getId(), amount);
        investAccount.setFreeze(invest.getAmount());
        // 如果是新手标
        if (account.getIsNew()) {
            Account acc = new Account();
            acc.setIsNew(false);
            acc.setId(account.getId());
            this.accountMapper.updateByPrimaryKeySelective(acc);
        }
        // 更新用户余额
        investAccount.setBalance(invest.getAmount().multiply(new BigDecimal(-1)));
        investAccount.setPieceBalance(invest.getPieceAmount().multiply(new BigDecimal(-1)));
        investAccount.setTotalInvestAmount(amount);
        this.accountService.updateBalance(investAccount, invest.getAmount().multiply(new BigDecimal(-1)), Enums.FundType.Buy, invest.getId(), "扣除投资金额");
        Account upAcc = this.accountMapper.selectByPrimaryKey(investAccount.getId());
        if (upAcc.getBalance().compareTo(BigDecimal.ZERO) < 0) {
            throw new UtilException("余额不足");
        }
    }

    /**
     * 回款期数
     *
     * @return
     * @throws UtilException
     */
    private HashMap<Date, Object[]> getPeriods(Debt debt, BigDecimal amount, List<BillDebt> list) {
        LinkedHashMap<Date, Object[]> map = new LinkedHashMap<Date, Object[]>();
        BigDecimal corpus = BigDecimal.ZERO;
        BigDecimal interest = BigDecimal.ZERO;
        int i = 1;
        for (BillDebt bill : list) {
            if (bill.getPeriods() == bill.getMaxPeriods())
                corpus = amount;
            interest = (amount.multiply(debt.getRate().divide(BigDecimal.valueOf(365), 8, BigDecimal.ROUND_HALF_UP)).multiply(BigDecimal.valueOf(CommonUtil.compareDay(new Date(), bill.getPaymentTime())))).setScale(2, BigDecimal.ROUND_HALF_UP);
            map.put(bill.getPaymentTime(), new Object[]{corpus, interest, CommonUtil.compareDay(new Date(), bill.getPaymentTime()), i, bill.getGroup()});
            i++;
        }
        return map;
    }

    /**
     * 生成回款计划
     *
     * @return
     */
    @Transactional(rollbackFor = {Exception.class})
    public void createBill(Transfer transfer, List<Invest> list) throws UtilException {
        BigDecimal amount;
        BigDecimal corpus;
        BigDecimal interest;
        int periodDay;
        int periods;
        String group;
        Debt debt = this.cache.getDebt(transfer.getDebtId());
        Account debtAccount = new Account();
        Account investAccount = new Account();
        List<BillDebt> billList = this.billDebtMapper.selectPaymentByTransfer(transfer.getId(), true);
        for (Invest invest : list) {
            BigDecimal totalInterest = BigDecimal.ZERO;
            amount = invest.getAmount();
            HashMap<Date, Object[]> map = this.getPeriods(debt, amount, billList);
            for (Date dt : map.keySet()) {
                corpus = (BigDecimal) map.get(dt)[0];
                interest = (BigDecimal) map.get(dt)[1];
                periodDay = (int) map.get(dt)[2];
                periods = (int) map.get(dt)[3];
                group = map.get(dt)[4].toString();
                this.billDebtService.saveTransfer(transfer.getAccountId(), invest.getAccountId(), corpus,BigDecimal.ZERO, interest, dt, transfer.getDebtId(), (byte) periods, (byte) map.size(), periodDay, invest.getId(), false, transfer.getId(), group);
                totalInterest = totalInterest.add(interest);
            }
            investAccount.setId(invest.getAccountId());
            investAccount.setCollectCorpus(amount);
            investAccount.setCollectInterest(totalInterest);
            this.accountService.updateAccountTotal(investAccount);
        }
        BigDecimal totalInterest = BigDecimal.ZERO;
        BigDecimal realCorpus;
        BillDebt billDebt = new BillDebt();
        for (BillDebt bill : billList) {
            realCorpus = bill.getPaymentCorpus();
            interest = CommonUtil.calculateInterest(bill.getPaymentTime(), bill.getPaymentInterest(), bill.getPeriodDay(), true);
            billDebt.setId(bill.getId());
            billDebt.setRealPaymentCorpus(realCorpus);
            billDebt.setRealPaymentInterest(interest);
            billDebt.setRealPaymentTime(new Date());
            billDebt.setState((byte) Enums.State.Complete.getIndex());
            totalInterest = totalInterest.add(interest);
            this.billDebtMapper.updateByPrimaryKeySelective(billDebt);
        }
        debtAccount.setId(transfer.getAccountId());
        debtAccount.setBalance(transfer.getAmount().add(totalInterest));
        this.accountService.updateBalance(debtAccount, transfer.getAmount().add(totalInterest), Enums.FundType.Transfer, transfer.getId(), "增加债转金额");
    }


    /**
     * 发放投标奖励
     *
     * @return
     * @throws UtilException
     */
    public void sendInvestAward(List<Invest> list, Transfer transfer) throws UtilException {
        BigDecimal totalAward = BigDecimal.ZERO;

        for (Invest invest : list) {
            if (invest.getAwardType() > 0) {
                if (invest.getAwardNumber().compareTo(BigDecimal.ZERO) == 1) {
                    BigDecimal award = BigDecimal.ZERO;
                    Account acc = new Account();
                    acc.setId(invest.getAccountId());
                    if (invest.getAwardType() == Enums.AwardType.Scale.getIndex())
                        award = invest.getAwardNumber().multiply(invest.getAmount());
                    else
                        award = invest.getAwardNumber();
                    award = award.setScale(2, BigDecimal.ROUND_HALF_UP);
                    if (award.compareTo(BigDecimal.ZERO) > 0) {
                        acc.setBalance(award);
                        totalAward = totalAward.add(award);
                        acc.setTotalRewardAmount(award);
                        this.accountService.updateBalance(acc, award, Enums.FundType.InvestAward, invest.getId(), "增加投标奖励");
                    }
                }
            }
        }
        if (totalAward.compareTo(BigDecimal.ZERO) > 0) {
            Account account = new Account();
            account.setId(transfer.getAccountId());
            account.setBalance(totalAward.multiply(BigDecimal.valueOf(-1)));
            this.accountService.updateBalance(account, totalAward, Enums.FundType.SendTransferAward, transfer.getId(), "发放债转奖励");
        }
    }

    @Transactional(rollbackFor = {Exception.class})
    public void cancelTransfer(Transfer transfer) throws UtilException {
        Account account = new Account();
        List<Invest> investorList = this.investMapper.selectByTransfer(transfer.getId(), false);
        for (Invest invest : investorList) {
            account.setId(invest.getAccountId());
            account.setBalance(invest.getAmount());
            this.accountService.updateBalance(account, invest.getAmount(), Enums.FundType.CancelInvest, invest.getId(), "标的撤销，返还投标金额");
        }
        //撤销受让人回款计划
        this.billDebtMapper.updateStateByTransferId(transfer.getId(), false, (byte) Enums.State.Cancel.getIndex());
        //恢复转让人回款计划
        this.billDebtMapper.updateStateByTransferId(transfer.getId(), true, (byte) Enums.State.Wait.getIndex());
        Transfer upTransfer = new Transfer();
        upTransfer.setId(transfer.getId());
        upTransfer.setState((byte) Enums.DebtState.Cancel.getIndex());
        this.transferMapper.updateByPrimaryKeySelective(upTransfer);
    }
}
