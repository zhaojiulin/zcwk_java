/**
 *
 */
package com.zcwk.core.service;

import com.zcwk.core.dao.AccountMapper;
import com.zcwk.core.model.Account;
import com.zcwk.core.model.BankCard;
import com.zcwk.core.util.UtilException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zcwk.core.dao.BankCardMapper;

/**
 * @author dengqun
 *         <p/>
 *         上午11:09:45
 */

@Service
public class BankCardService {
    @Autowired
    BankCardMapper bankCardMapper;
    @Autowired
    AccountMapper accountMapper;

    public void deleteCard(long accountId) throws UtilException {
        Account account = this.accountMapper.selectByPrimaryKey(accountId);
        if (account.getBindCard())
            throw new UtilException("您已经绑卡，不能删除此卡");
        this.bankCardMapper.deleteBankCard(accountId);
    }

    public BankCard getBankCardByAccountId(long accountId){
        return bankCardMapper.selectByAccount(accountId);
    }
}
