/**
 *
 */
package com.zcwk.core.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zcwk.core.dao.PartnerMapper;
import com.zcwk.core.model.News;
import com.zcwk.core.model.Partner;
import com.zcwk.core.util.Result;
import com.zcwk.core.util.UtilException;

/**
 * @author dengqun
 *         <p/>
 *         上午11:11:21
 */

@Service
public class PartnerService {
    @Autowired
    PartnerMapper partnerMapper;

    /**
     * 添加合作伙伴
     *
     * @return
     * @throws UtilException
     */
    @Transactional(rollbackFor = {Exception.class})
    public void save(Partner partner) throws UtilException {
        try {
            if (partner.getId() > 0)
                this.partnerMapper.updateByPrimaryKeySelective(partner);
            else {
                partner.setTime(new Date());
                this.partnerMapper.insert(partner);
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw new UtilException("添加时出现异常");
        }
    }

    /**
     * 根据id删除partner
     *
     * @param id
     * @return
     * @throws UtilException
     */
    @Transactional(rollbackFor = {Exception.class})
    public void deletePartnerById(long id) throws UtilException {
        try {
            partnerMapper.deleteByPrimaryKey(id);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw new UtilException("删除时异常情况发生请检查");
        }

    }

    /**
     * 修改Partner
     *
     * @param Partner
     * @return
     * @throws UtilException
     */
    @Transactional(rollbackFor = {Exception.class})
    public void updatePartner(Partner partner) throws UtilException {

        try {
            partnerMapper.updateByPrimaryKey(partner);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw new UtilException("修改时异常情况发生请检查");
        }
    }

    /**
     * 根据id查询
     *
     * @param id
     * @return
     * @throws UtilException
     */
    public Partner selectPartnerByid(long id) throws UtilException {

        Partner partner;
        try {
            partner = partnerMapper.selectByPrimaryKey(id);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw new UtilException("查询异常");
        }

        if (partner != null) {
            return partner;
        } else {
            return null;
        }
    }

    /**
     * 查询合作伙伴带分页
     *
     * @param pageNo
     * @param pageSize
     * @return
     * @throws UtilException
     */
    public PageInfo selectAll(int pageNo, int pageSize, String state)
            throws UtilException {
        PageHelper.startPage(pageNo, pageSize);
        List<Partner> list;
        try {
            list = this.partnerMapper.selectAll(state);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw new UtilException("查询时发生异常");
        }
        PageInfo page = new PageInfo(list);
        return page;
    }
}
