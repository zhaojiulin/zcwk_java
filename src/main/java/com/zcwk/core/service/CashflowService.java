package com.zcwk.core.service;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zcwk.core.dao.AccountMapper;
import com.zcwk.core.dao.CashflowMapper;
import com.zcwk.core.model.Account;
import com.zcwk.core.model.Cashflow;

import com.zcwk.core.util.*;

@Service
public class CashflowService {

    @Autowired
    CashflowMapper cashflowMapper;
    @Autowired
    AccountMapper accountMapper;

    public void add(long accountId, Enums.FundType type, BigDecimal amount,
                    long relationId, String memo) {
        Account account = this.accountMapper.selectByPrimaryKey(accountId);
        Cashflow cf = new Cashflow();
        cf.setAccountId(account.getId());
        cf.setAmount(amount);
        cf.setBalance(account.getBalance());
        cf.setFreeze(account.getFreeze());
        cf.setMemo(memo);
        cf.setRelationId(relationId);
        cf.setType((byte) type.getIndex());
        cf.setTime(new Date());
        this.cashflowMapper.insertSelective(cf);
    }

}
