package com.zcwk.core.service;

import java.util.HashMap;
import java.util.Map;

import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.timevale.esign.sdk.tech.bean.OrganizeBean;
import com.timevale.esign.sdk.tech.bean.PersonBean;
import com.timevale.esign.sdk.tech.bean.PosBean;
import com.timevale.esign.sdk.tech.bean.result.AddAccountResult;
import com.timevale.esign.sdk.tech.bean.result.AddSealResult;
import com.timevale.esign.sdk.tech.bean.result.LoginResult;
import com.timevale.esign.sdk.tech.bean.result.Result;
import com.timevale.esign.sdk.tech.bean.seal.OrganizeTemplateType;
import com.timevale.esign.sdk.tech.bean.seal.PersonTemplateType;
import com.timevale.esign.sdk.tech.bean.seal.SealColor;
import com.timevale.esign.sdk.tech.impl.constants.OrganRegType;
import com.timevale.esign.sdk.tech.impl.constants.SignType;
import com.timevale.esign.sdk.tech.service.EsignsdkService;
import com.timevale.esign.sdk.tech.service.SealService;
import com.timevale.esign.sdk.tech.service.SignService;
import com.timevale.esign.sdk.tech.service.factory.AccountServiceFactory;
import com.timevale.esign.sdk.tech.service.factory.EsignsdkServiceFactory;
import com.timevale.esign.sdk.tech.service.factory.SealServiceFactory;
import com.timevale.esign.sdk.tech.service.factory.SignServiceFactory;
import com.zcwk.core.dao.UserMapper;
import com.zcwk.core.model.User;
import com.zcwk.core.util.CommonUtil;
import com.zcwk.core.util.Enums;
import com.zcwk.core.util.UtilCache;
import com.zcwk.core.util.UtilException;
import com.zcwk.core.util.Enums.UserNature;

/**
 * E签宝
 * @author zp
 * @since 2016年12月30日
 */
@Service
public class ESignService {
    
	private static final Logger	LOGGER	= Logger.getLogger(ESignService.class);
	
    private com.timevale.esign.sdk.tech.service.AccountService eSignAccountService = AccountServiceFactory.instance();
    private SealService sealService = SealServiceFactory.instance();
    private SignService signService = SignServiceFactory.instance();
    private EsignsdkService sdkService = EsignsdkServiceFactory.instance();
    
    @Autowired
    UtilCache utilCache;
    @Autowired
    UserMapper userMapper;
    @Autowired
    SystemSetService systemSetService;
    
    private String devId;//开发者账户id（项目登录接口返回）
    private String projectId;//e签宝分配给接入系统的项目编号
    private String projectSecret;//e签宝分配给接入系统的项目编号对应的校验码

    /**
     * E签宝签署-用户+平台
     * @param borrowUser 借款用户
     * @param investor	投资用户
     * @param srcPdfFile pdf文件地址
     * @throws UtilException
     */
    public void doESignPdf(User borrowUser,User investor,String srcPdfFile) throws UtilException{
    	Object o = this.systemSetService.getParamVal(Enums.Params.ESign.getKey());
    	if(o == null){
//    		System.out.println("E签宝电子签章未开启.........");
    		return;
    	}
    	Map<String, String> config = CommonUtil.parseMap(o.toString());
    	//E签宝-初始化-项目账户登录
    	//devId可放入redis中，此处先获取，若为空，则需初始化eSignDevId
    	this.devId = this.utilCache.getCode("eSignDevId");
    	if(StringUtils.isEmpty(this.devId)){
    		this.projectId = config.get("projectId");
    		this.projectSecret = config.get("projectSecret");
    		this.devId = this.eSignInit();
    		this.utilCache.setDevId("eSignDevId", this.devId, 60 * 60 * 5);//登录保存状态，测试服务器保存较短，上线时再开放
    	}
    	//e签宝账户开户及创建印章
    	String accountId = borrowUser.geteSignAccountId();
    	String sealData = borrowUser.geteSignSealData();
    	if(StringUtils.isEmpty(accountId)){
    		if(borrowUser.getUserNature() == (byte) UserNature.Organize.getIndex() && (StringUtils.isEmpty(borrowUser.getMobile()) || StringUtils.isEmpty(borrowUser.getIdCard()) || StringUtils.isEmpty(borrowUser.getLegalName()) || StringUtils.isEmpty(borrowUser.getLegalIdNo()))){
    			System.out.println(borrowUser.getRealName() + "创建e签宝印章失败，用户信息不全！==========PDF==========" + srcPdfFile);
    			throw new UtilException(borrowUser.getRealName() + "创建e签宝印章失败，用户信息不全！==========PDF==========" + srcPdfFile);
    		}
    		
    		Map<String,Object> map = null;
    		if(borrowUser.getUserNature() == (byte)Enums.UserNature.Organize.getIndex()){
    			//企业
    			map = this.addAndCreateESignSealOrganize(borrowUser.getEmail(), borrowUser.getMobile(), borrowUser.getRealName(), 
    					0, "", borrowUser.getIdCard(), borrowUser.getLegalName(), borrowUser.getLegalIdNo(), 0, "", "", 1, "", "");
    		}else if(borrowUser.getUserNature() == (byte)Enums.UserNature.Persion.getIndex()){
    			//个人
    			map = this.addAndCreateESignSealPersion(borrowUser.getEmail(), borrowUser.getMobile(), borrowUser.getRealName(), 
    					borrowUser.getIdCard(), 0, "", "", "");
    		}
    		accountId = map.get("eSignAccountId").toString();
    		sealData = map.get("eSignSealData").toString();
    		User user = new User();
    		user.setId(borrowUser.getId());
    		user.seteSignAccountId(accountId);
    		user.seteSignSealData(sealData);
    		this.userMapper.updateByPrimaryKeySelective(user);//保存印章数据
    	}
    	//摘要签署
    	this.eSignSign(accountId, sealData, srcPdfFile, srcPdfFile, config.get("borrowUserKey"), config.get("selfKey"));
    }
    
    /**
     * E签宝-初始化-项目账户登录
     * @throws UtilException 
     */
    @SuppressWarnings("deprecation")
	public String eSignInit() throws UtilException{
    	Result result = sdkService.init(this.projectId, this.projectSecret);
    	System.out.println("E签宝-初始化,返回数据：" + JSONObject.fromObject(result));
    	if(result.getErrCode() == 0){
    		System.out.println("========================E签宝-初始化成功========================");
    		
    		LoginResult loginResult = sdkService.login();
    		System.out.println("E签宝-项目账户登录,返回数据：" + JSONObject.fromObject(loginResult));
    		if(loginResult.getErrCode() == 0){
    			System.out.println("========================E签宝-项目账户登录成功========================");
    			return loginResult.getAccountId();
    		}else {
    			System.out.println("========================E签宝-项目账户登录失败，" + loginResult.getMsg());
    			throw new UtilException("========================E签宝-项目账户登录失败，" + loginResult.getMsg());
			}
    	}else {
    		System.out.println("========================E签宝-初始化失败，" + result.getMsg());
    		throw new UtilException("========================E签宝-初始化失败，" + result.getMsg());
		}
    }
    
    /**
     * e签宝企业个人开户及创建印章
     * @param email 邮箱地址(可空)
     * @param mobile 手机号码(不可空)
     * @param name	姓名(不可空)
     * @param idNo	身份证号(不可空)
     * @param personArea 用户归属地(可空)默认0
     * @param organ 所属公司(可空)
     * @param title 职位(可空)
     * @param address 常用地址(可空)
     * @return String 创建后账号在e签宝平台中的唯一标识
     * @throws UtilException
     */
    public Map<String,Object> addAndCreateESignSealPersion(String email,String mobile,String name,String idNo,
    		int personArea,String organ,String title,String address) throws UtilException{
    	//添加e签宝个人账户
    	String accountId = this.addESignPersion(email, mobile, name, idNo, personArea, organ, title, address);
    	//创建e签宝个人印章
    	String sealData = this.createEsignSealPerson(accountId);
    	
    	Map<String,Object> map = new HashMap<String,Object>();
    	map.put("eSignAccountId", accountId);
    	map.put("eSignSealData", sealData);
    	return map;
    }
    
    /**
     * e签宝企业账户开户及创建印章
     * @param email 邮箱地址(可空)
     * @param mobile 手机号码(不可空)
     * @param name	机构名称(不可空)
     * @param organType	单位类型，0-普通企业，1-社会团体，2-事业单位，3-民办非企业单位，4-党政及国家机构，默认0(可空)
     * @param regCode 工商注册号(可空)
     * @param organCode 组织机构代码号或社会信用代码号(不可空)
     * @param legalName 法定代表姓名，当注册类型为2时必填(可空)
     * @param legalIdNo 法定代表身份证号/护照号，当注册类型为2时必填(可空)
     * @param legalArea 法定代表人归属地,0-大陆，1-香港，2-澳门，3-台湾，4-外籍，默认0(可空)
     * @param address 公司地址(可空)
     * @param scope 经营范围(可空)
     * @param regType 企业注册类型，0:组织机构代码号;1:多证合一，默认组织机构代码号(可空)
     * @param hText 生成印章中的横向文内容(可空)
     * @param qText 生成印章中的下弦文内容(可空)
     * @return map :  "eSignAccountId":e签宝账号  "eSignSealData":e签宝企业印章数据
     * @throws UtilException 
     */
    public Map<String,Object> addAndCreateESignSealOrganize(String email,String mobile,String name,Integer organType,
    		String regCode,String organCode,String legalName,String legalIdNo,Integer legalArea,String address,
    		String scope,Integer regType,String hText,String qText) throws UtilException{
    	
    	//添加e签宝企业账户
    	String accountId = this.addESignOrganize(email, mobile, name, organType, regCode, organCode, legalName, legalIdNo, legalArea, address, scope, regType);
    	//创建e签宝企业印章
    	String sealData = this.createESignSealOrganize(accountId, hText, qText);
    	
    	Map<String,Object> map = new HashMap<String,Object>();
    	map.put("eSignAccountId", accountId);
    	map.put("eSignSealData", sealData);
		return map;
    	
    }
    
    /**
     * E签宝-摘要签署
     * @param accountId e签宝平台中的唯一标识(不可空)
     * @param sealData 电子印章数据(不可空)
     * @param srcPdfFile 待签署文档本地路径，含文档名(不可空)
     * @param dstPdfFile 签署后文档本地路径，含文档名(不可空)
     * @param userKey 定位关键字-用户(不可空)
     * @param selfKey 定位关键字-平台(不可空)
     * @throws UtilException 
     */
    public void eSignSign(String accountId,String sealData,String srcPdfFile,String dstPdfFile,String userKey,String selfKey) throws UtilException{
    	//用户签署
    	this.eSignUserSign(accountId, sealData, srcPdfFile, dstPdfFile, userKey);
    	//平台签署
    	this.eSignSelfSign(srcPdfFile, dstPdfFile, 0, selfKey);
    }
    
    /**
     * 添加e签宝个人账户
     * @param email 邮箱地址(可空)
     * @param mobile 手机号码(不可空)
     * @param name	姓名(不可空)
     * @param idNo	身份证号(不可空)
     * @param personArea 用户归属地(可空)默认0
     * @param organ 所属公司(可空)
     * @param title 职位(可空)
     * @param address 常用地址(可空)
     * @return String 创建后账号在e签宝平台中的唯一标识
     * @throws UtilException
     */
    public String addESignPersion(String email,String mobile,String name,String idNo,
    		int personArea,String organ,String title,String address) throws UtilException{
    	//个人信息详情
    	PersonBean psn = new PersonBean();
    	psn.setMobile(mobile).setEmail(email).setName(name).setIdNo(idNo).setPersonArea(personArea);
    	System.out.println("========================添加e签宝个人账户,请求数据：" + JSONObject.fromObject(psn));
    	AddAccountResult r = eSignAccountService.addAccount(devId, psn);
    	System.out.println("========================添加e签宝个人账户,返回数据：" + JSONObject.fromObject(r));
    	if(r.getErrCode() == 0){
    		System.out.println("========================添加e签宝个人账户成功========================" + mobile);
    		return r.getAccountId();
    	}else {
    		System.out.println("========================创建个人用户："+mobile+"E签宝签章失败：" + r.getMsg());
    		throw new UtilException("========================创建个人用户："+mobile+"E签宝签章失败：" + r.getMsg());
		}
    }
    
    /**
     * 添加e签宝企业账户
     * @param email 邮箱地址(可空)
     * @param mobile 手机号码(不可空)
     * @param name	机构名称(不可空)
     * @param organType	单位类型，0-普通企业，1-社会团体，2-事业单位，3-民办非企业单位，4-党政及国家机构，默认0(可空)
     * @param regCode 工商注册号(可空)
     * @param organCode 组织机构代码号或社会信用代码号(不可空)
     * @param legalName 法定代表姓名，当注册类型为2时必填(可空)
     * @param legalIdNo 法定代表身份证号/护照号，当注册类型为2时必填(可空)
     * @param legalArea 法定代表人归属地,0-大陆，1-香港，2-澳门，3-台湾，4-外籍，默认0(可空)
     * @param address 公司地址(可空)
     * @param scope 经营范围(可空)
     * @param regType 企业注册类型，0:组织机构代码号;1:多证合一，默认组织机构代码号(可空)
     * @return String 创建后账号在e签宝平台中的唯一标识
     * @throws UtilException 
     */
    public String addESignOrganize(String email,String mobile,String name,Integer organType,
    		String regCode,String organCode,String legalName,String legalIdNo,Integer legalArea,String address,
    		String scope,Integer regType) throws UtilException {
    	//企业信息详情
    	OrganizeBean org = new OrganizeBean();
    	org.setEmail(email);
    	org.setMobile(mobile);
    	org.setName(name);
    	org.setOrganType(organType);
    	org.setUserType(2);//注册类型：2：法人注册；1：代理人注册;默认2
    	org.setRegCode(regCode);
    	org.setOrganCode(organCode);
    	org.setLegalName(legalName);
    	org.setLegalIdNo(legalIdNo);
    	org.setLegalArea(legalArea);
    	org.setAddress(address);
    	org.setScope(scope);
    	OrganRegType organRegType = null;//企业注册类型：0：组织机构代码号；1：多证合一（默认）
        if (regType == 0) {
            organRegType = OrganRegType.NORMAL;
        } else if (regType == 1) {
            organRegType = OrganRegType.MERGE;
        }
    	org.setRegType(organRegType);
    	System.out.println("========================添加e签宝企业账户,请求数据：" + JSONObject.fromObject(org));
    	AddAccountResult r = eSignAccountService.addAccount(devId, org);
    	System.out.println("========================添加e签宝企业账户,返回数据：" + JSONObject.fromObject(r));
    	if(r.getErrCode() == 0){
    		System.out.println("========================添加e签宝企业账户成功========================" + mobile);
    		return r.getAccountId();
    	}else {
    		System.out.println("========================创建用户："+mobile+"E签宝签章失败：" + r.getMsg());
    		throw new UtilException("========================创建用户："+mobile+"E签宝签章失败：" + r.getMsg());
		}
    }
    
    /**
     * 创建e签宝个人印章
     * @param accountId 待创建印章的账户标识(不可空)
     * @return String 最终生成的电子印章数据
     * @throws UtilException
     */
    public String createEsignSealPerson(String accountId)  throws UtilException{
    	AddSealResult r = sealService.addTemplateSeal(devId, accountId, PersonTemplateType.SQUARE, SealColor.RED);
    	System.out.println("========================创建e签宝个人印章,返回数据：" + JSONObject.fromObject(r));
    	if(r.getErrCode() == 0){
    		System.out.println("========================创建e签宝个人印章成功========================" + accountId);
    		return r.getSealData();
    	}else {
    		System.out.println("========================创建个人用户："+accountId+"E签宝签章失败：" + r.getMsg());
    		throw new UtilException("========================创建个人用户："+accountId+"E签宝签章失败：" + r.getMsg());
		}
    }
    
    /**
     * 创建e签宝企业印章
     * @param accountId 待创建印章的账户标识(不可空)
     * @param hText 生成印章中的横向文内容(可空)
     * @param qText 生成印章中的下弦文内容(可空)
     * @return String 最终生成的电子印章数据
     * @throws UtilException 
     */
    public String createESignSealOrganize(String accountId,String hText,String qText) throws UtilException{
//    	System.out.println("========================创建e签宝企业印章,请求数据：" + accountId + "======" + hText + "======" + qText + "======" + OrganizeTemplateType.STAR + "======" +SealColor.RED);
    	AddSealResult r = sealService.addTemplateSeal(devId, accountId, OrganizeTemplateType.STAR, SealColor.RED, hText, qText); 
    	System.out.println("========================创建e签宝企业印章,返回数据：" + JSONObject.fromObject(r));
    	if(r.getErrCode() == 0){
    		System.out.println("========================创建e签宝企业印章成功========================" + accountId);
    		return r.getSealData();
    	}else {
    		System.out.println("========================创建用户："+accountId+"E签宝签章失败：" + r.getMsg());
    		throw new UtilException("========================创建用户："+accountId+"E签宝签章失败：" + r.getMsg());
		}
    }
    
    /**
     * E签宝-平台用户摘要签署
     * @param accountId e签宝平台中的唯一标识(不可空)
     * @param sealData 电子印章数据(不可空)
     * @param srcPdfFile 待签署文档本地路径，含文档名(不可空)
     * @param dstPdfFile 签署后文档本地路径，含文档名(不可空)
     * @param key 定位关键字(不可空)
     * @throws UtilException 
     */
    public Boolean eSignUserSign(String accountId,String sealData,String srcPdfFile,String dstPdfFile,String key) throws UtilException{
    	//签章位置信息
    	PosBean posBean = new PosBean();
    	posBean.setPosType(1);//默认关键字定位
    	posBean.setKey(key);//关键字
    	
    	Result r = signService.localSignPDF(devId, accountId, sealData, srcPdfFile, dstPdfFile, posBean, SignType.Key, "");
    	System.out.println("========================E签宝平台用户摘要签署,返回数据：" + JSONObject.fromObject(r));
    	if(r.getErrCode() == 0){
    		System.out.println("========================E签宝平台用户摘要签署成功========================" + dstPdfFile);
    		return true;
    	}else {
    		System.out.println("========================E签宝-平台用户摘要签署失败：" + r.getMsg());
    		throw new UtilException("========================E签宝-平台用户摘要签署失败：" + r.getMsg());
		}
    	
    }
    
    /**
     * E签宝-平台自身摘要签署
     * @param srcPdfFile 待签署文档本地路径，含文档名(不可空)
     * @param dstPdfFile 签署后文档本地路径，含文档名(不可空)
     * @param sealId 签署印章的标识，为0表示用默认印章签署，默认0(可空)
     * @param key 定位关键字(不可空)
     * @return
     * @throws UtilException 
     */
    public Boolean  eSignSelfSign(String srcPdfFile,String dstPdfFile,Integer sealId,String key) throws UtilException{
    	//签章位置信息
    	PosBean posBean = new PosBean();
    	posBean.setPosType(1);//默认关键字定位
    	posBean.setKey(key);//关键字
    	
    	
    	Result r = signService.localSignPDF(devId, srcPdfFile, dstPdfFile, posBean, sealId, SignType.Key, "");
    	System.out.println("========================E签宝-平台自身摘要签署,返回数据：" + JSONObject.fromObject(r));
    	if(r.getErrCode() == 0){
    		System.out.println("========================E签宝-平台自身摘要签署成功========================" + dstPdfFile);
    		return true;
    	}else {
    		System.out.println("========================E签宝-平台自身摘要签署失败：" + r.getMsg());
    		throw new UtilException("========================E签宝-平台自身摘要签署失败：" + r.getMsg());
		}
    }
    
    public static void main(String[] args) throws UtilException {
    	ESignService service = new ESignService();
    	service.devId = "1213";
    	service.projectId = "1111563517";
    	service.projectSecret = "95439b0863c241c63a861b87d1e647b7";
    	service.eSignInit();
    	Map<String,Object> map = null;
    	int userNature = 1;
    	if(userNature == 0){
    		map = service.addAndCreateESignSealPersion("", "13333333353", "个人测试", "360730198902261416", 0, "", "", "");
    	}else if(userNature == 1){
    		map = service.addAndCreateESignSealOrganize("", "15555555555", "测试测试abc", 0, "", "12330109574384023E", "丁韩瑶", "530000198010199130", 0, "", "", 1, "横向文内容", "下弦文内容");
    	}
    	service.eSignSign(map.get("eSignAccountId").toString(), map.get("eSignSealData").toString(), "E:/eSignTest.pdf", "E:/eSignTest.pdf", "借款方（签署）：", "居间方（签署）：");
	}
    
}
