package com.zcwk.core.service;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.zcwk.core.dao.*;
import net.rubyeye.xmemcached.MemcachedClient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSON;
import com.lowagie.text.DocumentException;
import com.zcwk.core.dao.AccountMapper;
import com.zcwk.core.dao.AutoInvestMapper;
import com.zcwk.core.dao.BillDebtMapper;
import com.zcwk.core.dao.DebtMapper;
import com.zcwk.core.dao.InvestMapper;
import com.zcwk.core.dao.TransferMapper;
import com.zcwk.core.model.Account;
import com.zcwk.core.model.AutoInvest;
import com.zcwk.core.model.BillDebt;
import com.zcwk.core.model.Debt;
import com.zcwk.core.model.Invest;
import com.zcwk.core.model.PlatformInfo;
import com.zcwk.core.model.Transfer;
import com.zcwk.core.model.User;
import com.zcwk.core.queue.ActionFactory;
import com.zcwk.core.queue.ActionInterface;
import com.zcwk.core.util.CommonUtil;
import com.zcwk.core.util.DateUtil;
import com.zcwk.core.util.Enums;
import com.zcwk.core.util.MD5Util;
import com.zcwk.core.util.PdfUtil;
import com.zcwk.core.util.UtilCache;
import com.zcwk.core.util.UtilException;

@Service
public class TaskService {
    @Autowired
    BillDebtMapper billDebtMapper;
    @Autowired
    UtilCache cache;
    @Autowired
    DebtMapper debtMapper;
    @Autowired
    TransferMapper transferMapper;
    @Autowired
    InvestService investService;
    @Autowired
    InvestMapper investMapper;
    @Autowired
    FreezeService freezeService;
    @Autowired
    DebtService debtService;
    @Autowired
    AutoInvestMapper autoInvestMapper;
    @Autowired
    AccountMapper accountMapper;
    @Autowired
    InviteService inviteService;
    @Autowired
    PdfUtil pdfUtil;
    @Autowired
    PlatformService platformService;
    @Autowired
    SystemSetService systemSetService;
    @Autowired
    TransferService transferService;
    @Autowired
    @Qualifier("mqClient")
    MemcachedClient mq;
    @Autowired
    ESignService eSignService;
   @Autowired
    UserMapper userMapper;
    @Autowired
    QueueService queueService;
    /*
    *1分钟一次自动投标
     */
    @Scheduled(fixedRate = 60000)
    @Transactional(rollbackFor = {Exception.class})
    public void autoInvest() {
        Debt debt = this.debtMapper.selectAutoDebt((byte) Enums.DebtState.ApprovalPass.getIndex());
        if (debt != null) {
            Debt updateDebt = new Debt();
            updateDebt.setId(debt.getId());
            updateDebt.setLastAutoTime(new Date());
            this.debtMapper.updateByPrimaryKeySelective(updateDebt);
            HashMap map = new HashMap();
            BigDecimal debtAmount = debt.getAmount().subtract(debt.getInvestedAmount());
            while (debtAmount.compareTo(debt.getMinInvestAmount()) > 0) {
                AutoInvest auto = this.autoInvestMapper.selectByLastTime(debt);
                if (auto == null || map.containsKey(auto.getAccountId()))
                    return;
                map.put(auto.getAccountId(), auto);
                Account account = this.accountMapper.selectByPrimaryKey(auto.getAccountId());
                BigDecimal buyAmount = account.getBalance().subtract(auto.getMinBalance());
                if (buyAmount.compareTo(auto.getMaxBuy()) > 0)
                    buyAmount = auto.getMaxBuy();
                if (buyAmount.compareTo(account.getBalance()) > 0)
                    buyAmount = account.getBalance();
                if (buyAmount.compareTo(debt.getMaxInvestAmount()) > 0)
                    buyAmount = debt.getMaxInvestAmount();
                if (buyAmount.compareTo(auto.getMinBuy()) >= 0) {
                    try {
                        this.debtService.buy(debt.getId(), buyAmount.setScale(0, BigDecimal.ROUND_DOWN), account.getId(), true, "", "",5);
                        debt = this.debtMapper.selectByPrimaryKey(debt.getId());
                        debtAmount = debt.getAmount().subtract(debt.getInvestedAmount());
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                        return;
                    }
                }
                this.autoInvestMapper.updateLastTime(auto.getAccountId());
            }
        }
    }

//    /*
//   *1分钟一次自动满标
//    */
//    @Scheduled(fixedRate = 60000)
//    @Transactional(rollbackFor = {Exception.class})
//    public void checkFullTransfer() throws UtilException, IOException, DocumentException {
//        List<Transfer> transfers = this.transferMapper.selectFullTransfer((byte) Enums.DebtState.ApprovalPass.getIndex());
//        List<Invest> list = new ArrayList<Invest>();
//        Transfer fullTransfer = new Transfer();
//        for (Transfer transfer : transfers) {
//            fullTransfer.setState((byte) Enums.DebtState.InPayment.getIndex());
//            fullTransfer.setId(transfer.getId());
//            int updateState = this.transferMapper.updateByPrimaryKeySelective(fullTransfer);
//            if (updateState < 1)
//                throw new UtilException("自动满标审核失败");
//            list = this.investMapper.selectByTransfer(transfer.getId(), false);
//            this.freezeService.thawBuy(list); // 解冻投标金额
//            this.transferService.createBill(transfer, list);// 生成回款计划
//            this.transferService.sendInvestAward(list, transfer); //发送投资奖励
//            //发放续投奖励
//            this.debtService.sendPieceAward(list);
//            //发放邀请奖励
//            this.inviteService.sendInviteAward(list);
//        }
//    }

//    /*
//    *检查过期债转
//     */
//    //@Scheduled(cron = "0 0 0 * * ?")
//    @Scheduled(fixedRate = 3600000)
//    @Transactional(rollbackFor = {Exception.class})
//    public void checkCutTransfer() throws UtilException {
//        Date dt = new Date();
//        List<Transfer> transfers = this.transferMapper.selectCutTransfer(dt, (byte) Enums.DebtState.ApprovalPass.getIndex());
//        for (Transfer transfer : transfers) {
//            this.transferService.cancelTransfer(transfer);
//        }
//    }

    /*
    *1分钟一次自动满标
     */
    @Scheduled(fixedRate = 60000)
    @Transactional(rollbackFor = {Exception.class})
    public void checkFullDebt() throws UtilException, IOException, DocumentException {
        Date dt = new Date();
        List<Debt> debts = this.debtMapper.selectFullDebt(dt, (byte) Enums.DebtState.ApprovalPass.getIndex());
        List<Invest> list = new ArrayList<Invest>();
        for (Debt debt : debts) {
            Debt fullDebt = new Debt();
            fullDebt.setState((byte) Enums.DebtState.InPayment.getIndex());
            fullDebt.setFullTime(new Date());
            fullDebt.setId(debt.getId());
            if (debt.getCutTime() != null && dt.compareTo(debt.getCutTime()) > 0 )
                fullDebt.setIsCut(true);
            int updateState = this.debtMapper.updateByPrimaryKeySelective(fullDebt);
            if (updateState < 1)
                throw new UtilException("自动满标审核失败");
            list = this.investService.getListByDebtId(debt.getId());
            // 生成满标回款计划
            if (debt.getInterestType() == Enums.InterestType.Full.getIndex()) {
                this.freezeService.thawBuy(list); // 解冻投标金额
                this.debtService.createBill(debt, list);// 生成回款计划
            }

            //搜索编号：1489113307
            if ((debt.getAmount()).compareTo(debt.getInvestedAmount())==0){
                Invest invest = this.investMapper.selectLatest(debt.getId());
                if(invest!=null){
                    User user = this.userMapper.selectByAccountId(invest.getAccountId());
                    this.queueService.insertQueue(Enums.Actions.FullDebtAward.getIndex(), user.getId(), null, BigDecimal.ZERO);
                }
            }
            //System.out.println("开始执行：" + CommonUtil.getFormatTime(new Date(), "yyyy-MM-dd hh:mm:ss"));
            //生成借款协议
            //this.CreateProtocol(list);
            //System.out.println("结束执行：" + CommonUtil.getFormatTime(new Date(), "yyyy-MM-dd hh:mm:ss"));
            // 发放投资奖励
            this.debtService.sendInvestAward(list);
            //发放续投奖励
            this.debtService.sendPieceAward(list);
            //发放邀请奖励
            this.inviteService.sendInviteAward(list);

            this.cache.delDebt(debt.getId());
        }
    }


    /*
    *每天凌晨检查逾期标
     */
    //@Scheduled(cron = "0 0 0 * * ?")
    @Scheduled(fixedRate = 3600000*2)
    @Transactional(rollbackFor = {Exception.class})
    public void checkOverdueDebt() {
        List<BillDebt> list = this.billDebtMapper.selectOverdueDebt((byte) Enums.State.Wait.getIndex());
        if (list.size() > 0) {
            Debt debt = null;
            Debt updateDebt = new Debt();
            BillDebt billDebt = new BillDebt();
            int day = 0;
            BigDecimal overdueFine = BigDecimal.ZERO;
            HashMap map = new HashMap();
            for (BillDebt bill : list) {
                debt = this.debtMapper.selectByPrimaryKey(bill.getDebtId());
                day = DateUtil.daysBetween(bill.getPaymentTime(), DateUtil.getNow());
                if (day > 0 && debt != null && debt.getOverdueRate().compareTo(BigDecimal.ZERO) > 0) {
                    billDebt.setId(bill.getId());
                    billDebt.setIsOverdue(true);
                    billDebt.setOverdueDay((byte) day);
                    overdueFine = ((bill.getPaymentCorpus().add(bill.getPaymentInterest())).multiply(debt.getOverdueRate()).multiply(BigDecimal.valueOf(day))).setScale(2, BigDecimal.ROUND_HALF_UP);
                    billDebt.setOverdueFine(overdueFine);
                    this.billDebtMapper.updateByPrimaryKeySelective(billDebt);
                    if (!map.containsKey(debt.getId()))
                        map.put(debt.getId(), debt);
                }
            }
            for (Object obj : map.values()) {
                Debt dt = (Debt) obj;
                if (dt.getState() != (byte) Enums.DebtState.Overdue.getIndex()) {
                    updateDebt.setId(dt.getId());
                    updateDebt.setState((byte) Enums.DebtState.Overdue.getIndex());
                    this.debtMapper.updateByPrimaryKeySelective(updateDebt);
                    this.cache.delDebt(dt.getId());
                }
            }
        }
    }


    @Scheduled(fixedRate = 100)
    @Transactional(rollbackFor = {Exception.class})
    public void receiveMessages() throws Exception {
        String actionStr = this.mq.get("Actions");
        if (actionStr != null) {
            System.out.println("receive queue:" + actionStr);
            HashMap map = JSON.parseObject(actionStr, HashMap.class);
            ActionInterface action = ActionFactory.create((int) map.get("action"));
            if (action != null)
                action.exec(map);
        }
    }

    //生成借款协议
    @Scheduled(fixedRate = 60000)
    //@Transactional(rollbackFor = {Exception.class})
    public void createProtocol() throws IOException, DocumentException, UtilException {
        List<Invest> list = this.investMapper.selectNoProtocol();
        Debt debt = null;
        User borrow = null;
        User investor = null;
        String protocol = null;
        for (Invest invest : list) {
            PlatformInfo platformInfo = this.cache.getPlatformInfo();
            Object _baseUrl = this.systemSetService.getParamVal(Enums.Params.Base.getKey());
            Invest upInvest = new Invest();
            debt = this.cache.getDebt(invest.getDebtId());
            if (debt != null && (debt.getInterestType() == Enums.InterestType.Now.getIndex() || (debt.getInterestType() == Enums.InterestType.Full.getIndex() && debt.getState() == Enums.DebtState.InPayment.getIndex()))) {
                Account borrowAccount = this.cache.getAccount(debt.getAccountId());
                Account investorAccount = this.cache.getAccount(invest.getAccountId());
                if (borrowAccount != null && investorAccount != null) {
                    long borrowId = borrowAccount.getUserId();
                    long investorId = investorAccount.getUserId();
                    borrow = this.cache.getUser(borrowId);
                    investor = this.cache.getUser(investorId);
                    List<BillDebt> bills = this.billDebtMapper.selectByInvestId(invest.getId(), "");
                    if (!invest.getIsTransfer())
                        protocol = this.cache.getProtocol(Enums.ProtocolType.Debt.getIndex());
                    if (debt != null && borrow != null && protocol != null && investor != null) {
                        String proName = MD5Util.MD5(invest.getAccountId() + "" + invest.getId());
                        String debtTitleStr = "{debtTitle}";
                        String paymentTypeStr = "{paymentType}";
                        String borrowNameStr = "{borrowName}";
                        String borrowIdCardStr = "{borrowIdCard}";
                        String investorNameStr = "{investorName}";
                        String investorIdCardStr = "{investorIdCard}";
                        String periodStr = "{period}";
                        String rateStr = "{rate}";
                        String amountStr = "{amount}";
                        String platformNameStr = "{platformName}";
                        String addressStr = "{address}";
                        String baseUrlStr = "{baseUrl}";
                        String timeStr = "{time}";
                        String totalPaymentStr = "{totalPayment}";
                        String beginPaymentListStr = "{paymentList}";
                        String endPaymentListStr = "{/paymentList}";
                        String paymentTimeStr = "{paymentTime}";
                        String paymentAmountStr = "{paymentAmount}";
                        String investorPhoneStr = "{investorPhone}";
                        String borrowPhoneStr = "{borrowPhone}";

                        String debtAmount="{debtAmount}";//项目金额
                        String buyDate="{buyDate}";//购买日期
                        String lastPaymentDate="{lastPaymentDate}";//最后还款时间
                        String collateral="{collateral}";//抵押物
                        String lastPaymentDateTemp="";

                        String paymentTmp ="";
                        if(protocol.indexOf(beginPaymentListStr)>-1 &&  protocol.indexOf(endPaymentListStr)>-1){
                            paymentTmp=protocol.substring(protocol.indexOf(beginPaymentListStr) + beginPaymentListStr.length(), protocol.indexOf(endPaymentListStr));
                        }
                        StringBuilder payment = new StringBuilder();
                        BigDecimal totalPayment = BigDecimal.ZERO;
                        BigDecimal sumAmount = BigDecimal.ZERO;
                        for (BillDebt bill : bills) {
                            sumAmount = bill.getPaymentCorpus().add(bill.getPaymentInterest());
                            payment.append(paymentTmp.replace(paymentTimeStr, CommonUtil.formatDate("yyyy-MM-dd", bill.getPaymentTime())).replace(paymentAmountStr, sumAmount.toString()));
                            totalPayment = totalPayment.add(sumAmount);
                            lastPaymentDateTemp=CommonUtil.formatDate("yyyy-MM-dd", bill.getPaymentTime());
                        }
                        String paymentType = null;
                        for (Enums.PaymentType t : Enums.PaymentType.values()) {
                            if ((byte) t.getIndex() == debt.getPaymentType()) {
                                paymentType = t.getName();
                            }
                        }
                        protocol = protocol.replace(borrowNameStr, borrow.getRealName())
                                .replace(borrowIdCardStr, borrow.getIdCard())
                                .replace(borrowPhoneStr,borrow.getMobile())
                                .replace(investorNameStr, investor.getRealName())
                                .replace(investorIdCardStr, investor.getIdCard())
                                .replace(investorPhoneStr,investor.getMobile())
                                .replace(periodStr, debt.getPeriod() + (debt.getPeriodUnit() == Enums.PeriodUnitType.Month.getIndex() ? "个月" : "天"))
                                .replace(rateStr, debt.getRate().multiply(BigDecimal.valueOf(100)).setScale(2).toString())
                                .replace(amountStr, invest.getAmount().toString())
                                .replace(platformNameStr, platformInfo.getPlatformName())
                                .replace(addressStr, platformInfo.getAddress())
                                .replace(baseUrlStr, _baseUrl == null ? "" : _baseUrl.toString())
                                .replace(timeStr, CommonUtil.formatDate("yyyy-MM-dd", new Date()))
                                .replace(totalPaymentStr, totalPayment.toString())
                                .replace(debtTitleStr, debt.getTitle())
                                .replace(collateral,(debt.getCollateral()!=null?debt.getCollateral():"") )
                                .replace(debtAmount, debt.getAmount().toString())
                                .replace(buyDate,CommonUtil.formatDate("yyyy-MM-dd", invest.getTime()))
                                .replace(lastPaymentDate,lastPaymentDateTemp)
                                .replace(paymentTypeStr, paymentType);
                        StringBuilder str = new StringBuilder(protocol);

                        if(protocol.indexOf(beginPaymentListStr)>-1 &&  protocol.indexOf(endPaymentListStr)>-1){
                            str.replace(protocol.indexOf(beginPaymentListStr), protocol.indexOf(endPaymentListStr) + endPaymentListStr.length(), payment.toString());
                        }
                        this.pdfUtil.create(proName, str.toString());
                        upInvest.setId(invest.getId());
                        upInvest.setProtocol(proName);
                        this.investMapper.updateByPrimaryKeySelective(upInvest);
                        //e签宝印章处理
                        String srcPdfFile = this.pdfUtil.getOutPath() + proName + ".pdf";
                        eSignService.doESignPdf(borrow,investor,srcPdfFile);
                    }
                }
            }
        }
    }

}