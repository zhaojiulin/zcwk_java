package com.zcwk.core.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zcwk.core.dao.WithdrawMapper;
import com.zcwk.core.model.Withdraw;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zcwk.core.dao.FreezeMapper;
import com.zcwk.core.model.Account;
import com.zcwk.core.model.Freeze;
import com.zcwk.core.model.Invest;
import com.zcwk.core.util.Enums;
import com.zcwk.core.util.UtilException;
import org.springframework.transaction.annotation.Transactional;

@Service
public class FreezeService {
    @Autowired
    FreezeMapper freezeMapper;
    @Autowired
    AccountService accountService;
    @Autowired
    WithdrawMapper withdrawMapper;

    @Transactional(rollbackFor = {Exception.class})
    public void save(long accountId, Enums.FundType type, long relationId,
                     BigDecimal amount) throws UtilException {
        Freeze freeze = new Freeze();
        freeze.setAccountId(accountId);
        freeze.setAmount(amount);
        freeze.setRelationId(relationId);
        freeze.setState((byte) Enums.State.Wait.getIndex());
        freeze.setType((byte) type.getIndex());
        freeze.setTime(new Date());
        this.freezeMapper.insertSelective(freeze);
        //Account acc = new Account();
        //acc.setId(accountId);
        //acc.setFreeze(amount);
        //this.accountService.updateBalance(acc, amount, type, relationId, type.getName() + "增加账户冻结金额");
    }

    // 解冻购买金额
    @Transactional(rollbackFor = {Exception.class})
    public void thawBuy(List<Invest> list)
            throws UtilException {
        Account account = new Account();
        for (Invest invest : list) {
            int result = this.freezeMapper.updateByRelationId(
                    (byte) Enums.FundType.Buy.getIndex(), invest.getId(),
                    (byte) Enums.State.Complete.getIndex());
            if (result > 0) {
                account.setId(invest.getAccountId());
                account.setFreeze(invest.getAmount().multiply(
                        BigDecimal.valueOf(-1)));
                this.accountService.updateBalance(account, invest.getAmount().multiply(
                        BigDecimal.valueOf(-1)), Enums.FundType.ThawBuyFreeze, invest.getId(), "解冻投标冻结金额");
            }
        }
    }

    //解冻提现金额
    public void thawWithdraw(long accountId, long withdrawId, int state) throws UtilException {
        this.freezeMapper.updateByRelationId(
                (byte) Enums.FundType.Withdraw.getIndex(), withdrawId,
                (byte) Enums.State.Complete.getIndex());
        Withdraw withdraw = this.withdrawMapper.selectByPrimaryKey(withdrawId);
        Account account = new Account();
        account.setId(accountId);
        account.setFreeze(withdraw.getAmount().multiply(BigDecimal.valueOf(-1)));
        this.accountService.updateBalance(account, withdraw.getAmount().multiply(BigDecimal.valueOf(-1)), Enums.FundType.ThawWithdrawFreeze, withdrawId, "解冻提现冻结金额");
        if (state == Enums.State.Cancel.getIndex()) {
            account = new Account();
            account.setId(accountId);
            account.setBalance(withdraw.getAmount());
            account.setPieceBalance(withdraw.getPieceAmount());
            this.accountService.updateBalance(account, withdraw.getAmount(), Enums.FundType.CancelWithdraw, withdrawId, "提现审核未通过，返还冻结金额");
        }
    }

    /**
     * 根据条件获冻结记录
     *
     * @return
     * @throws Exception
     */
    public PageInfo getFreezeByParam(int pageNo, int pageSize, String param, String bTime, String eTime, String accountId, String type) {
        PageHelper.startPage(pageNo, pageSize);
        List<HashMap> list = this.freezeMapper.selectByParam(accountId, bTime, eTime, param, type);
        PageInfo page = new PageInfo(list);
        return page;
    }
}
