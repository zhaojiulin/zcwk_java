package com.zcwk.core.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.zcwk.core.dao.UserLevelMapper;
import com.zcwk.core.model.UserLevel;
import com.zcwk.core.util.UtilException;

import java.util.Date;
import java.util.List;

/**
 * @author dengqun
 *         <p/>
 *         上午11:31:34
 */
@Service
public class UserLevelService {
    @Autowired
    UserLevelMapper userLevelMapper;

    /**
     * 获取所有用户等级
     *
     * @return
     * @throws Exception
     */
    public PageInfo getAll(int pageNo, int pageSize) {
        PageHelper.startPage(pageNo, pageSize);
        List list = this.userLevelMapper.selectLevelAll();
        PageInfo page = new PageInfo(list);
        return page;
    }

    /**
     * 根据ID获取用户等级
     *
     * @return
     * @throws Exception
     */
    public UserLevel getLevelById(long id) {
        return this.userLevelMapper.selectByPrimaryKey(id);
    }

    /**
     * 保存用户等级
     *
     * @return
     * @throws Exception
     */
    public void save(UserLevel level) {
        if (level.getId() > 0)
            this.userLevelMapper.updateByPrimaryKeySelective(level);
        else {
            level.setTime(new Date());
            this.userLevelMapper.insertSelective(level);
        }
    }
}
