package com.zcwk.core.service;

import java.math.BigDecimal;
import java.util.*;
import java.util.regex.Pattern;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zcwk.core.recharge.GatWayFactory;
import com.zcwk.core.recharge.GatWayInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zcwk.core.dao.*;
import com.zcwk.core.model.*;
import com.zcwk.core.util.*;

@Service
public class AccountService {
    @Autowired
    AccountMapper accountMapper;
    @Autowired
    UtilCache cache;
    @Autowired
    DataSafety dataSafety;
    @Autowired
    CashflowService cashflowService;
    @Autowired
    CashflowMapper cashflowMapper;
    @Autowired
    FreezeService freezeService;
    @Autowired
    WithdrawService withdrawService;
    @Autowired
    WithdrawMapper withdrawMapper;
    @Autowired
    RechargeService rechargeService;
    @Autowired
    RechargeMapper rechargeMapper;
    @Autowired
    UserMapper userMapper;
    @Autowired
    UserService userService;
    @Autowired
    BankCardMapper bankCardMapper;
    @Autowired
    BillDebtMapper billDebtMapper;
    @Autowired
    AutoInvestMapper autoInvestMapper;
    @Autowired
    SystemSetService systemSetService;
    @Autowired
    QueueService queueService;

    @SuppressWarnings("unused")
    @Transactional(rollbackFor = {Exception.class})
    public Account save(User user) throws UtilException {
        if (user.getId() == null)
            throw new UtilException("用户不能为空");
        Account account = this.accountMapper.selectByUserId(user.getId());
        if (account == null) {
            account = new Account();
            account.setUserId(user.getId());
            //检查是否增加体验金
            //Object val = this.systemSetService.getParamVal(Enums.Params.Experience.getKey());
            //if (val != null)
            //    account.setExperienceAmount(new BigDecimal(val.toString()));
            this.accountMapper.insertSelective(account);
            this.dataSafety.encryptAccount(account.getId());
        } else
            throw new UtilException("账户已存在");
        return account;
    }

    public Account getAccount(long id) throws UtilException {
        Account account = this.cache.getAccount(id);
        return account;
    }

    @Transactional(rollbackFor = {Exception.class})
    public void updateBalance(Account account, BigDecimal amount,Enums.FundType type, long relationId, String memo) throws UtilException {
        if (this.dataSafety.checkAccount(account.getId())) {
            int result = this.accountMapper.updateAccount(account);
            if (result < 1)
                throw new UtilException("资金不足");
            this.dataSafety.encryptAccount(account.getId());
            this.cashflowService.add(account.getId(), type, amount, relationId, memo);
            this.cache.delAccount(account.getId());
        } else
            throw new UtilException("账户出现异常，请联系管理员");
    }

    public int updateAccountTotal(Account account) throws UtilException {
        int result = this.accountMapper.updateAccount(account);
        if (result < 1)
            throw new UtilException("更新投资用户资金失败，请联系管理员");
        this.cache.delAccount(account.getId());
        return result;
    }

    @Transactional(rollbackFor = {Exception.class})
    public void withdraw(long userId, long accountId, BigDecimal amount, String payPassword,int route) throws UtilException {
        boolean flag= cache.checkFrequentAction("withdraw_"+accountId,2);
        if(!flag){
            throw new UtilException("请勿频繁操作");
        }
        Object key = this.systemSetService.getParamVal(Enums.Params.Key.getKey());
        if (key == null)
            throw new UtilException("请求异常");
        if (amount.compareTo(BigDecimal.ZERO) <= 0)
            throw new UtilException("提现金额必须大于0");
        if (amount.compareTo(BigDecimal.valueOf(99999999))> 0)
            throw new UtilException("提现金额不正确");
        User user = this.userMapper.selectByPrimaryKey(userId);
        if (!user.getPayPassword().equals(CommonUtil.createPwd(payPassword, key.toString())))
            throw new UtilException("交易密码输入错误.");
        Account account = this.accountMapper.selectByPrimaryKey(accountId);
        if (account.getBalance().subtract(amount).compareTo(BigDecimal.ZERO) < 0)
            throw new UtilException("账户余额不足.");
        BankCard bankCard = this.bankCardMapper.selectByAccount(accountId);
        if (!account.getBindCard() && bankCard == null || bankCard.getId() <= 0)
            throw new UtilException("请先绑定银行卡");
        Withdraw withdraw = this.withdrawService.save(accountId, amount, bankCard.getId(),route);
        //增加冻结金额
        this.freezeService.save(accountId, Enums.FundType.Withdraw, withdraw.getId(), amount);
        //减少账户余额
        Account acc = new Account();
        acc.setId(accountId);
        acc.setBalance(BigDecimal.valueOf(-1).multiply(withdraw.getAmount()));
        acc.setPieceBalance(BigDecimal.valueOf(-1).multiply(withdraw.getPieceAmount()));
        acc.setFreeze(withdraw.getAmount());
        this.updateBalance(acc, BigDecimal.valueOf(-1).multiply(withdraw.getAmount()), Enums.FundType.Withdraw, withdraw.getId(), "冻结提现金额");
        //this.queueService.insertQueue(Enums.Actions.Withdraw.getIndex(), user.getId(), withdraw, withdraw.getAmount());


        account = this.accountMapper.selectByPrimaryKey(user.getAccountId());
        if (account.getBalance().compareTo(BigDecimal.ZERO) < 0)
            throw new UtilException("余额不足");
    }


    //审核提现
    @Transactional(rollbackFor = {Exception.class})
    public void auditWithdraw(long withdrawId, long auditUserId, byte state, String memo,BigDecimal fee,BigDecimal otherFee,BigDecimal noInvestedFee) throws UtilException {
        boolean flag= cache.checkFrequentAction("auditWithdraw_"+withdrawId,2);
        if(!flag){
            throw new UtilException("请勿频繁操作");
        }
        Withdraw withdraw = this.withdrawMapper.selectByPrimaryKey(withdrawId);
        if (withdraw != null && withdraw.getAuditState() == Enums.State.Wait.getIndex()) {
            Withdraw w = new Withdraw();
            w.setId(withdraw.getId());
            w.setAuditState(state);
            w.setAuditTime(new Date());
            w.setAuditUserId(auditUserId);
            w.setMemo(memo);
            w.setFee(fee);
            w.setOtherFee(otherFee);
            w.setNoInvestedFee(noInvestedFee);
            BigDecimal arriveAmount=withdraw.getAmount().subtract(fee).subtract(otherFee).subtract(noInvestedFee);
            w.setArriveAmount(arriveAmount);
            if(arriveAmount.compareTo(BigDecimal.valueOf(2)) <=0){
                throw new UtilException("到账金额必须大于2元");
            }
            if (state == Enums.State.Cancel.getIndex()) {
                withdraw.setState(state);
                User user = this.userService.getUserByAccountId(withdraw.getAccountId());
                this.queueService.insertQueue(Enums.Actions.WithdrawFail.getIndex(), user.getId(), withdraw, withdraw.getAmount());
                this.freezeService.thawWithdraw(withdraw.getAccountId(), withdrawId, state);
            }
            this.withdrawMapper.updateByPrimaryKeySelective(w);
        } else
            throw new UtilException("提现账单状态不可审核" + withdrawId);
    }



    //审批提现
    @Transactional(rollbackFor = {Exception.class})
    public void payWithdraw(long withdrawId, long payUserId, byte state, String memo) throws Exception {
        boolean flag= cache.checkFrequentAction("payWithdraw_"+withdrawId,2);
        if(!flag){
            throw new UtilException("请勿频繁操作");
        }
        Withdraw withdraw = this.withdrawMapper.selectByPrimaryKey(withdrawId);
        Object key=this.systemSetService.getParamVal(Enums.Params.withdrawConfig.getKey());
        Map<String, String> config =null;
        if(key!=null){
            config=CommonUtil.parseMap(key.toString());
        }
        if (withdraw != null && withdraw.getPayState() == Enums.State.Wait.getIndex()) {
            Withdraw w = new Withdraw();
            w.setId(withdraw.getId());
            w.setPayState(state);
            w.setPayTime(new Date());
            w.setPayUserId(payUserId);
            w.setPayMemo(memo);
            if (state == (byte) Enums.State.Complete.getIndex() && config!=null && config.get("openWithHolding")!=null && config.get("openWithHolding").equals("1") && config.get("GateWay")!=null) {
                w.setOrderNo(createOrderNo(withdraw.getAccountId()));
                w.setGateWay((byte) Integer.parseInt(config.get("GateWay")));
            }
            this.withdrawMapper.updateByPrimaryKeySelective(w);
            this.freezeService.thawWithdraw(withdraw.getAccountId(), withdrawId, state);
            if (state == (byte) Enums.State.Complete.getIndex()) {
                Account account = new Account();
                account.setId(withdraw.getAccountId());
                account.setTotalWithdrawAmount(withdraw.getAmount());
                this.updateAccountTotal(account);
                withdraw.setState(state);
                if (config!=null && config.get("openWithHolding")!=null && config.get("openWithHolding").equals("1")) {
                    withdrawService.payWithdraw(withdrawId, w.getOrderNo(), withdraw.getAccountId(), withdraw.getArriveAmount(),config.get("GateWay"));
                }
                User user = this.userService.getUserByAccountId(withdraw.getAccountId());
                this.queueService.insertQueue(Enums.Actions.Withdraw.getIndex(), user.getId(), withdraw, withdraw.getAmount());
                if(withdraw!=null && withdraw.getAmount()!=null){
                    this.cache.setTotal(Enums.TotalItems.TotalWithdrawAmount.getKey(), withdraw.getAmount().doubleValue());
                    this.cache.setTotal(Enums.TotalItems.TodayWithdrawAmount.getKey(), withdraw.getAmount().doubleValue());
                }
            } else {
                withdraw.setState(state);
                User user = this.userService.getUserByAccountId(withdraw.getAccountId());
                this.queueService.insertQueue(Enums.Actions.WithdrawFail.getIndex(), user.getId(), withdraw, withdraw.getAmount());
            }
        } else
            throw new UtilException("提现账单状态不可审批" + withdrawId);
    }

    //线下充值
    @Transactional(rollbackFor = {Exception.class})
    public void lineRecharge(String mobile, BigDecimal amount, String memo) throws UtilException {
        if (mobile.isEmpty() || mobile == null)
            throw new UtilException("充值对象不能为空");
        if (amount.compareTo(BigDecimal.ZERO) <= 0)
            throw new UtilException("充值金额输入不正确");
        if (memo.isEmpty() || memo == null)
            throw new UtilException("充值描述不能为空");
        User user = this.userService.getUserByMobile(mobile);
        if (user == null)
            throw new UtilException("充值对象不存在");
        if (user.getRealName().isEmpty() || user.getIdCard().isEmpty())
            throw new UtilException("此用户未实名");
        boolean flag= cache.checkFrequentAction("lineRecharge_"+mobile,3);
        if(!flag){
            throw new UtilException("请勿频繁操作");
        }
        this.rechargeService.lineRecharge(user, amount, Enums.GatWay.RG.getIndex(), memo);
    }

    //线下扣款
    @Transactional(rollbackFor = {Exception.class})
    public void lineDebit(String mobile, BigDecimal amount, String memo) throws UtilException {
        if (mobile.isEmpty() || mobile == null)
            throw new UtilException("扣款对象不能为空");
        if (amount.compareTo(BigDecimal.ZERO) <= 0)
            throw new UtilException("扣款金额输入不正确");
        if (memo.isEmpty() || memo == null)
            throw new UtilException("扣款描述不能为空");
        User user = this.userService.getUserByMobile(mobile);
        if (user == null)
            throw new UtilException("扣款对象不存在");
        if (user.getRealName().isEmpty() || user.getIdCard().isEmpty())
            throw new UtilException("此用户未实名");
        Account account = this.accountMapper.selectByPrimaryKey(user.getAccountId());
        if (account.getBalance().compareTo(BigDecimal.ZERO) < 0 || (account.getBalance().subtract(amount)).compareTo(BigDecimal.ZERO) < 0)
            throw new UtilException("账户余额不足");
        BigDecimal pieceAmount = CommonUtil.getPieceAmount(amount, account, Enums.FundType.Debit);
        Account acc = new Account();
        acc.setId(user.getAccountId());
        acc.setBalance(amount.multiply(BigDecimal.valueOf(-1)));
        acc.setPieceBalance(pieceAmount.multiply(BigDecimal.valueOf(-1)));
        this.updateBalance(acc, amount.multiply(BigDecimal.valueOf(-1)), Enums.FundType.Debit, 0, memo);
        account = this.accountMapper.selectByPrimaryKey(user.getAccountId());
        if (account.getBalance().compareTo(BigDecimal.ZERO) < 0)
            throw new UtilException("余额不足");
    }

    /**
     * 根据条件获取充值记录
     *
     * @return
     * @throws Exception
     */
    public PageInfo getRechargeByParam(int pageNo, int pageSize, String param, String bTime, String eTime, String gatWay, String memo, BigDecimal bAmount, BigDecimal eAmount, long accountId,Byte route,String state) throws UtilException {
        PageHelper.startPage(pageNo, pageSize);
        List<HashMap> list = this.rechargeMapper.selectByParam(bTime, eTime, param, gatWay, memo, bAmount, eAmount, accountId,route,state);
        PageInfo page = new PageInfo(list);
        return page;
    }

    /**
     * 根据条件获取资金记录
     *
     * @return
     * @throws Exception
     */
    public PageInfo getCashFlowByParam(int pageNo, int pageSize, String param, String bTime, String eTime, byte type, long accountId) throws UtilException {
        PageHelper.startPage(pageNo, pageSize);
        List<HashMap> list = this.cashflowMapper.selectByParam(bTime, eTime, type, param, accountId);
        PageInfo page = new PageInfo(list);
        return page;
    }

    /**
     * 根据条件获取提现记录
     *
     * @return
     * @throws Exception
     */
    public PageInfo getWithdrawByParam(int pageNo, int pageSize, String param, String bTime, String eTime, String auditState, String payState, long accountId,Byte route,String auditStartTime,String auditEndTime) throws UtilException {
        PageHelper.startPage(pageNo, pageSize);
        List<HashMap> list = this.withdrawMapper.selectByParam(bTime, eTime, auditState, payState, param, accountId,route,auditStartTime, auditEndTime);
        PageInfo page = new PageInfo(list);
        return page;
    }

    /**
     * 根据条件获取回款计划
     *
     * @return
     * @throws Exception
     */
    public PageInfo getPaymentByAccount(int pageNo, int pageSize, String bTime, String eTime, long accountId, String state, String investId) throws UtilException {
        PageHelper.startPage(pageNo, pageSize);
        List<HashMap> list = this.billDebtMapper.selectPaymentByAccount(bTime, eTime, accountId, state, investId);
        PageInfo page = new PageInfo(list);
        return page;
    }

    /**
     * 根据条件获取银行卡
     *
     * @return
     * @throws Exception
     */
    public PageInfo getBankCardByParam(int pageNo, int pageSize, String param, String bTime, String eTime, String accountId) throws UtilException {
        PageHelper.startPage(pageNo, pageSize);
        List<HashMap> list = this.bankCardMapper.selectByParam(bTime, eTime, param, accountId);
        PageInfo page = new PageInfo(list);
        return page;
    }

    public void saveBankCardById(BankCard bankCard) throws UtilException {
        if (!(Pattern.compile("^\\+?[1-9][0-9]*$").matcher(String.valueOf(bankCard.getCardId())).matches()))
            throw new UtilException("请输入正确的银行卡号");
        this.bankCardMapper.updateByPrimaryKeySelective(bankCard);
    }

    /**
     * 保存银行卡
     *
     * @return
     * @throws Exception
     */
    public void saveBankCardByAccount(BankCard bankCard) throws Exception {
        if(bankCard==null || bankCard.getAccountId()==null){
            throw new UtilException("银行信息不正确");
        }
        boolean flag= cache.checkFrequentAction("bindCard_"+bankCard.getAccountId(),3);
        if(!flag){
            throw new UtilException("请勿频繁操作");
        }
        BankCard _bank = this.bankCardMapper.selectByAccount(bankCard.getAccountId());
        if (!(Pattern.compile("^\\+?[1-9][0-9]*$").matcher(String.valueOf(bankCard.getCardId())).matches()))
            throw new UtilException("请输入正确的银行卡号");
//        if (bankCard.getMobile() != null && !(Pattern.compile("^1[34578]{1}[0-9]{9}").matcher(bankCard.getMobile()).matches()))
//            throw new UtilException("请输入正确的预留手机号");
        if (_bank != null) {
            bankCard.setId(_bank.getId());
            if (_bank.getIsDefault()){
                throw new UtilException("已绑定的银行卡不允许修改");
            }else{
                this.bankCardMapper.updateByPrimaryKeySelective(bankCard);
            }
        } else {
            Object key=this.systemSetService.getParamVal(Enums.Params.bindCard.getKey());
            Map<String, String> config =null;
            if(key!=null){
                config=CommonUtil.parseMap(key.toString());
            }
            if(config!=null && config.get("noNeedBind").equals("1")){
                bankCard.setTime(new Date());
                this.bankCardMapper.insertSelective(bankCard);
            }else{
                Object authGateWay = this.systemSetService.getParamVal(Enums.Params.AuthGateway.getKey());
                if (authGateWay == null)
                    throw new UtilException("绑卡异常,请稍后重试");
                GatWayInterface _gatWay = GatWayFactory.create(Integer.parseInt(authGateWay.toString()));
                if (_gatWay.bindCard(bankCard)) {
                    bankCard.setIsDefault(true);
                    bankCard.setTime(new Date());
                    try {
                        this.bankCardMapper.insertSelective(bankCard);
                        Account account = new Account();
                        account.setId(bankCard.getAccountId());
                        account.setBindCard(true);
                        this.accountMapper.updateByPrimaryKeySelective(account);
                        this.cache.delAccount(bankCard.getAccountId());
                        User user = this.userMapper.selectByAccountId(bankCard.getAccountId());
                        this.queueService.insertQueue(Enums.Actions.BindCard.getIndex(), user.getId(), null, BigDecimal.ZERO);
                    } catch (Exception e) {
                        throw new UtilException("请勿频繁操作");
                    }
                } else
                    throw new UtilException("绑卡失败,请核对银行卡信息");
            }
        }
    }

    /**
     * 绑卡
     *
     * @return
     * @throws Exception
     */
    @Transactional(rollbackFor = {Exception.class})
    public void bindCard(long accountId) throws Exception {
        Account acc = this.getAccount(accountId);
        if (!acc.getBindCard()) {
            Object authGateWay = this.systemSetService.getParamVal(Enums.Params.AuthGateway.getKey());
            if (authGateWay == null)
                throw new UtilException("绑卡异常");
            GatWayInterface _gatWay = GatWayFactory.create(Integer.parseInt(authGateWay.toString()));
            BankCard bankCard = this.bankCardMapper.selectByAccount(accountId);
            if (_gatWay.bindCard(bankCard)) {
                BankCard upBank = new BankCard();
                upBank.setId(bankCard.getId());
                upBank.setIsDefault(true);
                this.bankCardMapper.updateByPrimaryKeySelective(upBank);
                Account account = new Account();
                account.setId(accountId);
                account.setBindCard(true);
                this.accountMapper.updateByPrimaryKeySelective(account);
                this.cache.delAccount(accountId);
                this.queueService.insertQueue(Enums.Actions.BindCard.getIndex(), acc.getUserId(), null, BigDecimal.ZERO);
            } else
                throw new UtilException("绑卡失败");
        }
    }

    /**
     * 保存自动投标配置
     *
     * @return
     * @throws Exception
     */
    public void saveAutoInvest(AutoInvest auto) throws UtilException {
        if (!(Pattern.compile("(^[1-9][0-9][0-9]$)|(^[1-9][0-9]$)|(^1000&)|(^[1-9]$)$").matcher(String.valueOf(auto.getMinPeriodDay())).matches()))
            throw new UtilException("请输入正确的项目期限");
        else if (!(Pattern.compile("(^[1-9][0-9][0-9]$)|(^[1-9][0-9]$)|(^1000&)|(^[1-9]$)$").matcher(String.valueOf(auto.getMaxPeriodDay())).matches()))
            throw new UtilException("请输入正确的项目期限");
        else if (auto.getMinPeriodDay() >= auto.getMaxPeriodDay())
            throw new UtilException("请输入正确的项目期限");
        else if (!(Pattern.compile("^(?!0+$)(?!0*\\.0*$)\\d{1,8}(\\.\\d{1,2})?$").matcher(String.valueOf(auto.getMinRate())).matches()) || auto.getMinRate().compareTo(BigDecimal.valueOf(100)) > -1)
            throw new UtilException("请输入正确的预期年化");
        else if (!(Pattern.compile("^(?!0+$)(?!0*\\.0*$)\\d{1,8}(\\.\\d{1,2})?$").matcher(String.valueOf(auto.getMaxRate())).matches()) || auto.getMaxRate().compareTo(BigDecimal.valueOf(100)) > -1)
            throw new UtilException("请输入正确的预期年化");
        else if (auto.getMinRate().compareTo(auto.getMaxRate()) > -1)
            throw new UtilException("请输入正确的预期年化");
        else if (!(Pattern.compile("^\\+?[0-9][0-9]*$").matcher(String.valueOf(auto.getMinBalance())).matches()) || auto.getMinBalance().compareTo(BigDecimal.valueOf(9999999)) > -1)
            throw new UtilException("请输入正确的账户保留金额");
        else if (!(Pattern.compile("^\\+?[1-9][0-9]*$").matcher(String.valueOf(auto.getMinBuy())).matches()) || auto.getMinBalance().compareTo(BigDecimal.valueOf(9999999)) > -1)
            throw new UtilException("请输入正确的单标金额");
        else if (!(Pattern.compile("^\\+?[1-9][0-9]*$").matcher(String.valueOf(auto.getMaxBuy())).matches()) || auto.getMinBalance().compareTo(BigDecimal.valueOf(9999999)) > -1)
            throw new UtilException("请输入正确的单标金额");
        else if (auto.getMinBuy().compareTo(auto.getMaxBuy()) > -1)
            throw new UtilException("请输入正确的单标金额");
        if (auto.getMinRate().compareTo(BigDecimal.ZERO) > 0)
            auto.setMinRate(auto.getMinRate().divide(BigDecimal.valueOf(100)));
        if (auto.getMaxRate().compareTo(BigDecimal.ZERO) > 0)
            auto.setMaxRate(auto.getMaxRate().divide(BigDecimal.valueOf(100)));
        AutoInvest au = this.autoInvestMapper.selectByAccount(auto.getAccountId());
        if (au != null) {
            this.autoInvestMapper.updateByAccount(auto);
        } else {
            auto.setLastAutoTime(new Date());
            this.autoInvestMapper.insertSelective(auto);
        }
    }

    /**
     * 更改自动投标状态
     *
     * @return
     * @throws Exception
     */
    public void changeAutoInvest(long accountId, byte state) {
        this.autoInvestMapper.updateState(accountId, state);
    }

    /**
     * 根据账户ID获取自动投标信息
     *
     * @return
     * @throws Exception
     */
    public AutoInvest getAutoInvest(long accountId) {
        return this.autoInvestMapper.selectByAccount(accountId);
    }

    /**
     * 查询回款计划日期
     */
    public List<HashMap> selectPaymentDays(long accountId) {
        return this.billDebtMapper.selectPaymentDays(accountId);
    }

    /**
     * 充值成功绑卡
     * @param accountId
     * @param userId
     * @throws UtilException
     */
    public void bindCard(long accountId,long userId) throws UtilException{
        BankCard bankCard = this.bankCardMapper.selectByAccount(accountId);
        if(bankCard!=null && !bankCard.getIsDefault()){
            BankCard upBank = new BankCard();
            upBank.setId(bankCard.getId());
            upBank.setIsDefault(true);
            this.bankCardMapper.updateByPrimaryKeySelective(upBank);
            Account account = new Account();
            account.setId(accountId);
            account.setBindCard(true);
            this.accountMapper.updateByPrimaryKeySelective(account);
            this.cache.delAccount(accountId);
            this.queueService.insertQueue(Enums.Actions.BindCard.getIndex(), userId, null, BigDecimal.ZERO);
        }
    }

    /**
     * 保存银行卡
     *
     * @return
     * @throws Exception
     */
    public void saveBankCardByAdmin(BankCard bankCard) throws Exception {
        System.err.println(bankCard.getMobile());
        if(bankCard==null || bankCard.getAccountId()==null){
            throw new UtilException("银行信息不正确");
        }
        boolean flag= cache.checkFrequentAction("bindCard_"+bankCard.getAccountId(),3);
        if(!flag){
            throw new UtilException("请勿频繁操作");
        }
        BankCard _bank = this.bankCardMapper.selectByAccount(bankCard.getAccountId());
        if (!(Pattern.compile("^\\+?[1-9][0-9]*$").matcher(String.valueOf(bankCard.getCardId())).matches()))
            throw new UtilException("请输入正确的银行卡号");
        if (bankCard.getMobile() != null && !(Pattern.compile("^1[34578]{1}[0-9]{9}").matcher(bankCard.getMobile()).matches()))
            throw new UtilException("请输入正确的预留手机号");
        if (_bank != null) {
            bankCard.setId(_bank.getId());
            if (_bank.getIsDefault()){
                throw new UtilException("已绑定的银行卡不允许修改");
            }else{
                bankCard.setIsDefault(true);
                bankCard.setTime(new Date());
                this.bankCardMapper.updateByPrimaryKeySelective(bankCard);
                Account account = new Account();
                account.setId(bankCard.getAccountId());
                account.setBindCard(true);
                this.accountMapper.updateByPrimaryKeySelective(account);
                this.cache.delAccount(bankCard.getAccountId());
            }
        } else {
            bankCard.setIsDefault(true);
            bankCard.setTime(new Date());
            this.bankCardMapper.insertSelective(bankCard);
            Account account = new Account();
            account.setId(bankCard.getAccountId());
            account.setBindCard(true);
            this.accountMapper.updateByPrimaryKeySelective(account);
            this.cache.delAccount(bankCard.getAccountId());
        }
    }

    private String createOrderNo(Long accountId) {
        return accountId.toString() + new Date().getTime();
    }

    /*获取提现记录信息*/
    public Withdraw getWithdraw(long withdrawId){
        return this.withdrawMapper.selectByPrimaryKey(withdrawId);
    }
}
