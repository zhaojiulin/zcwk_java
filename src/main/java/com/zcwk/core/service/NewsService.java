package com.zcwk.core.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zcwk.core.dao.NewsMapper;
import com.zcwk.core.model.News;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by pwx on 2016/7/13.
 */
@Service
public class NewsService {
    @Autowired
    NewsMapper newsMapper;

    /**
     * 根据条件获取所有公告
     *
     * @return
     * @throws Exception
     */
    public PageInfo getNewsByParam(int pageNo, int pageSize, String param, String bTime, String eTime, String state, String type) {
        PageHelper.startPage(pageNo, pageSize);
        List list = this.newsMapper.selectByParam(type, state, param, bTime, eTime);
        PageInfo page = new PageInfo(list);
        return page;
    }

    /**
     * 根据ID公告
     *
     * @return
     * @throws Exception
     */
    public News getNewsById(long id) {
        return this.newsMapper.selectByPrimaryKey(id);
    }
    /**
     * 保存公告
     *
     * @return
     * @throws Exception
     */
    public void save(News news) {
        if (news.getId() > 0)
            this.newsMapper.updateByPrimaryKeySelective(news);
        else {
            news.setTime(new Date());
            this.newsMapper.insertSelective(news);
        }
    }

    /**
     * 推荐
     * @return
     * @throws Exception
     */
    public int recommend(long id) {
        return this.newsMapper.recommend(id);
    }
}
