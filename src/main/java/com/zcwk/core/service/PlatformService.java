package com.zcwk.core.service;

import com.zcwk.core.dao.PlatformInfoMapper;
import com.zcwk.core.model.PlatformInfo;
import com.zcwk.core.util.UtilCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by pwx on 2016/8/2.
 */
@Service
public class PlatformService {
    @Autowired
    PlatformInfoMapper platformInfoMapper;
    @Autowired
    UtilCache cache;

    /**
     * 保存Seo信息
     *
     * @return
     * @throws Exception
     */
    public void save(PlatformInfo platform) {
        if (platform.getId() > 0)
            this.platformInfoMapper.updateByPrimaryKeySelective(platform);
        else
            this.platformInfoMapper.insertSelective(platform);
        this.cache.delPlatformInfo();
    }

    /**
     * 获取Seo信息
     *
     * @return
     * @throws Exception
     */
    public PlatformInfo get() {
        return this.cache.getPlatformInfo();
    }
}
