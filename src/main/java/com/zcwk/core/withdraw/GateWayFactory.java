package com.zcwk.core.withdraw;

import com.zcwk.core.withdraw.GateWayInterface;
import com.zcwk.core.util.Enums;
import com.zcwk.core.util.UtilBean;

/**
 * Created by pwx on 2017/4/27.
 */
public class GateWayFactory {
    public static GateWayInterface create(int index) {
        GateWayInterface gateWay = null;
        if (index == Enums.GatWay.FY.getIndex())
            gateWay = (GateWayInterface) UtilBean.applicationContext.getBean("FyGateWay");
        else if (index == Enums.GatWay.BF.getIndex())
            gateWay = (GateWayInterface) UtilBean.applicationContext.getBean("BfGateWay");
        return gateWay;
    }
}
