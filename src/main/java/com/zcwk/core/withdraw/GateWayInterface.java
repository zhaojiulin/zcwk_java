package com.zcwk.core.withdraw;

import com.zcwk.core.util.UtilException;

import java.math.BigDecimal;
import java.util.Map;

/**
 * Created by pwx on 2017/4/27.
 */
public interface GateWayInterface {

    String convertBank(String bank);

    String convertCity(String city);

    /*快速打款*/
    boolean payQuick(String bank,String city,BigDecimal amount,String cardNo,String accountName,String orderNo) throws Exception;

    boolean checkpayQuickOrder(String orderId,String startdt,String enddt) throws Exception;

    boolean payTP(Map<String, String> params) throws Exception;

    boolean callPay(Map<String, String> params) throws Exception;

}
