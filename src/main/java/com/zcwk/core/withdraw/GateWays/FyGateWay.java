package com.zcwk.core.withdraw.GateWays;

import com.zcwk.core.util.*;
import com.zcwk.core.withdraw.GateWayInterface;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.springframework.stereotype.Service;

import java.io.StringReader;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by pwx on 2017/4/27.
 */
@Service("FyGateWay")
public class FyGateWay extends BaseGateWay implements GateWayInterface {

    public String convertBank(String bank) {
        Map<String, String> map = new HashMap<>();
        map.put( "ICBC","0102");//        0102    中国工商银行
        map.put( "ABC","0103");//        0103    中国农业银行
        map.put( "BOC","0104");//        0104    中国银行
        map.put( "CCB","0105");//        0105    中国建设银行
        map.put( "BCM","0301");//        0301    交通银行
        map.put( "CMB","0308");//        0308    招商银行
        map.put( "CIB","0309");//        0309    兴业银行
        map.put("CMBC","0305");//        0305    中国民生银行
        map.put( "CGB","0306");//        0306    广东发展银行
        map.put( "PAB","0307");//        0307    平安银行股份有限公司
        map.put("SPDB","0310");//        0310    上海浦东发展银行
        map.put( "HXB","0304");//        0304    华夏银行
        map.put( "EGB","0315");//        0315    恒丰银行
        map.put("PSBC","0403");//        0403    中国邮政储蓄银行股份有限公司
        map.put("CEB","0303");//        0303    中国光大银行
        map.put("CITIC","0302");//        0302    中信银行
        //        1401    邯郸市城市信用社
        //        0313    其他城市商业银行
        //        0314    其他农村商业银行
        //        0316    浙商银行股份有限公司
        //        0318    渤海银行股份有限公司
        //        0423    杭州市商业银行
        //        0412    温州市商业银行
        //        0424    南京市商业银行
        //        0461    长沙市商业银行
        //        0409    济南市商业银行
        //        0422    石家庄市商业银行
        //        0458    西宁市商业银行
        //        0404    烟台市商业银行
        //        0462    潍坊市商业银行
        //        0515    德州市商业银行
        //        0431    临沂市商业银行
        //        0481    威海商业银行
        //        0497    莱芜市商业银行
        //        0450    青岛市商业银行
        //        0319    徽商银行
        //        0322    上海农村商业银行
        //        0402    其他农村信用合作社
        return map.get(bank);
    }

    public String convertCity(String city){
        Map<String, String> map = new HashMap<>();
        map.put("北京市", "1000");
        map.put("天津市", "1100");
        map.put("保定市", "1340");
        map.put("沧州市", "1430");
        map.put("承德市", "1410");
        map.put("邯郸市", "1270");
        map.put("衡水市", "1480");
        map.put("廊坊市", "1460");
        map.put("秦皇岛市", "1260");
        map.put("石家庄市", "1210");
        map.put("唐山市", "1240");
        map.put("邢台市", "1310");
        map.put("张家口市", "1380");
        map.put("长治市", "1660");
        map.put("大同市", "1620");
        map.put("晋城市", "1680");
        map.put("晋中市", "1750");
        map.put("临汾市", "1770");
        map.put("吕梁市", "1730");
        map.put("朔州市", "1690");
        map.put("太原市", "1610");
        map.put("忻州市", "1710");
        map.put("阳泉市", "1630");
        map.put("运城市", "1810");
        map.put("阿拉善盟", "2080");
        map.put("巴彦淖尔市", "2070");
        map.put("包头市", "1920");
        map.put("赤峰市", "1940");
        map.put("鄂尔多斯市", "2050");
        map.put("呼和浩特市", "1910");
        map.put("呼伦贝尔市", "1960");
        map.put("通辽市", "1990");
        map.put("乌海市", "1930");
        map.put("乌兰察布市", "2030");
        map.put("锡林郭勒盟", "2010");
        map.put("兴安盟", "1980");
        map.put("鞍山市", "2230");
        map.put("本溪市", "2250");
        map.put("朝阳市", "2340");
        map.put("大连市", "2220");
        map.put("丹东市", "2260");
        map.put("抚顺市", "2240");
        map.put("阜新市", "2290");
        map.put("葫芦岛市", "2276");
        map.put("锦州市", "2270");
        map.put("辽阳市", "2310");
        map.put("盘锦市", "2320");
        map.put("沈阳市", "2210");
        map.put("铁岭市", "2330");
        map.put("营口市", "2280");
        map.put("白城市", "2470");
        map.put("白山市", "2460");
        map.put("长春市", "2410");
        map.put("吉林市", "2420");
        map.put("辽源市", "2440");
        map.put("四平市", "2430");
        map.put("松原市", "2520");
        map.put("通化市", "2450");
        map.put("延边朝鲜族自治州", "2490");
        map.put("大庆市", "2650");
        map.put("大兴安岭地区", "2790");
        map.put("哈尔滨市", "2610");
        map.put("鹤岗市", "2670");
        map.put("黑河市", "2780");
        map.put("鸡西市", "2660");
        map.put("佳木斯市", "2690");
        map.put("牡丹江市", "2720");
        map.put("七台河市", "2740");
        map.put("齐齐哈尔市", "2640");
        map.put("双鸭山市", "2680");
        map.put("绥化市", "2760");
        map.put("伊春市", "2710");
        map.put("上海市", "2900");
        map.put("常州市", "3040");
        map.put("淮安市", "3080");
        map.put("连云港市", "3070");
        map.put("南京市", "3010");
        map.put("南通市", "3060");
        map.put("苏州市", "3050");
        map.put("宿迁市", "3090");
        map.put("泰州市", "3128");
        map.put("无锡市", "3020");
        map.put("徐州市", "3030");
        map.put("盐城市", "3110");
        map.put("扬州市", "3120");
        map.put("镇江市", "3140");
        map.put("杭州市", "3310");
        map.put("湖州市", "3360");
        map.put("嘉兴市", "3350");
        map.put("金华市", "3380");
        map.put("丽水市", "3430");
        map.put("宁波市", "3320");
        map.put("绍兴市", "3370");
        map.put("台州市", "3450");
        map.put("温州市", "3330");
        map.put("舟山市", "3420");
        map.put("衢州市", "3410");
        map.put("安庆市", "3680");
        map.put("蚌埠市", "3630");
        map.put("巢湖市", "3781");
        map.put("池州市", "3790");
        map.put("滁州市", "3750");
        map.put("阜阳市", "3720");
        map.put("合肥市", "3610");
        map.put("淮北市", "3660");
        map.put("淮南市", "3640");
        map.put("黄山市", "3710");
        map.put("六安市", "3760");
        map.put("马鞍山市", "3650");
        map.put("宿州市", "3740");
        map.put("铜陵市", "3670");
        map.put("芜湖市", "3620");
        map.put("宣城市", "3771");
        map.put("亳州市", "3722");
        map.put("福州市", "3910");
        map.put("龙岩市", "4050");
        map.put("南平市", "4010");
        map.put("宁德市", "4030");
        map.put("莆田市", "3940");
        map.put("泉州市", "3970");
        map.put("三明市", "3950");
        map.put("厦门市", "3930");
        map.put("漳州市", "3990");
        map.put("抚州市", "4370");
        map.put("赣州市", "4280");
        map.put("吉安市", "4350");
        map.put("景德镇市", "4220");
        map.put("九江市", "4240");
        map.put("南昌市", "4210");
        map.put("萍乡市", "4230");
        map.put("上饶市", "4330");
        map.put("新余市", "4260");
        map.put("宜春市", "4310");
        map.put("鹰潭市", "4270");
        map.put("滨州市", "4660");
        map.put("德州市", "4680");
        map.put("东营市", "4550");
        map.put("菏泽市", "4750");
        map.put("济南市", "4510");
        map.put("济宁市", "4610");
        map.put("莱芜市", "4634");
        map.put("聊城市", "4710");
        map.put("临沂市", "4730");
        map.put("青岛市", "4520");
        map.put("日照市", "4732");
        map.put("泰安市", "4630");
        map.put("威海市", "4650");
        map.put("潍坊市", "4580");
        map.put("烟台市", "4560");
        map.put("枣庄市", "4540");
        map.put("淄博市", "4530");
        map.put("安阳市", "4960");
        map.put("鹤壁市", "4970");
        map.put("焦作市", "5010");
        map.put("开封市", "4920");
        map.put("洛阳市", "4930");
        map.put("南阳市", "5130");
        map.put("平顶山市", "4950");
        map.put("三门峡市", "5050");
        map.put("商丘市", "5060");
        map.put("新乡市", "4980");
        map.put("信阳市", "5150");
        map.put("许昌市", "5030");
        map.put("郑州市", "4910");
        map.put("周口市", "5080");
        map.put("驻马店市", "5110");
        map.put("漯河市", "5040");
        map.put("濮阳市", "5020");
        map.put("鄂州市", "5310");
        map.put("恩施土家族苗族自治州", "5410");
        map.put("黄冈市", "5330");
        map.put("黄石市", "5220");
        map.put("荆门市", "5320");
        map.put("荆州市", "5370");
        map.put("潜江市", "5375");
        map.put("神农架林区", "5311");
        map.put("十堰市", "5230");
        map.put("随州市", "5286");
        map.put("天门市", "5374");
        map.put("武汉市", "5210");
        map.put("仙桃市", "5371");
        map.put("咸宁市", "5360");
        map.put("襄阳市", "5280");
        map.put("孝感市", "5350");
        map.put("宜昌市", "5260");
        map.put("常德市", "5580");
        map.put("长沙市", "5510");
        map.put("郴州市", "5630");
        map.put("衡阳市", "5540");
        map.put("怀化市", "5670");
        map.put("娄底市", "5620");
        map.put("邵阳市", "5550");
        map.put("湘潭市", "5530");
        map.put("湘西土家族苗族自治州", "5690");
        map.put("益阳市", "5610");
        map.put("永州市", "5650");
        map.put("岳阳市", "5570");
        map.put("张家界市", "5590");
        map.put("株洲市", "5520");
        map.put("潮州市", "5869");
        map.put("东莞市", "6020");
        map.put("佛山市", "5880");
        map.put("广州市", "5810");
        map.put("河源市", "5980");
        map.put("惠州市", "5950");
        map.put("江门市", "5890");
        map.put("揭阳市", "5865");
        map.put("茂名市", "5920");
        map.put("梅州市", "5960");
        map.put("清远市", "6010");
        map.put("汕头市", "5860");
        map.put("汕尾市", "5970");
        map.put("韶关市", "5820");
        map.put("深圳市", "5840");
        map.put("阳江市", "5990");
        map.put("云浮市", "5937");
        map.put("湛江市", "5910");
        map.put("肇庆市", "5930");
        map.put("中山市", "6030");
        map.put("珠海市", "5850");
        map.put("百色市", "6261");
        map.put("北海市", "6230");
        map.put("崇左市", "6128");
        map.put("防城港市", "6330");
        map.put("桂林市", "6170");
        map.put("贵港市", "6242");
        map.put("河池市", "6281");
        map.put("贺州市", "6225");
        map.put("来宾市", "6155");
        map.put("柳州市", "6140");
        map.put("南宁市", "6110");
        map.put("钦州市", "6311");
        map.put("梧州市", "6210");
        map.put("玉林市", "6240");
        map.put("海口市", "6410");
        map.put("三亚市", "6420");
        map.put("重庆市", "6530");
        map.put("阿坝藏族羌族自治州", "6790");
        map.put("巴中市", "6758");
        map.put("成都市", "6510");
        map.put("达州市", "6750");
        map.put("德阳市", "6580");
        map.put("甘孜藏族自治州", "6810");
        map.put("广安市", "6737");
        map.put("广元市", "6610");
        map.put("乐山市", "6650");
        map.put("凉山彝族自治州", "6840");
        map.put("眉山市", "6652");
        map.put("绵阳市", "6590");
        map.put("南充市", "6730");
        map.put("内江市", "6630");
        map.put("攀枝花市", "6560");
        map.put("遂宁市", "6620");
        map.put("雅安市", "6770");
        map.put("宜宾市", "6710");
        map.put("自贡市", "6550");
        map.put("泸州市", "6570");
        map.put("安顺市", "7110");
        map.put("毕节地区", "7090");
        map.put("贵阳市", "7010");
        map.put("六盘水市", "7020");
        map.put("黔东南苗族侗族自治州", "7130");
        map.put("黔南布依族苗族自治州", "7150");
        map.put("黔西南布依族苗族自治州", "7070");
        map.put("铜仁地区", "7050");
        map.put("遵义市", "7030");
        map.put("保山市", "7530");
        map.put("楚雄彝族自治州", "7380");
        map.put("大理白族自治州", "7510");
        map.put("德宏傣族景颇族自治州", "7540");
        map.put("迪庆藏族自治州", "7570");
        map.put("红河哈尼族彝族自治州", "7430");
        map.put("昆明市", "7310");
        map.put("丽江市", "7550");
        map.put("临沧市", "7580");
        map.put("怒江傈僳族自治州", "7560");
        map.put("普洱市", "7470");
        map.put("曲靖市", "7360");
        map.put("文山壮族苗族自治州", "7450");
        map.put("西双版纳傣族自治州", "7490");
        map.put("玉溪市", "7410");
        map.put("昭通市", "7340");
        map.put("阿里地区", "7810");
        map.put("昌都地区", "7720");
        map.put("拉萨市", "7700");
        map.put("林芝地区", "7830");
        map.put("那曲地区", "7790");
        map.put("日喀则地区", "7760");
        map.put("山南地区", "7740");
        map.put("安康市", "8010");
        map.put("宝鸡市", "7930");
        map.put("汉中市", "7990");
        map.put("商洛市", "8030");
        map.put("铜川市", "7920");
        map.put("渭南市", "7970");
        map.put("西安市", "7910");
        map.put("咸阳市", "7950");
        map.put("延安市", "8040");
        map.put("榆林市", "8060");
        map.put("白银市", "8240");
        map.put("定西市", "8290");
        map.put("甘南藏族自治州", "8380");
        map.put("嘉峪关市", "8220");
        map.put("金昌市", "8230");
        map.put("酒泉市", "8260");
        map.put("兰州市", "8210");
        map.put("临夏回族自治州", "8360");
        map.put("陇南市", "8310");
        map.put("平凉市", "8330");
        map.put("庆阳市", "8340");
        map.put("天水市", "8250");
        map.put("武威市", "8280");
        map.put("张掖市", "8270");
        map.put("果洛藏族自治州", "8570");
        map.put("海北藏族自治州", "8540");
        map.put("海东地区", "8520");
        map.put("海南藏族自治州", "8560");
        map.put("海西蒙古族藏族自治州", "8590");
        map.put("黄南藏族自治州", "8550");
        map.put("西宁市", "8510");
        map.put("玉树藏族自治州", "8580");
        map.put("固原市", "8741");
        map.put("石嘴山市", "8720");
        map.put("吴忠市", "8731");
        map.put("银川市", "8710");
        map.put("阿克苏地区", "8910");
        map.put("阿拉尔市", "9031");
        map.put("阿勒泰地区", "9020");
        map.put("巴音郭楞蒙古自治州", "8880");
        map.put("博尔塔拉蒙古自治州", "8870");
        map.put("昌吉回族自治州", "8850");
        map.put("哈密地区", "8840");
        map.put("和田地区", "8960");
        map.put("喀什地区", "8940");
        map.put("克拉玛依市", "8820");
        map.put("克孜勒苏柯尔克孜自治州", "8930");
        map.put("石河子市", "9028");
        map.put("塔城地区", "9010");
        map.put("图木舒克市", "9032");
        map.put("吐鲁番地区", "8830");
        map.put("乌鲁木齐市", "8810");
        map.put("伊犁哈萨克自治州", "8980");

        //去除市的
        map.put("北京", "1000");
        map.put("天津", "1100");
        map.put("保定", "1340");
        map.put("沧州", "1430");
        map.put("承德", "1410");
        map.put("邯郸", "1270");
        map.put("衡水", "1480");
        map.put("廊坊", "1460");
        map.put("秦皇岛", "1260");
        map.put("石家庄", "1210");
        map.put("唐山", "1240");
        map.put("邢台", "1310");
        map.put("张家口", "1380");
        map.put("长治", "1660");
        map.put("大同", "1620");
        map.put("晋城", "1680");
        map.put("晋中", "1750");
        map.put("临汾", "1770");
        map.put("吕梁", "1730");
        map.put("朔州", "1690");
        map.put("太原", "1610");
        map.put("忻州", "1710");
        map.put("阳泉", "1630");
        map.put("运城", "1810");
        map.put("阿拉善盟", "2080");
        map.put("巴彦淖尔", "2070");
        map.put("包头", "1920");
        map.put("赤峰", "1940");
        map.put("鄂尔多斯", "2050");
        map.put("呼和浩特", "1910");
        map.put("呼伦贝尔", "1960");
        map.put("通辽", "1990");
        map.put("乌海", "1930");
        map.put("乌兰察布", "2030");
        map.put("锡林郭勒盟", "2010");
        map.put("兴安盟", "1980");
        map.put("鞍山", "2230");
        map.put("本溪", "2250");
        map.put("朝阳", "2340");
        map.put("大连", "2220");
        map.put("丹东", "2260");
        map.put("抚顺", "2240");
        map.put("阜新", "2290");
        map.put("葫芦岛", "2276");
        map.put("锦州", "2270");
        map.put("辽阳", "2310");
        map.put("盘锦", "2320");
        map.put("沈阳", "2210");
        map.put("铁岭", "2330");
        map.put("营口", "2280");
        map.put("白城", "2470");
        map.put("白山", "2460");
        map.put("长春", "2410");
        map.put("吉林", "2420");
        map.put("辽源", "2440");
        map.put("四平", "2430");
        map.put("松原", "2520");
        map.put("通化", "2450");
        map.put("大庆", "2650");

        map.put("哈尔滨", "2610");
        map.put("鹤岗", "2670");
        map.put("黑河", "2780");
        map.put("鸡西", "2660");
        map.put("佳木斯", "2690");
        map.put("牡丹江", "2720");
        map.put("七台河", "2740");
        map.put("齐齐哈尔", "2640");
        map.put("双鸭山", "2680");
        map.put("绥化", "2760");
        map.put("伊春", "2710");
        map.put("上海", "2900");
        map.put("常州", "3040");
        map.put("淮安", "3080");
        map.put("连云港", "3070");
        map.put("南京", "3010");
        map.put("南通", "3060");
        map.put("苏州", "3050");
        map.put("宿迁", "3090");
        map.put("泰州", "3128");
        map.put("无锡", "3020");
        map.put("徐州", "3030");
        map.put("盐城", "3110");
        map.put("扬州", "3120");
        map.put("镇江", "3140");
        map.put("杭州", "3310");
        map.put("湖州", "3360");
        map.put("嘉兴", "3350");
        map.put("金华", "3380");
        map.put("丽水", "3430");
        map.put("宁波", "3320");
        map.put("绍兴", "3370");
        map.put("台州", "3450");
        map.put("温州", "3330");
        map.put("舟山", "3420");
        map.put("衢州", "3410");
        map.put("安庆", "3680");
        map.put("蚌埠", "3630");
        map.put("巢湖", "3781");
        map.put("池州", "3790");
        map.put("滁州", "3750");
        map.put("阜阳", "3720");
        map.put("合肥", "3610");
        map.put("淮北", "3660");
        map.put("淮南", "3640");
        map.put("黄山", "3710");
        map.put("六安", "3760");
        map.put("马鞍山", "3650");
        map.put("宿州", "3740");
        map.put("铜陵", "3670");
        map.put("芜湖", "3620");
        map.put("宣城", "3771");
        map.put("亳州", "3722");
        map.put("福州", "3910");
        map.put("龙岩", "4050");
        map.put("南平", "4010");
        map.put("宁德", "4030");
        map.put("莆田", "3940");
        map.put("泉州", "3970");
        map.put("三明", "3950");
        map.put("厦门", "3930");
        map.put("漳州", "3990");
        map.put("抚州", "4370");
        map.put("赣州", "4280");
        map.put("吉安", "4350");
        map.put("景德镇", "4220");
        map.put("九江", "4240");
        map.put("南昌", "4210");
        map.put("萍乡", "4230");
        map.put("上饶", "4330");
        map.put("新余", "4260");
        map.put("宜春", "4310");
        map.put("鹰潭", "4270");
        map.put("滨州", "4660");
        map.put("德州", "4680");
        map.put("东营", "4550");
        map.put("菏泽", "4750");
        map.put("济南", "4510");
        map.put("济宁", "4610");
        map.put("莱芜", "4634");
        map.put("聊城", "4710");
        map.put("临沂", "4730");
        map.put("青岛", "4520");
        map.put("日照", "4732");
        map.put("泰安", "4630");
        map.put("威海", "4650");
        map.put("潍坊", "4580");
        map.put("烟台", "4560");
        map.put("枣庄", "4540");
        map.put("淄博", "4530");
        map.put("安阳", "4960");
        map.put("鹤壁", "4970");
        map.put("焦作", "5010");
        map.put("开封", "4920");
        map.put("洛阳", "4930");
        map.put("南阳", "5130");
        map.put("平顶山", "4950");
        map.put("三门峡", "5050");
        map.put("商丘", "5060");
        map.put("新乡", "4980");
        map.put("信阳", "5150");
        map.put("许昌", "5030");
        map.put("郑州", "4910");
        map.put("周口", "5080");
        map.put("驻马店", "5110");
        map.put("漯河", "5040");
        map.put("濮阳", "5020");
        map.put("鄂州", "5310");
        map.put("黄冈", "5330");
        map.put("黄石", "5220");
        map.put("荆门", "5320");
        map.put("荆州", "5370");
        map.put("潜江", "5375");
        map.put("十堰", "5230");
        map.put("随州", "5286");
        map.put("天门", "5374");
        map.put("武汉", "5210");
        map.put("仙桃", "5371");
        map.put("咸宁", "5360");
        map.put("襄阳", "5280");
        map.put("孝感", "5350");
        map.put("宜昌", "5260");
        map.put("常德", "5580");
        map.put("长沙", "5510");
        map.put("郴州", "5630");
        map.put("衡阳", "5540");
        map.put("怀化", "5670");
        map.put("娄底", "5620");
        map.put("邵阳", "5550");
        map.put("湘潭", "5530");
        map.put("益阳", "5610");
        map.put("永州", "5650");
        map.put("岳阳", "5570");
        map.put("张家界", "5590");
        map.put("株洲", "5520");
        map.put("潮州", "5869");
        map.put("东莞", "6020");
        map.put("佛山", "5880");
        map.put("广州", "5810");
        map.put("河源", "5980");
        map.put("惠州", "5950");
        map.put("江门", "5890");
        map.put("揭阳", "5865");
        map.put("茂名", "5920");
        map.put("梅州", "5960");
        map.put("清远", "6010");
        map.put("汕头", "5860");
        map.put("汕尾", "5970");
        map.put("韶关", "5820");
        map.put("深圳", "5840");
        map.put("阳江", "5990");
        map.put("云浮", "5937");
        map.put("湛江", "5910");
        map.put("肇庆", "5930");
        map.put("中山", "6030");
        map.put("珠海", "5850");
        map.put("百色", "6261");
        map.put("北海", "6230");
        map.put("崇左", "6128");
        map.put("防城港", "6330");
        map.put("桂林", "6170");
        map.put("贵港", "6242");
        map.put("河池", "6281");
        map.put("贺州", "6225");
        map.put("来宾", "6155");
        map.put("柳州", "6140");
        map.put("南宁", "6110");
        map.put("钦州", "6311");
        map.put("梧州", "6210");
        map.put("玉林", "6240");
        map.put("海口", "6410");
        map.put("三亚", "6420");
        map.put("重庆", "6530");
        map.put("巴中", "6758");
        map.put("成都", "6510");
        map.put("达州", "6750");
        map.put("德阳", "6580");
        map.put("广安", "6737");
        map.put("广元", "6610");
        map.put("乐山", "6650");
        map.put("眉山", "6652");
        map.put("绵阳", "6590");
        map.put("南充", "6730");
        map.put("内江", "6630");
        map.put("攀枝花", "6560");
        map.put("遂宁", "6620");
        map.put("雅安", "6770");
        map.put("宜宾", "6710");
        map.put("自贡", "6550");
        map.put("泸州", "6570");
        map.put("安顺", "7110");
        map.put("贵阳", "7010");
        map.put("六盘水", "7020");
        map.put("遵义", "7030");
        map.put("保山", "7530");
        map.put("昆明", "7310");
        map.put("丽江", "7550");
        map.put("临沧", "7580");
        map.put("普洱", "7470");
        map.put("曲靖", "7360");
        map.put("玉溪", "7410");
        map.put("昭通", "7340");
        map.put("拉萨", "7700");
        map.put("安康", "8010");
        map.put("宝鸡", "7930");
        map.put("汉中", "7990");
        map.put("商洛", "8030");
        map.put("铜川", "7920");
        map.put("渭南", "7970");
        map.put("西安", "7910");
        map.put("咸阳", "7950");
        map.put("延安", "8040");
        map.put("榆林", "8060");
        map.put("白银", "8240");
        map.put("定西", "8290");
        map.put("嘉峪关", "8220");
        map.put("金昌", "8230");
        map.put("酒泉", "8260");
        map.put("兰州", "8210");
        map.put("陇南", "8310");
        map.put("平凉", "8330");
        map.put("庆阳", "8340");
        map.put("天水", "8250");
        map.put("武威", "8280");
        map.put("张掖", "8270");
        map.put("西宁", "8510");
        map.put("固原", "8741");
        map.put("石嘴山", "8720");
        map.put("吴忠", "8731");
        map.put("银川", "8710");
        map.put("阿拉尔", "9031");
        map.put("克拉玛依", "8820");
        map.put("石河子", "9028");
        map.put("图木舒克", "9032");
        map.put("乌鲁木齐", "8810");

        return map.get(city);
    }

    /**
     * 打款
     * @param bank
     * @param city
     * @param amount
     * @param cardNo
     * @param accountName
     * @param orderNo
     * @return
     * @throws Exception
     */
    public boolean payQuick(String bank,String city,BigDecimal amount,String cardNo,String accountName,String orderNo) throws Exception {
        super.init(Enums.GatWay.FY.getIndex());
        String postUrl="https://fht.fuiou.com/req.do";//https://fht.fuiou.com/req.do
        String gatWayKey =this.config.get("gatWayKey");
        String merid =this.config.get("authPcMchntCd");//商户号
        String reqtype="payforreq";//请求类型
        StringBuffer sign = new StringBuffer();
        String bankNo=convertBank(bank);
        String cityNo=convertCity(city);
        if(amount.compareTo(BigDecimal.valueOf(2))< 0){
            throw new UtilException("提现金额不正确");
        }
        int money=amount.multiply(BigDecimal.valueOf(100)).intValue();

//        HashMap<String,String> map=new HashMap<String,String>();
//        map.put("ver", "1.00");
//        map.put("merdt", CommonUtil.getFormatTime(new Date(), "yyyyMMdd"));
//        map.put("orderno", orderNo);
//        map.put("bankno", bankNo);
//        map.put("branchnm", null);
//        map.put("cityno", cityNo);
//        map.put("accntno", cardNo);
//        map.put("accntnm", accountName);
//        map.put("amt", amount.multiply(BigDecimal.valueOf(100)).toString());
//        map.put("entseq", null);
//        map.put("memo", null);
//        map.put("mobile", null);
//        Collection<String> keyset= map.keySet();
//        List list=new ArrayList<String>(keyset);
//        Collections.sort(list);
//        for(int i=0;i<list.size();i++){
//            sign.append(map.get(list.get(i)) + "|");
//        }
//        String xml=sign.substring(0,sign.length()-1);
//        System.err.println(xml);
//        xml=SHA1Util.SHA1(xml);
//        System.err.println(xml);
//        xml=xml+"|"+gatWayKey;
//        xml=SHA1Util.SHA1(xml);
//        System.err.println(xml);

        String xml = "<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"yes\"?>"+
                "<payforreq>"+
                "<ver>1.00</ver>"+
                "<merdt>"+ CommonUtil.getFormatTime(new Date(), "yyyyMMdd")+"</merdt>"+
                "<orderno>"+orderNo+"</orderno>"+
                "<bankno>"+bankNo+"</bankno>"+
                "<cityno>"+cityNo+"</cityno>"+
                "<accntno>"+cardNo+"</accntno>"+
                "<accntnm>"+accountName+"</accntnm>"+
                "<amt>"+String.valueOf(money)+"</amt>"+
                "</payforreq>";
        String signDataStr=merid+"|"+gatWayKey+"|"+reqtype+"|"+xml;
        String mac= MD5Util.MD5(signDataStr).toUpperCase();//md5 16进制
        String param="xml="+xml +"&merid="+merid+"&reqtype="+reqtype+"&mac="+mac;
        System.err.println(param);
        String rsXml=UtilHttp.sendPost(postUrl,param);
        System.err.println("rsXml="+rsXml);
        SAXBuilder builder = new SAXBuilder();
        Document doc = builder.build(new StringReader(rsXml));
        Element root = doc.getRootElement();
        String status = root.getChild("ret").getText();
        if (status.equals("000000")) {
            return true;
        }else{
            return false;
        }
    }

    /**
     * 查询打款订单结果
     * @param orderId
     * @return
     * @throws Exception
     */
    public boolean checkpayQuickOrder(String orderId,String startdt,String enddt) throws Exception{
        super.init(Enums.GatWay.FY.getIndex());
        String postUrl="https://fht.fuiou.com/req.do";
        String gatWayKey =this.config.get("gatWayKey");
        String merid = this.config.get("authPcMchntCd");//商户号
        String reqtype="qrytransreq";//请求类型
        String xml = "<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"yes\"?>"+
                "<qrytransreq>"+
                "<ver>1.00</ver>"+
                "<busicd>AP01</busicd>"+
                "<orderno>"+orderId+"</orderno>"+
                "<startdt>"+startdt+"</startdt>"+
                "<enddt>"+enddt+"</enddt>"+
                "<transst>1</transst>"+
                "</qrytransreq>";
        String signDataStr=merid+"|"+gatWayKey+"|"+reqtype+"|"+xml;
        String mac = MD5Util.MD5(signDataStr).toUpperCase();
        String param="xml="+xml +"&merid="+merid+"&reqtype="+reqtype+"&mac="+mac;
        System.err.println(param);
        String rsXml=UtilHttp.sendPost(postUrl,param);
        System.err.println("rsXml="+rsXml);
        SAXBuilder builder = new SAXBuilder();
        Document doc = builder.build(new StringReader(rsXml));
        Element root = doc.getRootElement();
//        List<Element> list=root.getChildren("trans");
//        for(Element e:list) {
//            System.out.println("订单:"+e.getChildText("orderno"));
//            System.out.println("状态:"+e.getChildText("state"));
//        }
        String status = root.getChild("ret").getText();
        if (status.equals("000000")) {
            String state = root.getChild("trans").getChild("state").getText();
            if(root.getChild("trans")!=null && root.getChild("trans").getChild("state")!=null && state.equals("1"))
                return true;
            return false;
        }else{
            return false;
        }
    }

    //退票
    public boolean payTP(Map<String, String> params) throws Exception{
        super.init(Enums.GatWay.FY.getIndex());
        String orderno = params.get("orderno");
        String merdt = params.get("merdt");
        String fuorderno = params.get("fuorderno");
        String tpmerdt = params.get("tpmerdt");
        String futporderno = params.get("futporderno");
        String accntno = params.get("accntno");
        String accntnm = params.get("accntnm");
        String bankno = params.get("bankno");
        String amt = params.get("amt");
        String state = params.get("state");
        String result = params.get("result");
        String reason = params.get("reason");
        String mac = params.get("mac");
        String gatWayKey =this.config.get("gatWayKey");
        String merid = this.config.get("authPcMchntCd");//商户号
        String signPlain = merid + "|" + gatWayKey + "|" + orderno + "|" + merdt + "|" + accntno + "|" + amt;
        String sign = MD5Util.MD5(signPlain);
        System.err.println("退票通知:"+params);
        if(mac!=null && mac.equals(sign)){
            return true;
        }else{
            return false;
        }
    }

    public boolean callPay(Map<String, String> params) throws Exception{
        return false;
    }

}
