package com.zcwk.core.withdraw.GateWays;

import com.zcwk.core.service.SystemSetService;
import com.zcwk.core.util.CommonUtil;
import com.zcwk.core.util.Enums;
import com.zcwk.core.util.UtilException;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

/**
 * Created by pwx on 2017/4/27.
 */
public class BaseGateWay {

    Map<String, String> config = null;

    String baseUrl = null;
    String apiBaseUrl = null;
    @Autowired
    SystemSetService systemSetService;

    public void init(int gatWay) throws UtilException {
        Object _url = this.systemSetService.getParamVal(Enums.Params.Base.getKey());
        this.baseUrl = _url == null ? "" : _url.toString();
        Object _apiUrl = this.systemSetService.getParamVal(Enums.Params.ApiBaseUrl.getKey());
        this.apiBaseUrl = _apiUrl == null ? "" : _apiUrl.toString();
        if (gatWay == Enums.GatWay.BF.getIndex())
            this.config = CommonUtil.parseMap(this.systemSetService.getParamVal(Enums.Params.BF.getKey()).toString());
        else if (gatWay == Enums.GatWay.HC.getIndex())
            this.config = CommonUtil.parseMap(this.systemSetService.getParamVal(Enums.Params.HC.getKey()).toString());
        else if (gatWay == Enums.GatWay.FY.getIndex())
            this.config = CommonUtil.parseMap(this.systemSetService.getParamVal(Enums.Params.FY.getKey()).toString());
    }
}
