package com.zcwk.core.withdraw.GateWays;

import com.zcwk.core.service.WithdrawService;
import com.zcwk.core.util.*;
import com.zcwk.core.withdraw.GateWayInterface;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.StringReader;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by pwx on 2017/4/27.
 */
@Service("BfGateWay")
public class BfGateWay extends BaseGateWay implements GateWayInterface {

    @Autowired
    WithdrawService withdrawService;
    @Value("${payforreq.bf_pay_pfx_path}")
    private String pfxPath;
    @Value("${payforreq.bf_pay_cer_path}")
    private String cerPath;

    public String convertBank(String bank) {
        Map<String, String> map = new HashMap<>();
        map.put( "ICBC","中国工商银行");
        map.put( "ABC","中国农业银行");
        map.put( "BOC","中国银行");
        map.put( "CCB","中国建设银行");
        map.put( "BCM","交通银行");
        map.put( "CMB","招商银行");
        map.put( "CIB","兴业银行");
        map.put("CMBC","中国民生银行");
        map.put( "CGB","广东发展银行");
        map.put( "PAB","平安银行股份有限公司");
        map.put("SPDB","上海浦东发展银行");
        map.put( "HXB","华夏银行");
        map.put( "EGB","恒丰银行");
        map.put("PSBC","中国邮政储蓄银行股份有限公司");
        map.put("CEB","中国光大银行");
        map.put("CITIC","中信银行");
        return map.get(bank);
    }

    public String convertCity(String city){
        return "";
    }

    /**
     * 打款
     * @param bank
     * @param city
     * @param amount
     * @param cardNo
     * @param accountName
     * @param orderNo
     * @return
     * @throws Exception
     */
    public boolean payQuick(String bank,String city,BigDecimal amount,String cardNo,String accountName,String orderNo) throws Exception {
        super.init(Enums.GatWay.BF.getIndex());
        //String postUrl="http://paytest.baofoo.com/baofoo-fopay/pay/BF0040001.do";//
        String postUrl="https://public.baofoo.com/baofoo-fopay/pay/BF0040001.do";
        String terminal_id = this.config.get("payTerminalID");    //终端号
        String member_id = this.config.get("merchantID");    //商户号
        String payKey = this.config.get("pay_key");
        File pfxFile = new File(this.pfxPath);
        if (!pfxFile.exists()) {
            throw new UtilException("私钥文件不存在！");
        }
        String data_content="";
        String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>"+
                "<trans_content>"+
                "<trans_reqDatas>"+
                "<trans_reqData>"+
                "<trans_no>"+orderNo+"</trans_no>"+
                "<trans_money>"+ amount.toString()+"</trans_money>"+
                "<to_acc_name>"+accountName+"</to_acc_name>"+
                "<to_acc_no>"+cardNo+"</to_acc_no>"+
                "<to_bank_name>"+ this.convertBank(bank)+"</to_bank_name>"+
                "<to_pro_name></to_pro_name>"+
                "<to_city_name></to_city_name>"+
                "<to_acc_dept></to_acc_dept>"+
                "<trans_card_id></trans_card_id>"+
                "<trans_mobile></trans_mobile>"+
                "<trans_summary></trans_summary>"+
                "</trans_reqData>"+
                "</trans_reqDatas>"+
                "</trans_content>";
        System.err.println("加密前密文="+xml);
        String base64str = SecurityUtil.Base64Encode(xml);
        data_content = RsaCodingUtil.encryptByPriPfxFile(base64str, this.pfxPath, payKey);
        System.err.println("加密后密文="+data_content);
        String param="member_id=" +member_id+"&terminal_id="+terminal_id+"&data_type=xml&version=4.0.0&data_content="+data_content;
        System.err.println("postUrl="+postUrl);
        System.err.println("param="+param);
        String rsJson=UtilHttp.sendPost(postUrl,param);
        File cerFile = new File(this.cerPath);
        if (!cerFile.exists()) {
            throw new UtilException("宝付公钥文件不存在！");
        }
        String rsXml = RsaCodingUtil.decryptByPubCerFile(rsJson, this.cerPath);
        rsXml = SecurityUtil.Base64Decode(rsXml);
        System.err.println("宝付代扣响应报文："+rsXml);
        SAXBuilder builder = new SAXBuilder();
        Document doc = builder.build(new StringReader(rsXml));
        Element root = doc.getRootElement();
        String status = root.getChild("trans_head").getChild("return_code").getText();
        if (status.equals("0000")) {
            return true;
        }else{
            return false;
        }
    }

    /**
     * 查询打款订单结果
     * @param orderId
     * @return
     * @throws Exception
     */
    public boolean checkpayQuickOrder(String orderId,String startdt,String enddt) throws Exception{
        super.init(Enums.GatWay.BF.getIndex());
        //String postUrl="http://paytest.baofoo.com/baofoo-fopay/pay/BF0040002.do";//https://public.baofoo.com/baofoo-fopay/pay/BF0040002.do
        String postUrl="https://public.baofoo.com/baofoo-fopay/pay/BF0040002.do";
        String terminal_id = this.config.get("payTerminalID");    //终端号
        String member_id = this.config.get("merchantID");    //商户号
        String payKey = this.config.get("pay_key");
        File pfxFile = new File(this.pfxPath);
        if (!pfxFile.exists()) {
            throw new UtilException("私钥文件不存在！");
        }
        String data_content="";
        String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>"+
                "<trans_content>"+
                "<trans_reqDatas>"+
                "<trans_reqData>"+
                "<trans_batchid></trans_batchid>"+
                "<trans_no>"+ orderId+"</trans_no>"+
                "</trans_reqData>"+
                "</trans_reqDatas>"+
                "</trans_content>";
        System.err.println(xml);
        String base64str = SecurityUtil.Base64Encode(xml);
        data_content = RsaCodingUtil.encryptByPriPfxFile(base64str, this.pfxPath, payKey);
        String param="member_id=" +member_id+"&terminal_id="+terminal_id+"&data_type=xml&version=4.0.0&data_content="+data_content;
        System.err.println("postUrl="+postUrl);
        System.err.println("param="+param);
        String rsJson=UtilHttp.sendPost(postUrl,param);
        File cerFile = new File(this.cerPath);
        if (!cerFile.exists()) {
            throw new UtilException("宝付公钥文件不存在！");
        }
        String rsXml = RsaCodingUtil.decryptByPubCerFile(rsJson, this.cerPath);
        rsXml = SecurityUtil.Base64Decode(rsXml);
        System.err.println("宝付代扣查询响应报文："+rsXml);
        SAXBuilder builder = new SAXBuilder();
        Document doc = builder.build(new StringReader(rsXml));
        Element root = doc.getRootElement();
        String status = root.getChild("trans_head").getChild("return_code").getText();
        if (status.equals("0000")) {
            return true;
        }else{
            return false;
        }
    }

    //退票
    public boolean payTP(Map<String, String> params) throws Exception{
        super.init(Enums.GatWay.BF.getIndex());
        return false;
    }

    public boolean callPay(Map<String, String> params) throws Exception{
        super.init(Enums.GatWay.BF.getIndex());
        String data_content = params.get("data_content");
        String terminal_id = this.config.get("payTerminalID");    //终端号
        String member_id = this.config.get("merchantID");    //商户号
        String payKey = this.config.get("pay_key");
        if(data_content==null || data_content.isEmpty()){
            throw new UtilException("加密串报文为空！");
        }
        File cerFile = new File(this.cerPath);
        if (!cerFile.exists()) {
            throw new UtilException("宝付公钥文件不存在！");
        }
        String rsXml = RsaCodingUtil.decryptByPubCerFile(data_content, this.cerPath);
        rsXml = SecurityUtil.Base64Decode(rsXml);
        System.err.println("宝付代扣通知响应报文："+rsXml);

        SAXBuilder builder = new SAXBuilder();
        Document doc = builder.build(new StringReader(rsXml));
        Element root = doc.getRootElement();
        String status = root.getChild("trans_reqDatas").getChild("trans_reqData").getChild("state").getText();
        if (status.equals("1")) {
            String trans_no= root.getChild("trans_reqDatas").getChild("trans_reqData").getChild("trans_no").getText();
            return withdrawService.callPaySucess(trans_no,(byte) Enums.GatWay.BF.getIndex());
        }else{
            return false;
        }
    }
}
