package com.zcwk.core.view;

import org.springframework.beans.BeanUtils;

import com.zcwk.core.model.AppPushMessage;

/**
 * app个推
 * @author pc
 *
 */
public class AppPushMessageView extends AppPushMessage{
	
	/** 是否注册：1是；2否；-1全部 **/
	private String isReg;
	
	/** 是否实名1是；2否；-1全部 **/
	private String isRealName;
	
	/** 是否投资1是；2否；-1全部 **/
	private String isInvest;
	
	/** 透传类型 **/
	private String transmissionType;
	
	/** 个人用户 **/
	private String[] mobiles;

	/** 开始时间 */
	private String bTime;

	/** 结束时间 */
	private String eTime;

	public String getIsReg() {
		return isReg;
	}

	public void setIsReg(String isReg) {
		this.isReg = isReg;
	}

	public String getIsRealName() {
		return isRealName;
	}

	public void setIsRealName(String isRealName) {
		this.isRealName = isRealName;
	}

	public String getIsInvest() {
		return isInvest;
	}

	public void setIsInvest(String isInvest) {
		this.isInvest = isInvest;
	}

	public String getTransmissionType() {
		return transmissionType;
	}

	public void setTransmissionType(String transmissionType) {
		this.transmissionType = transmissionType;
	}

	public String[] getMobiles() {
		return mobiles;
	}

	public void setMobiles(String[] mobiles) {
		this.mobiles = mobiles;
	}

	public String getbTime() {
		return bTime;
	}

	public void setbTime(String bTime) {
		this.bTime = bTime;
	}

	public String geteTime() {
		return eTime;
	}

	public void seteTime(String eTime) {
		this.eTime = eTime;
	}

	public void initRemark(){
		String remark = "";
		if(this.getIsInvest().equals("1")){
			remark +="是否投资用户：是";
		}else {
			if(this.getIsRealName().equals("1")){
				remark +="是否实名用户：是";
			}else {
				if(this.getIsReg().equals("1")){
					remark +="是否注册用户：是";
				}else {
					remark +="是否注册用户：否";
				}
			}
		}
		if(this.getMobiles().length > 0){
			remark += "个人用户";
		}
		this.setRemark(remark);
	}

	public static AppPushMessageView instance(AppPushMessage appPushMessage){
		AppPushMessageView view = new AppPushMessageView();
		BeanUtils.copyProperties(appPushMessage, view);
		return view;
	}
	
	public AppPushMessage prototype(){
		AppPushMessage appPushMessage = new AppPushMessageView();
		BeanUtils.copyProperties(this, appPushMessage);
		return appPushMessage;
	}
}
